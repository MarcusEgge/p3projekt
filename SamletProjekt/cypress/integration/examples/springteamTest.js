/// <reference types="Cypress" />



describe("Create/edit/delete Tests", () => {
    beforeEach(function () {
        login()
    })

    it("event", () => {
        cy.wait(300)
        cy.get('.event-box').its('length').then((eventLength) => {
            fillOutEventForms();
            validateNewEvent(eventLength);
            editEvent();
            deleteEvent(eventLength);
        })

    })

    it("Member", () => {
        cy.contains("Medlemmer").click();
        cy.wait(200)
        cy.get('.member_row').its('length').then((memberLenght) => {
            filloutProfileForms();
            validateMemberLenght(memberLenght + 1);
            editProfile();
            deleteProfile()
            validateMemberLenght(memberLenght)
        })



    })



});


function deleteEvent(length) {
    cy.contains('EditedName').click()
    cy.get('.event-box-selected').children().children('.col-md-1').click()
    cy.get('#btnDeleteEvent').click();
    cy.get('#confirmDeleteBtn').click();
    cy.get('.event-box').should('have.length', length);
}

function editEvent() {
    cy.contains('CypressName').click()
    //this is a not good selection, should be rewritten 
    cy.get('.event-box-selected').children().children('.col-md-1').click()
    cy.get("#editEventName").clear();
    cy.get("#editEventName").type("EditedName");
    cy.get("#btnSaveEventChanges").click();
    cy.contains('EditedName').should('be.visible')
}


function login() {
    cy.visit("http://localhost:8080/index.html");
    cy.get("#inputPassword").type("password");
    cy.get("#loginBtn").click();
    cy.url().should('include', 'admin/HTML/Events.html');
}

function fillOutEventForms() {
    cy.get('#btnCreateEvent').click()
    cy.get("#createStartDate").type("2019-10-20");
    cy.get("#createEndDate").type("2019-10-20");
    cy.get("#createStartTime").type("10:00");
    cy.get("#createEventName").type("CypressName");
    cy.get("#createEventAdress").type("CypressAdress");
    cy.get("#createEventTown").type("CypressTown");
    cy.get("#createEventCoaches").type("2");
    cy.get("#createEventJudges").type("1");
    cy.get("#btnSaveNewEvent").click();

}

function validateNewEvent(length) {
    cy.get('.event-box').should('have.length', length + 1);
    cy.contains('CypressName').should('be.visible');
}

function deleteProfile() {
    cy.contains("NewName").click();
    cy.get('.editProfileBtn').click();
    cy.get('.btnDeleteProfile').click();
    cy.get('#confirmDeleteBtn').click();
    cy.contains('NewName').should('not.exist')

}

function editProfile() {
    cy.contains("CypressName").click();
    cy.get('.editProfileBtn').click();
    cy.get("#editProfileName").clear().type("NewName");
    cy.get('.btnConfirmProfileChanges').click();
    cy.contains('CypressName').should('not.exist')
    cy.contains('NewName').should('exist')
}

function validateMemberLenght(length) {
    cy.get('.member_row').should("have.length", length);
}

function filloutProfileForms() {
    cy.get('.createProfileBtn').click();
    cy.get('#createProfileName').type("CypressName");
    cy.get('#radioMale').click();
    cy.get("#createProfileBirthday").type("2018-10-20");
    cy.get("#createProfileSignupDate").type("2018-10-20");
    cy.get("#createProfileAddress").type("CypressVej");
    cy.get("#createProfileCity").type("CypressTown");
    cy.get("#createProfilePhoneNumber").type("12345678");
    cy.get("#createProfileEmail").type("Cypress@IO");
    cy.get("#createProfileLicense").type("CY1234");
    cy.get('#gymnastYes').click();
    cy.get('#coachYes').click();
    cy.get('#judgeYes').click();
    cy.get('#photoConsentYes').click();

    cy.get('.btnConfirmCreateProfile').click()
}
$(document).ready(function () {
    //Callback: loads Navbar then sets CSS properties
    loadNavBar(function () {
        $(".events").css("max-height", (window.innerHeight - ($(".navbarColor").height() + 1)));
        $(".events").css("min-height", 200);
    });

    //When page is first loaded, season, events and members are loaded
    loadSeasons(function () {
        loadEvents(function () {
            loadMainWindow();
        });
    });

    //Adjust height of events when window is resized
    $(window).resize(function () {
        $(".events").css("max-height", (window.innerHeight - ($(".navbarColor").height() + 1)));
        adjustscrollheight();
    });

    //Edit event template loaded with the event's values
    $(document).on('click', '.btnEdit', function (event) {
        $(this).parents(".event-box").addClass("event-box-selected");
        $(this).parents(".event-box").siblings().removeClass("event-box-selected");
        event.stopPropagation();
        loadEditEvent(function () {
            insertEventValues();
        });
    });

    //Mark event as selected when clicked
    $(document).on('click', '.event-box', function () {
        $(this)
            .siblings()
            .removeClass("event-box-selected");
        $(this).addClass("event-box-selected");
        loadMainWindow();
    });

    //click checkbox to sign up or cancel sign up
    $(document).on('click', '.checkboxPerson', function () {
        changeGymnastSignupStatus($(this))
    });

    //Load create new event template
    $(document).on('click', "#btnCreateEvent", function () {
        loadCreateEvent()
    });

    //Load main window if create event is cancelled
    $(document).on('click', "#btnCancelCreateEvent", function () {
        loadMainWindow();
    });

    //copy license of a gymnast to clipboard
    $(document).on('click', '.fa-copy', function () {
        copyToClipboard($(this).parent());
        $(this)
            .parents("tr")
            .addClass("gymnast-copied");
    });

    //Sets the value of a dropdown menu to the select dropdown item
    $(document).on('click', '.dropdown-item', function () {
        $(this).parent().siblings("button").html($(this).html());
    });

    //Load a season
    $(document).on('click', '.season', function () {
        loadEvents(function () {
            loadMainWindow();
        });
    });

    //pop op when clicked "are u you sure u wish to delete?"
    $(document).on('click', '#btnDeleteEvent', function () {
        $("#myModal").css("display", "block");
    });

    //remove pop up
    $(document).on('click', '#cancelDeleteBtn', function () {
        $("#myModal").css("display", "none");
    });

    //delete event confirmed
    $(document).on('click', '#confirmDeleteBtn', function () {
        deleteEvent();
    });

    //Save new event changes
    $(document).on("click", "#btnSaveEventChanges", function () {
        saveEventChanges();
    });

    //Confirm creation of event
    $(document).on("click", "#btnSaveNewEvent", function () {
        createEvent();
    });

    //Creates a new season
    $(document).on('click', "#makeNewSeason", function () {
        createSeason();
    });

    //Change sign up status of volunteers
    $(document).on('click', ".attendence", function () {
        changeVolunteerSignupStatus($(this));
    })
});


/*********************************************
 * HTTP GET request
 **********************************************/

//Loads volunteer into Main Area's table
function loadVolunteerTable(role, func) {
    $("#table_area").load("Events-Templates/Table/Volunteer-Table.html", function () {
        if ($.fn.dataTable.isDataTable('#FormattedTable')) {
            $("#FormattedTable").DataTable().destroy();
        }
        $.ajax({
            type: "GET",
            url: "/../personSignupServlet",
            data: {
                seasonName: $("#selectSeason").text(),
                eventIndex: parseInt($(".event-box-selected")[0].getAttribute("data-id")),
                role: role
            },
            success: function (data) {
                if (!checkForError(data)) {
                    $('tbody').empty();
                    for (i = 0; i < data.length; i++) {
                        addVolunteer(data[i])
                    }
                    func();
                }
            }
        })
    });
}

//Loads gymnast into Main Area's table
function loadGymnastTable(func) {
    $("#table_area").load("Events-Templates/Table/Gymnast-Table.html", function () {
        if ($.fn.dataTable.isDataTable('#FormattedTable')) {
            $("#FormattedTable").DataTable().destroy();
        }
        $.ajax({
            type: "GET",
            url: "/../personSignupServlet",
            data: {
                seasonName: $("#selectSeason").text(),
                eventIndex: parseInt($(".event-box-selected")[0].getAttribute("data-id")),
                role: "gymnast",
            },
            success: function (data) {
                if (!checkForError(data)) {
                    $('tbody').empty();
                    for (i = 0; i < data.length; i++) {
                        addGymnast(data[i])
                    }
                    func();
                }
            }
        })
    });
}

function loadEvents(func) {
    $.ajax({
        type: "GET",
        url: "/../eventServlet",
        data: {
            seasonName: $("#selectSeason").text()
        },
        success: function (data) {
            if (!checkForError(data)) {
                $("#eventSpawn").empty();

                for (i = 0; i < data.length; i++) {
                    addEvent(data[i]);
                    if (data[i].numberOfCoachesSignedUp >= data[i].coachRequirement) {
                        $("#eventSpawn").find(".coachreq").last().addClass("requirement_met")
                    } else {
                        $("#eventSpawn").find(".coachreq").last().addClass("requirement_not_met")
                    }
                    if (data[i].numberOfJudgesSignedUp >= data[i].judgeRequirement) {
                        $("#eventSpawn").find(".judgereq").last().addClass("requirement_met")
                    } else {
                        $("#eventSpawn").find(".judgereq").last().addClass("requirement_not_met")
                    }
                }
                selectTopEvent();
                if (!($("#eventSpawn").text() === "")) {
                    func();
                }
            }
        },
        error: function (xhr) {
            alert("Get events: " + xhr.status);
        }
    })
}

//load seasons to the dropdown menu
function loadSeasons(func) {
    $.ajax({
        type: "GET",
        url: "/seasonServlet",
        success: function (data) {
            if (!checkForError(data)) {
                $("#seasonBtnSpawn").load("Events-Templates/Season-Dropdown.html", function () {
                    $("#selectSeason").text(data[0].name);
                    for (i = 0; i < data.length; i++) {
                        addSeason(data[i]);
                    }
                    $("#seasonSpawn").append("<a id='makeNewSeason' class='dropdown-item' href='#'>Lav næste sæson</a>");
                    func();
                });
            }
        }
    })
}

//inserts events values into the edit event template
function insertEventValues(){
    $.ajax({
        type: "GET",
        url: "/../eventServlet",
        data: {
            seasonName: $("#selectSeason").text(),
        },
        success: function (data) {
            var eventID = parseInt($(".event-box-selected")[0].getAttribute("data-id"));
            var obj;
            for (i = 0; i < data.length; i++) {
                if (data[i].id === eventID) {
                    obj = data[i];
                    break;
                }
            }

            var startDate = correctDateFormat(obj.startDate.date);
            var endDate = correctDateFormat(obj.endDate);
            var startTime = correctTimeFormat(obj.startDate.time);

            $("#editStartDate").val(startDate);
            $("#editStartTime").val(startTime);
            $("#editEndDate").val(endDate);
            $("#editEventName").val(obj.name);
            $("#editEventAdress").val(obj.address.road);
            $("#editEventTown").val(obj.address.city);
            $("#editEventCoaches").val(obj.coachRequirement);
            $("#editEventJudges").val(obj.judgeRequirement);
        }
    });
}

/*********************************************
 * HTTP POST request (create)
 **********************************************/

//TODO should select the recently created season
function createSeason(){
    $.ajax({
        type: "POST",
        url: "/seasonServlet",
        success: function (data) {
            checkForError(data);
            loadSeasons(function () {});
        }
    })
}

function createEvent(){
    var startDate = $("#createStartDate").val();
    var endDate = $("#createEndDate").val();
    var startTime = $("#createStartTime").val();
    var name = $("#createEventName").val();
    var address = $("#createEventAdress").val();
    var city = $("#createEventTown").val();
    var coaches = $("#createEventCoaches").val();
    var judges = $("#createEventJudges").val();

    var obj = {
        'startDate': startDate,
        'endDate': endDate,
        'startTime': startTime,
        'name': name,
        'address': address,
        'city': city,
        'coaches': coaches,
        'judges': judges
    };

    if ((startDate === "") ||
        (endDate === "") ||
        (startTime === "") ||
        (name === "") ||
        (address === "") ||
        (city === "") ||
        (coaches === "") ||
        (judges === "")) {
        alert("Fejl: Udfyld alle felter.")
    } else {
        $.ajax({
            type: "POST",
            url: "/../eventServlet",
            data: {
                action: "Create",
                seasonName: $("#selectSeason").text(),
                text: JSON.stringify(obj)
            },
            success: function (data) {
                if (!checkForError(data)) {
                    eventSavedPopUp();
                }
                loadEvents(function () {
                    loadMainWindow(); //TODO select new event after save
                });
            },
            error: function (xhr) {
                alert("Create event: " + xhr.status);
            }
        });
    }
}


/*********************************************
 * HTTP POST request (update)
 **********************************************/
function saveEventChanges(){
    var startDate = $("#editStartDate").val();
    var endDate = $("#editEndDate").val();
    var startTime = $("#editStartTime").val();
    var name = $("#editEventName").val();
    var address = $("#editEventAdress").val();
    var city = $("#editEventTown").val();
    var coaches = $("#editEventCoaches").val();
    var judges = $("#editEventJudges").val();

    var obj = {
        'startDate': startDate,
        'endDate': endDate,
        'startTime': startTime,
        'name': name,
        'address': address,
        'city': city,
        'coaches': coaches,
        'judges': judges
    };

    if ((startDate === "") ||
        (endDate === "") ||
        (startTime === "") ||
        (name === "") ||
        (address === "") ||
        (city === "") ||
        (coaches === "") ||
        (judges === "")) {
        alert("Fejl: Udfyld alle felter.")
    } else {
        $.ajax({
            type: "POST",
            url: "/../eventServlet/",
            data: {
                seasonName: $("#selectSeason").text(),
                action: "Update",
                ID: $(".event-box-selected")[0].getAttribute("data-id"),
                text: JSON.stringify(obj)
            },
            success: function (data) {
                if (!checkForError(data)) {
                    eventSavedPopUp();
                }
                loadEvents(function () {
                    loadMainWindow();
                });
            },
            error: function (xhr) {
                alert("Event update: " + xhr.status);
            }
        });
    }
}

function changeGymnastSignupStatus(signup){
    if (signup.is(":checked")) {
        var signupstate = "SIGNED_UP"
    } else {
        var signupstate = "NOT_SIGNED_UP"
    }
    $.ajax({
        type: "POST",
        url: "/../personSignupServlet",
        data: {
            personSignUpIndex: signup.parents('tr')[0].getAttribute("personsignup-id"),
            eventIndex: $(".event-box-selected")[0].getAttribute("data-id"),
            seasonName: $("#selectSeason").text(),
            state: signupstate
        },
        success: function (data) {
            if (!checkForError(data)) {
            }
        }
    })
}

function changeVolunteerSignupStatus(signup){
    var btnvalue = signup.text();
    var signupstate;
    if (btnvalue === "Ikke svaret") {
        signupstate = "UNDECIDED";
    } else if (btnvalue === "Deltager") {
        signupstate = "SIGNED_UP";
    } else if (btnvalue === "Deltager ikke") {
        signupstate = "NOT_SIGNED_UP";
    }
    $.ajax({
        type: "POST",
        url: "/personSignupServlet",
        data: {
            personSignUpIndex: signup.parents('tr')[0].getAttribute("personsignup-id"),
            eventIndex: $(".event-box-selected")[0].getAttribute("data-id"),
            seasonName: $("#selectSeason").text(),
            state: signupstate
        },
        success: function (data) {
            checkForError(data);
        }
    })
}



/*********************************************
 * HTTP POST request (delete)
 **********************************************/

function deleteEvent(){
    $.ajax({
        type: "POST",
        url: "/../eventServlet/",
        data: {
            action: "Delete",
            seasonName: $("#selectSeason").text(),
            id: $(".event-box-selected")[0].getAttribute("data-id")
        },
        success: function (data) {
            checkForError(data);
            $("#myModal").css("display", "none");
            $("#mainArea").empty();
            loadEvents(function () {
                loadMainWindow();
            });
            selectTopEvent();
        },
        error: function (xhr) {
            alert("Event delete: " + xhr.status);
        }
    })
}

/*********************************************
 * Regular functions
 **********************************************/
//copy gymnast's license to clipboard
function copyToClipboard(element) {
    var $temp = $("<input>");
    $("body").append($temp);
    $temp.val($(element).text()).select();
    document.execCommand("copy");
    $temp.remove();
}

//Load event into event spawn
function addEvent(event) {
    $("#eventSpawn").append(Mustache.render(eventTemplate(event), event))
}

//Loads a gymnast into Main Area's table based on its sign up boolean
function addGymnast(personSignup) {
    if (personSignup.state === "SIGNED_UP") {
        $('tbody').append(Mustache.render(gymnastInEvent(personSignup), personSignup));
    } else {
        $('tbody').append(Mustache.render(gymnastNotInEvent(personSignup), personSignup));
    }
}

//Loads a volunteer into Main Area's table
function addVolunteer(personSignup) {
    $('tbody').append(Mustache.render(volunteerInEvent(personSignup), personSignup));
}

//Checks HTTP requests for errors
function checkForError(data) {
    if ("error" === (data[0])) {
        alert(data[1]);
        return true;
    }
    return false;
}

//Loads members in the table if there is a event
function loadTable() {
    if (!($("#eventSpawn").text() === "")) {
        if ($("#GymnastsSubNav").hasClass("sub-navbar-link-selected")) {
            loadGymnastTable(function () {
                makeTable();
            });
        } else if ($("#CoachesSubNav").hasClass("sub-navbar-link-selected")) {
            loadVolunteerTable("coach", function () {
                makeTable();
            })
        } else if ($("#JudgesSubNav").hasClass("sub-navbar-link-selected")) {
            loadVolunteerTable("judge", function () {
                makeTable();
            })
        } else if ($("#TeamsSubNav").hasClass("sub-navbar-link-selected")) {

        } else if ($("#ScheduleSubNav").hasClass("sub-navbar-link-selected")) {

        }
    }
}

//Load edit event template
function loadEditEvent(func) {
    $("#mainArea").load("Events-Templates/Edit-Event.html", function () {
        func();
    });
}

//Load create event template
function loadCreateEvent() {
    $("#mainArea").load("Events-Templates/Create-Event.html");
}

//Loads template to hold members
function loadMainWindow() {
    $("#mainArea").load("Events-Templates/Table/Default-Table.html", function () {
        loadGymnastTable(function () {
            makeTable();
        });
    });
}

//Add the new season to the dropdown menu for season
function addSeason(season) {
    $("#seasonSpawn").append("<a class='dropdown-item season' href='#'>" + season.name + "</a>");
}

//Mark the top most event as selected
function selectTopEvent() {
    $(".event-box:first").addClass("event-box-selected");
}

//Pop up when changes are saved
function eventSavedPopUp() {
    $("#save-message1").css("display", "block");
    setTimeout(function () {
        $("#save-message1").css("opacity", 0);
    }, 2000);
    setTimeout(function () {
        $("#save-message1").css("display", "none");
        $("#save-message1").css("opacity", 1);
    }, 4000);
}

/*********************************************
 * HTML templates
 **********************************************/
//HTML to be appeneded to event spawn for each event
function eventTemplate(data) {
    var startDate = new Date(data.startDate.date.year - 1, data.startDate.date.month - 1, data.startDate.date.day);
    var endDate = new Date(data.endDate.year - 1, data.endDate.month - 1, data.endDate.day);
    var time = correctTimeFormat(data.startDate.time)
    var startDay = startDate.getDate();
    var startMonth = startDate.getMonth() + 1;
    var endDay = endDate.getDate();
    var endMonth = endDate.getMonth() + 1;

    startDay = correctFormat(startDay);
    startMonth = correctFormat(startMonth);
    endDay = correctFormat(endDay);
    endMonth = correctFormat(endMonth);

    var eventTemplate = "" +
        "<div data-id='{{id}}' class='event-box'>" +
        "<div class='row'>" +
        "<div class='col-md-11 event-name'><span>{{name}}</span></div>" +
        "<div class='col-md-1 override-padAndMargin'><i class='far fa-edit btnEdit'></i></div>" +
        "</div>" +
        "<div class='row'>" +
        "<div class='col-md-6'>" +
        "<div class='event-date'>Start: <span>" + startDay + "/" + startMonth + "</span></div>" +
        "<div class='event-date'>Kl. <span>" + time + "</span></div>" +
        "<div class='event-date'>Slut: <span>" + endDay + "/" + endMonth + "</span></div>" +
        "</div>" +
        "<div class='col-md-6'>" +
        "<div class='event-information'><span>{{address.road}}</span></div>" +
        "<div class='event-information'><span>{{address.city}}</span></div>" +
        "<div class='event-information'>Springere: <span class='font-weight-bold'>{{numberOfGymnastsSignedUp}}</span></div>" + //TODO Update after each personsignup
        "<div class='event-information'>Trænere: <span class='font-weight-bold coachreq'>{{numberOfCoachesSignedUp}}/{{coachRequirement}}</span></div>" +
        "<div class='event-information'>Dommere: <span class='font-weight-bold judgereq'>{{numberOfJudgesSignedUp}}/{{judgeRequirement}}</span></div>" +
        "</div>" +
        "</div>" +
        "</div>";

    return eventTemplate
}

//HTML for if gymnast is not in event
function gymnastNotInEvent(data) {
    var license = data.person.roleList[0].license;
    var phoneNumber = data.person.phoneNumberList[0];
    var email = data.person.emailList[0];

    var gymnastInEventTemplate = "" +
        "<tr personSignup-id='{{id}}'>" +
        "<td scope='row'>" +
        "<label class='container'>" +
        "<input type='checkbox' class='checkboxPerson'>" +
        "<span class= 'checkmark'></span>" +
        "</label>" +
        "</td>" +
        "<td class='name'>{{person.name}}</td>" +
        "<td>" + license + "<i class=\"far fa-copy\"></i></td>" +
        "<td>" + phoneNumber + "</td>" +
        "<td>" + email + "</td>" +
        "</tr>";

    return gymnastInEventTemplate
}

//HTML for if gymnast is in event
function gymnastInEvent(data) {
    var license = data.person.roleList[0].license;
    var phoneNumber = data.person.phoneNumberList[0];
    var email = data.person.emailList[0];

    var gymnastInEventTemplate = "" +
        "<tr personSignup-id='{{id}}'>" +
        "<td scope='row'>" +
        "<label class='container'>" +
        "<input type='checkbox' class='checkboxPerson' checked>" +
        "<span class= 'checkmark'></span>" +
        "</label>" +
        "</td>" +
        "<td class='name'>{{person.name}}</td>" +
        "<td>" + license + "<i class=\"far fa-copy\"></i></td>" +
        "<td>" + phoneNumber + "</td>" +
        "<td>" + email + "</td>" +
        "</tr>";

    return gymnastInEventTemplate
}

//HTML for if volunteer is in event
function volunteerInEvent(data) {
    var phoneNumber = data.person.phoneNumberList[0];
    var email = data.person.emailList[0];
    var isSingedUp;
    if ("UNDECIDED" === data.state) {
        isSingedUp = "Ikke svaret"
    } else if ("NOT_SIGNED_UP" === data.state) {
        isSingedUp = "Deltager ikke"
    } else {
        isSingedUp = "Deltager"
    }
    var volunteerTemplate = "" +
        "<tr personSignup-id='{{id}}'>" +
        "<td>" +
        "<div class=\"btn-group\" role=\"group\" aria-label=\"Button group with nested dropdown\">" +
        "<button id=\"btnGroupDrop1\" type=\"button\" class=\"btn btn-secondary dropdown-toggle attendenceBtn\"" +
        "data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">" + isSingedUp +
        "</button>" +
        "<div class=\"dropdown-menu\" aria-labelledby=\"btnGroupDrop1\" x-placement=\"bottom-start\">" +
        "<a class=\"dropdown-item attendence\" href=\"#\">Deltager</a>" +
        "<a class=\"dropdown-item attendence\" href=\"#\">Deltager ikke</a>" +
        "<a class=\"dropdown-item attendence\" href=\"#\">Ikke svaret</a>" +
        "</div>" +
        "</div>" +
        "</td>" +
        "<td class='name'>{{person.name}}</td>" +
        "<td>" + phoneNumber + "</td>" +
        "<td>" + email + "</td>" +
        "</tr>";

    return volunteerTemplate
}

$(document).ready(function(){


    // Loads different sub-navbars
    $(document).on('click', ".sub-navbar-link", function () {
        if ($("#ScheduleSubNav").hasClass("sub-navbar-link-selected")){
            loadEvents();
        }
        $(this)
            .siblings()
            .removeClass("sub-navbar-link-selected");
        $(this).addClass("sub-navbar-link-selected");
        loadTable();
    });

    // When hovering table row makes it dark grey
    $(document).on('mouseenter', 'tbody tr', function () {
        $(this).css("background-color", "darkgrey")
    });

    // Returns to normal color after hover is doney
    $(document).on('mouseleave', 'tbody tr', function () {
        colorRows();
    });

    // Makes table searchable
    $(document).on('keyup', '#myInput', function () {
        var value = $(this).val().toLowerCase();
        $(".myTable tr").filter(function () {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1);
            colorRows();
        });
    });

    //Adds blue border around searchbox on focus
    $(document).on('focus', '#myInput', function () {
        $(this).parents(".sub-navbar-offset").css("box-shadow", "0 0 0 0.2rem rgba(0,123,255,0.25)")
    });

    //Removes blue border around searchbox when not in focus
    $(document).on('blur', '#myInput', function () {
        $(this).parents(".sub-navbar-offset").css("box-shadow", "none")
    });

    // Colors rows after sorting of table
    $(document).on('click', 'thead th', function () {
        colorRows();
    });
});

function loadNavBar(func) {
    $(".navbarColor").load("Common-Templates/Navbar.html", function () {
        func();
    });
}

// Sets height for table scroll bar
function adjustscrollheight() {
    $(".dataTables_scrollBody").css("max-height", (window.innerHeight - ($(".navbarColor").height() + 1 //These numbers are margins, padding and some stuff arbitrary divs from bootstrap
        + $(".sub-navbar-offset").height() + 5 + 1 + 1
        + $(".dataTables_scrollHead").height()
        + 2)));
    $(".dataTables_scrollBody").css("min-height", 200);
}

// Color odd rows grey
function colorRows() {
    $("tbody tr:visible:odd").css("background-color", "rgb(242, 242, 242)");
    $("tbody tr:visible:even").css("background-color", "#fff");

}

// Formats table via a Bootstrap lib
function makeTable() {
    if (!$.fn.dataTable.isDataTable('#FormattedTable')) {
        $('#FormattedTable').DataTable({
            "scrollY": "400px",
            "scrollCollapse": true,
            "paging": false,
            "info": false,
            "filter": false,
            "autoWidth": false,
            "order" : [[1, "asc"]],
        });
        $('.dataTables_length').addClass("bs-select");
    }
    colorRows();
    adjustscrollheight();
}


//Returns dates in format yyyy-mm-dd
function correctDateFormat(dateObject) {
    var day;
    var month;
    var year;
    var dateString;

    day = correctFormat(dateObject.day);
    month = correctFormat(dateObject.month);
    year = dateObject.year;
    dateString = year + "-" + month + "-" + day;
    return dateString;
}


//Returns time in format hh:mm
function correctTimeFormat(time){
    var minutes;
    var hours;
    var timeString;

    minutes = correctFormat(time.minute);
    hours = correctFormat(time.hour);

    timeString = hours + ":" + minutes;

    return timeString;
}

// Prepends "0" if number less than 10
function correctFormat(obj){
    if (obj < 10) {
        return "0" + obj;
    } else {
        return obj;
    }
}
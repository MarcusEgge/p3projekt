$(document).ready(function () {
    //Callback: loads Navbar then sets CSS properties
    loadNavBar(function () {});
    loadMainWindow();

    $(window).resize(function () {
        adjustscrollheight();
    });

    //Click on row, to display profile info
    $(document).on('click', ".member_row", function () {
        $(this)
            .siblings()
            .removeClass("profile-selected");
        $(this).addClass("profile-selected");
        displayProfile();
    });

    //Cancel creation of profile and loading previous profile data
    $(document).on('click', ".btnCancelCreateProfile", function () {
        displayProfile();
    });

    // Display confirmation dialog when trying to delete profile
    $(document).on('click', '.btnDeleteProfile', function () {
        $("#myModal").css("display", "block");
    });

    // Cancel deletion of profile and removes confirmation dialog
    $(document).on('click', '#cancelDeleteBtn', function () {
        $("#myModal").css("display", "none");
    });

    //Confirm deletion of profile
    $(document).on('click', '#confirmDeleteBtn', function () {
        deleteProfile();
    });

    // Load profile creation template
    $(document).on('click', ".createProfileBtn", function () {
        $("#profileInfo").load("Members-Templates/Create-Profile.html")
    });

    // Confirm creation of profile
    $(document).on('click', ".btnConfirmCreateProfile", function () {
        createProfile();
    });

    // If there is a profile to edit opens edit profile template and loads existing info
    $(document).on('click', ".editProfileBtn", function () {
        if (!($(".profile-selected")[0] == null)) {
            displayEditProfile();
        }
    });

    // Confirm changes to profile
    $(document).on('click', ".btnConfirmProfileChanges", function () {
        editProfile();
    });

    // Colors odd rows after sorting table
    $(document).on('click', 'thead th', function () {
        colorRows();
    });
});

/*********************************************
 * HTTP GET request
 **********************************************/
// Loads edit profile window and inserts existing info into
function displayEditProfile() {
    $("#profileInfo").load("Members-Templates/Edit-Profile.html", function () {
        $.ajax({
            type: "GET",
            url: "/../personServlet",
            success: function (data) {
                if (!checkForError(data)) {
                    var profileID = parseInt($(".profile-selected")[0].getAttribute("profile-id")); // Id of selected profile
                    var obj;
                    // Sets obj to selected profile
                    for (i = 0; i < data.length; i++) {
                        if (data[i].id === profileID) {
                            obj = data[i];
                            break;
                        }
                    }
                    fillEditTemplate(obj)
                }
            }
        })
    });
}

// Load table data with data corresponding to roleIndex, where 0 = Gymnast, 1 = Coach, 2 = Judge
function loadProfileTable(roleIndex, func) {
    $.ajax({
        type: "GET",
        url: "/personServlet",
        success: function (data) {
            if (!checkForError(data)) {
                $('tbody').empty();
                for (i = 0; i < data.length; i++) {
                    // Appends profiles with an active corresponding role
                    if (data[i].roleList[roleIndex] != null && data[i].roleList[roleIndex].isActive)
                        appendProfile(data[i])
                }
                func();
            }
            displayProfile();
            selectTopProfile();
        }
    })
}

/*********************************************
 * HTTP POST request (create)
 **********************************************/
function createProfile() {
    //Retrieves values from input forms
    var name = $("#createProfileName").val();
    var birthday = $("#createProfileBirthday").val();
    var signupDate = $("#createProfileSignupDate").val();
    var phoneNumber = $("#createProfilePhoneNumber").val();
    var license = $("#createProfileLicense").val();
    var email = $("#createProfileEmail").val();
    var address = $("#createProfileAddress").val();
    var city = $("#createProfileCity").val();
    var isMale = radioButtonValidator($('[name="radioCreateGender"]'));
    var isGymnast = radioButtonValidator($('[name="radioCreateGymnast"]'));
    var isCoach = radioButtonValidator($('[name="radioCreateCoach"]'));
    var isJudge = radioButtonValidator($('[name="radioCreateJudge"]'));
    var photoConsent = radioButtonValidator($('[name="radioCreatePhotoConsent"]'));

    // Checking for required fields to be filled
    if (inputValidation(name, isMale, isGymnast, isCoach, isJudge, photoConsent)) {
        var obj = {
            'name': name,
            'birthday': birthday,
            'phoneNumber1': phoneNumber,
            'license': license,
            'email1': email,
            'address1': address,
            'city1': city,
            'isMale': isMale,
            'isGymnast': isGymnast,
            'isCoach': isCoach,
            'isJudge': isJudge,
            'photoConsent': photoConsent,
            'signupDate': signupDate
        };

        // Servlet request with new information
        $.ajax({
            type: "POST",
            url: "/../personServlet",
            data: {
                action: "Create",
                person: JSON.stringify(obj)
            },
            // If successful reload table to show updated information
            success: function (data) {
                checkForError(data)
                loadTable();
            }
        });
    }
}

/*********************************************
 * HTTP POST request (update)
 **********************************************/
function editProfile() {
    //Retrieves values from input forms
    var name = $("#editProfileName").val();
    var birthday = $("#editProfileBirthday").val();
    var signupDate = $("#editProfileSignupDate").val();
    var phoneNumber = $("#editProfilePhoneNumber").val();
    var license = $("#editProfileLicense").val();
    var email = $("#editProfileEmail").val();
    var address = $("#editProfileAddress").val();
    var city = $("#editProfileCity").val();
    var isMale = radioButtonValidator($('[name="radioEditGender"]'));
    var isGymnast = radioButtonValidator($('[name="radioEditGymnast"]'));
    var isCoach = radioButtonValidator($('[name="radioEditCoach"]'));
    var isJudge = radioButtonValidator($('[name="radioEditJudge"]'));
    var photoConsent = radioButtonValidator($('[name="radioEditPhotoConsent"]'));

    // Validates input and creates object to be sent to servlet
    if (inputValidation(name, isMale, isGymnast, isCoach, isJudge, photoConsent)) {
        var obj = {
            'name': name,
            'birthday': birthday,
            'phoneNumber1': phoneNumber,
            'license': license,
            'email1': email,
            'address1': address,
            'city1': city,
            'isMale': isMale,
            'isGymnast': isGymnast,
            'isCoach': isCoach,
            'isJudge': isJudge,
            'photoConsent': photoConsent,
            'signupDate': signupDate,
        };

        // Servlet request with new information
        $.ajax({
            type: "POST",
            url: "/../personServlet",
            data: {
                action: "Update",
                ID: $(".profile-selected")[0].getAttribute("profile-id"),
                person: JSON.stringify(obj)
            },
            // If successful reload table to show updated information
            success: function (data) {
                if (!checkForError(data)) {
                    eventSavedPopUp()
                }
                loadTable(); //TODO Load same profile again, instead of top profile
            },
        });
    }
}

/*********************************************
 * HTTP POST request (delete)
 **********************************************/
function deleteProfile() {
    // Request to servlet to delete profile and reload main area
    $.ajax({
        type: "POST",
        url: "/../personServlet/",
        data: {
            action: "Delete",
            id: parseInt($(".profile-selected")[0].getAttribute("profile-id"))
        },
        success: function (data) {
            checkForError(data);
            $("#myModal").css("display", "none");
            loadMainWindow();
        }
    })
}

/*********************************************
 * Regular functions
 **********************************************/
// Validates that required fields have been filled and prints alerts if not
function inputValidation(name, isMale, isGymnast, isCoach, isJudge) {
    if ((name === "") ||
        (isMale === "Error") ||
        (isGymnast === "Error") ||
        (isCoach === "Error") ||
        (isJudge === "Error") ||
        (photoConsent === "Error")) {
        alert("Fejl: Udfyld de påkrævede felter.")
        return false;
    } else if (
        (isGymnast === "false") &&
        (isCoach === "false") &&
        (isJudge === "false")) {
        alert("Fejl: Personen have mindst en af følgende roller:\nGymnast, Træner og Dommer")
        return false;
    } else {
        return true;
    }
}

function fillEditTemplate(obj) {
    // Makes variables to insert into template
    // If there is no such value in json obj there still has to be an empty variable to insert
    var birthDate = "";
    if ('birthday' in obj) {
        birthDate = correctDateFormat(obj.birthday);
    }

    var signupDate = "";
    if ('signupDate' in obj) {
        signupDate = correctDateFormat(obj.signupDate);
    }

    var license = "";
    if (obj.roleList[0] != null) {
        license = obj.roleList[0].license;
    }

    $("#editProfileName").val(obj.name);
    $("#editProfilePhoneNumber").val(obj.phoneNumberList[0]);
    $("#editProfileEmail").val(obj.emailList[0]);
    $("#editProfileLicense").val(license);
    $("#editProfileAddress").val(obj.addressList[0].road);
    $("#editProfileCity").val(obj.addressList[0].city);
    $("#editProfileBirthday").val(birthDate);
    $("#editProfileSignupDate").val(signupDate);
    profilesGender(obj);
    isProfileGymnast(obj);
    isProfileCoach(obj);
    isProfileJudge(obj);
    photoConsent(obj);
}

// Catches exeptions from servlets and prints them in an alertbox
function checkForError(data) {
    if ("error" === (data[0])) {
        alert(data[1]);
        return true;
    }
    return false;
}

function loadTable() {
    // If a table has been loaded previously the Bootstrap table is destroyed, to make place for the new one
    if ($.fn.dataTable.isDataTable('#FormattedTable')) {
        $("#FormattedTable").DataTable().destroy();
    }
    if ($("#GymnastsSubNav").hasClass("sub-navbar-link-selected")) {
        loadProfileTable(0, function () {
            makeTable();
        });
    } else if ($("#CoachesSubNav").hasClass("sub-navbar-link-selected")) {
        loadProfileTable(1, function () {
            makeTable();
        })
    } else if ($("#JudgesSubNav").hasClass("sub-navbar-link-selected")) {
        loadProfileTable(2, function () {
            makeTable();
        })
    }
    displayProfile();
}

function selectTopProfile() {
    $("tbody").find(".member_row:first").addClass("profile-selected");
}

// Popup in the top of the screen after save
function eventSavedPopUp() {
    $("#save-message1").css("display", "block");
    setTimeout(function () {
        $("#save-message1").css("opacity", 0);
    }, 2000);
    setTimeout(function () {
        $("#save-message1").css("display", "none");
        $("#save-message1").css("opacity", 1);
    }, 4000);
}

// Loads information about profile and displays it.
function displayProfile() {
    $.ajax({
        type: "GET",
        url: "/../personServlet",
        success: function (data) {
            if (!checkForError(data)) {
                // Sets obj to selected profile if there is a selected profile
                if (!($(".profile-selected")[0] == null)) {
                    var profileID = parseInt($(".profile-selected")[0].getAttribute("profile-id"));
                    var obj;
                    for (i = 0; i < data.length; i++) {
                        if (data[i].id === profileID) {
                            obj = data[i];
                            break;
                        }
                    }
                    insertIntoDisplayTemplate(obj);
                } else {
                    $("#profileInfo").empty();
                }
            }
        }
    })
}
// Loads html template and inserts information from obj
function insertIntoDisplayTemplate(obj) {
    $("#profileInfo").load("Members-Templates/Display-Profile.html", function () {

        var birthDate = "-";
        var signupDate = "-";
        var registrationDate = "-";
        var address1 = "-";
        var city1 = "-";
        var phoneNumber1 = "-";
        var email1 = "-";
        var license = "-";

        if ('birthday' in obj) {
            birthDate = correctDateFormat(obj.birthday);
        }

        if ('signupDate' in obj) {
            signupDate = correctDateFormat(obj.signupDate);
        }

        if ('registrationDate' in obj) {
            registrationDate = correctDateFormat(obj.registrationDate);
        }

        if (!(obj.addressList[0].road === "")) {
            address1 = obj.addressList[0].road;
        }

        if (!(obj.addressList[0].city === "")) {
            city1 = obj.addressList[0].city;
        }

        if (!(obj.phoneNumberList[0] === "")) {
            phoneNumber1 = obj.phoneNumberList[0];
        }

        if (!(obj.emailList[0] === "")) {
            email1 = obj.emailList[0];
        }

        if (obj.roleList[0] != null && !(obj.roleList[0].license === "")) {
            license = obj.roleList[0].license;
        }

        if (obj.isMale === true) {
            $("#gender").text("Mand");
        } else {
            $("#gender").text("Kvinde");
        }

        if (obj.photoConsent === true) {
            $("#photoConsent").text("Ja");
        } else {
            $("#photoConsent").text("Nej");
        }

        if (obj.roleList[0] != null && obj.roleList[0].isActive) {
            $("#isGymnast").text("Ja");
        } else {
            $("#isGymnast").text("Nej");
        }

        if (obj.roleList[1] != null && obj.roleList[1].isActive) {
            $("#isCoach").text("Ja");
        } else {
            $("#isCoach").text("Nej");
        }

        if (obj.roleList[2] != null && obj.roleList[2].isActive) {
            $("#isJudge").text("Ja");
        } else {
            $("#isJudge").text("Nej");
        }

        $("#name").text(obj.name);
        $("#signupDate").text(signupDate);
        $("#registrationDate").text(registrationDate);
        $("#birthday").text(birthDate);
        $("#adress").text(address1);
        $("#city").text(city1);
        $("#phoneNumber").text(phoneNumber1);
        $("#email").text(email1);
        $("#licenseNumber").text(license);
    });

}

// Returns the value of radiobuttons or error if there is no value
function radioButtonValidator(radioGroupName) {
    for (i = 0; i < radioGroupName.length; i++) {
        if (radioGroupName[i].checked) {
            return radioGroupName[i].value;
        }
    }
    return "Error"
}

function appendProfile(profile) {
    $('tbody').append(Mustache.render(profileTemplate(profile), profile))
}

// Load right side of the window containing table and sub-navbar with gymnast table selected.
function loadMainWindow() {
    $("#mainArea").load("Members-Templates/Table/Default-Table.html", function () {
        loadProfileTable(0, function () {
            makeTable();
        });
    });
}

// Sets radiobutton to existing value in edit template
function profilesGender(profile) {
    if (profile.isMale)
        $('[name="radioEditGender"]')[0].checked = true;
    else if (!profile.isMale)
        $('[name="radioEditGender"]')[1].checked = true;
}

// Sets radiobutton to existing value in edit template
function isProfileGymnast(profile) {
    if (profile.roleList[0] != null && profile.roleList[0].isActive) {
        $('[name="radioEditGymnast"]')[0].checked = true;
    }
    else {
        $('[name="radioEditGymnast"]')[1].checked = true;
    }
}

// Sets radiobutton to existing value in edit template
function isProfileCoach(profile) {
    if (profile.roleList[1] != null && profile.roleList[1].isActive) {
        $('[name="radioEditCoach"]')[0].checked = true;
    }
    else {
        $('[name="radioEditCoach"]')[1].checked = true;
    }
}

// Sets radiobutton to existing value in edit template
function isProfileJudge(profile) {
    if (profile.roleList[2] != null && profile.roleList[2].isActive) {
        $('[name="radioEditJudge"]')[0].checked = true;
    }
    else {
        $('[name="radioEditJudge"]')[1].checked = true;
    }
}

// Sets radiobutton to existing value in edit template
function photoConsent(profile) {
    if (profile.photoConsent === true) {
        $('[name="radioEditPhotoConsent"]')[0].checked = true;
    }
    else {
        $('[name="radioEditPhotoConsent"]')[1].checked = true;
    }
}
/*********************************************
 * HTML templates
 **********************************************/
function profileTemplate(gymnast) {

    var phoneNumber1 = gymnast.phoneNumberList[0];
    var email1 = gymnast.emailList[0];


    var gymnastTemplate = "" +
        "<tr class='member_row' profile-id='{{id}}'>" +
        "<td class='name'>" + "{{name}}" + "</td>" +
        "<td>" + phoneNumber1 + "</td>" +
        "<td>" + email1 + "</td>" +
        "<td>HOLD (TO DO)</td>" +
        "</tr>";


    return gymnastTemplate
}

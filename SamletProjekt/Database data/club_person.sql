-- MySQL dump 10.13  Distrib 8.0.13, for Win64 (x86_64)
--
-- Host: localhost    Database: club
-- ------------------------------------------------------
-- Server version	8.0.13

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `person`
--

DROP TABLE IF EXISTS `person`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `person` (
  `id_person` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `birthday` date DEFAULT NULL,
  `is_male` bit(1) NOT NULL,
  `reg_date` date NOT NULL,
  `signup_date` date DEFAULT NULL,
  `address1` varchar(99) DEFAULT NULL,
  `address2` varchar(99) DEFAULT NULL,
  `phone_number1` varchar(12) DEFAULT NULL,
  `phone_number2` varchar(12) DEFAULT NULL,
  `email1` varchar(50) DEFAULT NULL,
  `email2` varchar(50) DEFAULT NULL,
  `photo_consent` bit(1) NOT NULL,
  `gymnast_id` int(11) DEFAULT NULL,
  `coach_id` int(11) DEFAULT NULL,
  `judge_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_person`),
  UNIQUE KEY `personcol_UNIQUE` (`name`),
  UNIQUE KEY `gymnast_id_UNIQUE` (`gymnast_id`),
  UNIQUE KEY `coach_id_UNIQUE` (`coach_id`),
  UNIQUE KEY `judge_id_UNIQUE` (`judge_id`),
  CONSTRAINT `person_coach_key` FOREIGN KEY (`coach_id`) REFERENCES `coach` (`id_coach`) ON DELETE SET NULL,
  CONSTRAINT `person_gymnast_key` FOREIGN KEY (`gymnast_id`) REFERENCES `gymnast` (`id_gymnast`) ON DELETE SET NULL,
  CONSTRAINT `person_judge_key` FOREIGN KEY (`judge_id`) REFERENCES `judge` (`id_judge`) ON DELETE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-12-10 16:09:26

package Model;

public enum  PersonSignupState {
    SIGNED_UP, NOT_SIGNED_UP, UNDECIDED
}

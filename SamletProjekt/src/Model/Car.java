package Model;

import Model.Exceptions.CarCapacityException;
import Model.Exceptions.IllegalObjectException;
import Model.Exceptions.ObjectNotFoundException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

public class Car {
    public static final int SEATS = 4;
    private final int id;
    private int availableSeatCount;
    private Event event;
    private List<CarAssignment> carAssignmentList;

    Car(int id, Event event) {
        Club.checkForNull(event);
        Club.checkID(id);
        this.id = id;
        this.availableSeatCount = SEATS;
        this.event = event;
        this.carAssignmentList = new ArrayList<>();
    }

    Car(Event event, Person driver) {
        Club.checkForNull(event);
        Club.checkForNull(driver);

        this.id = Club.INSTANCE.getDatabaseManager().getNextID("car", "id_car");
        this.availableSeatCount = SEATS - 1;
        this.event = event;
        this.carAssignmentList = new ArrayList<>();

        CarAssignment carAssignment = new CarAssignment(true, driver, this);
        carAssignmentList.add(carAssignment);
        driver.addCarAssignment(carAssignment);
    }

    public Car(Event event) {
        Club.checkForNull(event);

        this.id = Club.INSTANCE.getDatabaseManager().getNextID("car", "id_car");
        this.availableSeatCount = SEATS;
        this.event = event;
        this.carAssignmentList = new ArrayList<>();
    }

    int getID() {
        return id;
    }

    int getAvailableSeatCount() {
        return availableSeatCount;
    }

    Event getEvent() {
        return event;
    }

    List<CarAssignment> getCarAssignmentList() {
        return Collections.unmodifiableList(carAssignmentList);
    }

    void addCarAssignment(CarAssignment carAssignment) {
        checkCarAssignment(carAssignment);

        carAssignmentList.add(carAssignment);
        availableSeatCount--;
    }

    private void checkCarAssignment(CarAssignment carAssignment) {
        Club.checkForNull(carAssignment);
        if ((carAssignment.isDriver() && hasDriver()) || this != carAssignment.getCar()
                || carAssignmentList.contains(carAssignment)) {
            throw new IllegalObjectException("The CarAssignment object cannot be added to the list in the Car object.");
        } else if (availableSeatCount <= 0) {
            throw new CarCapacityException("There are no more available seats in the Car object.");
        }
    }

    void deleteCarAssignment(CarAssignment carAssignment) {
        if (carAssignmentList.remove(carAssignment)) {
            availableSeatCount++;
        } else {
            throw new ObjectNotFoundException("The CarAssignment object was not found in the Car object.");
        }
    }

    void deleteAllCarAssignments() {
        CarAssignment carAssignment;

        for (int i = carAssignmentList.size() - 1; i >= 0; i--) {
            carAssignment = carAssignmentList.remove(i);
            carAssignment.deleteFromPerson();
        }

        availableSeatCount = SEATS;
    }

    void deleteAllPassengers() {
        CarAssignment carAssignment;

        for (int i = carAssignmentList.size() - 1; i >= 0; i--) {
            if (!carAssignmentList.get(i).isDriver()) {
                carAssignment = carAssignmentList.remove(i);
                carAssignment.deleteFromPerson();
                availableSeatCount++;
            }
        }
    }

    private boolean hasDriver() {
        for (CarAssignment c : carAssignmentList) {
            if (c.isDriver()) {
                return true;
            }
        }
        return false;
    }

    Person getDriver() {
        for (CarAssignment c : carAssignmentList) {
            if (c.isDriver()) {
                return c.getPerson();
            }
        }
        return null;
    }

    boolean hasPerson(Person person) {
        for (CarAssignment c : carAssignmentList) {
            if (c.getPerson().equals(person)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Car car = (Car) o;
        return id == car.id &&
                Objects.equals(event, car.event) &&
                Objects.equals(carAssignmentList, car.carAssignmentList);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, event, carAssignmentList);
    }
}
package Model;

import java.util.Objects;

public class Judge implements Role {
    private final int id;
    private transient Person person;
    private boolean isActive;

    Judge(int id, Person person, boolean isActive) {
        Club.checkForNull(person);
        Club.checkID(id);
        this.id = id;
        this.person = person;
        this.isActive = isActive;
    }

    Judge(Person person, boolean isActive) {
        Club.checkForNull(person);

        this.id = Club.INSTANCE.getDatabaseManager().getNextID("judge", "id_judge");
        this.person = person;
        this.isActive = isActive;
    }

    @Override
    public int getID() {
        return id;
    }

    @Override
    public Person getPerson() {
        return person;
    }

    @Override
    public boolean isActive() {
        return isActive;
    }

    @Override
    public void setActive(boolean active) {
        isActive = active;
    }

    @Override
    public boolean isGymnast() {
        return false;
    }

    @Override
    public boolean isCoach() {
        return false;
    }

    @Override
    public boolean isJudge() {
        return true;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Judge judge = (Judge) o;
        return Objects.equals(person, judge.person);
    }

    @Override
    public int hashCode() {
        return Objects.hash(person);
    }
}

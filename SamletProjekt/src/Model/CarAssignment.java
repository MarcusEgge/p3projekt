package Model;

import Model.Exceptions.IllegalIDException;

import java.util.Objects;

public class CarAssignment {
    private int id;
    private boolean isDriver;
    private Person person;
    private Car car;

    CarAssignment(int id, boolean isDriver, Person person, Car car) {
        checkArgumentsForNull(person, car);

        this.id = id;
        this.isDriver = isDriver;
        this.person = person;
        this.car = car;
    }

    CarAssignment(boolean isDriver, Person person, Car car) {
        checkArgumentsForNull(person, car);

        this.id = 0;
        this.isDriver = isDriver;
        this.person = person;
        this.car = car;
    }

    private void checkArgumentsForNull(Person person, Car car) {
        Club.checkForNull(person);
        Club.checkForNull(car);
    }

    int getID() {
        return id;
    }

    void setID(int id) {
        if (this.id != 0 || id <= 0) {
            throw new IllegalIDException("The CarAssignment id is illegal.");
        }
        this.id = id;
    }

    boolean isDriver() {
        return isDriver;
    }

    Person getPerson() {
        return person;
    }

    Car getCar() {
        return car;
    }

    void deleteFromPerson() {
        person.deleteCarAssignment(this);
    }

    void deleteFromCar(){
        car.deleteCarAssignment(this);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CarAssignment that = (CarAssignment) o;
        return  Objects.equals(person, that.person) &&
                Objects.equals(car.getEvent(), that.car.getEvent());
    }

    @Override
    public int hashCode() {
        return Objects.hash(person, car.getEvent());
    }
}

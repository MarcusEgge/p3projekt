package Model;

import Model.Exceptions.IllegalIDException;

import java.util.Objects;

public class TeamAssignment {
    private int id;
    private Team team;
    private TeamMember teamMember;
    private boolean isSignedUp;

    TeamAssignment(int id, Team team, TeamMember teamMember, boolean isSignedUp) {
        checkArgumentsForNull(team, teamMember);

        setID(id);
        this.team = team;
        this.isSignedUp = isSignedUp;
        this.teamMember = teamMember;
    }

    public TeamAssignment(Team team, TeamMember teamMember, boolean isSignedUp) {
        checkArgumentsForNull(team, teamMember);

        this.id = 0;
        this.team = team;
        this.isSignedUp = isSignedUp;
        this.teamMember = teamMember;
    }

    private void checkArgumentsForNull(Team team, TeamMember teamMember) {
        Club.checkForNull(team);
        Club.checkForNull(teamMember);
    }

    // -------------------- Getters and setters --------------------
    public int getID() {
        return id;
    }

    public void setID(int id) {
        if (this.id != 0 || id <= 0) {
            throw new IllegalIDException("The TeamAssignment id is illegal.");
        }
        this.id = id;
    }

    public Team getTeam() {
        return team;
    }

    TeamMember getTeamMember() {
        return teamMember;
    }

    public boolean isSignedUp() {
        return isSignedUp;
    }

    public void setSignedUp(boolean signedUp) {
        if (teamMember.isGymnast()) {
            if (isSignedUp && !signedUp) {
                team.deleteGymnastFromFutureEvents(teamMember);
            } else if (!isSignedUp && signedUp){
                team.addGymnastToFutureEvents(teamMember);
            }
        }
        isSignedUp = signedUp;
    }

    // -------------------- Methods for deleting 'this' --------------------
    void deleteFromTeam() {
        team.deleteTeamAssignment(this);
    }

    void deleteFromRole() {
        teamMember.deleteTeamAssignment(this);
    }

    // -------------------- Other methods --------------------
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TeamAssignment that = (TeamAssignment) o;
        return Objects.equals(team, that.team) &&
                Objects.equals(teamMember, that.teamMember);
    }

    @Override
    public int hashCode() {
        return Objects.hash(team, teamMember);
    }
}

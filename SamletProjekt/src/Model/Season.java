package Model;

import Model.Exceptions.IllegalIDException;
import Model.Exceptions.IllegalObjectException;
import Model.Exceptions.ObjectNotFoundException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;

public class Season implements Comparable<Season> {
    private final int id;
    private String name;
    private LocalDateTime startDate, endDate;
    private List<Event> eventList;
    private boolean isActive;

    public Season(int id, LocalDateTime startDate, LocalDateTime endDate, boolean isActive) {
        checkArgumentsForNull(startDate, endDate);
        Club.checkID(id);
        this.id = id;
        this.name = Integer.toString(startDate.getYear()) + "/" + Integer.toString(endDate.getYear());
        this.startDate = startDate;
        this.endDate = endDate;
        this.eventList = new ArrayList<>();
        this.isActive = isActive;
    }

    public Season(LocalDateTime startDate, LocalDateTime endDate) {
        checkArgumentsForNull(startDate, endDate);

        this.id = Club.INSTANCE.getDatabaseManager().getNextID("season", "id_season");
        this.name = Integer.toString(startDate.getYear()) + "/" + Integer.toString(endDate.getYear());
        this.startDate = startDate;
        this.endDate = endDate;
        this.eventList = new ArrayList<>();
        updateState();
    }

    private void checkArgumentsForNull(LocalDateTime startDate, LocalDateTime endDate) {
        Club.checkForNull(startDate);
        Club.checkForNull(endDate);
    }

    // -------------------- Simple getters and setters. --------------------
    public int getID() {
        return id;
    }

    public String getName() {
        return name;
    }

    public LocalDateTime getStartDate() {
        return startDate;
    }

    public LocalDateTime getEndDate() {
        return endDate;
    }

    public boolean isActive() {
        return isActive;
    }

    // -------------------- Methods related to 'eventList' --------------------
    public List<Event> viewEvents(){
        updateEvents();
        Collections.sort(eventList);
        return Collections.unmodifiableList(eventList);
    }

    private void updateEvents(){
        for (Event e: eventList) {
            e.updateState();
            e.countRoles();
        }
    }

    public Event viewEvent(int id) {
        for (Event e: eventList) {
            if (e.getID() == id) {
                return e;
            }
        }
        throw new ObjectNotFoundException("There was no such Event object in the Season object.");
    }

    void addEvent(Event event) {
        Club.checkForNull(event);

        if (this != event.getSeason() || eventList.contains(event)) {
            throw new IllegalObjectException("The Event object cannot be added to the list in the Event object.");
        }
        eventList.add(event);
    }

    public void createEvent(String name, Address address, String startDateTimeString, String endDateString,
                            int coachRequirement, int judgeRequirement){
        LocalDateTime startDate = LocalDateTime.parse(startDateTimeString, Club.DATE_TIME_FORMATTER);
        LocalDate endDate = LocalDate.parse(endDateString, Club.DATE_FORMATTER);

        Event event = new Event(name, address, startDate, endDate, this, coachRequirement, judgeRequirement);

        if (eventList.contains(event)) {
            throw new IllegalObjectException("Such an Event object already exists");
        }

        event.generateTeamSignupsFromList(Club.INSTANCE.viewTeams());  // Initializes a 'TeamSignup' object for each 'Team' object in 'teamList'.
        event.generatePersonSignupsFromList(Club.INSTANCE.viewPersons()); // Initializes a 'PersonSignup' object for each active 'Role' object in the club.

        eventList.add(event);
        Club.INSTANCE.getDatabaseManager().saveEventData(event);
    }

    public void editEvent(int eventID, String name, Address address, String startDate, String endDate,
                          int coachRequirement, int judgeRequirement){
        if (eventID <= 0) {
            throw new IllegalIDException("Event id is illegal.");
        }
        Event event = viewEvent(eventID);

        event.editBasicData(name, address, LocalDateTime.parse(startDate, Club.DATE_TIME_FORMATTER), LocalDate.parse(endDate,
                Club.DATE_FORMATTER), coachRequirement, judgeRequirement);
        Club.INSTANCE.getDatabaseManager().updateEventData(event);
    }

    public void deleteEvent(int id){
        if (id <= 0) {
            throw new IllegalIDException("Event id is illegal.");
        }
        Event event = viewEvent(id);

        eventList.remove(event);
        event.delete();
        Club.INSTANCE.getDatabaseManager().deleteObjectData("event", "id_event", event.getID());
    }

    // -------------------- Other methods --------------------

    void updateState(){
        LocalDateTime now = LocalDateTime.now();

        isActive = startDate.isBefore(now) && endDate.isAfter(now);
    }

    public void distributeDrivers(){ // TODO: Unfinished method.
        for (Event e: eventList){
            e.clearAllCars();
            distributeForEvent(e);
        }
    }

    private void distributeForEvent(Event event){      // TODO: Unfinished method.
        List<Person> personList = event.getPersonsSignedUp();
        int numberOfCars = personList.size() % Car.SEATS != 0 ? personList.size() / Car.SEATS + 1 : personList.size() / Car.SEATS;

        for (int i = 0; i < numberOfCars; i++){
            event.addCar(chooseDriver(personList));
        }
    }

    private Person chooseDriver(List<Person> personList){    // TODO: Unfinished method.
        int bound = personList.size();
        Random random = new Random();

        return personList.remove(random.nextInt(bound));
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null || getClass() != that.getClass()) {
            return false;
        }
        Season season = (Season) that;
        return Objects.equals(name, season.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    @Override
    public int compareTo(Season that) {
        return this.startDate.compareTo(that.startDate);
    }
}

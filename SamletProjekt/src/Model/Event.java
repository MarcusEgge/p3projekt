package Model;

import Model.Exceptions.IllegalFormatException;
import Model.Exceptions.IllegalObjectException;
import Model.Exceptions.ObjectNotFoundException;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import static java.time.temporal.ChronoUnit.DAYS;

public class Event implements Comparable<Event>{
    private final int id;
    private String name;
    private Address address;
    private LocalDateTime startDate;
    private LocalDate endDate;
    private transient Season season;
    private transient List<PersonSignup> personSignupList;
    private List<TeamSignup> teamSignupList;
    private List<Car> carList;
    private int coachRequirement, judgeRequirement, numberOfGymnastsSignedUp, numberOfCoachesSignedUp, numberOfJudgesSignedUp;
    private boolean isActive;

    Event(int id, String name, Address address, LocalDateTime startDate, LocalDate endDate, Season season,
                 int coachRequirement, int judgeRequirement, boolean isActive) {
        checkArguments(name, address, startDate, endDate, season);

        Club.checkID(id);
        this.id = id;
        this.name = name;
        this.address = address;
        this.startDate = startDate;
        this.endDate = endDate;
        this.season = season;
        this.personSignupList = new ArrayList<>();
        this.teamSignupList = new ArrayList<>();
        this.carList = new ArrayList<>();
        this.isActive = isActive;
        this.coachRequirement = coachRequirement;
        this.judgeRequirement = judgeRequirement;
    }

    Event(String name, Address address, LocalDateTime startDate, LocalDate endDate, Season season,
                 int coachRequirement, int judgeRequirement) {
        checkArguments(name, address, startDate, endDate, season);

        this.id = Club.INSTANCE.getDatabaseManager().getNextID("event", "id_event");
        this.name = name;
        this.address = address;
        this.startDate = startDate;
        this.endDate = endDate;
        this.season = season;
        this.personSignupList = new ArrayList<>();
        this.teamSignupList = new ArrayList<>();
        this.carList = new ArrayList<>();
        this.coachRequirement = coachRequirement;
        this.judgeRequirement = judgeRequirement;

        updateState();
    }

    private void checkArguments(String name, Address address, LocalDateTime startDate, LocalDate endDate, Season season) {
        Club.checkForNull(name);
        Club.checkForNull(address);
        Club.checkForNull(startDate);
        Club.checkForNull(endDate);
        Club.checkForNull(season);

        if (name.isEmpty()){
            throw new IllegalFormatException("The name of an Event object cannot be an empty string.");
        }
    }

    // -------------------- Simple getters and setters --------------------
    int getID() {
        return id;
    }

    String getName() {
        return name;
    }

    void setName(String name) {
        Club.checkForNull(name);
        this.name = name;
    }

    Address getAddress() {
        return address;
    }

    void setAddress(Address address) {
        Club.checkForNull(address);
        this.address = address;
    }

    LocalDateTime getStartDate() {
        return startDate;
    }

    void setStartDate(LocalDateTime startDate) {
        Club.checkForNull(startDate);
        this.startDate = startDate;
    }

    LocalDate getEndDate() {
        return endDate;
    }

    void setEndDate(LocalDate endDate) {
        Club.checkForNull(endDate);
        this.endDate = endDate;
    }

    Season getSeason() {
        return season;
    }

    int getCoachRequirement() {
        return coachRequirement;
    }

    int getJudgeRequirement() {
        return judgeRequirement;
    }

    int getNumberOfGymnastsSignedUp() {
        return numberOfGymnastsSignedUp;
    }

    int getNumberOfCoachesSignedUp() {
        return numberOfCoachesSignedUp;
    }

    int getNumberOfJudgesSignedUp() {
        return numberOfJudgesSignedUp;
    }

    public boolean isActive() {
        return isActive;
    }

    // -------------------- Methods related to field 'personSignupList' --------------------
    List<PersonSignup> getPersonSignupList() {
        return Collections.unmodifiableList(personSignupList);
    }

    public List<PersonSignup> viewGymnastPersonSignups() {
        List<PersonSignup> gymnastPersonSignupList = new ArrayList<>();

        for (PersonSignup p : personSignupList) {
            if (p.getRole().isGymnast()) {
                gymnastPersonSignupList.add(p);
            }
        }
        return Collections.unmodifiableList(gymnastPersonSignupList);
    }

    public List<PersonSignup> viewCoachPersonSignups() {
        List<PersonSignup> gymnastPersonSignupList = new ArrayList<>();

        for (PersonSignup p : personSignupList) {
            if (p.getRole().isCoach()) {
                gymnastPersonSignupList.add(p);
            }
        }
        return Collections.unmodifiableList(gymnastPersonSignupList);
    }

    public List<PersonSignup> viewJudgePersonSignups() {
        List<PersonSignup> gymnastPersonSignupList = new ArrayList<>();

        for (PersonSignup p : personSignupList) {
            if (p.getRole().isJudge()) {
                gymnastPersonSignupList.add(p);
            }
        }
        return Collections.unmodifiableList(gymnastPersonSignupList);
    }

    List<Person> getPersonsSignedUp() {
        int length = personSignupList.size();
        Person person;
        List<Person> personList = new ArrayList<>();

        for (int i = 0; i < length; i++) {
            person = personSignupList.get(i).getPerson();
            if (!hasDuplicatePerson(i + 1, length, person)) { // If the person does not have another 'PersonSignup'
                personList.add(person);                             // object further on in the list.
            }
        }

        return personList;
    }

    private boolean hasDuplicatePerson(int start, int length, Person person) {
        for (int i = start; i < length; i++) {
            if (personSignupList.get(i).getPerson().equals(person)) {
                return true;
            }
        }
        return false;
    }

    void addPersonSignup(PersonSignup personSignup) {
        Club.checkForNull(personSignup);
        if (this != personSignup.getEvent() || personSignupList.contains(personSignup)) {
            throw new IllegalObjectException("The PersonSignup object cannot be added to the list in the Event object.");
        }
        personSignupList.add(personSignup);
    }

    void generatePersonSignupsFromList(List<Person> personList) {
        Club.checkForNull(personList);

        for (Person p : personList) {
            generatePersonSignupsFromPerson(p, true, true, true);
        }
    }

    void generatePersonSignupsFromPerson(Person person, boolean makeGymnastSignup, boolean makeCoachSignup, boolean makeJudgeSignup) {
        Club.checkForNull(person);

        if (makeGymnastSignup && person.isGymnast()) {
            makePersonSignupForGymnast((Gymnast) person.getRole(Person.GYMNAST_INDEX));
        }
        if (makeCoachSignup && person.isCoach()) {
            makePersonSignupForCoachOrJudge(person.getRole(Person.COACH_INDEX));
        }
        if (makeJudgeSignup && person.isJudge()) {
            makePersonSignupForCoachOrJudge(person.getRole(Person.JUDGE_INDEX));
        }
    }

    private void makePersonSignupForGymnast(Gymnast gymnast) {
        PersonSignup personSignup = new PersonSignup(gymnast, this);
        Team team;
        TeamSignup temp, teamSignup;
        int index;

        for (TeamAssignment t : gymnast.getTeamAssignmentList()) { // Loops through the 'TeamAssignment' objects of 'gymnast' in order to connect
            if (t.isSignedUp()) {                                  // 'personSignup' to the correct 'TeamSignup' objects.
                team = t.getTeam();
                temp = new TeamSignup(team, this, false);
                index = teamSignupList.indexOf(temp);

                if (index == -1) { // If field 'teamSignupList' does not contain 'teamSignup'.
                    throw new IllegalObjectException("Could not find TeamSignup for the PersonSignup of person " + gymnast.getPerson().getID() + ".");
                }

                teamSignup = teamSignupList.get(index);

                personSignup.addTeamSignup(teamSignup);
                teamSignup.addPersonSignup(personSignup);
            }
        }
        gymnast.getPerson().addPersonSignup(personSignup);
        this.personSignupList.add(personSignup);
    }

    private void makePersonSignupForCoachOrJudge(Role role) {
        PersonSignup personSignup = new PersonSignup(role, this);

        role.getPerson().addPersonSignup(personSignup);
        this.personSignupList.add(personSignup);
    }

    public void editPersonSignup(int personSignupID, PersonSignupState signupState){
        for (PersonSignup p : personSignupList) {
            if (p.getID() == personSignupID) {
                p.setState(signupState);
                Club.INSTANCE.getDatabaseManager().updatePersonSignupData(p);
                return;
            }
        }
        throw new ObjectNotFoundException("When editing the PersonSignup object, the object did not exist.");
    }

    void deletePersonSignup(PersonSignup personSignup) {
        if (!personSignupList.remove(personSignup)) {
            throw new ObjectNotFoundException("The PersonSignup object was not found in the Event object.");
        }
    }

    // -------------------- Methods related to field 'teamSignupList' --------------------
    public List<TeamSignup> viewTeamSignups() {
        return Collections.unmodifiableList(teamSignupList);
    }

    void addTeamSignup(TeamSignup teamSignup) {
        Club.checkForNull(teamSignup);
        if (this != teamSignup.getEvent() || teamSignupList.contains(teamSignup)) {
            throw new IllegalObjectException("The TeamSignup object cannot be added to the list in the Event object.");
        }
        teamSignupList.add(teamSignup);
    }

    void generateTeamSignupsFromList(List<Team> teamList) {
        TeamSignup teamSignup;
        Club.checkForNull(teamList);

        for (Team t : teamList) {
            teamSignup = new TeamSignup(t, this, false);

            teamSignupList.add(teamSignup);
            t.addTeamSignup(teamSignup);
        }
    }

    void editTeamSignup(int teamSignupID, boolean isSignedUp) {
        for (TeamSignup t : teamSignupList) {
            if (t.getID() == teamSignupID) {
                t.setSignedUp(isSignedUp);
                updatePersonSignups(t.getPersonSignupList());
                // TODO: Missing method for updating database.
                return;
            }
        }
        throw new ObjectNotFoundException("When editing the TeamSignup object, the object did not exist.");
    }

    private void updatePersonSignups(List<PersonSignup> personSignupList) {
        for (PersonSignup p: personSignupList) {
            Club.INSTANCE.getDatabaseManager().updatePersonSignupData(p);
        }
    }

    void deleteTeamSignup(TeamSignup teamSignup) {
        if(!teamSignupList.remove(teamSignup)) {
            throw new ObjectNotFoundException("The TeamSignup object was not found in the Event object.");
        }
    }

    // -------------------- Methods related to field 'carList' --------------------
    List<Car> getCarList() {
        return Collections.unmodifiableList(carList);
    }

    void addCar(Car car) {
        Club.checkForNull(car);
        if (!(this == car.getEvent()) || carList.contains(car)) {
            throw new IllegalObjectException("The Car object cannot be added to the list in the Event object.");
        }
        carList.add(car);
    }

    void addCar(Person driver) {
        Club.checkForNull(driver);
        if (personAlreadyHasCarAssignment(driver)) {
            throw new IllegalObjectException("This Person object is already assigned to a car in the Event object.");
        }
        carList.add(new Car(this, driver));
    }

    private boolean personAlreadyHasCarAssignment(Person person) {
        for (Car c : carList) {
            if (c.hasPerson(person)) {
                return true;
            }
        }
        return false;
    }

    public void distributePassengers() { // TODO: Unfinished method.
        for (Car c: carList) {
            c.deleteAllPassengers();
        }
    }

    void clearAllCars() {
        for (Car c : carList) {
            c.deleteAllCarAssignments();
        }
    }

    // -------------------- Methods for editing, updating and deleting 'this' --------------------
    void editBasicData(String name, Address address, LocalDateTime startDate, LocalDate endDate,
                              int coachRequirement, int judgeRequirement) {
        checkArguments(name, address, startDate, endDate, season);
        this.name = name;
        this.address = address;
        this.startDate = startDate;
        this.endDate = endDate;
        this.coachRequirement = coachRequirement;
        this.judgeRequirement = judgeRequirement;
    }
    void delete() {
        List<CarAssignment> carAssignmentList = getAllCarAssignments();

        for (PersonSignup p : personSignupList) {
            p.deleteFromPerson();
        }
        for (TeamSignup t : teamSignupList) {
            t.deleteFromTeam();
        }
        for (CarAssignment c : carAssignmentList) {
            c.deleteFromPerson();
        }
    }

    private List<CarAssignment> getAllCarAssignments() {
        List<CarAssignment> allCarAssignments = new ArrayList<>();

        for (Car c : carList) {
            allCarAssignments.addAll(c.getCarAssignmentList());
        }

        return allCarAssignments;
    }

    void updateState() {
        LocalDate now = LocalDate.now();

        isActive = endDate.isAfter(now);
    }

    void countRoles() {
        numberOfGymnastsSignedUp = countNumberOfGymnastsSignedUp();
        numberOfCoachesSignedUp = countNumberOfCoachesSignedUp();
        numberOfJudgesSignedUp = countNumberOfJudgesSignedUp();
    }

    private int countNumberOfGymnastsSignedUp() {
        int numberOfGymnasts = 0;

        for (PersonSignup p : personSignupList) {
            if (p.getState() == PersonSignupState.SIGNED_UP && p.getRole().isGymnast()) {
                numberOfGymnasts++;
            }
        }

        return numberOfGymnasts;
    }

    private int countNumberOfCoachesSignedUp() {
        int numberOfCoaches = 0;

        for (PersonSignup p : personSignupList) {
            if (p.getState() == PersonSignupState.SIGNED_UP && p.getRole().isCoach()) {
                numberOfCoaches++;
            }
        }

        return numberOfCoaches;
    }

    private int countNumberOfJudgesSignedUp() {
        int numberOfJudges = 0;

        for (PersonSignup p : personSignupList) {
            if (p.getState() == PersonSignupState.SIGNED_UP && p.getRole().isJudge()) {
                numberOfJudges++;
            }
        }

        return numberOfJudges;
    }

    // -------------------- Other methods --------------------
    public long daysTillDeadline() {
        LocalDateTime deadline = startDate.minusDays(3 * 7);
        LocalDateTime now = LocalDateTime.now().minusMinutes(10);

        return DAYS.between(now, deadline);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Event event = (Event) o;
        return Objects.equals(name, event.name) &&
                Objects.equals(address, event.address) &&
                Objects.equals(startDate, event.startDate) &&
                Objects.equals(endDate, event.endDate) &&
                Objects.equals(season, event.season);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, address, startDate, endDate, season);
    }

    @Override
    public int compareTo(Event that) {
        if (this.isActive == that.isActive) {
            if (this.startDate.equals(that.startDate)) {
                return this.name.compareTo(that.name);
            } else {
                return this.startDate.compareTo(that.startDate);
            }
        } else if (this.isActive) {
            return -1;
        } else {
            return 1;
        }
    }
}

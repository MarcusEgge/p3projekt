package Model;

import Model.Exceptions.IllegalFormatException;
import Model.Exceptions.IllegalIDException;
import Model.Exceptions.IllegalObjectException;
import Model.Exceptions.ObjectNotFoundException;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

public enum Club {
    INSTANCE;

    private List<Season> seasonList;
    private List<Person> personList;
    private List<Team> teamList;
    private DatabaseManager databaseManager;
    static final public DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
    static final public DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd");

    Club() {
        databaseManager = new DatabaseManager();
        personList = new ArrayList<>();
        seasonList = new ArrayList<>();
        teamList = new ArrayList<>();

        getDataFromDatabase();
    }

    private void getDataFromDatabase(){
        databaseManager.loadAllData(seasonList, personList, teamList);
    }

    DatabaseManager getDatabaseManager() {
        return databaseManager;
    }

    void clear(){ // For use in tests.
        seasonList.clear();
        personList.clear();
        teamList.clear();
    }

    // -------------------- Methods related to 'seasonList' --------------------
    public List<Season> viewSeasons() {
        updateSeasons();
        return Collections.unmodifiableList(seasonList);
    }

    public Season viewSeason(String seasonName) {
        updateSeasons();
        for (Season s : seasonList) {
            if (s.getName().equals(seasonName)) {
                return s;
            }
        }
        throw new ObjectNotFoundException("The season could not be found in the club.");
    }

    public void createSeason() {  // Creates a Season object.
        int startDateInd = 0, endDateInd = 1;
        Season season;
        LocalDateTime startDate, endDate;
        List<LocalDateTime> dateList;

        dateList = getDates(startDateInd, endDateInd);  // The method 'getDates' returns a list of two LocalDateTime
        startDate = dateList.get(startDateInd);         // objects.
        endDate = dateList.get(endDateInd);

        season = new Season(startDate, endDate);
        seasonList.add(season);
        Collections.sort(seasonList);
        databaseManager.saveSeasonData(season);   // Saving the Season object in the database.
    }

    private void updateSeasons() {
        for (Season s : seasonList) {
            s.updateState();
        }
    }

    private List<LocalDateTime> getDates(int startDateInd, int endDateInd) {
        int yearJump = 1;
        List<LocalDateTime> dateList;

        if (!seasonList.isEmpty()) {                                                      // If a former season exists,
            dateList = getDatesBasedOnFormerSeason(startDateInd, endDateInd, yearJump);   // we base the start date and
        } else {                                                                          // end date on the fields of
            dateList = getDatesBasedOnCurrentYear(startDateInd, endDateInd, yearJump);    // the former season. If not
        }                                                                                 // we derive the dates based
        // on the current year.
        return dateList;
    }

    private List<LocalDateTime> getDatesBasedOnFormerSeason(int startDateInd, int endDateInd, int yearJump) {
        List<LocalDateTime> dateList = new ArrayList<>();
        Season formerSeason = seasonList.get(seasonList.size() - 1);

        dateList.add(startDateInd, formerSeason.getStartDate().plusYears(yearJump));  // We add a year to the start date and
        dateList.add(endDateInd, formerSeason.getEndDate().plusYears(yearJump));      // end date of the former season.

        return dateList;
    }

    private List<LocalDateTime> getDatesBasedOnCurrentYear(int startDateInd, int endDateInd, int yearJump) {
        List<LocalDateTime> dateList = new ArrayList<>();
        List<String> stringDateList;

        stringDateList = getDatesAsStrings(startDateInd, endDateInd, yearJump, LocalDate.now());

        dateList.add(startDateInd, LocalDateTime.parse(stringDateList.get(startDateInd), DATE_TIME_FORMATTER));
        dateList.add(endDateInd, LocalDateTime.parse(stringDateList.get(endDateInd), DATE_TIME_FORMATTER));

        return dateList;
    }

    List<String> getDatesAsStrings(int startDateInd, int endDateInd, int yearJump, LocalDate now) {
        List<String> stringDateList = new ArrayList<>();
        String startDayAndMonth = "-08-01", endDayAndMonth = "-07-31", startTime = " 00:00", endTime = " 23:59";

        if (secondHalfOfSeason(now)) { // We always want the new season to cover the ongoing season. So if the current date is 1st of January 2019, then the season should be 2018/2019.
            stringDateList.add(startDateInd, Integer.toString(now.getYear() - yearJump) + startDayAndMonth + startTime);
            stringDateList.add(endDateInd, Integer.toString(now.getYear()) + endDayAndMonth + endTime);
        } else {
            stringDateList.add(startDateInd, Integer.toString(now.getYear()) + startDayAndMonth + startTime);
            stringDateList.add(endDateInd, Integer.toString(now.getYear() + yearJump) + endDayAndMonth + endTime);
        }

        return stringDateList;
    }

    private boolean secondHalfOfSeason(LocalDate now) {
        return now.isBefore(LocalDate.of(now.getYear(), 7, 31));
    }

    // -------------------- Methods related to 'personList' --------------------
    public List<Person> viewPersons() {
        return Collections.unmodifiableList(personList);
    }

    public Person viewPerson(int personID) {
        for (Person p: personList) {
            if (p.getID() == personID) {
                return p;
            }
        }

        throw new ObjectNotFoundException("No person with id " + personID + " was found.");
    }

    public void createPerson(String name, boolean isMale, List<Address> addrList, List<String> emailList,
                             List<String> phoneList, String bDay, String signUpDate, boolean photoConsent,
                             boolean isGymnast, boolean isCoach, boolean isJudge, String license,
                             List<String> teamListGymnast, List<String> teamListCoach) {
        Person person = new Person(name, isMale, LocalDate.now(), photoConsent);

        checkRoleBooleans(isGymnast, isCoach, isJudge); // Throws exception if all booleans are false.
        checkForNull(teamListGymnast);
        checkForNull(teamListCoach);

        setOptionalPersonInfo(person, addrList, emailList, phoneList, bDay, signUpDate); // Sets fields not covered by the constructor of 'Person'.
        registerRoles(person, isGymnast, isCoach, isJudge, license, teamListGymnast, teamListCoach);

        if (personList.contains(person)) {
            throw new IllegalObjectException("A person with name " + person.getName() + " already exists.");
        }
        personList.add(person);

        generatePersonSignupsForFutureEvents(person, true, true, true);

        databaseManager.savePersonData(person);
    }

    private void checkRoleBooleans(boolean isGymnast, boolean isCoach, boolean isJudge) {
        if (!isGymnast && !isCoach && !isJudge) {
            throw new IllegalObjectException("A person should have at least one role.");
        }
    }

    private void setOptionalPersonInfo(Person person, List<Address> addrList, List<String> emailList,
                                       List<String> phoneList, String bDay, String signUpDate) {
        checkForNull(bDay);
        checkForNull(signUpDate);
        person.setAddressList(addrList)
                .setEmailList(emailList)
                .setPhoneNumberList(phoneList)
                .setBirthday(bDay.isEmpty() ? null : LocalDate.parse(bDay, DATE_FORMATTER))
                .setSignupDate(signUpDate.isEmpty() ? null : LocalDate.parse(signUpDate, DATE_FORMATTER));
    }

    private void registerRoles(Person person, boolean isGymnast, boolean isCoach, boolean isJudge, String license,
                               List<String> teamNameListGymnast, List<String> teamNameListCoach) {
        if (isGymnast) {
            registerGymnast(person, license, teamNameListGymnast);
        }
        if (isCoach) {
            registerCoach(person, teamNameListCoach);
        }
        if (isJudge) {
            registerJudge(person);
        }
    }

    private void registerGymnast(Person person, String license, List<String> teamNameList) {
        TeamMember gymnast = new Gymnast(person, license, true);

        generateTeamAssignments(gymnast, teamNameList);
        person.setRole(gymnast, Person.GYMNAST_INDEX);
    }

    private void registerCoach(Person person, List<String> teamNameList) {
        Coach coach = new Coach(person, true);

        generateTeamAssignments(coach, teamNameList);
        person.setRole(coach, Person.COACH_INDEX);
    }

    private void generateTeamAssignments(TeamMember teamMember, List<String> teamNameList) {  // Generates a 'TeamAssignment' object
        TeamAssignment teamAssignment;                                                        // for each 'Team' object in 'teamList'.
        boolean isOnTeam;

        for (Team t : teamList) {
            isOnTeam = teamNameList.contains(t.getName());                // If 'teamNameList' contains the name of 't',
            // then the 'isSignedUp' field in the 'Team-
            teamAssignment = new TeamAssignment(t, teamMember, isOnTeam); // Assignment' object is set to 'true'.
            t.addTeamAssignment(teamAssignment);
            teamMember.addTeamAssignment(teamAssignment);
        }
    }

    private void registerJudge(Person person) {
        person.setRole(new Judge(person, true), Person.JUDGE_INDEX);
    }

    public void editPerson(int personID, String name, boolean isMale, List<Address> addrList, List<String> emailList,
                           List<String> phoneList, String bDay, String signUpDate, boolean photoConsent,
                           boolean isGymnast, boolean isCoach, boolean isJudge, String license,
                           List<String> teamListGymnast, List<String> teamListCoach) {
        Person person = viewPerson(personID);
        boolean makeGymnastSignups, makeCoachSignups, makeJudgeSignups;

        checkRoleBooleans(isGymnast, isCoach, isJudge);
        checkForNull(teamListGymnast);
        checkForNull(teamListCoach);

        if (!person.getName().equals(name) && personNameAlreadyExist(name)) {
            throw new IllegalObjectException("A person with this name already exists.");
        }

        try {  // Exceptions are thrown from the setters in 'Person'.
            setMandatoryPersonInfo(person, name, isMale, photoConsent);
            setOptionalPersonInfo(person, addrList, emailList, phoneList, bDay, signUpDate);

            makeGymnastSignups = editGymnast(person, isGymnast, license, teamListGymnast);  // The three methods return 'true' if a role is
            makeCoachSignups = editCoach(person, isCoach, teamListCoach);                   // created or has its 'isActive' field changed
            makeJudgeSignups = editJudge(person, isJudge);                                  // from 'false' to 'true.

            generatePersonSignupsForFutureEvents(person, makeGymnastSignups, makeCoachSignups, makeJudgeSignups);
            databaseManager.updatePersonData(person);
        } catch (NullPointerException | IllegalFormatException | IllegalObjectException e){
            databaseManager.restorePersonData(person);  // If an exception is throw, the data of 'person' is restored.
            throw new IllegalObjectException("Person edit failed; " + e.getClass().toString() + ".");
        }
    }

    private boolean personNameAlreadyExist(String name) {
        for (Person p: personList) {
            if (p.getName().equals(name)) {
                return true;
            }
        }
        return false;
    }

    private void setMandatoryPersonInfo(Person person, String name, boolean isMale, boolean photoConsent) {
        person.setName(name)
                .setGender(isMale)
                .setPhotoConsent(photoConsent);
    }

    private boolean editGymnast(Person person, boolean isGymnast, String license, List<String> teamNameList) {
        Gymnast gymnast = (Gymnast) person.getRole(Person.GYMNAST_INDEX);

        if (gymnast != null) { // If 'person' already contains a 'Gymnast' object.
            gymnast.setLicense(license);
            return doTeamMemberEdit(person, isGymnast, teamNameList, gymnast);
        } else if (isGymnast) { // If 'person' does not contain a 'Gymnast' object but should.
            registerGymnast(person, license, teamNameList);
            return true;
        }

        return false;
    }

    private boolean editCoach(Person person, boolean isCoach, List<String> teamNameList) {
        TeamMember coach = (TeamMember) person.getRole(Person.COACH_INDEX);

        if (coach != null) { // If 'person' already contains a 'Coach' object.
            return doTeamMemberEdit(person, isCoach, teamNameList, coach);
        } else if (isCoach) { // If 'person' does not contain a 'Coach' object but should.
            registerCoach(person, teamNameList);
            return true;
        }

        return false;
    }

    private boolean doTeamMemberEdit(Person person, boolean isTeamMember, List<String> teamNames, TeamMember teamMember) {
        if (teamMember.isActive() && !isTeamMember) {  // If the 'isActive' field in 'teamMember' is to be changed from 'true' to 'false'.
            teamMember.setActive(false); // Method 'setActive' calls method to delete all 'TeamAssignment' objects related to 'teamMember'.
            person.deletePersonSignupsToFutureEvents(teamMember);
        } else if (!teamMember.isActive() && isTeamMember) { // If the 'isActive' field in 'teamMember' is to be changed from 'false' to 'true'.
            teamMember.setActive(true);
            generateTeamAssignments(teamMember, teamNames);
            return true;
        } else if (teamMember.isActive() && isTeamMember) { // If the 'isActive' field in 'teamMember' is set to 'true' and will not be changed.
            teamMember.updateTeamAssignments(teamNames); // Updates the 'TeamAssignment' objects in 'teamMember' based on the names in 'teamNames'.
        }

        return false;
    }

    private boolean editJudge(Person person, boolean isJudge) {
        Role judge = person.getRole(Person.JUDGE_INDEX);

        if (judge != null) { // If 'person' already contains a 'Judge' object.
            if (judge.isActive() && !isJudge) { // If the 'isActive' field in 'judge' is to be changed from 'true' to 'false'.
                judge.setActive(false);
                person.deletePersonSignupsToFutureEvents(judge);
            } else if (!judge.isActive() && isJudge) { // If the 'isActive' field in 'judge' is to be changed from 'false' to 'true'.
                judge.setActive(true);
                return true;
            }
        } else if (isJudge) {  // If 'person' doesn't contain a 'Judge' object but should.
            registerJudge(person);
            return true;
        }
        return false;
    }

    private void generatePersonSignupsForFutureEvents(Person person, boolean makeGymnastSignup, boolean makeCoachSignup,
                                                      boolean makeJudgeSignup) {
        LocalDateTime now = LocalDateTime.now();

        for (Season s : seasonList) {
            if (s.getEndDate().isAfter(now)) {
                loopEventsInSeason(person, s.viewEvents(), now, makeGymnastSignup, makeCoachSignup, makeJudgeSignup);
            }
        }
    }

    private void loopEventsInSeason(Person person, List<Event> eventList, LocalDateTime now, boolean makeGymnastSignup,
                                    boolean makeCoachSignup, boolean makeJudgeSignup) {
        for (Event e : eventList) {
            if (e.getStartDate().isAfter(now)) {
                e.generatePersonSignupsFromPerson(person, makeGymnastSignup, makeCoachSignup, makeJudgeSignup);
            }
        }
    }

    public void deletePerson(int personID) {
        Person person = viewPerson(personID);

        personList.remove(person);
        person.delete();
        databaseManager.deleteObjectData("person", "id_person", person.getID());
    }

    // -------------------- Methods related to 'teamList' --------------------
    public List<Team> viewTeams() {
        return Collections.unmodifiableList(teamList);
    }

    public Team viewTeam(int teamID) {
        for (Team t : teamList) {
            if (t.getID() == teamID) {
                return t;
            }
        }
        throw new ObjectNotFoundException("No such team was found in the club.");
    }

    public void createTeam(String teamName) {
        Team team = new Team(teamName);

        if (teamList.contains(team)) {
            throw new IllegalObjectException("A team with name " + teamName + " already exists.");
        }

        generateTeamSignupsForFutureEvents(team);
        team.generateTeamAssignments(personList); // Generates a 'TeamAssignment' object for each 'Gymnast' and 'Coach' object.

        teamList.add(team);
        databaseManager.saveTeamData(team);
    }

    private void generateTeamSignupsForFutureEvents(Team team) {
        LocalDateTime now = LocalDateTime.now();

        for (Season s: seasonList){
            if (s.getEndDate().isAfter(now)){
                loopEventsInSeason(s.viewEvents(), team, now);
            }
        }
    }

    private void loopEventsInSeason(List<Event> eventList, Team team, LocalDateTime now) {
        TeamSignup teamSignup;

        for (Event e: eventList){
            if (e.getStartDate().isAfter(now)) {
                teamSignup = new TeamSignup(team, e, false);

                team.addTeamSignup(teamSignup);
                e.addTeamSignup(teamSignup);
            }
        }
    }

    public void editTeam(int teamID, String name, List<String> gymnastNames, List<String> coachNames) {
        Team team = viewTeam(teamID);

        if (!team.getName().equals(name) && teamNameAlreadyExists(name)) {
            throw new IllegalObjectException("A team with name " + name + " already exists.");
        }

        team.setName(name);
        team.updateTeamAssignments(gymnastNames, coachNames); // Updates the 'TeamAssignment' objects in 'team' based on
    }                                                         // the names in 'gymnastNames' and 'coachNames'.

    private boolean teamNameAlreadyExists(String teamName) {
        for (Team t : teamList) {
            if (t.getName().equals(teamName)) {
                return true;
            }
        }
        return false;
    }

    public void deleteTeam(int teamID) {
        Team team = viewTeam(teamID);

        teamList.remove(team);
        team.delete();
    }

    // -------------------- Static methods --------------------
    static void checkForNull(Object object) {
        if (object == null) {
            throw new NullPointerException();
        }
    }

    static void checkID(int id) {
        if (id <= 0) {
            throw new IllegalIDException("The id is illegal.");
        }
    }
}

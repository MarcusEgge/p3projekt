package Model;

import Model.Exceptions.IllegalFormatException;
import Model.Exceptions.IllegalObjectException;
import Model.Exceptions.ObjectNotFoundException;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;

public class Person {
    public final static int GYMNAST_INDEX = 0, COACH_INDEX = 1, JUDGE_INDEX = 2;
    private final int id;
    private String name;
    private boolean isMale;
    private LocalDate birthday, registrationDate, signupDate;
    private List<Address> addressList;
    private List<String> emailList, phoneNumberList;
    private List<Role> roleList;
    private transient List<PersonSignup> personSignupList;
    private List<CarAssignment> carAssignmentList;
    private boolean photoConsent;

    Person(int id, String name, boolean isMale, LocalDate registrationDate, boolean photoConsent) {
        Club.checkID(id);
        this.id = id;
        setName(name);
        this.isMale = isMale;
        this.birthday = null;
        this.registrationDate = registrationDate;
        this.signupDate = null;
        setAddressList(new ArrayList<>(2));
        setEmailList(new ArrayList<>(2));
        setPhoneNumberList(new ArrayList<>(2));
        this.roleList = new ArrayList<>(3);
        fillRoleList();
        this.personSignupList = new ArrayList<>();
        this.carAssignmentList = new ArrayList<>();
        this.photoConsent = photoConsent;
    }

    Person(String name, boolean isMale, LocalDate registrationDate, boolean photoConsent) {
        this.id = Club.INSTANCE.getDatabaseManager().getNextID("person", "id_person");
        setName(name);
        this.isMale = isMale;
        this.birthday = null;
        this.registrationDate = registrationDate;
        this.signupDate = null;
        setAddressList(new ArrayList<>(2));
        setEmailList(new ArrayList<>(2));
        setPhoneNumberList(new ArrayList<>(2));
        this.roleList = new ArrayList<>(3);
        fillRoleList();
        this.personSignupList = new ArrayList<>();
        this.carAssignmentList = new ArrayList<>();
        this.photoConsent = photoConsent;
    }

    private void fillRoleList() {
        for (int i = 0; i < 3; i++) {
            roleList.add(null);
        }
    }

    // -------------------- Simple getters and setters --------------------
    int getID() {
        return id;
    }

    String getName() {
        return name;
    }

    Person setName(String name) {
        Club.checkForNull(name);
        if (name.isEmpty()) {
            throw new IllegalFormatException("The name of a person cannot be an empty string.");
        }
        this.name = name;
        return this;
    }

    boolean isMale() {
        return isMale;
    }

    Person setGender(boolean isMale) {
        this.isMale = isMale;
        return this;
    }

    LocalDate getBirthday() {
        return birthday;
    }

    Person setBirthday(LocalDate birthday) {
        this.birthday = birthday;
        return this;
    }

    LocalDate getRegistrationDate() {
        return registrationDate;
    }

    LocalDate getSignupDate() {
        return signupDate;
    }

    Person setSignupDate(LocalDate signupDate) {
        this.signupDate = signupDate;
        return this;
    }

    List<Address> getAddressList() {
        return Collections.unmodifiableList(addressList);
    }

    Person setAddressList(List<Address> addressList) {
        Club.checkForNull(addressList);
        if (addressList.size() > 2) {
            throw new IllegalObjectException("List of Address objects is too big.");
        }
        while (addressList.size() < 2) {
            addressList.add(null);
        }
        this.addressList = addressList;
        return this;
    }

    List<String> getEmailList() {
        return Collections.unmodifiableList(emailList);
    }

    Person setEmailList(List<String> emailList) {
        Club.checkForNull(emailList);
        if (emailList.size() > 2) {
            throw new IllegalObjectException("List of emails is too big.");
        }
        while (emailList.size() < 2) {
            emailList.add(null);
        }
        this.emailList = emailList;
        return this;
    }

    List<String> getPhoneNumberList() {
        return Collections.unmodifiableList(phoneNumberList);
    }

    Person setPhoneNumberList(List<String> phoneNumberList) {
        Club.checkForNull(phoneNumberList);
        if (phoneNumberList.size() > 2) {
            throw new IllegalObjectException("List of phone numbers is too big.");
        }
        while (phoneNumberList.size() < 2) {
            phoneNumberList.add(null);
        }
        this.phoneNumberList = phoneNumberList;
        return this;
    }

    boolean getPhotoConsent() {
        return photoConsent;
    }

    Person setPhotoConsent(boolean photoConsent) {
        this.photoConsent = photoConsent;

        return this;
    }

    // -------------------- Methods related to 'roleList' --------------------
    Role getRole(int index) {
        if (indexIsInvalid(index)) {
            throw new IndexOutOfBoundsException("The index of the Role must not be lower than 0 or higher than 2.");
        }
        return roleList.get(index);
    }

    void setRole(Role role, int index) {
        if (indexIsInvalid(index)) {
            throw new IndexOutOfBoundsException("There is no such index in the role list.");
        }
        if ((role != null && (indexAndRoleDoNotMatch(role, index) || this != role.getPerson()))
                || willReplaceExistingRole(index)) {
            throw new IllegalObjectException("Index does not match the Role object or the person already has such a Role object");
        }
        roleList.set(index, role);
    }

    private boolean indexIsInvalid(int index) {
        return index != GYMNAST_INDEX && index != COACH_INDEX && index != JUDGE_INDEX;
    }

    private boolean indexAndRoleDoNotMatch(Role role, int index) {
        return (role.isGymnast() && index != GYMNAST_INDEX) || (role.isCoach() && index != COACH_INDEX)
                || (role.isJudge() && index != JUDGE_INDEX);
    }

    private boolean willReplaceExistingRole(int index) {
        return roleList.get(index) != null;
    }

    // -------------------- Methods related to 'personSignupList' --------------------
    List<PersonSignup> getPersonSignupList() {
        return Collections.unmodifiableList(personSignupList);
    }

    void getPersonSignupsSavedAndNotSaved(List<PersonSignup> saved, List<PersonSignup> notSaved) {
        Club.checkForNull(saved);
        Club.checkForNull(notSaved);

        for (PersonSignup p: personSignupList) {
            if (p.getID() == 0){
                notSaved.add(p);
            } else {
                saved.add(p);
            }
        }
    }

    void addPersonSignup(PersonSignup personSignup) {
        Club.checkForNull(personSignup);
        if (personSignup.getPerson() != this || personSignupList.contains(personSignup)) {
            throw new IllegalObjectException("Field person in the PersonSignup object did not match the Person object or Person object already has such a PersonSignup object.");
        }
        personSignupList.add(personSignup);
    }

    void deletePersonSignupsToFutureEvents(Role role) {
        LocalDateTime now = LocalDateTime.now();
        PersonSignup personSignup;

        Club.checkForNull(role);

        for (int i = personSignupList.size() - 1; i >= 0; i--) {
            personSignup = personSignupList.get(i);
            if (personSignup.getEvent().getStartDate().isAfter(now) && role == personSignup.getRole()) {
                personSignup.deleteFromEvent();
                personSignup.deleteFromTeamSignups();
                personSignup.deleteFromPerson();
            }
        }
    }

    void deletePersonSignup(PersonSignup personSignup) {
        if (!personSignupList.remove(personSignup)) {
            throw new ObjectNotFoundException("The PersonSignup object was not found in the Person object.");
        }
    }

    // -------------------- Methods related to 'carAssignmentList' --------------------
    List<CarAssignment> getCarAssignmentList() {
        return Collections.unmodifiableList(carAssignmentList);
    }

    void addCarAssignment(CarAssignment carAssignment) {
        Club.checkForNull(carAssignment);
        if (this != carAssignment.getPerson() || carAssignmentList.contains(carAssignment)) {
            throw new IllegalObjectException("The CarAssignment object cannot be added to the list in the Person object.");
        }
        this.carAssignmentList.add(carAssignment);
    }

    void deleteCarAssignment(CarAssignment carAssignment) {
        if (!carAssignmentList.remove(carAssignment)) {
            throw new ObjectNotFoundException("The CarAssignment object was not found in the Person object.");
        }
    }

    // -------------------- Other methods --------------------
    boolean isGymnast() {
        return roleList.get(GYMNAST_INDEX) != null && roleList.get(GYMNAST_INDEX).isActive();
    }

    boolean isCoach() {
        return roleList.get(COACH_INDEX) != null && roleList.get(COACH_INDEX).isActive();
    }

    boolean isJudge() {
        return roleList.get(JUDGE_INDEX) != null && roleList.get(JUDGE_INDEX).isActive();
    }

    void delete() {
        List<TeamAssignment> teamAssignmentList = getAllTeamAssignments();

        for (PersonSignup p : personSignupList) {
            p.deleteFromEvent();
            p.deleteFromTeamSignups();
        }

        for (TeamAssignment t : teamAssignmentList) {
            t.deleteFromTeam();
        }

        for (CarAssignment c : carAssignmentList) {
            c.deleteFromCar();
        }
    }

    private List<TeamAssignment> getAllTeamAssignments() {
        List<TeamAssignment> allTeamAssignments = new ArrayList<>();
        Gymnast gymnast = (Gymnast) roleList.get(GYMNAST_INDEX);
        Coach coach = (Coach) roleList.get(COACH_INDEX);

        if (gymnast != null) {
            allTeamAssignments.addAll(gymnast.getTeamAssignmentList());
        }
        if (coach != null) {
            allTeamAssignments.addAll(coach.getTeamAssignmentList());
        }

        return allTeamAssignments;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null || this.getClass() != that.getClass()) {
            return false;
        }
        Person person = (Person) that;
        return Objects.equals(this.name, person.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.name);
    }
}
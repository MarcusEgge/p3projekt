package Model;

import Model.Exceptions.IllegalIDException;
import Model.Exceptions.IllegalObjectException;
import Model.Exceptions.ObjectNotFoundException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

public class TeamSignup {
    private int id;
    private Team team;
    private Event event;
    private List<PersonSignup> personSignupList;
    private boolean isSignedUp;

    public TeamSignup(int id, Team team, Event event, boolean isSignedUp) {
        checkArgumentsForNull(team, event);

        setID(id);
        this.team = team;
        this.event = event;
        this.personSignupList = new ArrayList<>();
        this.isSignedUp = isSignedUp;
    }

    public TeamSignup(Team team, Event event, boolean isSignedUp) {
        checkArgumentsForNull(team, event);

        this.id = 0;
        this.team = team;
        this.event = event;
        this.personSignupList = new ArrayList<>();
        this.isSignedUp = isSignedUp;
    }

    private void checkArgumentsForNull(Team team, Event event) {
        Club.checkForNull(team);
        Club.checkForNull(event);
    }

    // -------------------- Simple getters and setters --------------------
    public int getID() {
        return id;
    }

    public void setID(int id) {
        if (this.id != 0 || id <= 0) {
            throw new IllegalIDException("The TeamSignup id is illegal.");
        }
        this.id = id;
    }

    public Team getTeam() {
        return team;
    }

    public Event getEvent() {
        return event;
    }

    public boolean isSignedUp() {
        return isSignedUp;
    }

    void setSignedUp(boolean signedUp) {
        if (isSignedUp != signedUp) {
            isSignedUp = signedUp;

            for (PersonSignup p: personSignupList) {
                p.setState(isSignedUp ? PersonSignupState.SIGNED_UP : PersonSignupState.NOT_SIGNED_UP);
            }
        }
    }

    // -------------------- Methods related to 'personSignupList' --------------------
    public List<PersonSignup> getPersonSignupList() {
        return Collections.unmodifiableList(personSignupList);
    }

    void addPersonSignup(PersonSignup personSignup) {
        Club.checkForNull(personSignup);
        if (this.event != personSignup.getEvent() || !personSignup.getRole().isGymnast() || personSignupList.contains(personSignup)) {
            throw new IllegalObjectException("The PersonSignup object cannot be added to the list in the TeamSignup object.");
        }
        personSignupList.add(personSignup);
    }

    void deletePersonSignup(PersonSignup personSignup) {
        if (!personSignupList.remove(personSignup)) {
            throw new ObjectNotFoundException("The PersonSignup object was not found in the TeamSignup object.");
        }
    }

    // -------------------- Methods for deleting 'this' --------------------
    void deleteFromEvent() {
        event.deleteTeamSignup(this);
    }

    void deleteFromTeam() {
        team.deleteTeamSignup(this);
    }

    void deleteFromPersonSignups() {
        for (PersonSignup p : personSignupList) {
            p.deleteTeamSignup(this);
        }
    }

    // -------------------- Other methods --------------------
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TeamSignup that = (TeamSignup) o;
        return Objects.equals(team, that.team) &&
                Objects.equals(event, that.event);
    }

    @Override
    public int hashCode() {
        return Objects.hash(team, event);
    }
}

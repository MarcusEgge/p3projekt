package Model;

import Model.Exceptions.IllegalFormatException;
import Model.Exceptions.IllegalObjectException;
import Model.Exceptions.ObjectNotFoundException;

import java.time.LocalDateTime;
import java.util.*;

public class Team implements Comparable<Team> {
    private final int id;
    private String name;
    private List<TeamSignup> teamSignupList;
    private List<TeamAssignment> teamAssignmentList;

    public Team(int id, String name) {
        checkName(name);
        Club.checkID(id);
        this.id = id;
        this.name = name;
        this.teamSignupList = new ArrayList<>();
        this.teamAssignmentList = new ArrayList<>();
    }

    public Team(String name) {
        checkName(name);

        this.id = Club.INSTANCE.getDatabaseManager().getNextID("team", "id_team");
        this.name = name;
        this.teamSignupList = new ArrayList<>();
        this.teamAssignmentList = new ArrayList<>();
    }

    private void checkName(String name) {
        Club.checkForNull(name);
        if (name.isEmpty()) {
            throw new IllegalFormatException("Name of Team object cannot be empty string.");
        }
    }

    // -------------------- Simple getters and setters --------------------
    public int getID() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        checkName(name);

        this.name = name;
    }

    // -------------------- Methods related to 'teamSignupList' --------------------
    public List<TeamSignup> getTeamSignupList() {
        return Collections.unmodifiableList(teamSignupList);
    }

    public void addTeamSignup(TeamSignup teamSignup) {
        Club.checkForNull(teamSignup);
        if (this != teamSignup.getTeam() || teamSignupList.contains(teamSignup)) {
            throw new IllegalObjectException("The TeamSignup object cannot be added to the list in the Team object.");
        }
        teamSignupList.add(teamSignup);
    }

    void deleteTeamSignup(TeamSignup teamSignup) {
        if (!teamSignupList.remove(teamSignup)) {
            throw new ObjectNotFoundException("The TeamSignup object was not found in the Team object.");
        }
    }

    void deleteGymnastFromFutureEvents(TeamMember gymnast) {
        LocalDateTime now = LocalDateTime.now();
        Event event;
        Club.checkForNull(gymnast);

        for (TeamSignup t : teamSignupList) {
            event = t.getEvent();
            if (event.getStartDate().isAfter(now)) {
                deleteGymnast(gymnast, t, event);
            }
        }
    }

    private void deleteGymnast(TeamMember gymnast, TeamSignup teamSignup, Event event) {
        PersonSignup personSignup, temp;
        List<PersonSignup> personSignupList = teamSignup.getPersonSignupList();
        int index;

        temp = new PersonSignup(gymnast, event);
        index = personSignupList.indexOf(temp);

        if (index == -1) { // If 'personSignupList' does not contain 'temp'.
            throw new ObjectNotFoundException("Gymnast has no PersonSignup object in the TeamSignup.");
        }

        personSignup = personSignupList.get(index); // Getting the real object ('temp' is a copy).

        personSignup.deleteTeamSignup(teamSignup);
        teamSignup.deletePersonSignup(personSignup);
    }

    void addGymnastToFutureEvents(TeamMember gymnast) {
        LocalDateTime now = LocalDateTime.now();
        Event event;
        Club.checkForNull(gymnast);

        for (TeamSignup t : teamSignupList) {
            event = t.getEvent();
            if (event.getStartDate().isAfter(now)) {
                addToEvent(event, gymnast, t);
            }
        }
    }

    private void addToEvent(Event event, TeamMember gymnast, TeamSignup teamSignup) {
        PersonSignup personSignup;
        List<PersonSignup> eventPersonSignups = event.getPersonSignupList();
        int index;

        personSignup = new PersonSignup(gymnast, event);
        index = eventPersonSignups.indexOf(personSignup);

        if (index != -1) { // If 'event' already contains a 'PersonSignup' object with 'gymnast' as its 'role' field.
            personSignup = eventPersonSignups.get(index);
        } else {
            event.addPersonSignup(personSignup);
        }
        personSignup.addTeamSignup(teamSignup);
        teamSignup.addPersonSignup(personSignup);
    }

    // -------------------- Methods related to 'teamAssignmentList' --------------------
    public List<TeamAssignment> getTeamAssignmentList() {
        return Collections.unmodifiableList(teamAssignmentList);
    }

    List<Person> getGymnastsInTeam() {
        List<Person> gymnastList = new ArrayList<>();

        for (TeamAssignment t : teamAssignmentList) {
            if (t.isSignedUp() && t.getTeamMember().isGymnast()) {
                gymnastList.add(t.getTeamMember().getPerson());
            }
        }
        return gymnastList;
    }

    List<Person> getCoachesInTeam() {
        List<Person> coachList = new ArrayList<>();

        for (TeamAssignment t : teamAssignmentList) {
            if (t.isSignedUp() && t.getTeamMember().isCoach()) {
                coachList.add(t.getTeamMember().getPerson());
            }
        }
        return coachList;
    }

    public void addTeamAssignment(TeamAssignment teamAssignment) {
        Club.checkForNull(teamAssignment);
        if (this != teamAssignment.getTeam() || teamAssignmentList.contains(teamAssignment)) {
            throw new IllegalObjectException("The TeamAssignment object cannot be added to the list in the Team object.");
        }
        teamAssignmentList.add(teamAssignment);
    }

    public void generateTeamAssignments(List<Person> personList) {
        Club.checkForNull(personList);
        for (Person p : personList) {
            if (p.isGymnast()) {
                makeTeamAssignment((TeamMember) p.getRole(Person.GYMNAST_INDEX));
            }
            if (p.isCoach()) {
                makeTeamAssignment((TeamMember) p.getRole(Person.COACH_INDEX));
            }
        }
    }

    private void makeTeamAssignment(TeamMember teamMember) {
        TeamAssignment teamAssignment = new TeamAssignment(this, teamMember, false);

        teamMember.addTeamAssignment(teamAssignment);
        teamAssignmentList.add(teamAssignment);
    }

    void updateTeamAssignments(List<String> gymnastNames, List<String> coachNames) {
        TeamMember teamMember;
        boolean isSignedUp;

        Club.checkForNull(gymnastNames);
        Club.checkForNull(coachNames);

        for (TeamAssignment t : teamAssignmentList) {
            teamMember = t.getTeamMember();
            if (teamMember.isGymnast()) {
                isSignedUp = gymnastNames.contains(teamMember.getPerson().getName());
                t.setSignedUp(isSignedUp); // Method 'setSignedUp' calls method to delete or add 'teamMember' to 'TeamSignup' objects related to future events.
            }
            if (teamMember.isCoach()) {
                isSignedUp = coachNames.contains(teamMember.getPerson().getName());
                t.setSignedUp(isSignedUp);
            }
        }
    }

    void deleteTeamAssignment(TeamAssignment teamAssignment) {
        if (!teamAssignmentList.remove(teamAssignment)) {
            throw new ObjectNotFoundException("The TeamAssignment object was not found in the Team object.");
        }
    }

    // -------------------- Other methods --------------------
    void delete() {
        for (TeamAssignment ta : teamAssignmentList) {
            ta.deleteFromRole();
        }

        for (TeamSignup ts : teamSignupList) {
            ts.deleteFromEvent();
            ts.deleteFromPersonSignups();
        }
    }

    @Override
    public String toString() {
        return "Team{" +
                "id=" + id +
                '}';
    }

    @Override
    public int compareTo(Team that) {
        return this.name.compareTo(that.name);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Team team = (Team) o;
        return Objects.equals(name, team.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }
}

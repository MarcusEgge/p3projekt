package Model.Exceptions;

public class IllegalObjectException extends RuntimeException {
    public IllegalObjectException(String message) {
        super(message);
    }
}

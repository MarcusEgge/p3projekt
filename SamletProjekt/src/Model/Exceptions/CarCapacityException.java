package Model.Exceptions;

public class CarCapacityException extends RuntimeException{
    public CarCapacityException(String message) {
        super(message);
    }
}

package Model.Exceptions;

public class IllegalFormatException extends RuntimeException{
    public IllegalFormatException(String message) {
        super(message);
    }
}

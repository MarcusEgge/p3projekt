package Model.Exceptions;

public class DatabaseErrorException extends RuntimeException{
    public DatabaseErrorException(String message, Throwable cause) {
        super(message, cause);
    }
}

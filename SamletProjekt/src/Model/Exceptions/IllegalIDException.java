package Model.Exceptions;

public class IllegalIDException extends RuntimeException{
    public IllegalIDException(String message) {
        super(message);
    }
}

package Model;

import Model.Exceptions.IllegalIDException;
import Model.Exceptions.IllegalObjectException;
import Model.Exceptions.ObjectNotFoundException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

public class PersonSignup {
    private int id;
    private  Person person;
    private Role role;
    private transient Event event;
    private List<TeamSignup> teamSignupList;
    private PersonSignupState state;

    PersonSignup(int id, Role role, Event event, PersonSignupState state) {
        checkArguments(role, event, state);

        setID(id);
        this.person = role.getPerson();
        this.role = role;
        this.event = event;
        this.teamSignupList = new ArrayList<>();
        this.state = state;
    }

    PersonSignup(Role role, Event event) {
        Club.checkForNull(role);
        Club.checkForNull(event);

        this.id = 0;
        this.person = role.getPerson();
        this.role = role;
        this.event = event;
        this.teamSignupList = new ArrayList<>();
        this.state = role.isGymnast() ? PersonSignupState.NOT_SIGNED_UP : PersonSignupState.UNDECIDED;
    }

    private void checkArguments(Role role, Event event, PersonSignupState state) {
        Club.checkForNull(role);
        Club.checkForNull(event);
        Club.checkForNull(state);

        if (role.isGymnast() && state.equals(PersonSignupState.UNDECIDED)) {
            throw new IllegalObjectException("The state of a PersonSignup object with a Gymnast object for field 'role' cannot be undecided");
        }
    }

    int getID() {
        return id;
    }

    void setID(int id) {
        if (this.id != 0 || id <= 0) {
            throw new IllegalIDException("The PersonSignup id is illegal.");
        }
        this.id = id;
    }

    Person getPerson() {
        return person;
    }

    Role getRole() {
        return role;
    }

    Event getEvent() {
        return event;
    }

    List<TeamSignup> getTeamSignupList() {
        return Collections.unmodifiableList(teamSignupList);
    }

    void addTeamSignup(TeamSignup teamSignup){
        Club.checkForNull(teamSignup);
        if (this.event != teamSignup.getEvent() || !role.isGymnast() || teamSignupList.contains(teamSignup)) {
            throw new IllegalObjectException("The TeamSignup object cannot be added to the list in the PersonSignup object.");
        }
        teamSignupList.add(teamSignup);
    }

    boolean hasTeamSignup(int teamSignupID) {
        for (TeamSignup t: teamSignupList) {
            if (t.getID() == teamSignupID) {
                return true;
            }
        }
        return false;
    }

    void deleteTeamSignup(TeamSignup teamSignup) {
        if(!teamSignupList.remove(teamSignup)){
            throw new ObjectNotFoundException("The TeamSignup object was not found in the PersonSignup object.");
        }
    }

    PersonSignupState getState() {
        return state;
    }

    void setState(PersonSignupState state) {
        if (role.isGymnast() && state.equals(PersonSignupState.UNDECIDED)) {
            throw new IllegalObjectException("A PersonSignup with a Gymnast object for role cannot be undecided.");
        }
        this.state = state;
    }

    void deleteFromEvent(){
        event.deletePersonSignup(this);
    }

    void deleteFromTeamSignups(){
        for (TeamSignup t: teamSignupList) {
            t.deletePersonSignup(this);
        }
    }

    void deleteFromPerson(){
        person.deletePersonSignup(this);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PersonSignup that = (PersonSignup) o;
        return Objects.equals(person, that.person) &&
                Objects.equals(role, that.role) &&
                Objects.equals(event, that.event);
    }

    @Override
    public int hashCode() {
        return Objects.hash(person, role, event);
    }
}

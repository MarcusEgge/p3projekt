package Model;

import Model.Exceptions.IllegalObjectException;
import Model.Exceptions.ObjectNotFoundException;

import java.util.*;

public class Coach implements TeamMember {
    private final int id;
    private transient Person person;
    private List<TeamAssignment> teamAssignmentList;
    private boolean isActive;

    Coach(int id, Person person, boolean isActive) {
        Club.checkForNull(person);
        Club.checkID(id);
        this.id = id;
        this.person = person;
        this.teamAssignmentList = new ArrayList<>();
        this.isActive = isActive;
    }

    Coach(Person person, boolean isActive) {
        Club.checkForNull(person);

        this.id = Club.INSTANCE.getDatabaseManager().getNextID("coach", "id_coach");
        this.person = person;
        this.teamAssignmentList = new ArrayList<>();
        this.isActive = isActive;
    }

    // -------------------- Methods from the 'Role' interface --------------------
    @Override
    public int getID() {
        return id;
    }

    @Override
    public Person getPerson() {
        return person;
    }

    @Override
    public boolean isActive() {
        return isActive;
    }

    @Override
    public void setActive(boolean active) {
        if (isActive && !active) {
            deleteAllTeamAssignments();
        }
        isActive = active;
    }

    private void deleteAllTeamAssignments() {
        for (int i = teamAssignmentList.size() - 1; i >= 0; i--){
            teamAssignmentList.remove(i).deleteFromTeam();
        }
    }

    @Override
    public boolean isGymnast() {
        return false;
    }

    @Override
    public boolean isCoach() {
        return true;
    }

    @Override
    public boolean isJudge() {
        return false;
    }

    // -------------------- Methods from the 'TeamMember' interface --------------------
    @Override
    public List<TeamAssignment> getTeamAssignmentList(){
        return Collections.unmodifiableList(teamAssignmentList);
    }

    @Override
    public void getTeamAssignmentsSavedAndNotSaved(List<TeamAssignment> saved, List<TeamAssignment> notSaved) {
        Club.checkForNull(saved);
        Club.checkForNull(notSaved);

        for (TeamAssignment t: teamAssignmentList) {
            if (t.getID() == 0) {  // If the 'id' field in 't' equals 0, then 't' has not been saved to the database.
                notSaved.add(t);
            } else {
                saved.add(t);
            }
        }
    }

    @Override
    public void addTeamAssignment(TeamAssignment teamAssignment) {
        Club.checkForNull(teamAssignment);
        if (this != teamAssignment.getTeamMember() || teamAssignmentList.contains(teamAssignment)) {
            throw new IllegalObjectException("The TeamAssignment object cannot be added to the list in the Coach object.");
        }
        teamAssignmentList.add(teamAssignment);
    }

    @Override
    public void updateTeamAssignments(List<String> teamNameList) {
        for (TeamAssignment t: teamAssignmentList) {
            t.setSignedUp(teamNameList.contains(t.getTeam().getName()));
        }
    }

    @Override
    public void deleteTeamAssignment(TeamAssignment teamAssignment) {
        if (!teamAssignmentList.remove(teamAssignment)) {
            throw new ObjectNotFoundException("The TeamAssignment object was not found in the Coach object.");
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Coach coach = (Coach) o;
        return Objects.equals(person, coach.person);
    }

    @Override
    public int hashCode() {
        return Objects.hash(person);
    }
}

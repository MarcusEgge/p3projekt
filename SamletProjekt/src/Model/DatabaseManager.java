package Model;

import Model.Exceptions.DatabaseErrorException;
import Model.Exceptions.IllegalIDException;
import Model.Exceptions.IllegalObjectException;

import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

class DatabaseManager {
    private Calendar calendar;

    DatabaseManager() {
        this.calendar = Calendar.getInstance();
    }

    private Connection connectToDatabase() throws SQLException {
        String url = "jdbc:mysql://localhost:3306/club?serverTimezone=UTC";
        String user = "root";
        String password = "password";
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return DriverManager.getConnection(url, user, password);
    }

    // -------------------- Methods for loading data from the database --------------------
    void loadAllData(List<Season> seasonList, List<Person> personList, List<Team> teamList) {
        List<Event> eventList = new ArrayList<>();

        try (Connection conn = connectToDatabase()) {
            loadTeams(conn, teamList); // Loads data from the table named 'team'.

            loadSeasons(conn, seasonList, eventList); // Loads data from the tables named 'season', 'event' and 'car'.

            loadPersons(conn, personList, eventList, teamList); // Loads data from the tables named 'person', 'gymnast', 'coach', 'judge', 'team_assignment', 'person_signup' and 'car_assignment'.

            loadTeamSignups(conn, eventList, teamList); // Loads data from the tables named 'team_signup' and 'signup_indexes'.
        } catch (SQLException e) {
            throw new DatabaseErrorException("Error when loading data from database.", e);
        }
    }

    private void loadTeams(Connection conn, List<Team> teamList) throws SQLException {
        String query = "select * from team";  // Selects all rows and columns from the table 'team'.
        try (Statement stmnt = conn.createStatement(); ResultSet resSet = stmnt.executeQuery(query)) {
            while (resSet.next()) {
                teamList.add(new Team(resSet.getInt("id_team"), resSet.getString("name")));
            }
        } catch (SQLException e) {
            throw e;
        }
    }

    private void loadSeasons(Connection conn, List<Season> seasonList, List<Event> eventList) throws SQLException {
        Season season;
        String query = "SELECT * FROM season"; // Selects all rows and columns from the table 'season'.

        try (Statement stmnt = conn.createStatement(); ResultSet resSet = stmnt.executeQuery(query)) {
            while (resSet.next()) {
                season = loadSeason(resSet);
                loadEvents(conn, season, eventList);
                seasonList.add(season);
            }
        } catch (SQLException e) {
            throw e;
        }
    }

    private Season loadSeason(ResultSet resSet) throws SQLException {
        return new Season(resSet.getInt("id_season")
                , resSet.getTimestamp("start_date", calendar).toLocalDateTime()
                , resSet.getTimestamp("end_date", calendar).toLocalDateTime()
                , resSet.getBoolean("is_active"));
    }

    private void loadEvents(Connection conn, Season season, List<Event> eventList) throws SQLException {
        Event event;
        String query = "SELECT * FROM event WHERE season_id = " + season.getID(); // Selects all columns from the rows in table 'event' where the value
                                                                                  // in the column named 'season_id' equals the 'id' field of 'season'.
        try (Statement stmnt = conn.createStatement(); ResultSet resSet = stmnt.executeQuery(query)) {
            while (resSet.next()) {
                event = loadEvent(resSet, season);
                loadCars(conn, event);
                eventList.add(event);
                season.addEvent(event);
            }
        } catch (SQLException e) {
            throw e;
        }
    }

    private Event loadEvent(ResultSet resSet, Season season) throws SQLException {
        return new Event(resSet.getInt("id_event")
                , resSet.getString("name")
                , new Address(resSet.getString("address"))
                , resSet.getTimestamp("start_date", calendar).toLocalDateTime()
                , resSet.getDate("end_date").toLocalDate()
                , season
                , resSet.getInt("coach_need")
                , resSet.getInt("judge_need")
                , resSet.getBoolean("is_active"));
    }

    private void loadCars(Connection conn, Event event) throws SQLException {
        Car car;
        String query = "SELECT * FROM car WHERE event_id = " + event.getID(); // Selects all columns from the rows in table 'car' where the value
                                                                              // in the column named 'event_id' equals the 'id' field of 'event'.
        try (Statement stmnt = conn.createStatement(); ResultSet resSet = stmnt.executeQuery(query)) {
            while (resSet.next()) {
                car = loadCar(resSet, event);
                event.addCar(car);
            }

        } catch (SQLException e) {
            throw e;
        }
    }

    private Car loadCar(ResultSet resSet, Event event) throws SQLException {
        return new Car(resSet.getInt("car.id_car"), event);
    }

    private void loadPersons(Connection conn, List<Person> personList, List<Event> eventList, List<Team> teamList) throws SQLException {
        Person person;
        String query = "SELECT * FROM person "                 // Selects all columns from all rows of table 'person'
                + "LEFT JOIN gymnast "                         // while joining all columns from rows of tables 'gym-
                + "ON person.gymnast_id = gymnast.id_gymnast " // nast', 'coach' and 'judge' where the primary key
                + "LEFT JOIN coach "                           // corresponds to the values of the columns 'gymnast_id',
                + "ON person.coach_id = coach.id_coach "       // 'coach_id' and 'id_judge' in table 'person'.
                + "LEFT JOIN judge "
                + "ON person.judge_id = judge.id_judge";

        try (Statement stmnt = conn.createStatement(); ResultSet resSet = stmnt.executeQuery(query)) {
            while (resSet.next()) {
                person = loadPerson(resSet);
                loadRoles(conn, resSet, person, teamList);
                loadPersonSignups(conn, person, eventList);
                loadCarAssignments(conn, person, eventList);
                personList.add(person);
            }
        } catch (SQLException e) {
            throw e;
        }
    }

    private Person loadPerson(ResultSet resSet) throws SQLException {
        Person person = new Person(resSet.getInt("id_person"), resSet.getString("name")
                , resSet.getBoolean("is_male"), resSet.getDate("reg_date").toLocalDate()
                , resSet.getBoolean("photo_consent"));

        loadOptionalFields(person, resSet);

        return person;
    }

    private void loadOptionalFields(Person person, ResultSet resSet) throws SQLException {
        List<Address> addressList = new ArrayList<>();
        List<String> emailList = new ArrayList<>(), phoneNumberList = new ArrayList<>();
        Date bDay = resSet.getDate("birthday"), signupDate = resSet.getDate("signup_date");
        loadDataForLists(resSet, addressList, emailList, phoneNumberList);

        person.setAddressList(addressList)
                .setEmailList(emailList)
                .setPhoneNumberList(phoneNumberList)
                .setBirthday(bDay == null ? null : bDay.toLocalDate())
                .setSignupDate(signupDate == null ? null : signupDate.toLocalDate());
    }

    private void loadDataForLists(ResultSet resSet, List<Address> addressList, List<String> emailList, List<String> phoneNumberList) throws SQLException {
        addressList.add(resSet.getString("address1") == null ? null : new Address(resSet.getString("address1")));
        addressList.add(resSet.getString("address2") == null ? null : new Address(resSet.getString("address2")));

        emailList.add(resSet.getString("email1"));
        emailList.add(resSet.getString("email2"));

        phoneNumberList.add(resSet.getString("phone_number1"));
        phoneNumberList.add(resSet.getString("phone_number2"));
    }

    private void loadRoles(Connection conn, ResultSet resSet, Person person, List<Team> teamList) throws SQLException {
        person.setRole(loadRole(conn, resSet, "gymnast", person, teamList), Person.GYMNAST_INDEX);
        person.setRole(loadRole(conn, resSet, "coach", person, teamList), Person.COACH_INDEX);
        person.setRole(loadRole(conn, resSet, "judge", person, teamList), Person.JUDGE_INDEX);

    }

    private Role loadRole(Connection conn, ResultSet resSet, String roleType, Person person, List<Team> teamList) throws SQLException {
        int id = resSet.getInt("person." + roleType + "_id");
        Role role = null;

        if (id != 0) { // If 'id' equals 0, 'person' does not have a 'Role' object of this type.
            if ("gymnast".equals(roleType)) {
                role = new Gymnast(id, person, resSet.getString("license"), resSet.getBoolean("gymnast.is_active"));
                loadTeamAssignments(conn, (TeamMember) role, roleType, teamList);
            } else if ("coach".equals(roleType)) {
                role = new Coach(id, person, resSet.getBoolean("coach.is_active"));
                loadTeamAssignments(conn, (TeamMember) role, roleType, teamList);
            } else {
                role = new Judge(id, person, resSet.getBoolean("judge.is_active"));
            }
        }

        return role;
    }

    private void loadTeamAssignments(Connection conn, TeamMember teamMember, String roleType, List<Team> teamList) throws SQLException {
        TeamAssignment teamAssignment;
        String query = "SELECT * FROM team_assignment WHERE " + roleType + "_id = " + teamMember.getID(); // Selects all columns from table 'team_assignment' where either
                                                                                                          // 'gymnast_id' or 'coach_id' equals the 'id' field of 'teamMember'.
        try (Statement stmnt = conn.createStatement(); ResultSet resSet = stmnt.executeQuery(query)) {
            while (resSet.next()) {
                teamAssignment = loadTeamAssignment(resSet, teamMember, teamList);

                teamMember.addTeamAssignment(teamAssignment);
                teamAssignment.getTeam().addTeamAssignment(teamAssignment);
            }
        } catch (SQLException e) {
            throw e;
        }
    }

    private TeamAssignment loadTeamAssignment(ResultSet resSet, TeamMember teamMember, List<Team> teamList) throws SQLException {
        Team team = getTeamFromList(resSet.getInt("team_id"), teamList);


        return new TeamAssignment(resSet.getInt("id_teamassignment"), team, teamMember, resSet.getBoolean("is_signed_up"));
    }

    private void loadPersonSignups(Connection conn, Person person, List<Event> eventList) throws SQLException {
        PersonSignup personSignup;
        String query = "SELECT * FROM person_signup WHERE person_id = " + person.getID(); // Selects all columns from the rows in table 'person_signup' where the value
                                                                                          // in the column named 'person_id' equals the 'id' field of 'person'.
        try (Statement stmnt = conn.createStatement(); ResultSet resSet = stmnt.executeQuery(query)) {
            while (resSet.next()) {
                personSignup = loadPersonSignup(resSet, person, eventList);
                personSignup.getEvent().addPersonSignup(personSignup);
                person.addPersonSignup(personSignup);
            }
        } catch (SQLException e) {
            throw e;
        }
    }

    private PersonSignup loadPersonSignup(ResultSet resSet, Person person, List<Event> eventList) throws SQLException {
        Event event = getEventFromList(resSet.getInt("event_id"), eventList);
        Role role = getRole(resSet, person);
        PersonSignupState isSignedUp = PersonSignupState.values()[resSet.getInt("is_signed_up")];

        return new PersonSignup(resSet.getInt("id_personsignup"), role, event, isSignedUp);
    }

    private Role getRole(ResultSet resSet, Person person) throws SQLException {
        int gymnastID = resSet.getInt("gymnast_id"), coachID = resSet.getInt("coach_id");  // Method 'getInt' returns 0 if the
        int judgeID = resSet.getInt("judge_id");                                                       // value of the column is null.
        Role gymnast = person.getRole(Person.GYMNAST_INDEX), coach = person.getRole(Person.COACH_INDEX);
        Role judge = person.getRole(Person.JUDGE_INDEX);

        if (gymnastID != 0 && gymnastID == gymnast.getID()) {
            return gymnast;
        } else if (coachID != 0 && coachID == coach.getID()) {
            return coach;
        } else if (judgeID != 0 && judgeID == judge.getID()) {
            return judge;
        } else {
            return null;
        }
    }

    private void loadCarAssignments(Connection conn, Person person, List<Event> eventList) throws SQLException {
        CarAssignment carAssignment;
        String query = "SELECT * FROM car_assignment WHERE person_id = " + person.getID(); // Selects all columns from the rows in table 'car_assignment' where the value
                                                                                           // in the column named 'person_id' equals the 'id' field of 'person'.
        try (Statement stmnt = conn.createStatement(); ResultSet resSet = stmnt.executeQuery(query)) {
            while (resSet.next()) {
                carAssignment = loadCarAssignment(resSet, person, eventList);

                carAssignment.getCar().addCarAssignment(carAssignment);
                person.addCarAssignment(carAssignment);
            }
        } catch (SQLException e) {
            throw e;
        }
    }

    private CarAssignment loadCarAssignment(ResultSet resSet, Person person, List<Event> eventList) throws SQLException {
        int carID = resSet.getInt("car_assignment.car_id");
        Car car = getCar(eventList, carID);

        return new CarAssignment(resSet.getInt("id_carassignment"), resSet.getBoolean("is_driver"), person, car);
    }

    private Car getCar(List<Event> eventList, int carID) {
        for (Event e : eventList) {
            for (Car c : e.getCarList()) {
                if (carID == c.getID()) {
                    return c;
                }
            }
        }
        return null;
    }

    private void loadTeamSignups(Connection conn, List<Event> eventList, List<Team> teamList) throws SQLException {
        TeamSignup teamSignup;
        String query = "SELECT * FROM team_signup"; // Selects all columns of all rows in table 'team_signup'.

        try (Statement stmnt = conn.createStatement(); ResultSet resSet = stmnt.executeQuery(query)) {
            while (resSet.next()) {
                teamSignup = loadTeamSignup(resSet, eventList, teamList);

                teamSignup.getEvent().addTeamSignup(teamSignup);
                teamSignup.getTeam().addTeamSignup(teamSignup);

                addRelatedPersonSignups(conn, teamSignup);
            }
        } catch (SQLException e) {
            throw e;
        }
    }

    private TeamSignup loadTeamSignup(ResultSet resSet, List<Event> eventList, List<Team> teamList) throws SQLException {
        Event event = getEventFromList(resSet.getInt("event_id"), eventList);
        Team team = getTeamFromList(resSet.getInt("team_id"), teamList);

        return new TeamSignup(resSet.getInt("id_teamsignup"), team, event, resSet.getBoolean("is_signed_up"));
    }

    private Event getEventFromList(int eventID, List<Event> eventList) {
        for (Event e : eventList) {
            if (e.getID() == eventID) {
                return e;
            }
        }

        return null;
    }

    private Team getTeamFromList(int teamID, List<Team> teamList) {
        for (Team t : teamList) {
            if (t.getID() == teamID) {
                return t;
            }
        }
        return null;
    }

    private void addRelatedPersonSignups(Connection conn, TeamSignup teamSignup) throws SQLException {
        PersonSignup personSignup;
        Event event = teamSignup.getEvent();
        String query = "SELECT signup_indexes.personsignup_id, person_signup.event_id FROM signup_indexes " // Selects  the 'id' values of all 'PersonSignup' objects related to
                + "JOIN person_signup ON signup_indexes.personsignup_id = person_signup.id_personsignup "   // 'teamSignup' as well as the 'id' of the 'Event' object each
                + "AND signup_indexes.teamsignup_id = " + teamSignup.getID();                               // 'PersonSignup' object is connected to.

        try (Statement stmnt = conn.createStatement(); ResultSet resSet = stmnt.executeQuery(query)) {
            while (resSet.next()) {
                validatePersonSignupData(resSet, teamSignup);
                personSignup = getPersonSignupFromList(resSet.getInt("personsignup_id"), event.getPersonSignupList());
                teamSignup.addPersonSignup(personSignup);
                personSignup.addTeamSignup(teamSignup);
            }
        } catch (SQLException e) {
            throw e;
        }
    }

    private void validatePersonSignupData(ResultSet resSet, TeamSignup teamSignup) throws SQLException {
        if (resSet.getInt("person_signup.event_id") != teamSignup.getEvent().getID()) {
            throw new IllegalObjectException("Found an illegal entry in the database: PersonSignup and TeamSign don't share event but should.");
        }
    }

    // -------------------- Method for saving data of a 'Season' object --------------------
    void saveSeasonData(Season season) {
        Club.checkForNull(season);

        try (Connection conn = connectToDatabase(); Statement stmnt = conn.createStatement();) {
            stmnt.executeUpdate("INSERT INTO season VALUES("
                    + season.getID() + ", "
                    + "'" + season.getName() + "', "
                    + "'" + season.getStartDate().format(Club.DATE_TIME_FORMATTER) + "', "
                    + "'" + season.getEndDate().format(Club.DATE_TIME_FORMATTER) + "', "
                    + formatBoolean(season.isActive()) + ")");
        } catch (SQLException e) {
            throw new DatabaseErrorException("An error occurred while saving data of season " + season.getID() + ".", e);
        }
    }

    // -------------------- Methods for saving data of an 'Event' object --------------------
    void saveEventData(Event event) {
        List<TeamSignup> teamSignupList = event.viewTeamSignups();
        List<PersonSignup> personSignupList = event.getPersonSignupList();

        try (Connection conn = connectToDatabase(); Statement stmnt = conn.createStatement();) {
            stmnt.executeUpdate("INSERT INTO event VALUES (" + event.getID() + ", "
                    + "'" + event.getName() + "', "
                    + event.getAddress().toString() + ", "
                    + "'" + event.getStartDate().format(Club.DATE_TIME_FORMATTER) + "', "
                    + "'" + event.getEndDate().format(Club.DATE_FORMATTER) + "', "
                    + formatBoolean(event.isActive()) + ", "
                    + event.getSeason().getID() + ", "
                    + event.getCoachRequirement() + ", "
                    + event.getJudgeRequirement() + ")");

            saveTeamSignups(conn, teamSignupList);
            savePersonSignups(conn, personSignupList);
        } catch (SQLException e) {
            throw new DatabaseErrorException("An error occurred while saving data of event " + event.getID() + ".", e);
        }
    }

    private void saveTeamSignups(Connection conn, List<TeamSignup> teamSignupList) throws SQLException {
        try (Statement stmnt = conn.createStatement()) {
            for (TeamSignup t : teamSignupList) {
                t.setID(loadID(conn, "team_signup", "id_teamsignup"));
                stmnt.executeUpdate("INSERT INTO team_signup VALUES(" + t.getID() + ", "
                        + formatBoolean(t.isSignedUp()) + ", "
                        + t.getTeam().getID() + ", "
                        + t.getEvent().getID() + ")");
            }
        } catch (SQLException e) {
            throw e;
        }
    }

    // -------------------- Methods for saving data of a 'Person' object --------------------
    void savePersonData(Person person) {
        Club.checkForNull(person);
        List<PersonSignup> personSignupList = person.getPersonSignupList();
        Role gymnast = person.getRole(Person.GYMNAST_INDEX);
        Role coach = person.getRole(Person.COACH_INDEX);
        Role judge = person.getRole(Person.JUDGE_INDEX);

        try (Connection conn = connectToDatabase()) {
            savePerson(conn, person);
            saveRoles(conn, gymnast, coach, judge);
            savePersonSignups(conn, personSignupList);
        } catch (SQLException e) {
            throw new DatabaseErrorException("An error occurred while saving data of person " + person.getID() + ".", e);
        }
    }

    private void savePerson(Connection conn, Person person) throws SQLException {
        try (Statement stmnt = conn.createStatement()) {
            stmnt.executeUpdate("INSERT INTO person VALUES(" + (person.getID()) + ", "
                    + formatString(person.getName()) + ", "
                    + formatLocalDate(person.getBirthday()) + ", "
                    + formatBoolean(person.isMale()) + ", "
                    + formatLocalDate(person.getRegistrationDate()) + ", "
                    + formatLocalDate(person.getSignupDate()) + ", "
                    + String.valueOf(person.getAddressList().get(0)) + ", "
                    + String.valueOf(person.getAddressList().get(1)) + ", "
                    + formatString(person.getPhoneNumberList().get(0)) + ", "
                    + formatString(person.getPhoneNumberList().get(1)) + ", "
                    + formatString(person.getEmailList().get(0)) + ", "
                    + formatString(person.getEmailList().get(1)) + ", "
                    + formatBoolean(person.getPhotoConsent()) + ", "
                    + null + ", " + null + ", " + null + ")");
        } catch (SQLException e) {
            throw e;
        }
    }

    private void saveRoles(Connection conn, Role gymnast, Role coach, Role judge) throws SQLException {
        if (gymnast != null) {
            saveGymnast(conn, (Gymnast) gymnast);
        }
        if (coach != null) {
            saveCoachOrJudge(conn, coach, "coach");
        }
        if (judge != null) {
            saveCoachOrJudge(conn, judge, "judge");
        }
    }

    private void saveGymnast(Connection conn, Gymnast gymnast) throws SQLException {
        try (Statement stmnt = conn.createStatement();) {
            stmnt.executeUpdate("INSERT INTO gymnast VALUES(" + gymnast.getID() + ", "
                    + formatString(gymnast.getLicense()) + ", "
                    + formatBoolean(gymnast.isActive()) + ", "
                    + gymnast.getPerson().getID() + ")");

            updateIndexInPerson(conn, gymnast, "gymnast");
            saveTeamAssignments(conn, gymnast.getTeamAssignmentList());
        } catch (SQLException e) {
            throw e;
        }
    }

    private void saveCoachOrJudge(Connection conn, Role role, String tableName) throws SQLException {
        try (Statement stmnt = conn.createStatement()) {
            stmnt.executeUpdate("INSERT INTO " + tableName + " VALUES(" + role.getID() + ", "
                    + formatBoolean(role.isActive()) + ", "
                    + role.getPerson().getID() + ")");
            updateIndexInPerson(conn, role, tableName);

            if (role.isCoach()) {
                saveTeamAssignments(conn, ((TeamMember) role).getTeamAssignmentList());
            }
        } catch (SQLException e) {
            throw e;
        }
    }

    private void updateIndexInPerson(Connection conn, Role role, String tableName) throws SQLException {
        try (Statement stmnt = conn.createStatement()) {
            stmnt.executeUpdate("UPDATE person SET " + tableName + "_id = " + role.getID()
                    + " WHERE id_person = " + role.getPerson().getID());
        } catch (SQLException e) {
            throw e;
        }
    }

    private void savePersonSignups(Connection conn, List<PersonSignup> personSignupList) throws SQLException {
        for (PersonSignup p : personSignupList) {
            p.setID(loadID(conn, "person_signup", "id_personsignup"));
            savePersonSignup(conn, p);
        }
    }

    private void savePersonSignup(Connection conn, PersonSignup personSignup) throws SQLException {
        Role role = personSignup.getRole();
        String gymnastID = role.isGymnast() ? Integer.toString(role.getID()) : null;
        String coachID = role.isCoach() ? Integer.toString(role.getID()) : null;
        String judgeID = role.isJudge() ? Integer.toString(role.getID()) : null;

        try (Statement stmnt = conn.createStatement()) {
            stmnt.executeUpdate("INSERT INTO person_signup VALUES(" + personSignup.getID() + ", "
                    + personSignup.getState().ordinal() + ", "
                    + personSignup.getPerson().getID() + ", "
                    + gymnastID + ", "
                    + coachID + ", "
                    + judgeID + ", "
                    + personSignup.getEvent().getID() + ")");

            saveTeamSignupRelations(conn, personSignup);
        } catch (SQLException e) {
            throw e;
        }
    }

    private void saveTeamSignupRelations(Connection conn, PersonSignup personSignup) throws SQLException {
        try (Statement stmnt = conn.createStatement()) {
            for (TeamSignup t : personSignup.getTeamSignupList()) {
                stmnt.executeUpdate("INSERT INTO signup_indexes VALUES(" + personSignup.getID() + ", " + t.getID() + ")");
            }
        } catch (SQLException e) {
            throw e;
        }
    }

    // -------------------- Methods for saving data of a 'Car' object --------------------
    void saveCarData(Car car) {
        try (Connection conn = connectToDatabase(); Statement stmnt = conn.createStatement()) {
            stmnt.executeUpdate("INSERT INTO car VALUES(" + car.getID() + ", "
                    + car.getAvailableSeatCount() + ", "
                    + car.getEvent().getID() + ")");
            saveCarAssignments(conn, car.getCarAssignmentList());
        } catch (SQLException e) {
            throw new DatabaseErrorException("An error occurred while saving data of car " + car.getID() + ".", e);
        }
    }

    private void saveCarAssignments(Connection conn, List<CarAssignment> carAssignmentList) throws SQLException {
        for (CarAssignment c : carAssignmentList) {
            c.setID(loadID(conn, "car_assignment", "id_carassignment"));
            saveCarAssignment(conn, c);
        }
    }

    private void saveCarAssignment(Connection conn, CarAssignment carAssignment) throws SQLException {
        try (Statement stmnt = conn.createStatement()) {
            stmnt.executeUpdate("INSERT INTO car_assignment VALUES(" + carAssignment.getID() + ", "
                    + carAssignment.getCar().getID() + ", "
                    + carAssignment.getPerson().getID() + ", "
                    + formatBoolean(carAssignment.isDriver()) + ")");
        } catch (SQLException e) {
            throw e;
        }
    }

    // -------------------- Methods for saving data of a 'Team' object --------------------
    void saveTeamData(Team team) {
        try (Connection conn = connectToDatabase()) {
            saveTeam(conn, team);
            saveTeamAssignments(conn, team.getTeamAssignmentList());
            saveTeamSignups(conn, team.getTeamSignupList());
        } catch (SQLException e) {
            throw new DatabaseErrorException("An error occurred while saving data of team " + team.getID() + ".", e);
        }
    }

    private void saveTeam(Connection conn, Team team) throws SQLException {
        try (Statement stmnt = conn.createStatement()) {
            stmnt.executeUpdate("INSERT INTO team VALUES ("
                    + team.getID() + ", "
                    + "'" + team.getName() + "')");
        } catch (SQLException e) {
            throw e;
        }
    }

    private void saveTeamAssignments(Connection conn, List<TeamAssignment> teamAssignmentList) throws SQLException {
        for (TeamAssignment t : teamAssignmentList) {
            t.setID(loadID(conn, "team_assignment", "id_teamassignment"));
            saveTeamAssignment(conn, t);
        }
    }

    private void saveTeamAssignment(Connection conn, TeamAssignment teamAssignment) throws SQLException {
        TeamMember teamMember = teamAssignment.getTeamMember();
        String gymnastID = teamMember.isGymnast() ? Integer.toString(teamMember.getID()) : null;
        String coachID = teamMember.isCoach() ? Integer.toString(teamMember.getID()) : null;

        try (Statement stmnt = conn.createStatement()) {
            stmnt.executeUpdate("INSERT INTO team_assignment VALUES (" + teamAssignment.getID() + ", "
                    + gymnastID + ", "
                    + coachID + ", "
                    + formatBoolean(teamAssignment.isSignedUp()) + ", "
                    + teamAssignment.getTeam().getID() + ")");
        } catch (SQLException e) {
            throw e;
        }
    }

    // -------------------- Methods for restoring and updating data of a 'Person' object --------------------
    void restorePersonData(Person person) {
        String query = "SELECT * FROM person WHERE id_person = " + person.getID();

        try (Connection conn = connectToDatabase(); Statement stmnt = conn.createStatement();
             ResultSet resSet = stmnt.executeQuery(query)) {
            if (resSet.next()) {
                restorePerson(person, resSet);
            }
        } catch (SQLException e) {
            throw new DatabaseErrorException("An error occurred while restoring data of person " + person.getID() + ".", e);
        }
    }

    private void restorePerson(Person person, ResultSet resSet) throws SQLException {
        Person personOriginal = loadPerson(resSet);

        person.setName(personOriginal.getName())
                .setGender(personOriginal.isMale())
                .setBirthday(personOriginal.getBirthday())
                .setSignupDate(personOriginal.getSignupDate())
                .setAddressList(personOriginal.getAddressList())
                .setEmailList(personOriginal.getEmailList())
                .setPhoneNumberList(personOriginal.getPhoneNumberList())
                .setPhotoConsent(personOriginal.getPhotoConsent());
    }

    void updatePersonData(Person person) {
        try (Connection conn = connectToDatabase()) {
            updatePerson(conn, person);
            updateRoles(conn, person);
            updatePersonSignups(conn, person);
        } catch (SQLException e) {
            throw new DatabaseErrorException("An error occurred while updating data of person " + person.getID() + ".", e);
        }
    }

    private void updatePerson(Connection conn, Person person) throws SQLException {
        try (Statement stmnt = conn.createStatement()) {
            stmnt.executeUpdate("UPDATE person SET name = " + formatString(person.getName()) + ", "
                    + "birthday = " + formatLocalDate(person.getBirthday()) + ", "
                    + "is_male = " + formatBoolean(person.isMale()) + ", "
                    + "reg_date = " + formatLocalDate(person.getRegistrationDate()) + ", "
                    + "signup_date = " + formatLocalDate(person.getSignupDate()) + ", "
                    + "address1 = " + String.valueOf(person.getAddressList().get(0)) + ", "
                    + "address2 = " + String.valueOf(person.getAddressList().get(1)) + ", "
                    + "phone_number1 = " + formatString(person.getPhoneNumberList().get(0)) + ", "
                    + "phone_number2 = " + formatString(person.getPhoneNumberList().get(1)) + ", "
                    + "email1 = " + formatString(person.getEmailList().get(0)) + ", "
                    + "email2 = " + formatString(person.getEmailList().get(1)) + ", "
                    + "photo_consent = " + formatBoolean(person.getPhotoConsent()) + " "
                    + "WHERE id_person = " + person.getID());
        } catch (SQLException e) {
            throw e;
        }
    }

    private void updateRoles(Connection conn, Person person) throws SQLException {
        String query = "SELECT gymnast_id, coach_id, judge_id FROM person WHERE id_person = " + person.getID();
        Gymnast gymnast = (Gymnast) person.getRole(Person.GYMNAST_INDEX);
        Role coach = person.getRole(Person.COACH_INDEX), judge = person.getRole(Person.JUDGE_INDEX);

        try (Statement stmnt = conn.createStatement(); ResultSet resSet = stmnt.executeQuery(query)) {
            if (resSet.next()) {
                updateGymnast(conn, resSet, gymnast);
                updateCoachOrJudge(conn, resSet, coach, "coach");
                updateCoachOrJudge(conn, resSet, judge, "judge");
            }
        } catch (SQLException e) {
            throw e;
        }
    }

    private void updateGymnast(Connection conn, ResultSet resSet, Gymnast gymnast) throws SQLException {
        int gymnastID = resSet.getInt("gymnast_id");

        if (gymnastID == 0 && gymnast != null) {
            saveGymnast(conn, gymnast);
            updateIndexInPerson(conn, gymnast, "gymnast");
        } else if (gymnastID != 0 && gymnast != null) {
            try (Statement stmnt = conn.createStatement()) {
                stmnt.executeUpdate("UPDATE gymnast "
                        + "SET license = " + formatString(gymnast.getLicense()) + ", is_active = " + formatBoolean(gymnast.isActive()) + " "
                        + "WHERE id_gymnast = " + gymnastID);
                updateTeamAssignments(conn, gymnast, "gymnast");
            } catch (SQLException e) {
                throw e;
            }
        }
    }

    private void updateCoachOrJudge(Connection conn, ResultSet resSet, Role role, String tableName) throws SQLException {
        int roleID = resSet.getInt(tableName + "_id");

        if (roleID == 0 && role != null) {
            saveCoachOrJudge(conn, role, tableName);
        } else if (roleID != 0 && role != null) {
            try (Statement stmnt = conn.createStatement()) {
                stmnt.executeUpdate("UPDATE " + tableName + " "
                        + "SET is_active = " + formatBoolean(role.isActive()) + " "
                        + "WHERE id_" + tableName + " = " + roleID);
                if (role.isCoach()) {
                    updateTeamAssignments(conn, (TeamMember) role, "coach");
                }
            } catch (SQLException e) {
                throw e;
            }
        }
    }

    private void updateTeamAssignments(Connection conn, TeamMember teamMember, String roleType) throws SQLException {
        String query = "SELECT id_teamassignment FROM team_assignment WHERE " + roleType + "_id = " + teamMember.getID();
        List<TeamAssignment> saved = new ArrayList<>(), notSaved = new ArrayList<>();
        TeamAssignment teamAssignment;
        int id;

        try (Statement stmnt = conn.createStatement(); ResultSet resSet = stmnt.executeQuery(query)) {
            teamMember.getTeamAssignmentsSavedAndNotSaved(saved, notSaved);
            while (resSet.next()) {
                id = resSet.getInt("id_teamassignment");
                teamAssignment = getTeamAssignmentFromList(id, saved);

                if (teamAssignment == null) {
                    deleteData(conn, "team_assignment", "id_teamassignment", id);
                } else {
                    updateTeamAssignment(conn, teamAssignment);
                }
            }
            saveTeamAssignments(conn, notSaved);
        } catch (SQLException e) {
            throw e;
        }
    }

    private TeamAssignment getTeamAssignmentFromList(int teamAssignmentID, List<TeamAssignment> teamAssignmentList) {
        for (TeamAssignment t : teamAssignmentList) {
            if (teamAssignmentID == t.getID()) {
                return t;
            }
        }
        return null;
    }

    private void updateTeamAssignment(Connection conn, TeamAssignment teamAssignment) throws SQLException {
        try (Statement stmnt = conn.createStatement()) {
            stmnt.executeUpdate("UPDATE team_assignment "
                    + "SET is_signed_up = " + formatBoolean(teamAssignment.isSignedUp()) + " "
                    + "WHERE id_teamassignment = " + teamAssignment.getID());
        } catch (SQLException e) {
            throw e;
        }
    }

    private void updatePersonSignups(Connection conn, Person person) throws SQLException {
        String query = "SELECT id_personSignup FROM person_signup WHERE person_id = " + person.getID();
        List<PersonSignup> saved = new ArrayList<>(), notSaved = new ArrayList<>();
        PersonSignup personSignup;
        int id;

        person.getPersonSignupsSavedAndNotSaved(saved, notSaved);

        try (Statement stmnt = conn.createStatement(); ResultSet resSet = stmnt.executeQuery(query)) {
            while (resSet.next()) {
                id = resSet.getInt("id_personsignup");
                personSignup = getPersonSignupFromList(id, saved);
                if (personSignup != null) {
                    updatePersonSignup(conn, personSignup);
                } else {
                    deleteData(conn, "person_signup", "id_personsignup", id);
                }
            }
            savePersonSignups(conn, notSaved);
        } catch (SQLException e) {
            throw e;
        }
    }

    private PersonSignup getPersonSignupFromList(int personSignupID, List<PersonSignup> personSignupList) {
        for (PersonSignup p : personSignupList) {
            if (p.getID() == personSignupID) {
                return p;
            }
        }

        return null;
    }

    // -------------------- Method for updating data of an 'Event' object --------------------
    void updateEventData(Event event) {
        try (Connection conn = connectToDatabase(); Statement stmnt = conn.createStatement()) {
            stmnt.executeUpdate("UPDATE event set name = " + formatString(event.getName()) + ", "
                    + "address = " + event.getAddress().toString() + ", "
                    + "start_date = '" + event.getStartDate().format(Club.DATE_TIME_FORMATTER) + "', "
                    + "end_date = '" + event.getEndDate().format(Club.DATE_FORMATTER) + "', "
                    + "is_active = " + formatBoolean(event.isActive()) + ", "
                    + "coach_need = " + event.getCoachRequirement() + ", "
                    + "judge_need = " + event.getJudgeRequirement() + " "
                    + "WHERE id_event = " + event.getID());

        } catch (SQLException e) {
            throw new DatabaseErrorException("An error occurred while updating data of event " + event.getID() + ".", e);
        }
    }

    // -------------------- Methods for updating data of a 'PersonSignup' object --------------------
    void updatePersonSignupData(PersonSignup personSignup) {
        Club.checkForNull(personSignup);
        try (Connection conn = connectToDatabase()) {
            updatePersonSignup(conn, personSignup);
        } catch (SQLException e) {
            throw new DatabaseErrorException("An error occurred while updating data of person signup " + personSignup.getID() + ".", e);
        }
    }

    private void updatePersonSignup(Connection conn, PersonSignup personSignup) throws SQLException {
        try (Statement stmnt = conn.createStatement()) {
            stmnt.executeUpdate("UPDATE person_signup "
                    + "SET is_signed_up = " + personSignup.getState().ordinal() + " "
                    + "WHERE id_personsignup = " + personSignup.getID());
            if (personSignup.getRole().isGymnast()) {
                updateSignupIndexes(conn, stmnt, personSignup);
            }
        } catch (SQLException e) {
            throw e;
        }
    }

    private void updateSignupIndexes(Connection conn, Statement stmnt, PersonSignup personSignup) throws SQLException {
        String query = "SELECT * FROM signup_indexes WHERE personsignup_id = " + personSignup.getID();

        try (ResultSet resSet = stmnt.executeQuery(query)) {
            while (resSet.next()) {
                if (!personSignup.hasTeamSignup(resSet.getInt("teamsignup_id"))) {
                    deleteSignupIndex(conn, personSignup.getID(), resSet.getInt("teamsignup_id"));
                }
            }
        } catch (SQLException e) {
            throw e;
        }
    }

    private void deleteSignupIndex(Connection conn, int personSignupID, int teamSignupID) throws SQLException {
        try (Statement stmnt = conn.createStatement()) {
            stmnt.executeUpdate("DELETE FROM signup_indexes "
                    + "WHERE personsignup_id = " + personSignupID + " AND teamsignup_id = " + teamSignupID);
        } catch (SQLException e) {
            throw e;
        }
    }


    // -------------------- Methods for deleting data from database --------------------
    void deleteAllDataFromDatabase() { // For use in tests.
        try (Connection conn = connectToDatabase()) {
            deleteAllDataFromTable(conn, "season");
            deleteAllDataFromTable(conn, "person");
            deleteAllDataFromTable(conn, "team");
        } catch (SQLException e) {
            throw new DatabaseErrorException("An error occurred while deleting all data from database.", e);
        }
    }

    private void deleteAllDataFromTable(Connection conn, String tableName) throws SQLException { // For use in tests.
        try (Statement stmnt = conn.createStatement()) {
            stmnt.executeUpdate("DELETE FROM " + tableName);
        } catch (SQLException e) {
            throw e;
        }
    }

    void deleteObjectData(String tableName, String columnName, int id) {
        try (Connection conn = connectToDatabase()) {
            deleteData(conn, tableName, columnName, id);
        } catch (SQLException e) {
            throw new DatabaseErrorException("An error occurred while deleting data of object.\n"
                    + "   Table: " + tableName + ".\n"
                    + "   Column: " + columnName + ".", e);
        }
    }

    private void deleteData(Connection conn, String tableName, String columnName, int id) throws SQLException {
        try (Statement stmnt = conn.createStatement()) {
            stmnt.executeUpdate("DELETE FROM " + tableName + " WHERE " + columnName + " = " + id);
        } catch (SQLException e) {
            throw e;
        }
    }

    // -------------------- Methods for formatting fields of objects --------------------
    private String formatLocalDate(LocalDate localDate) {
        return localDate == null ? null : "'" + localDate.format(Club.DATE_FORMATTER) + "'";
    }

    private int formatBoolean(boolean bool) {
        return (bool ? 1 : 0);
    }

    private String formatString(String string) {
        return string == null ? null : "'" + string + "'";
    }

    // -------------------- Methods for retrieving the next primary key of a table --------------------
    int getNextID(String tableName, String columnName) {
        try (Connection conn = connectToDatabase()) {
            int nextID = loadID(conn, tableName, columnName);

            if (nextID <= 0) {
                throw new IllegalIDException("No valid id was retrieved.");
            }

            return nextID;
        } catch (SQLException e) {
            throw new DatabaseErrorException("Could not load an id for " + tableName + ".", e);
        }
    }

    private int loadID(Connection conn, String tableName, String columnName) throws SQLException { // Method retrieves the maximum value
        int nextID = 0;                                                                            // of the column 'columnName' in the
        String query = "SELECT max(" + columnName + ") from " + tableName;                                 // table 'tableName'.

        try (Statement stmnt = conn.createStatement(); ResultSet resSet = stmnt.executeQuery(query);) {
            if (resSet.next()) {
                nextID = resSet.getInt(1) + 1;
            }
            return nextID;
        } catch (SQLException e) {
            throw e;
        }
    }
}

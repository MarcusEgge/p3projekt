package Model;

import Model.Exceptions.IllegalFormatException;
import java.util.Objects;

public class Address {
    private String road;
    private String city;

    public Address(String road, String city) {
        Club.checkForNull(road);
        Club.checkForNull(city);

        this.road = road;
        this.city = city;
    }

    Address(String roadAndCity) {
        Club.checkForNull(roadAndCity);

        int divide = roadAndCity.indexOf('|');

        if (divide == -1 || roadAndCity.charAt(divide + 1) != ' ' || roadAndCity.charAt(divide - 1) != ' ') {
            throw new IllegalFormatException("The format of the address string is illegal.");
        }

        this.road = roadAndCity.substring(0, divide - 1);
        this.city = roadAndCity.substring(divide + 2);
    }

    String getRoad() {
        return road;
    }

    String getCity() {
        return city;
    }

    @Override
    public String toString() {
        return "'" + road + " | " + city + "'";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Address address = (Address) o;
        return Objects.equals(road, address.road) &&
                Objects.equals(city, address.city);
    }

    @Override
    public int hashCode() {
        return Objects.hash(road, city);
    }
}

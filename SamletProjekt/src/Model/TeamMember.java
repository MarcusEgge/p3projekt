package Model;

import java.util.List;

public interface TeamMember extends Role {
    List<TeamAssignment> getTeamAssignmentList();

    void getTeamAssignmentsSavedAndNotSaved(List<TeamAssignment> saved, List<TeamAssignment> notSaved);

    void addTeamAssignment(TeamAssignment teamAssignment);

    void updateTeamAssignments(List<String> teamNameList);

    void deleteTeamAssignment(TeamAssignment teamAssignment);
}

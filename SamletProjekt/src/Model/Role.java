package Model;

public interface Role {
    int getID();

    Person getPerson();

    boolean isActive();

    void setActive(boolean active);

    boolean isGymnast();

    boolean isCoach();

    boolean isJudge();
}
package Model;

import Model.Exceptions.IllegalObjectException;
import Model.Exceptions.ObjectNotFoundException;

import java.util.*;

class Gymnast implements TeamMember {
    private final int id;
    private transient Person person;
    private String license;
    private List<TeamAssignment> teamAssignmentList;
    private boolean isActive;

    Gymnast(int id, Person person, String license, boolean isActive) {
        Club.checkForNull(person);
        Club.checkID(id);

        this.id = id;
        this.person = person;
        this.license = license;
        this.teamAssignmentList = new ArrayList<>();
        this.isActive = isActive;
    }

    Gymnast(Person person, String license, boolean isActive) {
        Club.checkForNull(person);

        this.id = Club.INSTANCE.getDatabaseManager().getNextID("gymnast", "id_gymnast");
        this.person = person;
        this.license = license;
        this.teamAssignmentList = new ArrayList<>();
        this.isActive = isActive;
    }

    public String getLicense() {
        return license;
    }

    public void setLicense(String license) {
        this.license = license;
    }

    // -------------------- Methods from the 'Role' interface --------------------
    @Override
    public int getID() {
        return id;
    }

    @Override
    public Person getPerson() {
        return person;
    }

    @Override
    public boolean isActive() {
        return isActive;
    }

    @Override
    public void setActive(boolean active) {
        if (isActive && !active) {
            deleteAllTeamAssignments();
        }
        isActive = active;
    }

    private void deleteAllTeamAssignments() {
        for (int i = teamAssignmentList.size() - 1; i >= 0; i--) {
            teamAssignmentList.remove(i).deleteFromTeam();
        }
    }

    // -------------------- Methods from the 'TeamMember' interface --------------------
    @Override
    public boolean isGymnast() {
        return true;
    }

    @Override
    public boolean isCoach() {
        return false;
    }

    @Override
    public boolean isJudge() {
        return false;
    }

    @Override
    public List<TeamAssignment> getTeamAssignmentList() {
        return Collections.unmodifiableList(teamAssignmentList);
    }

    @Override
    public void getTeamAssignmentsSavedAndNotSaved(List<TeamAssignment> saved, List<TeamAssignment> notSaved) {
        Club.checkForNull(saved);
        Club.checkForNull(notSaved);

        for (TeamAssignment t: teamAssignmentList) {
            if (t.getID() == 0) { // If the 'id' field in 't' equals 0, then 't' has not been saved to the database.
                notSaved.add(t);
            } else {
                saved.add(t);
            }
        }
    }

    @Override
    public void addTeamAssignment(TeamAssignment teamAssignment) {
        Club.checkForNull(teamAssignment);
        if (this != teamAssignment.getTeamMember() || teamAssignmentList.contains(teamAssignment)) {
            throw new IllegalObjectException("The TeamAssignment object cannot be added to the list in the Gymnast object.");
        }
        teamAssignmentList.add(teamAssignment);
    }

    @Override
    public void updateTeamAssignments(List<String> teamNameList) {
        Club.checkForNull(teamNameList);

        for (TeamAssignment t : teamAssignmentList) {
            t.setSignedUp(teamNameList.contains(t.getTeam().getName())); // Method 'setSignedUp' calls method to delete or add 'this' to 'TeamSignup' objects related to future events.
        }
    }

    @Override
    public void deleteTeamAssignment(TeamAssignment teamAssignment) {
        if(!teamAssignmentList.remove(teamAssignment)){
            throw new ObjectNotFoundException("The TeamAssignment object was not found in the Gymnast object.");
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Gymnast gymnast = (Gymnast) o;
        return Objects.equals(person, gymnast.person);
    }

    @Override
    public int hashCode() {
        return Objects.hash(person);
    }
}
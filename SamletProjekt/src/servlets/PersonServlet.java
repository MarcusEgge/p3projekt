package servlets;

import Model.Address;
import Model.Exceptions.InvalidActionException;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@WebServlet(name = "personServlet", urlPatterns = "/personServlet/*")
public class PersonServlet extends ServletSuperClass {

    //Sends the persons to the front-end.
    protected void doGet(HttpServletRequest request, HttpServletResponse response) {
        try {
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");

            String json = gson.toJson(club.viewPersons());

            try {
                response.getWriter().write(json);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (RuntimeException e) {
            sendException(e, response);
        }
    }

    //There is multiple actions it will take depending on whats in the request, as can be seen by the if statement.
    protected void doPost(HttpServletRequest request, HttpServletResponse response) {
        try {
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");

            String action = request.getParameter("action");

            if (action.equals("Create")) {
                createPerson(request, response);
            } else if (action.equals("Update")) {
                updatePerson(request, response);
            } else if (action.equals("Delete")) {
                deletePerson(request, response);
            } else {
                throw new InvalidActionException(action + " is not a valid action");
            }
        } catch (RuntimeException e) {
            sendException(e, response);
        }

    }

    //Creates a new person in the club and sends all the persons back to the front-end.
    private void createPerson(HttpServletRequest request, HttpServletResponse response) {
        String json = request.getParameter("person").trim();


        //The request have a person object in Json format, which is parsed into a Json object that
        // the loadPerson uses to load the data from the front-end.
        HashMap<String, Object> objectMap = loadPerson(new JsonParser().parse(json).getAsJsonObject());


        club.createPerson((String) objectMap.get("name"), (boolean) objectMap.get("isMale"), (List<Address>) objectMap.get("addressList"),
                (List<String>) objectMap.get("emailList"), (List<String>) objectMap.get("phoneList"), (String) objectMap.get("birthday"),
                (String) objectMap.get("signupDate"), (boolean) objectMap.get("photoConsent"), (boolean) objectMap.get("isGymnast"),
                (boolean) objectMap.get("isCoach"), (boolean) objectMap.get("isJudge"), (String) objectMap.get("license"),
                (List<String>) objectMap.get("teamListGymnast"), (List<String>) objectMap.get("teamListGymnast"));

        json = gson.toJson(club.viewPersons());

        try {
            response.getWriter().write(json);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    //Does the same as the create person, except that it calls editPerson.
    private void updatePerson(HttpServletRequest request, HttpServletResponse response) {
        String json = request.getParameter("person").trim();
        int id = Integer.parseInt(request.getParameter("ID"));


        HashMap<String, Object> objectMap = loadPerson(new JsonParser().parse(json).getAsJsonObject());


        club.editPerson(id, (String) objectMap.get("name"), (boolean) objectMap.get("isMale"), (List<Address>) objectMap.get("addressList"),
                (List<String>) objectMap.get("emailList"), (List<String>) objectMap.get("phoneList"), (String) objectMap.get("birthday"),
                (String) objectMap.get("signupDate"), (boolean) objectMap.get("photoConsent"), (boolean) objectMap.get("isGymnast"),
                (boolean) objectMap.get("isCoach"), (boolean) objectMap.get("isJudge"), (String) objectMap.get("license"),
                (List<String>) objectMap.get("teamListGymnast"), (List<String>) objectMap.get("teamListGymnast"));

        try {
            response.getWriter().write(json);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    //loadPerson loads all the data in the jsonobject. It puts the information into a hashMap and returns it.
    HashMap<String, Object> loadPerson(JsonObject jsonObject) {
        HashMap<String, Object> objectMap = new HashMap<>();
        objectMap.put("name", jsonObject.get("name").getAsString());
        objectMap.put("isMale", jsonObject.get("isMale").getAsBoolean());

        List<Address> addressList = new ArrayList<>();
        List<String> emailList = new ArrayList<>();
        List<String> phoneList = new ArrayList<>();
        String address1 = jsonObject.get("address1").getAsString();
        String city1 = jsonObject.get("city1").getAsString();

        addressList.add(new Address(address1, city1));
        objectMap.put("addressList", addressList);

        emailList.add(jsonObject.get("email1").getAsString());
        phoneList.add(jsonObject.get("phoneNumber1").getAsString());

        objectMap.put("emailList", emailList);
        objectMap.put("phoneList", phoneList);
        /*
        Secondary adress, email and phone is not implemented yet
        addressList.add(new Address(jsonObject.get("address2").getAsString(),jsonObject.get("municipality2").getAsString() + jsonObject.get("postalCode2")));
        emailList.add(jsonObject.get("email2").getAsString());
        phoneList.add(jsonObject.get("phoneNumber2").getAsString());

        Teams are not implemented yet
        teamListCoach.add();
        teamListGymnast.add();
        */

        objectMap.put("birthday", jsonObject.get("birthday").getAsString());
        objectMap.put("signupDate", jsonObject.get("signupDate").getAsString());
        objectMap.put("photoConsent", jsonObject.get("photoConsent").getAsBoolean());
        objectMap.put("isGymnast", jsonObject.get("isGymnast").getAsBoolean());
        objectMap.put("isCoach", jsonObject.get("isCoach").getAsBoolean());
        objectMap.put("isJudge", jsonObject.get("isJudge").getAsBoolean());
        objectMap.put("license", jsonObject.get("license").getAsString());
        List<String> teamListGymnast = new ArrayList<>();
        List<String> teamListCoach = new ArrayList<>();
        objectMap.put("teamListGymnast", teamListGymnast);
        objectMap.put("teamListCoach", teamListCoach);

        return objectMap;
    }

    private void deletePerson(HttpServletRequest request, HttpServletResponse response) {
        int id = Integer.parseInt(request.getParameter("id").trim());
        club.deletePerson(id);
        response.setContentType("text/plain");
        try {
            response.getWriter().write("Hejsa");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
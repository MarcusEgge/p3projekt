package servlets;

import Model.Address;
import Model.Exceptions.InvalidActionException;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;


@WebServlet(name = "eventServlet", urlPatterns = "/eventServlet/*")
public class EventServlet extends ServletSuperClass {

    //Gets the events from the season send with the request and sends them to the front-end.
    protected void doGet(HttpServletRequest request, HttpServletResponse response) {
        try {
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");

            String seasonName = request.getParameter("seasonName");

            String json = gson.toJson(club.viewSeason(seasonName).viewEvents());

            gson.toJson(new ArrayList<>());
            response.getWriter().write(json);
        } catch (RuntimeException e) {
            sendException(e, response);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    //There is multiple actions it will take depending on whats in the request, as can be seen by the if statement.
    protected void doPost(HttpServletRequest request, HttpServletResponse response) {

        try {
            String action = request.getParameter("action");

            if (action.equals("Create")) {
                createEvent(request);
            } else if (action.equals("Update")) {
                updateEvent(request);
            } else if (action.equals("Delete")) {
                deleteEvent(request);
            } else {
                throw new InvalidActionException(action + " is not a valid action");
            }
        } catch (RuntimeException e) {
            sendException(e, response);
        }

    }

    private void createEvent(HttpServletRequest request) {
        String json = request.getParameter("text").trim();
        String seasonName = request.getParameter("seasonName");

        //The request have a person object in Json format, which is parsed into a Json object that
        // the loadPerson uses to load the data from the front-end.
        HashMap<String, Object> objectMap = loadEvent(new JsonParser().parse(json).getAsJsonObject());

        club.viewSeason(seasonName).createEvent((String) objectMap.get("name"), (Address) objectMap.get("address"),
                objectMap.get("startDate") + " " + objectMap.get("startTime"),
                (String) objectMap.get("endDate"), (int) objectMap.get("coaches"), (int) objectMap.get("judges"));
    }

    private void updateEvent(HttpServletRequest request) {
        String json = request.getParameter("text").trim();
        String seasonName = request.getParameter("seasonName");
        int eventId = Integer.parseInt(request.getParameter("ID"));

        HashMap<String, Object> objectMap = loadEvent(new JsonParser().parse(json).getAsJsonObject());

        club.viewSeason(seasonName).editEvent(eventId, (String) objectMap.get("name"), (Address) objectMap.get("address"),
                objectMap.get("startDate") + " " + objectMap.get("startTime"),
                (String) objectMap.get("endDate"), (int) objectMap.get("coaches"), (int) objectMap.get("judges"));
    }

    //loadEvent loads all the data in the jsonobject. It puts the information into a hashMap and returns it.
    HashMap<String, Object> loadEvent(JsonObject jsonObject) {
        HashMap<String, Object> objectMap = new HashMap<>();
        objectMap.put("name", jsonObject.get("name").getAsString());

        String street = jsonObject.get("address").getAsString();
        String city = jsonObject.get("city").getAsString();
        objectMap.put("address", new Address(street, city));

        objectMap.put("startDate", jsonObject.get("startDate").getAsString());
        objectMap.put("startTime", jsonObject.get("startTime").getAsString());
        objectMap.put("endDate", jsonObject.get("endDate").getAsString());
        objectMap.put("coaches", jsonObject.get("coaches").getAsInt());
        objectMap.put("judges", jsonObject.get("judges").getAsInt());

        return objectMap;
    }

    private void deleteEvent(HttpServletRequest request) {
        int id = Integer.parseInt(request.getParameter("id"));
        String seasonName = request.getParameter("seasonName");
        club.viewSeason(seasonName).deleteEvent(id);
    }
}
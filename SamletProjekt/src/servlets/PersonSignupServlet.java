package servlets;

import Model.PersonSignupState;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "personSignupServlet", urlPatterns = "/personSignupServlet/*")
public class PersonSignupServlet extends ServletSuperClass {

    //Gets the personSignups for the season and event send with the request
    protected void doGet(HttpServletRequest request, HttpServletResponse response) {
        try {
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");
            String seasonName = request.getParameter("seasonName");
            int eventIndex = Integer.parseInt(request.getParameter("eventIndex"));

            try {
                response.getWriter().write(findSignups(request.getParameter("role"), seasonName, eventIndex));
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (RuntimeException e) {
            sendException(e, response);
        }


    }

    //Checks what role we wnats and returns them as a Json string
    private String findSignups(String role, String seasonName, int eventIndex) {

        if ("gymnast".equals(role)) {
            return gson.toJson(club.viewSeason(seasonName).viewEvent(eventIndex).viewGymnastPersonSignups());
        } else if ("coach".equals(role)) {
            return gson.toJson(club.viewSeason(seasonName).viewEvent(eventIndex).viewCoachPersonSignups());
        } else {
            return gson.toJson(club.viewSeason(seasonName).viewEvent(eventIndex).viewJudgePersonSignups());
        }
    }

    //Edits the personSignups
    protected void doPost(HttpServletRequest request, HttpServletResponse response) {
        try {

            int eventId = Integer.parseInt(request.getParameter("eventIndex"));
            String seasonName = request.getParameter("seasonName");
            int personSignupId = Integer.parseInt(request.getParameter("personSignUpIndex"));

            club.viewSeason(seasonName).viewEvent(eventId).editPersonSignup(personSignupId, getSignupState(request.getParameter("state")));
        } catch (RuntimeException e) {
            sendException(e, response);
        }
    }

    private PersonSignupState getSignupState(String state) {
        switch (state) {
            case "SIGNED_UP":
                return PersonSignupState.SIGNED_UP;
            case "NOT_SIGNED_UP":
                return PersonSignupState.NOT_SIGNED_UP;
            case "UNDECIDED":
                return PersonSignupState.UNDECIDED;
            default:
                return PersonSignupState.UNDECIDED;
        }
    }

}
package servlets;


import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@WebServlet(name = "seasonServlet", urlPatterns = "/seasonServlet/*")
public class SeasonServlet extends ServletSuperClass {


    //Gets all the seasons and will create one if none exist.
    protected void doGet(HttpServletRequest request, HttpServletResponse response) {
        try {
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");

            if (club.viewSeasons().size() == 0) {
                club.createSeason();
            }

            String json = gson.toJson(club.viewSeasons());
            try {
                response.getWriter().write(json);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (RuntimeException e) {
            sendException(e, response);
        }

    }

    //Creates a new season and then sends that seasons name back to the front end.
    protected void doPost(HttpServletRequest request, HttpServletResponse response) {
        try {
            response.setContentType("text/plain");
            response.setCharacterEncoding("UTF-8");

            club.createSeason();

            String json = gson.toJson(club.viewSeasons().get(club.viewSeasons().size() - 1).getName());

            try {
                response.getWriter().write(json);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (RuntimeException e) {
            sendException(e, response);
        }
    }
}

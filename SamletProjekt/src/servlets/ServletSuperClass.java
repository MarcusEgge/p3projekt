package servlets;

import Model.Club;
import com.google.gson.Gson;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

//The class that all our servlets, except login servlet, extends.
// The sendException method is used to send error messages to the front end and is used by all the servlets
public class ServletSuperClass extends HttpServlet {
    Club club = Club.INSTANCE;
    Gson gson = new Gson();

    void sendException(RuntimeException e, HttpServletResponse response) {
        String[] errorList = {"error", e.getMessage()};
        try {
            response.setContentType("json");
            response.setCharacterEncoding("UTF-8");
            response.getWriter().write(gson.toJson(errorList));
            response.getWriter().close();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
    }
}

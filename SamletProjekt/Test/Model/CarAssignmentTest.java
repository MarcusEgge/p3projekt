package Model;

import Model.Exceptions.IllegalIDException;
import Model.Exceptions.ObjectNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Objects;

import static org.junit.jupiter.api.Assertions.*;

class CarAssignmentTest {
    private Car car, car2;
    private Season season;
    private Event event, event2;
    private Person driver;
    private CarAssignment carAssignment, carAssignment2, carAssignment3;


    // 1 season, 2 event, 1 person, 2 car , 3 carAssignment.
    @BeforeEach
    void initObjects() {
        season = new Season(LocalDateTime.now(), LocalDateTime.now());
        event = new Event("Dancup", new Address("Kalderupvej 42", "9000 Aalborg"), LocalDateTime.now(), LocalDate.now(), season, 6, 6);
        event2 = new Event("SpringDanmark", new Address("Kalderupvej 42", "9000 Aalborg"), LocalDateTime.now(), LocalDate.now(), season, 6, 6);
        driver = new Person("Driver", true, LocalDate.now(), true);
        car = new Car(event);
        car2 = new Car(event2);
        carAssignment = new CarAssignment(true, driver, car);
        driver.addCarAssignment(carAssignment); // carAssignment gets added to person driver.
        car.addCarAssignment(carAssignment); // carAssignment gets added to car.
        carAssignment2 = new CarAssignment(false, driver, car);
        carAssignment3 = new CarAssignment(1, true, driver, car2); // Another carAssignment made with the second constructor.
    }

    @Test
    void constructor01() {
        assertTrue(carAssignment.isDriver());
        assertEquals(carAssignment.getPerson(), driver);
        assertEquals(carAssignment.getCar(), car);
    }

    @Test
    void constructor02() {
        assertTrue(carAssignment.isDriver());
        assertEquals(carAssignment3.getPerson(), driver);
        assertEquals(carAssignment3.getCar(), car2);
    }

    // Exception test. First constructor not allowed to give null as person.
    @Test
    void constructor03() {
        assertThrows(NullPointerException.class, () -> new CarAssignment(true, null, car));
    }

    // Exception test. First constructor not allowed to give null as car.
    @Test
    void constructor04() {
        assertThrows(NullPointerException.class, () -> new CarAssignment(true, driver, null));
    }

    // Exception test. Second constructor not allowed to give null as person.
    @Test
    void constructor05() {
        assertThrows(NullPointerException.class, () -> new CarAssignment(1, true, null, car));
    }

    // Exception test. Second constructor not allowed to give null as car.
    @Test
    void constructor06() {
        assertThrows(NullPointerException.class, () -> new CarAssignment(1, true, driver, null));
    }

    @Test
    void getID() {
        assertEquals(0, carAssignment2.getID());
    }

    @Test
    void setID01() {
        carAssignment2.setID(1);
        assertEquals(1, carAssignment2.getID());
    }

    // Exception test. Not allowed to change id when it already have one.
    @Test
    void setID02() {
        carAssignment2.setID(1);
        IllegalIDException exception = assertThrows(IllegalIDException.class, () -> carAssignment2.setID(1));
        assertEquals(exception.getMessage(), "The CarAssignment id is illegal.");
    }

    // Exception test. Not allowed to change id to 0.
    @Test
    void setID03() {
        IllegalIDException exception = assertThrows(IllegalIDException.class, () -> carAssignment2.setID(0));
        assertEquals(exception.getMessage(), "The CarAssignment id is illegal.");
    }

    // Exception test. Not allowed to change id to a negative integer as id.
    @Test
    void setID04() {
        IllegalIDException exception = assertThrows(IllegalIDException.class, () -> carAssignment2.setID(-1));
        assertEquals(exception.getMessage(), "The CarAssignment id is illegal.");
    }

    @Test
    void getCar() {
        assertEquals(car, carAssignment.getCar());
    }

    // carAssignment deleted from car
    @Test
    void deleteFromCar() {
        assertTrue(car.getCarAssignmentList().contains(carAssignment));
        carAssignment.deleteFromCar();
        assertFalse(car.getCarAssignmentList().contains(carAssignment));
    }

    // carAssignment deleted from Person driver.
    @Test
    void deleteFromPerson() {
        assertTrue(driver.getCarAssignmentList().contains(carAssignment));
        carAssignment.deleteFromPerson();
        assertFalse(driver.getCarAssignmentList().contains(carAssignment));
    }

    @Test
    void isDriver() {
        assertTrue(carAssignment.isDriver());
        assertFalse(carAssignment2.isDriver());
    }

    @Test
    void equals01() {
        assertEquals(carAssignment, carAssignment2);
    }

    @Test
    void equals02() {
        assertNotEquals(carAssignment, carAssignment3);
    }

    @Test
    void equals03() {
        assertNotEquals(carAssignment, carAssignment3);
    }

    @Test
    void hashCode01() {
        assertEquals(Objects.hash(driver, car.getEvent()), carAssignment.hashCode());
    }
}
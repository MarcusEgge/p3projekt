package Model;

import Model.*;
import Model.Exceptions.IllegalIDException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class TeamAssignmentTest {
    private Gymnast gymnast;
    private Person person;
    private List<TeamAssignment> teamAssignmentsList;
    private Team team;
    private TeamAssignment teamAssignment;

    //1 Person, 1 Gymnast, 1 Team, 1 TeamAssignment and a list of teamAssignmentsList.
    @BeforeEach
    public void initObjects() {
        teamAssignmentsList = new ArrayList<>();
        person = new Person("Åge", true, LocalDate.now(), true);
        gymnast = new Gymnast(person, "121202", true);
        team = new Team("Red");
        teamAssignment = new TeamAssignment(team, gymnast, true);
    }

    @Test
    void constructor01() {
        TeamAssignment teamAssignmentTest = new TeamAssignment(10, team, gymnast, true);
        assertEquals(10, teamAssignmentTest.getID());
        assertEquals(team,teamAssignmentTest.getTeam());
        assertEquals(gymnast, teamAssignmentTest.getTeamMember());
        assertEquals(true, teamAssignmentTest.isSignedUp());
    }

    //Exception test. Not allowed to initiate with id = 0.
    @Test
    void constructor02() {
        IllegalIDException e = assertThrows(IllegalIDException.class, () ->new TeamAssignment(0, team, gymnast, true));
        assertEquals("The TeamAssignment id is illegal.", e.getMessage());
    }

    //Exception test. Not allowed to initiate with a negative id.
    @Test
    void constructor03() {
        IllegalIDException e = assertThrows(IllegalIDException.class, () ->new TeamAssignment(-1, team, gymnast, true));
        assertEquals("The TeamAssignment id is illegal.", e.getMessage());
    }

    @Test
    void getID() {
        assertEquals(0, teamAssignment.getID());
    }

    //Exception test. Not allowed to change id.
    @Test
    void setID02() {
        TeamAssignment teamAssignmentTest = new TeamAssignment(10, team, gymnast, true);
        IllegalIDException e = assertThrows(IllegalIDException.class, () -> teamAssignmentTest.setID(1));
        assertEquals("The TeamAssignment id is illegal.", e.getMessage());
    }

    @Test
    void getTeam() {
        assertEquals(team, teamAssignment.getTeam());
    }

    @Test
    void isSignedUp() {
        assertTrue(teamAssignment.isSignedUp());
    }

    @Test
    void setSignedUp1() {
        teamAssignment.setSignedUp(false);
        assertFalse(teamAssignment.isSignedUp());
    }

    @Test
    void setSignedUp2() {
        teamAssignment.setSignedUp(false);
        teamAssignment.setSignedUp(true);
        assertTrue(teamAssignment.isSignedUp());
    }

    @Test
    void getTeamMember() {
        assertEquals(gymnast, teamAssignment.getTeamMember());
    }

    @Test
    void deleteFromTeam() {
        team.addTeamAssignment(teamAssignment);
        teamAssignment.deleteFromTeam();

        assertFalse(teamAssignment.getTeam().getTeamAssignmentList().contains(teamAssignment));
    }

    @Test
    void deleteFromRole() {
        gymnast.addTeamAssignment(teamAssignment);
        teamAssignment.deleteFromRole();

        assertFalse(teamAssignment.getTeamMember().getTeamAssignmentList().contains(teamAssignment));
    }

    @Test
    void hashCode01() {
        TeamAssignment teamAssignmentTest = new TeamAssignment(10, team, gymnast, false);
        assertEquals(teamAssignmentTest.hashCode(), teamAssignment.hashCode());
    }

    @Test
    void equals01() {
        TeamAssignment teamAssignmentTest = new TeamAssignment(10, team, gymnast, false);
        assertEquals(teamAssignmentTest, teamAssignment);
    }

    @Test
    void equals02(){
        TeamAssignment teamAssignmentTest = new TeamAssignment(10, new Team("Blue"),gymnast,false);
        assertNotEquals(teamAssignmentTest,teamAssignment);
    }

    @Test
    void equals03(){
        TeamAssignment teamAssignmentTest = new TeamAssignment(10, team,new Coach(person,true),false);
        assertNotEquals(teamAssignmentTest,teamAssignment);
    }
}
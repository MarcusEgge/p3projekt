package Model;

import Model.Exceptions.IllegalFormatException;
import Model.Exceptions.IllegalIDException;
import Model.Exceptions.IllegalObjectException;
import Model.Exceptions.ObjectNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class PersonTest {

    private Gymnast gymnast;
    private Season season;
    private Event event1;
    private Person person;
    private Team team;
    private PersonSignup personSignup;
    private TeamSignup teamSignup;
    private LocalDateTime today;
    private Address address;
    private int judgeAndCoach;

    @BeforeEach
    void initObjects() {
        today = LocalDateTime.now();
        person = new Person("Anders", true, today.toLocalDate(), true);
        team = new Team("Rød");
        gymnast = new Gymnast(person, "121202", true);
        season = new Season(today, today);
        address=new Address("Kalderupvej 21", "9000 Aalborg");
        judgeAndCoach = 6;
        event1 = new Event("SpringDanmark", address, today, today.toLocalDate(), season, judgeAndCoach, judgeAndCoach);
        teamSignup = new TeamSignup(team, event1, true);
        personSignup = new PersonSignup(gymnast, event1);
    }

    @Test
    void getID() {
        Person personTest = new Person(1, "Peter", true, today.toLocalDate(), false);
        assertEquals(1, personTest.getID());
    }

    // Exception test. Not allowed to give a ID at 0 or under.
    @Test
    void checkID() {
        IllegalIDException e = assertThrows(IllegalIDException.class, () -> new Person(0, "Peter", true, today.toLocalDate(), false));
        assertEquals("The id is illegal.", e.getMessage());

    }

    @Test
    void getRegistrationDate() {
        assertEquals(today.toLocalDate(), person.getRegistrationDate());
    }

    @Test
    void getSignupDate() {
        assertNull(person.getSignupDate());
    }

    @Test
    void setSignupDate() {
        person.setSignupDate(today.toLocalDate());
        assertEquals(today.toLocalDate(), person.getSignupDate());
    }

    @Test
    void getPhotoConsent() {
        assertTrue(person.getPhotoConsent());
    }

    @Test
    void setPhotoConsent() {
        person.setPhotoConsent(false);
        assertFalse(person.getPhotoConsent());
    }


    @Test
    void getName() {
        assertEquals("Anders", person.getName());

    }

    @Test
    void setName01() {
        person.setName("Dunno");
        assertEquals("Dunno", person.getName());
    }

    // Exception test. Not allowed to give a empty string as a name.
    @Test
    void setName02() {
        IllegalFormatException e = assertThrows(IllegalFormatException.class, () -> person.setName(""));

        assertEquals("The name of a person cannot be an empty string.", e.getMessage());
    }

    @Test
    void isMale() {
        assertTrue(person.isMale());
    }

    @Test
    void setGender() {
        person.setGender(false);
        assertFalse(person.isMale());
    }

    @Test
    void getAddressList() {
        assertEquals(2, person.getAddressList().size());
        assertNull(person.getAddressList().get(0));
        assertNull(person.getAddressList().get(1));
    }

    @Test
    void setAddressList01() {
        List<Address> stringList = new ArrayList<>();
        Address address1 = new Address("Kalderupvej 23", "9000 Aalborg");
        Address address2 = new Address("Kalderupvej 19", "9000 Aalborg");
        stringList.add(address1);
        stringList.add(address2);
        person.setAddressList(stringList);
        assertEquals(stringList, person.getAddressList());
    }

    // Exception test. Not allowed to give a list bigger then 2.
    @Test
    void setAddressList02() {
        List<Address> stringList = new ArrayList<>();
        Address address1 = new Address("Kalderupvej 23", "9000 Aalborg");
        Address address2 = new Address("Kalderupvej 19", "9000 Aalborg");
        Address address3 = new Address("Envej 12", "9000 Aalborg");
        stringList.add(address1);
        stringList.add(address2);
        stringList.add(address3);
        IllegalObjectException e = assertThrows(IllegalObjectException.class, () -> person.setAddressList(stringList));
        assertEquals("List of Address objects is too big.", e.getMessage());
    }

    @Test
    void setAddressList03() {
        List<Address> stringList = new ArrayList<>();
        Address address1 = new Address("Kalderupvej 23", "9000 Aalborg");
        stringList.add(address1);
        person.setAddressList(stringList);

        assertEquals(address1, person.getAddressList().get(0));
        assertNull(person.getAddressList().get(1));
    }


    @Test
    void getEmailList() {
        assertEquals(2, person.getEmailList().size());
        assertNull(person.getEmailList().get(0));
        assertNull(person.getEmailList().get(1));

    }


    @Test
    void setEmailList01() {
        List<String> stringList = new ArrayList<>();
        stringList.add("200neder@gmail.com");
        stringList.add("Nedergaden@yahoo.dk");
        person.setEmailList(stringList);
        assertEquals(stringList, person.getEmailList());
    }

    // Exception test. Not allowed to give a list bigger then 2.
    @Test
    void setEmailList02() {
        List<String> stringList = new ArrayList<>();
        stringList.add("200neder@gmail.com");
        stringList.add("Nedergaden@yahoo.dk");
        stringList.add("test@test.com");
        IllegalObjectException e = assertThrows(IllegalObjectException.class, () -> person.setEmailList(stringList));
        assertEquals("List of emails is too big.", e.getMessage());
    }

    @Test
    void setEmailList03() {
        String email = "200neder@gmail.com";
        List<String> stringList = new ArrayList<>();
        stringList.add(email);
        person.setEmailList(stringList);
        assertEquals(email, person.getEmailList().get(0));
        assertNull(person.getEmailList().get(1));
    }

    @Test
    void getPhoneNumberList() {
        assertEquals(2, person.getPhoneNumberList().size());
        assertNull(person.getPhoneNumberList().get(0));
        assertNull(person.getPhoneNumberList().get(1));

    }

    @Test
    void setPhoneNumberList01() {
        List<String> stringList = new ArrayList<>();
        stringList.add("29682898");
        stringList.add("65897845");
        person.setPhoneNumberList(stringList);
        assertEquals(stringList, person.getPhoneNumberList());
    }


    // Exception test. Not allowed to give a list bigger then 2.
    @Test
    void setPhoneNumberList02() {
        List<String> stringList = new ArrayList<>();
        stringList.add("29682898");
        stringList.add("65897845");
        stringList.add("23456780");
        IllegalObjectException e = assertThrows(IllegalObjectException.class, () -> person.setPhoneNumberList(stringList));
        assertEquals("List of phone numbers is too big.", e.getMessage());
    }

    @Test
    void setPhoneNumberList03() {
        String number = "29682898";
        List<String> stringList = new ArrayList<>();
        stringList.add(number);
        person.setPhoneNumberList(stringList);

        assertEquals(number, person.getPhoneNumberList().get(0));
        assertNull(person.getPhoneNumberList().get(1));
    }

    @Test
    void getBirthday() {
        assertEquals(null, person.getBirthday());

    }

    @Test
    void setAndGetBirthday() {
        person.setBirthday(today.toLocalDate());
        assertEquals(today.toLocalDate(), person.getBirthday());
    }


    @Test
    void getRole01() {
        assertEquals(null, person.getRole(Person.GYMNAST_INDEX));

    }

    // Exception test. Not allowed to give a value in get role over 2 or under 0.
    @Test
    void getRole02() {
        IndexOutOfBoundsException e = assertThrows(IndexOutOfBoundsException.class, () -> person.getRole(3));
        assertEquals("The index of the Role must not be lower than 0 or higher than 2.", e.getMessage());

    }

    @Test
    void setRole01() {
        person.setRole(gymnast, Person.GYMNAST_INDEX);
        assertEquals(gymnast, person.getRole(Person.GYMNAST_INDEX));
    }

    // Exception test. Not allowed to give a value in setRole over 2.
    @Test
    void setRole02() {
        IndexOutOfBoundsException e = assertThrows(IndexOutOfBoundsException.class, () -> person.setRole(gymnast, 3));
        assertEquals("There is no such index in the role list.", e.getMessage());
    }

    // Exception test. Not allowed to give a null value in setRole .
    @Test
    void setRole03() {
        person.setRole(gymnast, Person.GYMNAST_INDEX);
        IllegalObjectException exception = assertThrows(IllegalObjectException.class, () -> person.setRole(null, Person.GYMNAST_INDEX));
        assertEquals("Index does not match the Role object or the person already has such a Role object", exception.getMessage());
    }

    // Exception test. Not allowed to give a value in setRole that dont match the index.
    @Test
    void setRole04() {
        IllegalObjectException e = assertThrows(IllegalObjectException.class, () -> person.setRole(gymnast, Person.COACH_INDEX));
        assertEquals("Index does not match the Role object or the person already has such a Role object", e.getMessage());
    }

    // Exception test. Not allowed to give a value in setRole that dont match the person its put into
    @Test
    void setRole05() {
        Person personPeter = new Person("Peter", true, today.toLocalDate(), true);
        Gymnast gymnastPeter = new Gymnast(personPeter, "223344", true);
        IllegalObjectException e = assertThrows(IllegalObjectException.class, () -> person.setRole(gymnastPeter, Person.GYMNAST_INDEX));
        assertEquals("Index does not match the Role object or the person already has such a Role object", e.getMessage());
    }

    // Exception test. Not allowed to give a value in setRole if there already is one at the given index.
    @Test
    void setRole06() {
        person.setRole(gymnast, Person.GYMNAST_INDEX);
        IllegalObjectException e = assertThrows(IllegalObjectException.class, () -> person.setRole(gymnast, Person.GYMNAST_INDEX));
        assertEquals("Index does not match the Role object or the person already has such a Role object", e.getMessage());
    }

    @Test
    void getPersonSignupList() {
        assertTrue(person.getPersonSignupList().isEmpty());

    }

    @Test
    void addPersonSignup01() {
        person.addPersonSignup(personSignup);
        assertEquals(personSignup, person.getPersonSignupList().get(0));
    }

    // Exception test. Not allowed to give a value in addPersonSignup if it already has it
    @Test
    void addPersonSignup02() {
        person.addPersonSignup(personSignup);
        IllegalObjectException e = assertThrows(IllegalObjectException.class, () -> person.addPersonSignup(personSignup));
        assertEquals("Field person in the PersonSignup object did not match the Person object or Person object already has such a PersonSignup object.", e.getMessage());
    }

    @Test
    void deletePersonSignup01(){
        person.addPersonSignup(personSignup);
        person.deletePersonSignup(personSignup);
        assertTrue(person.getPersonSignupList().isEmpty());
    }

    // Exception test. Not allowed to give a value in deletePersonSignup if the personSignup does not exist.
    @Test
    void deletePersonSignup02(){
        ObjectNotFoundException e = assertThrows(ObjectNotFoundException.class,()->person.deletePersonSignup(personSignup));
        assertEquals("The PersonSignup object was not found in the Person object.",e.getMessage());
    }

    @Test
    void deletePersonSignupsToFutureEvents(){
        event1.setStartDate(today.plusDays(5));
        person.setRole(gymnast, Person.GYMNAST_INDEX);

        person.addPersonSignup(personSignup);
        event1.addPersonSignup(personSignup);

        personSignup.addTeamSignup(teamSignup);
        teamSignup.addPersonSignup(personSignup);

        person.deletePersonSignupsToFutureEvents(gymnast);

        assertFalse(event1.getPersonsSignedUp().contains(person));
        assertEquals(0, person.getPersonSignupList().size());
        assertEquals(0, teamSignup.getPersonSignupList().size());

    }
    @Test
    void isGymnast01(){
        assertFalse(person.isGymnast());
    }

    @Test
    void isGymnast02() {
        person.setRole(gymnast, Person.GYMNAST_INDEX);
        assertTrue(person.isGymnast());
    }

    @Test
    void isCoach01(){
        assertFalse(person.isCoach());

    }
    @Test
    void isCoach02() {
        Coach coach = new Coach(person, true);
        person.setRole(coach, Person.COACH_INDEX);
        assertTrue(person.isCoach());
    }

    @Test
    void isJudge01(){
        assertFalse(person.isJudge());

    }

    @Test
    void isJudge02() {
        Judge judge = new Judge(person, true);
        person.setRole(judge, Person.JUDGE_INDEX);
        assertTrue(person.isJudge());
    }

    @Test
    void delete01() {
        person.setRole(gymnast, Person.GYMNAST_INDEX);
        TeamAssignment teamAssignment = new TeamAssignment(team, gymnast, true);
        person.addPersonSignup(personSignup);
        event1.addPersonSignup(personSignup);

        personSignup.addTeamSignup(teamSignup);
        teamSignup.addPersonSignup(personSignup);

        team.addTeamAssignment(teamAssignment);
        gymnast.addTeamAssignment(teamAssignment);

        event1.addCar(person);

        person.delete();

        assertFalse(event1.getPersonsSignedUp().contains(person));
        assertFalse(team.getTeamAssignmentList().contains(teamAssignment));
        assertTrue(team.getTeamSignupList().isEmpty());
        assertEquals(4, event1.getCarList().get(0).getAvailableSeatCount());

        assertEquals(1, person.getPersonSignupList().size());
        assertEquals(1,person.getCarAssignmentList().size());
        assertEquals(1,gymnast.getTeamAssignmentList().size());
        assertEquals(1, personSignup.getTeamSignupList().size());
    }

    //
    @Test
    void delete02() {
        Coach coachTest = new Coach(person,true);
        PersonSignup personSignupTest = new PersonSignup(coachTest,event1);
        person.setRole(coachTest, Person.COACH_INDEX);
        TeamAssignment teamAssignment = new TeamAssignment(team, coachTest, true);
        person.addPersonSignup(personSignupTest);
        event1.addPersonSignup(personSignupTest);

        team.addTeamAssignment(teamAssignment);
        coachTest.addTeamAssignment(teamAssignment);

        event1.addCar(person);

        person.delete();

        assertFalse(event1.getPersonsSignedUp().contains(person));
        assertFalse(team.getTeamAssignmentList().contains(teamAssignment));
        assertEquals(4, event1.getCarList().get(0).getAvailableSeatCount());

        assertEquals(1, person.getPersonSignupList().size());
        assertEquals(1,person.getCarAssignmentList().size());
        assertEquals(1,coachTest.getTeamAssignmentList().size());
    }

    @Test
    void delete03() {
        Coach coachTest = new Coach(person,true);
        PersonSignup personSignupTest = new PersonSignup(coachTest,event1);
        person.setRole(coachTest, Person.COACH_INDEX);
        TeamAssignment teamAssignmentTest = new TeamAssignment(team, coachTest, true);

        person.addPersonSignup(personSignupTest);
        event1.addPersonSignup(personSignupTest);

        team.addTeamAssignment(teamAssignmentTest);
        coachTest.addTeamAssignment(teamAssignmentTest);


        person.setRole(gymnast, Person.GYMNAST_INDEX);
        TeamAssignment teamAssignment = new TeamAssignment(team, gymnast, true);

        person.addPersonSignup(personSignup);
        event1.addPersonSignup(personSignup);

        personSignup.addTeamSignup(teamSignup);
        teamSignup.addPersonSignup(personSignup);

        team.addTeamAssignment(teamAssignment);
        gymnast.addTeamAssignment(teamAssignment);

        event1.addCar(person);

        person.delete();


        assertFalse(event1.getPersonsSignedUp().contains(person));
        assertFalse(team.getTeamAssignmentList().contains(teamAssignmentTest));
        assertFalse(team.getTeamAssignmentList().contains(teamAssignment));
        assertTrue(team.getTeamSignupList().isEmpty());
        assertEquals(4, event1.getCarList().get(0).getAvailableSeatCount());

        assertEquals(2, person.getPersonSignupList().size());
        assertEquals(1,person.getCarAssignmentList().size());
        assertEquals(1,gymnast.getTeamAssignmentList().size());
        assertEquals(1, personSignup.getTeamSignupList().size());
        assertEquals(1,coachTest.getTeamAssignmentList().size());

    }

    @Test
    void getCarAssignmentList(){
        assertTrue(person.getCarAssignmentList().isEmpty());
    }

    @Test
    void addCarAssignment01(){
        CarAssignment carAssignmentTest = new CarAssignment(false,person,new Car(event1));
        person.addCarAssignment(carAssignmentTest);
        assertEquals(1,person.getCarAssignmentList().size());
    }

    // Exception test. Not allowed to give a value in addCarAssignment that match a carAssignment that already exist.
    @Test
    void addCarAssignment02(){
        CarAssignment carAssignmentTest = new CarAssignment(false,person,new Car(event1));
        person.addCarAssignment(carAssignmentTest);
        IllegalObjectException e = assertThrows(IllegalObjectException.class,()->person.addCarAssignment(carAssignmentTest));
        assertEquals("The CarAssignment object cannot be added to the list in the Person object.",e.getMessage());
    }

    // Exception test. Not allowed to give a value in addCarAssignment if it does not match the person its added to.
    @Test
    void addCarAssignment03(){
        Person personPeter = new Person("Peter",true,today.toLocalDate(),true);
        CarAssignment carAssignmentTest = new CarAssignment(false,personPeter,new Car(event1));
        IllegalObjectException e = assertThrows(IllegalObjectException.class,()->person.addCarAssignment(carAssignmentTest));
        assertEquals("The CarAssignment object cannot be added to the list in the Person object.",e.getMessage());

    }

    @Test
    void deleteCarAssignment01(){
        CarAssignment carAssignmentTest = new CarAssignment(false,person,new Car(event1));
        person.addCarAssignment(carAssignmentTest);
        person.deleteCarAssignment(carAssignmentTest);
        assertTrue(person.getCarAssignmentList().isEmpty());
    }

    // Exception test. Not allowed to give a value in deleteCarAssignment if the carAssignment does not exist in person.
    @Test
    void deleteCarAssignment02(){
        CarAssignment carAssignmentTest = new CarAssignment(false,person,new Car(event1));
        ObjectNotFoundException e = assertThrows(ObjectNotFoundException.class,()->person.deleteCarAssignment(carAssignmentTest));
        assertEquals("The CarAssignment object was not found in the Person object.",e.getMessage());

    }


    @Test
    void getPersonSignupsSavedAndNotSaved() {
        List<PersonSignup> saved = new ArrayList<>();
        List<PersonSignup> notSaved = new ArrayList<>();

        person.addPersonSignup(new PersonSignup(1,gymnast,new Event("Dancup1",address,today,today.toLocalDate(),season,judgeAndCoach,judgeAndCoach),PersonSignupState.SIGNED_UP));
        person.addPersonSignup(new PersonSignup(2,gymnast,new Event("Dancup2",address,today,today.toLocalDate(),season,judgeAndCoach,judgeAndCoach),PersonSignupState.SIGNED_UP));
        person.addPersonSignup(new PersonSignup(gymnast,new Event("Dancup3",address,today,today.toLocalDate(),season,judgeAndCoach,judgeAndCoach)));
        person.addPersonSignup(new PersonSignup(4,gymnast,new Event("Dancup4",address,today,today.toLocalDate(),season,judgeAndCoach,judgeAndCoach),PersonSignupState.SIGNED_UP));
        person.addPersonSignup(new PersonSignup(gymnast,new Event("Dancup5",address,today,today.toLocalDate(),season,judgeAndCoach,judgeAndCoach)));
        person.addPersonSignup(new PersonSignup(6,gymnast,new Event("Dancup6",address,today,today.toLocalDate(),season,judgeAndCoach,judgeAndCoach),PersonSignupState.SIGNED_UP));
        person.addPersonSignup(new PersonSignup(5,gymnast,new Event("Dancup7",address,today,today.toLocalDate(),season,judgeAndCoach,judgeAndCoach),PersonSignupState.SIGNED_UP));

        person.getPersonSignupsSavedAndNotSaved(saved,notSaved);

        assertEquals(5,saved.size());
        assertEquals(2,notSaved.size());


    }


    @Test
    void equals01() {
        Person person1 = new Person("Anders", true, today.toLocalDate(), true);
        assertTrue(person1.equals(person));
    }

    @Test
    void equals02() {
        Person person1 = new Person("Donners", true, today.toLocalDate(), true);
        assertFalse(person1.equals(person));
    }

    @Test
    void equals03() {
        Person person1 = new Person("Anker", true, today.toLocalDate(), true);
        assertFalse(person1.equals(person));
    }

    @Test
    void equals04() {
        assertEquals(person,person);

    }

    @Test
    void equals05(){
        assertNotEquals(person, event1);

    }

    @Test
    void equals06(){
        assertNotEquals(person,null);
    }

    @Test
    void hashCode01(){
        Person personTest = new Person("Anders", true, today.toLocalDate(), true);
        assertEquals(person.hashCode(), personTest.hashCode());

    }

    @Test
    void hashCode02(){
        Person personTest = new Person("Peter", true, today.toLocalDate(), true);
        assertNotEquals(person.hashCode(), personTest.hashCode());

    }
}
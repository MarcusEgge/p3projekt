package Model;

import Model.Exceptions.DatabaseErrorException;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class DatabaseManagerTest {
    private static DatabaseManager databaseManager = new DatabaseManager();

    static private List<Person> personList1 = new ArrayList<>(), personList2 = new ArrayList<>(), personList3 = new ArrayList<>();
    static private List<Season> seasonList1 = new ArrayList<>(), seasonList2 = new ArrayList<>(), seasonList3 = new ArrayList<>();
    static private List<Team> teamList1 = new ArrayList<>(), teamList2 = new ArrayList<>(), teamList3 = new ArrayList<>();

    private static LocalDateTime nowDT = LocalDateTime.now();
    private static LocalDate nowD = LocalDate.now();

    private static LocalDateTime eventStartDate = nowDT.minusDays(30);
    private static LocalDate eventEndDate = nowD.minusDays(29);

    @BeforeAll
    static void beforeAll() {
        List<Season> seasonListTemp = new ArrayList<>();
        List<Address> addrList = new ArrayList<>();
        List<String> emailList = new ArrayList<>(), phoneList = new ArrayList<>();
        List<String> gymTeamNames = new ArrayList<>(), coaTeamNames = new ArrayList<>();

        // Clearing existing data from the instance of Club and the database.
        Club.INSTANCE.clear();
        databaseManager.deleteAllDataFromDatabase();

        // Saving different type of objects.
        preparePersonData(addrList, emailList, phoneList, gymTeamNames, coaTeamNames);

        Club.INSTANCE.createSeason();
        Club.INSTANCE.createSeason();

        Club.INSTANCE.createTeam("Team 1");
        Club.INSTANCE.viewSeasons().get(0).createEvent("Event 1", new Address("Road One", "City One"), eventStartDate.format(Club.DATE_TIME_FORMATTER), eventEndDate.format(Club.DATE_FORMATTER),5, 3);
        Club.INSTANCE.createPerson("Person 1", true, addrList, emailList, phoneList, "", "", true, false, false, true, null, new ArrayList<>(), new ArrayList<>());

        Club.INSTANCE.createTeam("Team 2");
        Club.INSTANCE.createPerson("Person 2", false, new ArrayList<>(), new ArrayList<>(), new ArrayList<>(), "1998-02-23", nowD.plusMonths(1).format(Club.DATE_FORMATTER), false, true, true, false, "0123456789", gymTeamNames, coaTeamNames);
        Club.INSTANCE.viewSeasons().get(0).createEvent("Event 2", new Address("Road Two", "City Two"), eventStartDate.plusDays(60).format(Club.DATE_TIME_FORMATTER), eventEndDate.plusDays(60).format(Club.DATE_FORMATTER),2, 2);

        Club.INSTANCE.createTeam("Team 3");

        databaseManager.saveCarData(new Car(Club.INSTANCE.viewSeasons().get(0).viewEvent(2), Club.INSTANCE.viewPerson(1)));

        databaseManager.loadAllData(seasonList1, personList1, teamList1);

        // Updating an 'Event' object and both 'Person' objects.
        Club.INSTANCE.viewSeasons().get(0).editEvent(2, "Event 2.0", new Address("Road Three", "City Three"), eventStartDate.plusDays(61).format(Club.DATE_TIME_FORMATTER), eventEndDate.plusDays(61).format(Club.DATE_FORMATTER), 5, 5);
        Club.INSTANCE.editPerson(1, "Person 1.0", false, new ArrayList<>(), new ArrayList<>(), new ArrayList<>(), "", "", false, true, true, false, "9876543210", gymTeamNames, coaTeamNames);
        Club.INSTANCE.editPerson(2, "Person 2.0", true, addrList, emailList, phoneList, "1998-02-23", nowD.format(Club.DATE_FORMATTER), true, false, false, true, null, new ArrayList<>(), new ArrayList<>());

        databaseManager.loadAllData(seasonList2, personList2, teamList2);
        databaseManager.loadAllData(seasonListTemp, new ArrayList<>(), new ArrayList<>());

        // Updating a 'PersonSignup' and a 'Person' object.
        gymTeamNames.remove("Team 2");
        coaTeamNames.remove("Team 1");
        Club.INSTANCE.editPerson(1, "Person 1.0", false, new ArrayList<>(), new ArrayList<>(), new ArrayList<>(), "", "", false, true, true, false, "9876543210", gymTeamNames, coaTeamNames);

        seasonListTemp.get(0).viewEvent(2).editPersonSignup(4, PersonSignupState.SIGNED_UP);

        databaseManager.loadAllData(seasonList3, personList3, teamList3);
    }

    private static void preparePersonData(List<Address> addrList, List<String> emailList, List<String> phoneList, List<String> gymTeamNames, List<String> coaTeamNames){
        addrList.add(new Address("Road 1", "1010 City"));
        addrList.add(null);

        emailList.add("person@email.com");
        emailList.add(null);

        phoneList.add("88 88 88 88");
        phoneList.add(null);

        gymTeamNames.add("Team 1");
        gymTeamNames.add("Team 2");

        coaTeamNames.add("Team 1");
        coaTeamNames.add("Team 2");
    }

    @AfterAll
    static void afterAll() {
        Club.INSTANCE.clear();
        databaseManager.deleteAllDataFromDatabase();
    }

    // ---------- Tests related to saving the two 'Season' objects ----------
    @Test
    void saveSeasonData01() {
        Season season1Loaded = seasonList1.get(0), season2Loaded = seasonList1.get(1);

        assertEquals(1, season1Loaded.getID());
        assertEquals(2, season2Loaded.getID());
    }

    @Test
    void saveSeasonData02() {
        Season season1Loaded = seasonList1.get(0), season2Loaded = seasonList1.get(1);

        assertEquals("2018/2019", season1Loaded.getName());
        assertEquals("2019/2020", season2Loaded.getName());
    }

    @Test
    void saveSeasonData03() {
        Season season1Loaded = seasonList1.get(0), season2Loaded = seasonList1.get(1);

        assertEquals("2018-08-01 00:00", season1Loaded.getStartDate().format(Club.DATE_TIME_FORMATTER));
        assertEquals("2019-08-01 00:00", season2Loaded.getStartDate().format(Club.DATE_TIME_FORMATTER));
    }

    @Test
    void saveSeasonData04() {
        Season season1Loaded = seasonList1.get(0), season2Loaded = seasonList1.get(1);

        assertEquals("2019-07-31 23:59", season1Loaded.getEndDate().format(Club.DATE_TIME_FORMATTER));
        assertEquals("2020-07-31 23:59", season2Loaded.getEndDate().format(Club.DATE_TIME_FORMATTER));
    }

    @Test
    void saveSeasonData05() { // A new 'Season' object with a name identical to an existing object should not be saved.
        DatabaseErrorException e = assertThrows(DatabaseErrorException.class, () -> databaseManager.saveSeasonData(new Season(seasonList1.get(0).getStartDate(), seasonList1.get(0).getEndDate())));
        assertEquals("An error occurred while saving data of season 3.", e.getMessage());
        assertEquals("Duplicate entry '2018/2019' for key 'name_UNIQUE'", e.getCause().getMessage());
    }

    // ---------- Tests related to saving the two 'Event' objects ----------
    @Test
    void saveEventData01() { // Number and distribution of 'Event' objects.
        Season season1Loaded = seasonList1.get(0), season2Loaded = seasonList1.get(1);

        assertEquals(2, season1Loaded.viewEvents().size());
        assertTrue(season2Loaded.viewEvents().isEmpty());
    }

    @Test
    void saveEventData02() { // Id of 'Event' objects.
        Event event1Loaded = seasonList1.get(0).viewEvents().get(1), event2Loaded = seasonList1.get(0).viewEvents().get(0);

        assertEquals(1, event1Loaded.getID());
        assertEquals(2, event2Loaded.getID());
    }

    @Test
    void saveEventData03() { // Name of 'Event' objects.
        Event event1Loaded = seasonList1.get(0).viewEvents().get(1), event2Loaded = seasonList1.get(0).viewEvents().get(0);

        assertEquals("Event 1", event1Loaded.getName());
        assertEquals("Event 2", event2Loaded.getName());
    }

    @Test
    void saveEventData04() { // Address of 'Event' objects.
        Event event1Loaded = seasonList1.get(0).viewEvents().get(1), event2Loaded = seasonList1.get(0).viewEvents().get(0);

        assertEquals(new Address("Road One", "City One"), event1Loaded.getAddress());
        assertEquals(new Address("Road Two", "City Two"), event2Loaded.getAddress());

    }

    @Test
    void saveEventData05() { // Start date of 'Event' objects.
        Event event1Loaded = seasonList1.get(0).viewEvents().get(1), event2Loaded = seasonList1.get(0).viewEvents().get(0);

        assertEquals(eventStartDate.format(Club.DATE_TIME_FORMATTER), event1Loaded.getStartDate().format(Club.DATE_TIME_FORMATTER));
        assertEquals(eventStartDate.plusDays(60).format(Club.DATE_TIME_FORMATTER), event2Loaded.getStartDate().format(Club.DATE_TIME_FORMATTER));
    }

    @Test
    void saveEventData06() { // End date of 'Event' objects.
        Event event1Loaded = seasonList1.get(0).viewEvents().get(1), event2Loaded = seasonList1.get(0).viewEvents().get(0);

        assertEquals(eventEndDate.format(Club.DATE_FORMATTER), event1Loaded.getEndDate().format(Club.DATE_FORMATTER));
        assertEquals(eventEndDate.plusDays(60).format(Club.DATE_FORMATTER), event2Loaded.getEndDate().format(Club.DATE_FORMATTER));
    }

    @Test
    void saveEventData07() { // State of 'Event' objects.
        Event event1Loaded = seasonList1.get(0).viewEvents().get(1), event2Loaded = seasonList1.get(0).viewEvents().get(0);

        assertFalse(event1Loaded.isActive());
        assertTrue(event2Loaded.isActive());
    }

    @Test
    void saveEventData08() { // Coach requirement of 'Event' objects.
        Event event1Loaded = seasonList1.get(0).viewEvents().get(1), event2Loaded = seasonList1.get(0).viewEvents().get(0);

        assertEquals(5, event1Loaded.getCoachRequirement());
        assertEquals(2, event2Loaded.getCoachRequirement());
    }

    @Test
    void saveEventData09() { // Judge requirement of 'Event' objects.
        Event event1Loaded = seasonList1.get(0).viewEvents().get(1), event2Loaded = seasonList1.get(0).viewEvents().get(0);

        assertEquals(3, event1Loaded.getJudgeRequirement());
        assertEquals(2, event2Loaded.getJudgeRequirement());
    }

    @Test
    void saveEventData10() {  // Number of 'PersonSignup' objects in each 'Event' object.
        Event event1Loaded = seasonList1.get(0).viewEvents().get(1), event2Loaded = seasonList1.get(0).viewEvents().get(0);

        assertTrue(event1Loaded.getPersonSignupList().isEmpty());
        assertEquals(3, event2Loaded.getPersonSignupList().size());
    }

    @Test
    void saveEventData11() { // Ids of 'PersonSignup' objects in the 'Event' object with id 2.
        List<PersonSignup> personSignupList2 = seasonList1.get(0).viewEvent(2).getPersonSignupList();

        assertEquals(1, personSignupList2.get(0).getID());
        assertEquals(2, personSignupList2.get(1).getID());
        assertEquals(3, personSignupList2.get(2).getID());
    }

    @Test
    void saveEventData12() { // 'Person' objects of 'PersonSignup' objects in the 'Event' object with id 2.
        Person person1Loaded = personList1.get(0), person2Loaded = personList1.get(1);
        List<PersonSignup> personSignupList2 = seasonList1.get(0).viewEvent(2).getPersonSignupList();

        assertTrue(personSignupList2.get(0).getPerson() == person1Loaded);
        assertTrue(personSignupList2.get(1).getPerson() == person2Loaded);
        assertTrue(personSignupList2.get(2).getPerson() == person2Loaded);
    }

    @Test
    void saveEventData13() { // 'Role' objects of 'PersonSignup' objects in the 'Event' object with id 2.
        Person person1Loaded = personList1.get(0), person2Loaded = personList1.get(1);
        List<PersonSignup> personSignupList2 = seasonList1.get(0).viewEvent(2).getPersonSignupList();

        assertTrue(personSignupList2.get(0).getRole() == person1Loaded.getRole(Person.JUDGE_INDEX));
        assertTrue(personSignupList2.get(1).getRole() == person2Loaded.getRole(Person.GYMNAST_INDEX));
        assertTrue(personSignupList2.get(2).getRole() == person2Loaded.getRole(Person.COACH_INDEX));
    }

    @Test
    void saveEventData14() { // States of 'PersonSignup' objects in the 'Event' object with id 2.
        Event event2Loaded = seasonList1.get(0).viewEvent(2);
        List<PersonSignup> personSignupList2 = event2Loaded.getPersonSignupList();

        assertEquals(PersonSignupState.UNDECIDED, personSignupList2.get(0).getState());
        assertEquals(PersonSignupState.NOT_SIGNED_UP, personSignupList2.get(1).getState());
        assertEquals(PersonSignupState.UNDECIDED, personSignupList2.get(2).getState());
    }

    @Test
    void saveEventData15() { // Number of 'TeamSignup' objects in 'PersonSignup' objects in the 'Event' object with id 2.
        List<PersonSignup> personSignupList2 = seasonList1.get(0).viewEvent(2).getPersonSignupList();

        assertTrue(personSignupList2.get(0).getTeamSignupList().isEmpty());
        assertEquals(2, personSignupList2.get(1).getTeamSignupList().size());
        assertTrue(personSignupList2.get(2).getTeamSignupList().isEmpty());
    }

    @Test
    void saveEventData16() { // Number of 'TeamSignup' objects in each 'Event' object.
        Event event1Loaded = seasonList1.get(0).viewEvent(1), event2Loaded = seasonList1.get(0).viewEvent(2);

        assertEquals(1, event1Loaded.viewTeamSignups().size());
        assertEquals(3, event2Loaded.viewTeamSignups().size());
    }

    @Test
    void saveEventData17() {  // Ids of 'TeamSignup' objects in both 'Event' objects.
        Event event1Loaded = seasonList1.get(0).viewEvent(1), event2Loaded = seasonList1.get(0).viewEvent(2);
        List<TeamSignup> teamSignupList1 = event1Loaded.viewTeamSignups();
        List<TeamSignup> teamSignupList2 = event2Loaded.viewTeamSignups();

        assertEquals(1, teamSignupList1.get(0).getID());

        assertEquals(2, teamSignupList2.get(0).getID());
        assertEquals(3, teamSignupList2.get(1).getID());
        assertEquals(4, teamSignupList2.get(2).getID());
    }

    @Test
    void saveEventData18() { // 'Team' objects of 'TeamSignup' objects in both 'Event' objects.
        Event event1Loaded = seasonList1.get(0).viewEvent(1), event2Loaded = seasonList1.get(0).viewEvent(2);
        Team team1Loaded = teamList1.get(0), team2Loaded = teamList1.get(1), team3Loaded = teamList1.get(2);
        List<TeamSignup> teamSignupList1 = event1Loaded.viewTeamSignups(), teamSignupList2 = event2Loaded.viewTeamSignups();

        assertTrue(team1Loaded == teamSignupList1.get(0).getTeam());

        assertTrue(team1Loaded == teamSignupList2.get(0).getTeam());
        assertTrue(team2Loaded == teamSignupList2.get(1).getTeam());
        assertTrue(team3Loaded == teamSignupList2.get(2).getTeam());
    }

    @Test
    void saveEventData19() { // Number of 'PersonSignup' objects in 'TeamSignup' objects in both 'Event' objects.
        Event event1Loaded = seasonList1.get(0).viewEvent(1), event2Loaded = seasonList1.get(0).viewEvent(2);
        List<TeamSignup> teamSignupList1 = event1Loaded.viewTeamSignups(), teamSignupList2 = event2Loaded.viewTeamSignups();

        assertTrue(teamSignupList1.get(0).getPersonSignupList().isEmpty());

        assertEquals(1, teamSignupList2.get(0).getPersonSignupList().size());
        assertEquals(1, teamSignupList2.get(1).getPersonSignupList().size());
        assertTrue(teamSignupList2.get(2).getPersonSignupList().isEmpty());
    }

    @Test
    void saveEventData20() { // Number of 'Car' objects in both 'Event' objects.
        Event event1Loaded = seasonList1.get(0).viewEvent(1), event2Loaded = seasonList1.get(0).viewEvent(2);

        assertTrue(event1Loaded.getCarList().isEmpty());
        assertEquals(1, event2Loaded.getCarList().size());
    }

    @Test
    void saveEventData21() { // Id of 'Car' object in the 'Event' object with id 2.
        Event event2Loaded = seasonList1.get(0).viewEvent(2);
        Car car1 = event2Loaded.getCarList().get(0);

        assertEquals(1, car1.getID());
    }

    // ---------- Tests related to saving the two 'Person' objects ----------
    @Test
    void savePersonData01() { // Number of 'Person' objects.
        assertEquals(2, personList1.size());
    }

    @Test
    void savePersonData02() { // Ids of the 'Person' objects.
        Person person1Loaded = personList1.get(0), person2Loaded = personList1.get(1);

        assertEquals(1, person1Loaded.getID());
        assertEquals(2, person2Loaded.getID());
    }

    @Test
    void savePersonData03() { // Names of the 'Person' objects.
        Person person1Loaded = personList1.get(0), person2Loaded = personList1.get(1);

        assertEquals("Person 1", person1Loaded.getName());
        assertEquals("Person 2", person2Loaded.getName());
    }

    @Test
    void savePersonData04() { // Genders of the 'Person' objects.
        Person person1Loaded = personList1.get(0), person2Loaded = personList1.get(1);

        assertTrue(person1Loaded.isMale());
        assertFalse(person2Loaded.isMale());
    }

    @Test
    void savePersonData05() {  // Field 'photoConsent' of the 'Person' objects.
        Person person1Loaded = personList1.get(0), person2Loaded = personList1.get(1);

        assertTrue(person1Loaded.getPhotoConsent());
        assertFalse(person2Loaded.getPhotoConsent());
    }

    @Test
    void savePersonData06() { // Field 'birthday' of the 'Person' objects.
        Person person1Loaded = personList1.get(0), person2Loaded = personList1.get(1);

        assertNull(person1Loaded.getBirthday());
        assertEquals("1998-02-23", person2Loaded.getBirthday().format(Club.DATE_FORMATTER));
    }

    @Test
    void savePersonData07() { // Field 'registrationDate' of the 'Person' objects.
        Person person1Loaded = personList1.get(0), person2Loaded = personList1.get(1);

        assertEquals(nowD.format(Club.DATE_FORMATTER), person1Loaded.getRegistrationDate().format(Club.DATE_FORMATTER));
        assertEquals(nowD.format(Club.DATE_FORMATTER), person2Loaded.getRegistrationDate().format(Club.DATE_FORMATTER));
    }

    @Test
    void savePersonData08() { // Field 'signupDate' of the 'Person' objects.
        Person person1Loaded = personList1.get(0), person2Loaded = personList1.get(1);

        assertNull(person1Loaded.getSignupDate());
        assertEquals(nowD.plusMonths(1).format(Club.DATE_FORMATTER), person2Loaded.getSignupDate().format(Club.DATE_FORMATTER));
    }

    @Test
    void savePersonData09() { // Field 'addressList' of the 'Person' objects.
        Person person1Loaded = personList1.get(0), person2Loaded = personList1.get(1);

        assertEquals(new Address("Road 1", "1010 City"), person1Loaded.getAddressList().get(0));
        assertNull(person1Loaded.getAddressList().get(1));

        assertNull(person2Loaded.getAddressList().get(0));
        assertNull(person2Loaded.getAddressList().get(1));
    }

    @Test
    void savePersonData10() { // Field 'emailList' of the 'Person' objects.
        Person person1Loaded = personList1.get(0), person2Loaded = personList1.get(1);

        assertEquals("person@email.com", person1Loaded.getEmailList().get(0));
        assertNull(person1Loaded.getEmailList().get(1));

        assertNull(person2Loaded.getEmailList().get(0));
        assertNull(person2Loaded.getEmailList().get(1));
    }

    @Test
    void savePersonData11() {  // Field 'phoneNumberList' of the 'Person' objects.
        Person person1Loaded = personList1.get(0), person2Loaded = personList1.get(1);

        assertEquals("88 88 88 88", person1Loaded.getPhoneNumberList().get(0));
        assertNull(person1Loaded.getPhoneNumberList().get(1));

        assertNull(person2Loaded.getPhoneNumberList().get(0));
        assertNull(person2Loaded.getPhoneNumberList().get(1));
    }

    @Test
    void savePersonData12() {  // Number of 'PersonSignup' objects in each 'Person' object.
        Person person1Loaded = personList1.get(0), person2Loaded = personList1.get(1);

        assertEquals(1, person1Loaded.getPersonSignupList().size());
        assertEquals(2, person2Loaded.getPersonSignupList().size());
    }

    @Test
    void savePersonData13() { // Asserting that 'Event' and 'Person' objects contain the same 'PersonSignup' objects.
        Person person1Loaded = personList1.get(0), person2Loaded = personList1.get(1);
        Event event2 = seasonList1.get(0).viewEvent(2);

        assertTrue(event2.getPersonSignupList().get(0) == person1Loaded.getPersonSignupList().get(0));

        assertTrue(event2.getPersonSignupList().get(1) == person2Loaded.getPersonSignupList().get(0));
        assertTrue(event2.getPersonSignupList().get(2) == person2Loaded.getPersonSignupList().get(1));
    }

    @Test
    void savePersonData14() { // Asserting that 'Event' and 'PersonSignup' objects contain the same 'TeamSignup' objects.
        Person person2Loaded = personList1.get(1);
        Event event2 = seasonList1.get(0).viewEvent(2);
        TeamSignup teamSignup1 = person2Loaded.getPersonSignupList().get(0).getTeamSignupList().get(0);
        TeamSignup teamSignup2 = person2Loaded.getPersonSignupList().get(0).getTeamSignupList().get(1);

        assertTrue(event2.viewTeamSignups().get(0) == teamSignup1);
        assertTrue(event2.viewTeamSignups().get(1) == teamSignup2);
    }

    @Test
    void savePersonData15() { // Number of 'CarAssignment' objects in each 'Person' object.
        Person person1Loaded = personList1.get(0), person2Loaded = personList1.get(1);

        assertEquals(1, person1Loaded.getCarAssignmentList().size());
        assertTrue(person2Loaded.getCarAssignmentList().isEmpty());
    }

    @Test
    void savePersonData16() { // Asserting that the right 'Role' objects are null.
        Person person1Loaded = personList1.get(0), person2Loaded = personList1.get(1);

        assertNull(person1Loaded.getRole(Person.GYMNAST_INDEX));
        assertNotNull(person2Loaded.getRole(Person.GYMNAST_INDEX));

        assertNull(person1Loaded.getRole(Person.COACH_INDEX));
        assertNotNull(person2Loaded.getRole(Person.COACH_INDEX));

        assertNotNull(person1Loaded.getRole(Person.JUDGE_INDEX));
        assertNull(person2Loaded.getRole(Person.JUDGE_INDEX));
    }

    @Test
    void savePersonData17() { // Ids of all 'Role' objects.
        Person person1Loaded = personList1.get(0), person2Loaded = personList1.get(1);
        Role judge1 = person1Loaded.getRole(Person.JUDGE_INDEX);
        Role gymnast2 = person2Loaded.getRole(Person.GYMNAST_INDEX);
        Role coach2 = person2Loaded.getRole(Person.COACH_INDEX);

        assertEquals(1, judge1.getID());

        assertEquals(1, gymnast2.getID());
        assertEquals(1, coach2.getID());
    }

    @Test
    void savePersonData18() { // Fields of the 'Gymnast' object in the 'Person' object with id 2.
        Person person2Loaded = personList1.get(1);
        Gymnast gymnast2 = (Gymnast) person2Loaded.getRole(Person.GYMNAST_INDEX);

        assertEquals("0123456789", gymnast2.getLicense());
        assertTrue(gymnast2.isActive());
        assertEquals(3, gymnast2.getTeamAssignmentList().size());
    }

    @Test
    void savePersonData19() { // 'TeamAssignment' objects of the 'Gymnast' in the 'Person' with id 2.
        Person person2Loaded = personList1.get(1);
        Gymnast gymnast2 = (Gymnast) person2Loaded.getRole(Person.GYMNAST_INDEX);
        List<TeamAssignment> teamAssignmentList2 = gymnast2.getTeamAssignmentList();

        assertTrue(teamList1.get(0) == teamAssignmentList2.get(0).getTeam());
        assertTrue(teamAssignmentList2.get(0).isSignedUp());

        assertTrue(teamList1.get(1) == teamAssignmentList2.get(1).getTeam());
        assertTrue(teamAssignmentList2.get(1).isSignedUp());

        assertTrue(teamList1.get(2) == teamAssignmentList2.get(2).getTeam());
        assertFalse(teamAssignmentList2.get(2).isSignedUp());
    }

    @Test
    void savePersonData20() {  // Fields of the 'Coach' in the 'Person' with id 2.
        Person person2Loaded = personList1.get(1);
        Coach coach2 = (Coach)person2Loaded.getRole(Person.COACH_INDEX);

        assertEquals(1, coach2.getID());
        assertTrue(coach2.isActive());
        assertEquals(3, coach2.getTeamAssignmentList().size());
    }

    @Test
    void savePersonData21() { // 'TeamAssignment' objects of the 'Coach' in the 'Person' with id 2.
        Person person2Loaded = personList1.get(1);
        Coach coach2 = (Coach)person2Loaded.getRole(Person.COACH_INDEX);
        List<TeamAssignment> teamAssignmentList2 = coach2.getTeamAssignmentList();

        assertEquals(teamList1.get(0), teamAssignmentList2.get(0).getTeam());
        assertTrue(teamAssignmentList2.get(0).isSignedUp());

        assertEquals(teamList1.get(1), teamAssignmentList2.get(1).getTeam());
        assertTrue(teamAssignmentList2.get(1).isSignedUp());

        assertEquals(teamList1.get(2), teamAssignmentList2.get(2).getTeam());
        assertFalse(teamAssignmentList2.get(2).isSignedUp());
    }

    @Test
    void savePersonData22() {   // Fields of the 'Judge' in the 'Person' with id 1.
        Person person1Loaded = personList1.get(0);
        Role judge1 = person1Loaded.getRole(Person.JUDGE_INDEX);

        assertEquals(1, judge1.getID());
        assertTrue(judge1.isActive());
    }

    @Test
    void savePersonData23() { // Any attempt to save a 'Person' with a name that already exist in the database should be hindered.
        DatabaseErrorException e = assertThrows(DatabaseErrorException.class, () ->
                databaseManager.savePersonData(new Person("Person 2.0", true, nowD, false)));
        assertEquals("An error occurred while saving data of person 3.", e.getMessage());
        assertEquals("Duplicate entry 'Person 2.0' for key 'personcol_UNIQUE'", e.getCause().getMessage());
    }

    // ---------- Tests related to saving the two 'Team' objects ----------
    @Test
    void saveTeamData01() {  // Number of 'Team' objects.
        assertEquals(3, teamList1.size());
    }

    @Test
    void saveTeamData02() { // Ids of 'Team' objects.
        assertEquals(1, teamList1.get(0).getID());
        assertEquals(2, teamList1.get(1).getID());
        assertEquals(3, teamList1.get(2).getID());
    }

    @Test
    void saveTeamData03() { // Names of 'Team' objects.
        assertEquals("Team 1", teamList1.get(0).getName());
        assertEquals("Team 2", teamList1.get(1).getName());
        assertEquals("Team 3", teamList1.get(2).getName());
    }

    @Test
    void saveTeamData04() { // Number of 'TeamAssignment' objects in each 'Team'.
        assertEquals(2, teamList1.get(0).getTeamAssignmentList().size());
        assertEquals(2, teamList1.get(1).getTeamAssignmentList().size());
        assertEquals(2, teamList1.get(2).getTeamAssignmentList().size());
    }

    @Test
    void saveTeamData05() {  // Ids of 'TeamAssignment' objects in both 'Team' objects.
        List<TeamAssignment> teamAssignmentList1 = teamList1.get(0).getTeamAssignmentList();
        List<TeamAssignment> teamAssignmentList2 = teamList1.get(1).getTeamAssignmentList();
        List<TeamAssignment> teamAssignmentList3 = teamList1.get(2).getTeamAssignmentList();

        assertEquals(1, teamAssignmentList1.get(0).getID());
        assertEquals(3, teamAssignmentList1.get(1).getID());

        assertEquals(2, teamAssignmentList2.get(0).getID());
        assertEquals(4, teamAssignmentList2.get(1).getID());

        assertEquals(5, teamAssignmentList3.get(0).getID());
        assertEquals(6, teamAssignmentList3.get(1).getID());
    }

    @Test
    void saveTeamData06() { // Asserting that the 'Role' and 'Team' objects contain the same 'TeamAssignment' objects.
        Person person2Loaded = personList1.get(1);
        TeamMember gymnast2 = (TeamMember) person2Loaded.getRole(Person.GYMNAST_INDEX);
        TeamMember coach2 = (TeamMember) person2Loaded.getRole(Person.COACH_INDEX);
        List<TeamAssignment> teamAssignmentList1 = teamList1.get(0).getTeamAssignmentList();
        List<TeamAssignment> teamAssignmentList2 = teamList1.get(1).getTeamAssignmentList();
        List<TeamAssignment> teamAssignmentList3 = teamList1.get(2).getTeamAssignmentList();

        assertTrue(teamAssignmentList1.get(0) == gymnast2.getTeamAssignmentList().get(0));
        assertTrue(teamAssignmentList1.get(1) == coach2.getTeamAssignmentList().get(0));

        assertTrue(teamAssignmentList2.get(0) == gymnast2.getTeamAssignmentList().get(1));
        assertTrue(teamAssignmentList2.get(1) == coach2.getTeamAssignmentList().get(1));

        assertTrue(teamAssignmentList3.get(0) == gymnast2.getTeamAssignmentList().get(2));
        assertTrue(teamAssignmentList3.get(1) == coach2.getTeamAssignmentList().get(2));
    }

    @Test
    void saveTeamData07() { // Number of 'TeamSignup' objects in each 'Team' object.
        Team team1Loaded = teamList1.get(0), team2Loaded = teamList1.get(1), team3Loaded = teamList1.get(2);

        assertEquals(2, team1Loaded.getTeamSignupList().size());
        assertEquals(1, team2Loaded.getTeamSignupList().size());
        assertEquals(1, team3Loaded.getTeamSignupList().size());
    }

    @Test
    void saveTeamData08() { // Asserting that 'Team' and 'Event' objects contain the same 'TeamSignup' objects.
        Event event1Loaded = seasonList1.get(0).viewEvent(1), event2Loaded = seasonList1.get(0).viewEvent(2);
        List<TeamSignup> teamSignupList1 = teamList1.get(0).getTeamSignupList();
        List<TeamSignup> teamSignupList2 = teamList1.get(1).getTeamSignupList();
        List<TeamSignup> teamSignupList3 = teamList1.get(2).getTeamSignupList();

        assertTrue(teamSignupList1.get(0) == event1Loaded.viewTeamSignups().get(0));
        assertTrue(teamSignupList1.get(1) == event2Loaded.viewTeamSignups().get(0));

        assertTrue(teamSignupList2.get(0) == event2Loaded.viewTeamSignups().get(1));

        assertTrue(teamSignupList3.get(0) == event2Loaded.viewTeamSignups().get(2));
    }

    @Test
    void saveTeamData09() { // Any attempt to save a 'Team' with a name that already exist in the database should be hindered.
        DatabaseErrorException e = assertThrows(DatabaseErrorException.class, () -> databaseManager.saveTeamData(new Team("Team 1")));
        assertEquals("An error occurred while saving data of team 4.", e.getMessage());
        assertEquals("Duplicate entry 'Team 1' for key 'name_UNIQUE'", e.getCause().getMessage());
    }

    // ---------- Testing method 'saveCarData' ----------
    @Test
    void saveCarData() {
        Car carLoaded = seasonList1.get(0).viewEvent(2).getCarList().get(0);
        Person person1Loaded = personList1.get(0);

        assertEquals(1, carLoaded.getID());
        assertEquals(1, carLoaded.getCarAssignmentList().size());
        assertTrue(carLoaded.getCarAssignmentList().get(0) == person1Loaded.getCarAssignmentList().get(0));
    }

    // ---------- Tests related to updating the two 'Person' objects ----------
    @Test
    void updatePersonData01() { // Ids of the 'Person' objects after first update.
        Person person1Updated = personList2.get(1), person2Updated = personList2.get(0);

        assertEquals(1, person1Updated.getID());
        assertEquals(2, person2Updated.getID());
    }

    @Test
    void updatePersonData02() { // Names of the 'Person' objects after first update.
        Person person1Updated = personList2.get(1), person2Updated = personList2.get(0);

        assertEquals("Person 1.0", person1Updated.getName());
        assertEquals("Person 2.0", person2Updated.getName());
    }

    @Test
    void updatePersonData03() { // Genders of the 'Person' objects after first update.
        Person person1Updated = personList2.get(1), person2Updated = personList2.get(0);

        assertFalse(person1Updated.isMale());
        assertTrue(person2Updated.isMale());
    }
    @Test
    void updatePersonData04() { // Registration dates of the 'Person' objects after first update.
        Person person1Updated = personList2.get(1), person2Updated = personList2.get(0);

        assertEquals(nowD.format(Club.DATE_FORMATTER), person1Updated.getRegistrationDate().format(Club.DATE_FORMATTER));
        assertEquals(nowD.format(Club.DATE_FORMATTER), person2Updated.getRegistrationDate().format(Club.DATE_FORMATTER));
    }

    @Test
    void updatePersonData05() { // Field 'photoConsent' of the 'Person' objects after first update.
        Person person1Updated = personList2.get(1), person2Updated = personList2.get(0);

        assertFalse(person1Updated.getPhotoConsent());
        assertTrue(person2Updated.getPhotoConsent());
    }

    @Test
    void updatePersonData06() { // Birthdays' of the 'Person' objects after first update.
        Person person1Updated = personList2.get(1), person2Updated = personList2.get(0);

        assertNull(person1Updated.getBirthday());
        assertEquals("1998-02-23", person2Updated.getBirthday().format(Club.DATE_FORMATTER));
    }

    @Test
    void updatePersonData07() { // Signup dates of the 'Person' objects after first update.
        Person person1Updated = personList2.get(1), person2Updated = personList2.get(0);

        assertNull(person1Updated.getSignupDate());
        assertEquals(nowD.format(Club.DATE_FORMATTER), person2Updated.getSignupDate().format(Club.DATE_FORMATTER));
    }

    @Test
    void updatePersonData08() { // Fields 'addressList', 'emailList' and 'phoneNumberList' of the 'Person' object with id 1 after first update.
        Person person1Updated = personList2.get(1);

        assertNull(person1Updated.getAddressList().get(0));
        assertNull(person1Updated.getAddressList().get(1));

        assertNull(person1Updated.getEmailList().get(0));
        assertNull(person1Updated.getEmailList().get(1));

        assertNull(person1Updated.getPhoneNumberList().get(0));
        assertNull(person1Updated.getPhoneNumberList().get(1));
    }

    @Test
    void updatePersonData09() { // Fields 'addressList', 'emailList' and 'phoneNumberList' of the 'Person' object with id 2 after first update.
        Person person2Updated = personList2.get(0);

        assertEquals(new Address("Road 1", "1010 City"), person2Updated.getAddressList().get(0));
        assertNull(person2Updated.getAddressList().get(1));

        assertEquals("person@email.com", person2Updated.getEmailList().get(0));
        assertNull(person2Updated.getEmailList().get(1));

        assertEquals("88 88 88 88", person2Updated.getPhoneNumberList().get(0));
        assertNull(person2Updated.getPhoneNumberList().get(1));
    }

    @Test
    void updatePersonData10(){ // Neither of the 'Person' objects should have 'Role' objects that are null.
        Person person1Updated = personList2.get(1), person2Updated = personList2.get(0);

        assertNotNull(person1Updated.getRole(Person.GYMNAST_INDEX));
        assertNotNull(person1Updated.getRole(Person.COACH_INDEX));
        assertNotNull(person1Updated.getRole(Person.JUDGE_INDEX));

        assertNotNull(person2Updated.getRole(Person.GYMNAST_INDEX));
        assertNotNull(person2Updated.getRole(Person.COACH_INDEX));
        assertNotNull(person2Updated.getRole(Person.JUDGE_INDEX));
    }

    @Test
    void updatePersonData11(){ // Field 'isActive' of all 'Role' objects.
        Person person1Updated = personList2.get(1), person2Updated = personList2.get(0);

        assertTrue(person1Updated.getRole(Person.GYMNAST_INDEX).isActive());
        assertTrue(person1Updated.getRole(Person.COACH_INDEX).isActive());
        assertFalse(person1Updated.getRole(Person.JUDGE_INDEX).isActive());

        assertFalse(person2Updated.getRole(Person.GYMNAST_INDEX).isActive());
        assertFalse(person2Updated.getRole(Person.COACH_INDEX).isActive());
        assertTrue(person2Updated.getRole(Person.JUDGE_INDEX).isActive());
    }

    @Test
    void updatePersonData12(){ // Ids of all 'Role' objects.
        Person person1Updated = personList2.get(1), person2Updated = personList2.get(0);

        assertEquals(2, person1Updated.getRole(Person.GYMNAST_INDEX).getID());
        assertEquals(2, person1Updated.getRole(Person.COACH_INDEX).getID());
        assertEquals(1, person1Updated.getRole(Person.JUDGE_INDEX).getID());

        assertEquals(1, person2Updated.getRole(Person.GYMNAST_INDEX).getID());
        assertEquals(1, person2Updated.getRole(Person.COACH_INDEX).getID());
        assertEquals(2, person2Updated.getRole(Person.JUDGE_INDEX).getID());
    }

    @Test
    void updatePersonData13(){ // Number of 'TeamAssignment' objects in all 'Role' objects.
        Person person1Updated = personList2.get(1), person2Updated = personList2.get(0);
        TeamMember gymnast1 = (TeamMember) person1Updated.getRole(Person.GYMNAST_INDEX), coach1 = (TeamMember) person1Updated.getRole(Person.COACH_INDEX);
        TeamMember gymnast2 = (TeamMember) person2Updated.getRole(Person.GYMNAST_INDEX), coach2 = (TeamMember) person2Updated.getRole(Person.COACH_INDEX);

        assertEquals(3, gymnast1.getTeamAssignmentList().size());
        assertEquals(3, coach1.getTeamAssignmentList().size());

        assertTrue(gymnast2.getTeamAssignmentList().isEmpty());
        assertTrue(coach2.getTeamAssignmentList().isEmpty());
    }

    @Test
    void updatePersonData14() { // Number and ids of 'PersonSignup' objects of both 'Person' objects after first update.
        Person person1Updated = personList2.get(1), person2Updated = personList2.get(0);

        assertEquals(2, person1Updated.getPersonSignupList().size());
        assertEquals(1, person2Updated.getPersonSignupList().size());

        assertEquals(4, person1Updated.getPersonSignupList().get(0).getID());
        assertEquals(5, person1Updated.getPersonSignupList().get(1).getID());
        assertEquals(6, person2Updated.getPersonSignupList().get(0).getID());
    }

    @Test
    void updatePersonData15() { // 'Role' objects of 'PersonSignup' objects of both 'Person' objects.
        Person person1Updated = personList2.get(1), person2Updated = personList2.get(0);

        assertTrue(person1Updated.getRole(Person.GYMNAST_INDEX) == person1Updated.getPersonSignupList().get(0).getRole());
        assertTrue(person1Updated.getRole(Person.COACH_INDEX) == person1Updated.getPersonSignupList().get(1).getRole());

        assertTrue(person2Updated.getRole(Person.JUDGE_INDEX) == person2Updated.getPersonSignupList().get(0).getRole());
    }

    @Test
    void updatePersonData16() { // Number of 'TeamSignup' objects of all 'PersonSignup' objects.
        Person person1Updated = personList2.get(1), person2Updated = personList2.get(0);

        assertEquals(2, person1Updated.getPersonSignupList().get(0).getTeamSignupList().size());
        assertTrue(person1Updated.getPersonSignupList().get(1).getTeamSignupList().isEmpty());

        assertTrue(person2Updated.getPersonSignupList().get(0).getTeamSignupList().isEmpty());

        assertTrue(person1Updated.getPersonSignupList().get(0).getTeamSignupList().get(0).getTeam() == teamList2.get(0));
        assertTrue(person1Updated.getPersonSignupList().get(0).getTeamSignupList().get(1).getTeam() == teamList2.get(1));
    }

    @Test
    void updatePersonData17() { // 'TeamSignup' objects of 'PersonSignup' object of 'Person' with id 1.
        Person person1Updated = personList2.get(1);

        assertEquals("Team 1", person1Updated.getPersonSignupList().get(0).getTeamSignupList().get(0).getTeam().getName());
        assertEquals("Team 2", person1Updated.getPersonSignupList().get(0).getTeamSignupList().get(1).getTeam().getName());

    }

    @Test
    void updatePersonDate18() { // Id and name of 'Person' object with id 1 after second update.
        Person person1Updated = personList3.get(1);

        assertEquals(1, person1Updated.getID());
        assertEquals("Person 1.0", person1Updated.getName());
    }

    @Test
    void updatePersonDate19() { // Number of 'PersonSignup' and 'TeamSignup' objects after second update.
        Person person1Updated = personList3.get(1);
        TeamSignup teamSignup = person1Updated.getPersonSignupList().get(0).getTeamSignupList().get(0);

        assertEquals(2, person1Updated.getPersonSignupList().size());

        assertEquals(1, person1Updated.getPersonSignupList().get(0).getTeamSignupList().size());
        assertTrue(person1Updated.getPersonSignupList().get(1).getTeamSignupList().isEmpty());

        assertEquals("Team 1", teamSignup.getTeam().getName());
    }

    @Test
    void updatePersonDate20() { // Number and fields of 'TeamAssignment' objects in 'Gymnast' object after second update.
        Person person1Updated = personList3.get(1);
        TeamMember gymnast1 = (TeamMember) person1Updated.getRole(Person.GYMNAST_INDEX);

        assertEquals(3, gymnast1.getTeamAssignmentList().size());

        assertTrue(gymnast1.getTeamAssignmentList().get(0).isSignedUp());
        assertFalse(gymnast1.getTeamAssignmentList().get(1).isSignedUp());
        assertFalse(gymnast1.getTeamAssignmentList().get(2).isSignedUp());

        assertEquals("Team 1", gymnast1.getTeamAssignmentList().get(0).getTeam().getName());
        assertEquals("Team 2", gymnast1.getTeamAssignmentList().get(1).getTeam().getName());
        assertEquals("Team 3", gymnast1.getTeamAssignmentList().get(2).getTeam().getName());
    }

    @Test
    void updatePersonDate21() {  // Number and fields of 'TeamAssignment' objects in 'Coach' object after second update.
        Person person1Updated = personList3.get(1);
        TeamMember coach1 = (TeamMember) person1Updated.getRole(Person.COACH_INDEX);

        assertEquals(3, coach1.getTeamAssignmentList().size());

        assertFalse(coach1.getTeamAssignmentList().get(0).isSignedUp());
        assertTrue(coach1.getTeamAssignmentList().get(1).isSignedUp());
        assertFalse(coach1.getTeamAssignmentList().get(2).isSignedUp());

        assertEquals("Team 1", coach1.getTeamAssignmentList().get(0).getTeam().getName());
        assertEquals("Team 2", coach1.getTeamAssignmentList().get(1).getTeam().getName());
        assertEquals("Team 3", coach1.getTeamAssignmentList().get(2).getTeam().getName());
    }

    @Test
    void updatePersonData22() { // Any attempt to update a 'Person' to have a name that already exist in the database should be hindered.
        DatabaseErrorException e = assertThrows(DatabaseErrorException.class, () -> databaseManager.updatePersonData(new Person(2, "Person 1.0", true, nowD, false)));
        assertEquals("An error occurred while updating data of person 2.", e.getMessage());
        assertEquals("Duplicate entry 'Person 1.0' for key 'personcol_UNIQUE'", e.getCause().getMessage());
    }

    // ---------- Testing the update of the 'Event' object with id 2 ----------
    @Test
    void updateEventData01() {
        Event event2Updated = seasonList2.get(0).viewEvents().get(0);

        assertEquals(2, event2Updated.getID());
        assertEquals("Event 2.0", event2Updated.getName());
        assertEquals(eventStartDate.plusDays(61).format(Club.DATE_TIME_FORMATTER), event2Updated.getStartDate().format(Club.DATE_TIME_FORMATTER));
        assertEquals(eventEndDate.plusDays(61).format(Club.DATE_FORMATTER), event2Updated.getEndDate().format(Club.DATE_FORMATTER));
        assertEquals(5, event2Updated.getCoachRequirement());
        assertEquals(5, event2Updated.getJudgeRequirement());
    }

    // ---------- Testing the update of the 'PersonSignup' object with id 4 ----------
    @Test
    void updatePersonSignupData() {
        PersonSignup personSignupBeforeUpdate = seasonList2.get(0).viewEvent(2).getPersonSignupList().get(1);
        PersonSignup personSignupUpdated = seasonList3.get(0).viewEvent(2).getPersonSignupList().get(1);

        assertEquals(4, personSignupBeforeUpdate.getID());
        assertEquals(4, personSignupUpdated.getID());

        assertEquals(PersonSignupState.NOT_SIGNED_UP, personSignupBeforeUpdate.getState());
        assertEquals(PersonSignupState.SIGNED_UP, personSignupUpdated.getState());
    }

    // ---------- Testing method 'restorePersonData' ----------
    @Test
    void restorePersonData01() {
        Person person2Updated = personList2.get(0);

        person2Updated.setName("Person 2")
                .setGender(false)
                .setPhotoConsent(false)
                .setBirthday(null)
                .setSignupDate(null)
                .setAddressList(new ArrayList<>())
                .setEmailList(new ArrayList<>())
                .setPhoneNumberList(new ArrayList<>());

        databaseManager.restorePersonData(person2Updated);

        assertEquals(2, person2Updated.getID());
        assertEquals("Person 2.0", person2Updated.getName());
        assertTrue(person2Updated.isMale());
        assertEquals(nowD.format(Club.DATE_FORMATTER), person2Updated.getRegistrationDate().format(Club.DATE_FORMATTER));
        assertTrue(person2Updated.getPhotoConsent());
        assertEquals("1998-02-23", person2Updated.getBirthday().format(Club.DATE_FORMATTER));
        assertEquals(nowD.format(Club.DATE_FORMATTER), person2Updated.getSignupDate().format(Club.DATE_FORMATTER));
        assertEquals(new Address("Road 1", "1010 City"), person2Updated.getAddressList().get(0));
        assertNull(person2Updated.getAddressList().get(1));
        assertEquals("person@email.com", person2Updated.getEmailList().get(0));
        assertNull(person2Updated.getEmailList().get(1));
        assertEquals("88 88 88 88", person2Updated.getPhoneNumberList().get(0));
        assertNull(person2Updated.getPhoneNumberList().get(1));
    }

    // ---------- Testing method 'getNextID' ----------
    @Test
    void getNextID01() {
        assertEquals(3, databaseManager.getNextID("season", "id_season"));
        assertEquals(3, databaseManager.getNextID("event", "id_event"));
        assertEquals(3, databaseManager.getNextID("person", "id_person"));
    }

    // ---------- Testing method 'deleteObjectData' ----------
    @Test
    void deleteObjectData01() {
        List<Person> personListTemp = new ArrayList<>();
        Person personTemp = new Person(3,"Person 3", true, nowD, false);

        databaseManager.savePersonData(personTemp);
        databaseManager.deleteObjectData("Person", "id_person", 3);
        databaseManager.loadAllData(new ArrayList<>(), personListTemp, new ArrayList<>());

        assertFalse(personListTemp.contains(personTemp));
    }
}
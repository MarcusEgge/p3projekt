package Model;

import Model.Exceptions.IllegalIDException;
import Model.Exceptions.IllegalObjectException;
import Model.Exceptions.ObjectNotFoundException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class ClubTest {
    private Club club;
    private LocalDateTime today;
    private List<String> emailList, phoneNumberList, teamListGymnast, teamListCoach;
    private List<Address> addressList;
    private String name, license, birthday, signupDay;
    private DateTimeFormatter dateTimeFormat;

    @BeforeAll
    static void beforeAll() {
        Club.INSTANCE.clear();
        Club.INSTANCE.getDatabaseManager().deleteAllDataFromDatabase();
    }

    @BeforeEach
    void initObjects() {
        club = Club.INSTANCE;
        dateTimeFormat = Club.DATE_TIME_FORMATTER;

        today = LocalDateTime.now();
        club.createTeam("Red");
        club.createTeam("Yellow");
        club.createSeason();
        club.createSeason();

        name = "Anders";
        license = "2121";

        birthday = today.minusYears(20).toLocalDate().toString();
        signupDay = today.minusYears(10).toLocalDate().toString();

        emailList = new ArrayList<>();
        emailList.add("email@gmail.com");

        addressList = new ArrayList<>();
        addressList.add(new Address("nederengade 21", "9000 Aalborg"));

        phoneNumberList = new ArrayList<>();
        phoneNumberList.add("2692929");

        teamListGymnast = new ArrayList<>();
        teamListGymnast.add("Red");

        teamListCoach = new ArrayList<>();
        teamListCoach.add("Yellow");

    }

    @AfterEach
    void afterEach() {
        club.clear();
        club.getDatabaseManager().deleteAllDataFromDatabase();
    }

    //Checks if the right numbers of seasons are returned in a list
    @Test
    void viewSeasons() {
        assertEquals(2, club.viewSeasons().size());
    }

    // ---------- Tests related to creating 'Season' objects ----------
    @Test
    void createSeason01() {
        String seasonName = today.getYear() + "/" + today.plusYears(1).getYear();
        Season season1 = club.viewSeasons().get(0);

        assertEquals(1, season1.getID());
        assertEquals(seasonName, season1.getName());

        assertEquals(today.getYear() + "-08-01 00:00", season1.getStartDate().format(dateTimeFormat));
        assertEquals(today.plusYears(1).getYear() + "-07-31 23:59", season1.getEndDate().format(dateTimeFormat));
    }

    @Test
    void createSeason02() {
        String seasonName = today.plusYears(1).getYear() + "/" + today.plusYears(2).getYear();
        Season season2 = club.viewSeasons().get(1);

        assertEquals(2, season2.getID());
        assertEquals(seasonName, season2.getName());
        assertEquals(today.plusYears(1).getYear() + "-08-01 00:00", season2.getStartDate().format(dateTimeFormat));
        assertEquals(today.plusYears(2).getYear() + "-07-31 23:59", season2.getEndDate().format(dateTimeFormat));
    }

    // ---------- Tests related to finding a specific 'Season' object ----------
    @Test
    void viewSeason01() {
        String seasonName = today.getYear() + "/" + today.plusYears(1).getYear();
        Season season1 = club.viewSeason(seasonName);

        assertEquals(1, season1.getID());
        assertEquals(seasonName, season1.getName());
        assertEquals(today.getYear() + "-08-01 00:00", season1.getStartDate().format(dateTimeFormat));
        assertEquals(today.plusYears(1).getYear() + "-07-31 23:59", season1.getEndDate().format(dateTimeFormat));
    }

    @Test
    void viewSeason02() {
        String seasonName = today.plusYears(1).getYear() + "/" + today.plusYears(2).getYear();
        Season season2 = club.viewSeason(seasonName);

        assertEquals(2, season2.getID());
        assertEquals(seasonName, season2.getName());
        assertEquals(today.plusYears(1).getYear() + "-08-01 00:00", season2.getStartDate().format(dateTimeFormat));
        assertEquals(today.plusYears(2).getYear() + "-07-31 23:59", season2.getEndDate().format(dateTimeFormat));
    }

    @Test
    void viewSeason03() {
        String seasonName = today.plusYears(2).getYear() + "/" + today.plusYears(3).getYear();

        ObjectNotFoundException e = assertThrows(ObjectNotFoundException.class, () -> club.viewSeason(seasonName));
        assertEquals("The season could not be found in the club.", e.getMessage());

    }

    // Test if all the teams are returned
    @Test
    void viewTeams() {
        assertEquals(2, club.viewTeams().size());
        assertEquals("Red", club.viewTeams().get(0).getName());
        assertEquals("Yellow", club.viewTeams().get(1).getName());
    }

    // ---------- Tests related to finding a specific 'Team' object ----------
    @Test
    void viewTeam01() {
        assertEquals("Red", club.viewTeam(1).getName());
    }

    @Test
    void viewTeam02() {
        assertEquals("Yellow", club.viewTeam(2).getName());
    }

    @Test
    void viewTeam03() {
        ObjectNotFoundException e = assertThrows(ObjectNotFoundException.class, () -> club.viewTeam(3));
        assertEquals("No such team was found in the club.", e.getMessage());
    }

    //Test for returning all persons
    @Test
    void viewPersonsAndCreatePerson() {
        club.createPerson(name, true, addressList, emailList, phoneNumberList,
                birthday, signupDay, true,
                true, false, false, license, teamListGymnast, teamListCoach);

        assertEquals(1, club.viewPersons().size());
    }

    // ---------- Tests related to creating 'Person' objects ----------
    @Test
    void createPerson01() {
        Person personExpected;
        Gymnast gymnastsTemp;

        club.createPerson(name, true, addressList, emailList, phoneNumberList,
                birthday, signupDay, true,
                true, false, false, license, teamListGymnast, teamListCoach);

        personExpected = club.viewPersons().get(0);
        gymnastsTemp = (Gymnast) personExpected.getRole(Person.GYMNAST_INDEX);

        assertEquals(name, personExpected.getName());
        assertTrue(personExpected.isMale());
        assertEquals(emailList, personExpected.getEmailList());
        assertEquals(addressList, personExpected.getAddressList());
        assertEquals(phoneNumberList, personExpected.getPhoneNumberList());
        assertEquals(birthday, personExpected.getBirthday().toString());
        assertEquals(today.toLocalDate(), personExpected.getRegistrationDate());
        assertEquals(signupDay, personExpected.getSignupDate().toString());
        assertTrue(personExpected.getPhotoConsent());
        assertTrue(personExpected.isGymnast());
        assertFalse(personExpected.isCoach());
        assertFalse(personExpected.isJudge());
        assertEquals(license, gymnastsTemp.getLicense());
        assertEquals(club.viewTeams().get(0), gymnastsTemp.getTeamAssignmentList().get(0).getTeam());
        assertNull(personExpected.getRole(Person.COACH_INDEX));
    }

    @Test
    void createPerson02() {
        String eventName;

        Address eventAddress;
        int judgeAndCoach;

        eventName = "Dancup";
        eventAddress = new Address("Enellerandenvej 2", "9000 Aalborg");
        judgeAndCoach = 2;

        club.viewSeasons().get(0).createEvent(eventName, eventAddress, today.plusMonths(1).format(dateTimeFormat),
                today.plusMonths(1).toLocalDate().toString(), judgeAndCoach, judgeAndCoach);
        club.viewSeasons().get(1).createEvent(eventName, eventAddress, today.plusMonths(1).format(dateTimeFormat),
                today.plusMonths(1).toLocalDate().toString(), judgeAndCoach, judgeAndCoach);

        assertEquals(2, club.viewSeasons().get(0).viewEvents().get(0).viewTeamSignups().size());
        assertEquals(2, club.viewSeasons().get(1).viewEvents().get(0).viewTeamSignups().size());

        assertEquals(0, club.viewSeasons().get(0).viewEvents().get(0).getPersonSignupList().size());
        assertEquals(0, club.viewSeasons().get(1).viewEvents().get(0).getPersonSignupList().size());


        club.createPerson(name, true, addressList, emailList, phoneNumberList,
                birthday, signupDay, true,
                true, false, false, license, teamListGymnast, teamListCoach);

        assertEquals(1, club.viewSeasons().get(0).viewEvents().get(0).getPersonSignupList().size());
        assertEquals(1, club.viewSeasons().get(1).viewEvents().get(0).getPersonSignupList().size());

    }

    // ---------- Tests related to throwing a exception if person only has one role when being created----------
    @Test
    void createPerson03() {
        IllegalObjectException e = assertThrows(IllegalObjectException.class,
                () -> club.createPerson(name, false, addressList, emailList, phoneNumberList
                        , birthday, signupDay, false, false, false, false
                        , license, teamListGymnast, teamListCoach));
        assertEquals("A person should have at least one role.", e.getMessage());
    }

    @Test
    void createPerson04() {
        assertThrows(NullPointerException.class,
                () -> club.createPerson(name, false, addressList, emailList, phoneNumberList
                        , birthday, signupDay, false, true, false, false
                        , license, null, teamListCoach));

    }

    @Test
    void createPerson05() {
        assertThrows(NullPointerException.class,
                () -> club.createPerson(name, false, addressList, emailList, phoneNumberList
                        , birthday, signupDay, false, true, true, false
                        , license, teamListGymnast, null));
    }

    @Test
    void createPerson06() {
        assertThrows(NullPointerException.class,
                () -> club.createPerson(name, false, addressList, emailList, phoneNumberList
                        , null, signupDay, false, true, true, false
                        , license, teamListGymnast, teamListCoach));
    }

    @Test
    void createPerson07() {
        assertThrows(NullPointerException.class,
                () -> club.createPerson(name, false, addressList, emailList, phoneNumberList
                        , birthday, null, false, true, true, false
                        , license, teamListGymnast, teamListCoach));
    }

    //Tests if a person with the same name already exist
    @Test
    void createPerson08() {
        club.createPerson(name, true, addressList, emailList, phoneNumberList
                , birthday, signupDay, true, true, false, false
                , license, teamListGymnast, teamListCoach);

        IllegalObjectException e = assertThrows(IllegalObjectException.class,
                () -> club.createPerson(name, false, addressList, emailList, phoneNumberList
                        , birthday, signupDay, false, false, true, false
                        , license, teamListGymnast, teamListCoach));
        assertEquals("A person with name " + name + " already exists.", e.getMessage());
    }

    // ---------- Tests related to finding a specific 'Person' object ----------
    @Test
    void viewPerson01() {

        club.createPerson(name, true, addressList, emailList, phoneNumberList,
                birthday, signupDay, true, true, false, false,
                license, teamListGymnast, teamListCoach);

        assertEquals(name, club.viewPerson(1).getName());
    }

    @Test
    void viewPerson02() {
        int id = 1;
        ObjectNotFoundException e = assertThrows(ObjectNotFoundException.class, () -> club.viewPerson(1));
        assertEquals("No person with id " + id + " was found.", e.getMessage());
    }

    // ---------- Tests related to deleting a specific 'Person' object ----------
    @Test
    void deletePerson01() {

        club.createPerson(name, true, addressList, emailList, phoneNumberList,
                birthday, signupDay, true, true, false, false,
                license, teamListGymnast, teamListCoach);

        assertFalse(club.viewPersons().isEmpty());
        club.deletePerson(club.viewPersons().get(0).getID());
        assertTrue(club.viewPersons().isEmpty());
    }

    //Throws a exception if the person does not exist
    @Test
    void deletePerson02() {
        int id = 2;

        club.createPerson(name, true, addressList, emailList, phoneNumberList,
                birthday, signupDay, true, true, false, false,
                license, teamListGymnast, teamListCoach);

        ObjectNotFoundException e = assertThrows(ObjectNotFoundException.class, () -> club.deletePerson(id));
        assertEquals("No person with id " + id + " was found.", e.getMessage());
    }

    // ---------- Tests related to editing a specific 'Person' object ----------
    @Test
    void editPerson01() {
        List<Person> membersTemp;
        Person personExpected;
        Gymnast gymnastsTemp;
        String nameNew = "Andersen";

        club.createPerson(name, true, addressList, emailList, phoneNumberList, birthday, signupDay,
                true, true, false, false, license, teamListGymnast, teamListCoach);

        club.editPerson(1, nameNew, true, addressList, emailList, phoneNumberList, birthday, signupDay,
                true, true, false, false, license, teamListGymnast, teamListCoach);

        membersTemp = club.viewPersons();
        personExpected = membersTemp.get(0);
        gymnastsTemp = (Gymnast) personExpected.getRole(Person.GYMNAST_INDEX);

        assertEquals(1, membersTemp.size());
        assertEquals(nameNew, personExpected.getName());
        assertEquals(true, personExpected.isMale());
        assertEquals(addressList, personExpected.getAddressList());
        assertEquals(emailList, personExpected.getEmailList());
        assertEquals(phoneNumberList, personExpected.getPhoneNumberList());
        assertEquals(birthday, personExpected.getBirthday().toString());
        assertEquals(signupDay, personExpected.getSignupDate().toString());
        assertEquals(true, personExpected.getPhotoConsent());
        assertEquals(true, personExpected.isGymnast());
        assertEquals(false, personExpected.isCoach());
        assertEquals(false, personExpected.isJudge());
        assertEquals(license, gymnastsTemp.getLicense());
        assertEquals(2, gymnastsTemp.getTeamAssignmentList().size());
        assertEquals(true, gymnastsTemp.getTeamAssignmentList().get(0).isSignedUp());
        assertEquals(false, gymnastsTemp.getTeamAssignmentList().get(1).isSignedUp());
        assertEquals(1, club.viewTeams().get(0).getGymnastsInTeam().size());
        assertEquals(0, club.viewTeams().get(1).getCoachesInTeam().size());
    }

    @Test
    void editPerson02() {
        List<String> emailListNew, phoneNumberListNew, teamListGymnastNew, teamListCoachNew;
        List<Address> addressListNew;
        String nameNew, licenseNew, dateStringNew;
        List<Person> membersTamp;
        Person personExpected;
        Gymnast gymnastsTemp;

        nameNew = "Andersen";
        licenseNew = "4242";
        dateStringNew = today.plusDays(10).toLocalDate().toString();

        emailListNew = new ArrayList<>();
        emailListNew.add("newEmail@gmail.com");

        addressListNew = new ArrayList<>();
        addressListNew.add(new Address("Nyvej 1", "1000 Nyborg"));

        phoneNumberListNew = new ArrayList<>();
        phoneNumberListNew.add("12345678");

        teamListGymnastNew = new ArrayList<>();
        teamListGymnastNew.add("Yellow");

        teamListCoachNew = new ArrayList<>();
        teamListCoachNew.add("Red");


        club.createPerson(name, true, addressList, emailList, phoneNumberList,
                birthday, signupDay, true, true,
                false, false, license, teamListGymnast, teamListCoach);

        club.editPerson(1, nameNew, false, addressListNew, emailListNew, phoneNumberListNew,
                dateStringNew, dateStringNew, false, true, true,
                true, licenseNew, teamListGymnastNew, teamListCoachNew);

        membersTamp = club.viewPersons();
        personExpected = membersTamp.get(0);
        gymnastsTemp = (Gymnast) personExpected.getRole(Person.GYMNAST_INDEX);

        assertEquals(1, membersTamp.size());
        assertEquals(nameNew, personExpected.getName());
        assertEquals(false, personExpected.isMale());
        assertEquals(addressListNew, personExpected.getAddressList());
        assertEquals(emailListNew, personExpected.getEmailList());
        assertEquals(phoneNumberListNew, personExpected.getPhoneNumberList());
        assertEquals(dateStringNew, personExpected.getBirthday().toString());
        assertEquals(LocalDate.now().format(Club.DATE_FORMATTER), personExpected.getRegistrationDate().toString());
        assertEquals(dateStringNew, personExpected.getSignupDate().toString());
        assertEquals(false, personExpected.getPhotoConsent());
        assertEquals(true, personExpected.isGymnast());
        assertEquals(true, personExpected.isCoach());
        assertEquals(true, personExpected.isJudge());
        assertEquals(licenseNew, gymnastsTemp.getLicense());
        assertEquals(0, club.viewTeams().get(0).getGymnastsInTeam().size());
        assertEquals(1, club.viewTeams().get(1).getGymnastsInTeam().size());
        assertEquals(1, club.viewTeams().get(0).getCoachesInTeam().size());
        assertEquals(0, club.viewTeams().get(1).getCoachesInTeam().size());

    }

    @Test
    void editPerson03() {
        List<Person> membersTamp;
        Person personExpected;
        club.createPerson(name, true, addressList, emailList, phoneNumberList,
                birthday, signupDay, true, true,
                false, true, license, teamListGymnast, teamListCoach);

        club.editPerson(1, name, true, addressList, emailList, phoneNumberList,
                birthday, signupDay, true, false, false, true, license,
                teamListGymnast, teamListCoach);

        membersTamp = club.viewPersons();
        personExpected = membersTamp.get(0);

        assertEquals(false, personExpected.isGymnast());
        assertEquals(false, personExpected.isCoach());
        assertEquals(true, personExpected.isJudge());
        assertEquals(0, club.viewTeams().get(0).getGymnastsInTeam().size());
        assertEquals(0, club.viewTeams().get(1).getCoachesInTeam().size());
    }

    @Test
    void editPerson04() {
        List<Person> membersTamp;
        Person personExpected;
        club.createPerson(name, true, addressList, emailList, phoneNumberList,
                birthday, signupDay, true, false,
                false, true, license, teamListGymnast, teamListCoach);

        club.editPerson(1, name, true, addressList, emailList, phoneNumberList, birthday, signupDay,
                true, true, false, true, license, teamListGymnast, teamListCoach);

        membersTamp = club.viewPersons();
        personExpected = membersTamp.get(0);

        assertEquals(true, personExpected.isGymnast());
        assertEquals(false, personExpected.isCoach());
        assertEquals(true, personExpected.isJudge());
        assertEquals(1, club.viewTeams().get(0).getGymnastsInTeam().size());
        assertEquals(0, club.viewTeams().get(1).getCoachesInTeam().size());
    }

    @Test
    void editPerson05() {
        List<Person> membersTamp;
        Person personExpected;
        club.createPerson(name, true, addressList, emailList, phoneNumberList,
                birthday, signupDay, true, false,
                true, true, license, teamListGymnast, teamListCoach);

        club.editPerson(1, name, true, addressList, emailList, phoneNumberList, birthday, signupDay,
                true, false, false, true, license, teamListGymnast, teamListCoach);

        membersTamp = club.viewPersons();
        personExpected = membersTamp.get(0);

        assertEquals(false, personExpected.isGymnast());
        assertEquals(false, personExpected.isCoach());
        assertEquals(true, personExpected.isJudge());
        assertEquals(0, club.viewTeams().get(0).getGymnastsInTeam().size());
        assertEquals(0, club.viewTeams().get(1).getCoachesInTeam().size());
    }

    @Test
    void editPerson06() {
        List<Person> membersTamp;
        Person personExpected;
        club.createPerson(name, true, addressList, emailList, phoneNumberList,
                birthday, signupDay, true, false,
                true, true, license, teamListGymnast, teamListCoach);

        club.editPerson(1, name, true, addressList, emailList, phoneNumberList, birthday, signupDay,
                true, false, true, false, license, teamListGymnast, teamListCoach);

        membersTamp = club.viewPersons();
        personExpected = membersTamp.get(0);

        assertEquals(false, personExpected.isGymnast());
        assertEquals(true, personExpected.isCoach());
        assertEquals(false, personExpected.isJudge());
        assertEquals(0, club.viewTeams().get(0).getGymnastsInTeam().size());
        assertEquals(1, club.viewTeams().get(1).getCoachesInTeam().size());
    }

    //Tests if a person is put into a team when made
    @Test
    void editPerson07() {
        List<Person> membersTamp;
        Person personExpected;
        club.createPerson(name, true, addressList, emailList, phoneNumberList,
                birthday, signupDay, true, true,
                true, true, license, teamListGymnast, teamListCoach);

        club.editPerson(1, name, true, addressList, emailList, phoneNumberList, birthday, signupDay,
                true, false, false, true, license, teamListGymnast, teamListCoach);

        club.editPerson(1, name, true, addressList, emailList, phoneNumberList, birthday, signupDay,
                true, true, true, false, license, teamListGymnast, teamListCoach);

        club.editPerson(1, name, true, addressList, emailList, phoneNumberList, birthday, signupDay,
                true, true, true, true, license, teamListGymnast, teamListCoach);


        membersTamp = club.viewPersons();
        personExpected = membersTamp.get(0);

        assertEquals(true, personExpected.isGymnast());
        assertEquals(true, personExpected.isCoach());
        assertEquals(true, personExpected.isJudge());
        assertEquals(1, club.viewTeams().get(0).getGymnastsInTeam().size());
        assertEquals(1, club.viewTeams().get(1).getCoachesInTeam().size());
    }

    //Tests that a exception is thrown if the person does not exist
    @Test
    void editPerson08() {
        int id = 2;
        club.createPerson(name, true, addressList, emailList, phoneNumberList,
                birthday, signupDay, true, true,
                true, true, license, teamListGymnast, teamListCoach);

        ObjectNotFoundException e = assertThrows(ObjectNotFoundException.class,
                () -> club.editPerson(id, name, true, addressList, emailList, phoneNumberList, birthday, signupDay,
                        true, false, true, false, license, teamListGymnast, teamListCoach));
        assertEquals("No person with id " + id + " was found.", e.getMessage());

    }

    @Test
    void editPerson09() {
        club.createPerson(name, true, addressList, emailList, phoneNumberList,
                birthday, signupDay, true, true,
                true, true, license, teamListGymnast, teamListCoach);

        IllegalObjectException e = assertThrows(IllegalObjectException.class,
                () -> club.editPerson(1, name, true, addressList, emailList, phoneNumberList, birthday,
                        signupDay, true, false, false, false, license,
                        teamListGymnast, teamListCoach));
        assertEquals("A person should have at least one role.", e.getMessage());

    }

    @Test
    void editPerson10() {
        club.createPerson(name, true, addressList, emailList, phoneNumberList,
                birthday, signupDay, true, true,
                true, true, license, teamListGymnast, teamListCoach);

        assertThrows(NullPointerException.class,
                () -> club.editPerson(1, name, true, addressList, emailList, phoneNumberList, birthday,
                        signupDay, true, true, true, true, license, null,
                        teamListCoach));

    }

    @Test
    void editPerson11() {
        club.createPerson(name, true, addressList, emailList, phoneNumberList, birthday, signupDay, true,
                true, true, true, license, teamListGymnast, teamListCoach);

        assertThrows(NullPointerException.class,
                () -> club.editPerson(1, name, true, addressList, emailList, phoneNumberList, birthday, signupDay,
                        true, true, true, true, license, teamListGymnast, null));

    }

    @Test
    void editPerson12() {
        String name2;

        club.createPerson(name, true, addressList, emailList, phoneNumberList, birthday, signupDay,
                true, true, true, false, license, teamListGymnast, teamListCoach);

        name2 = "Peter";

        club.createPerson(name2, true, addressList, emailList, phoneNumberList, birthday, signupDay,
                true, true, true, false, license, teamListGymnast, teamListCoach);

        IllegalObjectException e = assertThrows(IllegalObjectException.class,
                () -> club.editPerson(1, name2, true, addressList, emailList, phoneNumberList, birthday, signupDay,
                        true, true, true, false, license, teamListGymnast, teamListCoach));

        assertEquals("A person with this name already exists.", e.getMessage());
    }

    //Tests that a exception is thrown when a wrong format is used in the name
    @Test
    void editPerson13() {
        club.createPerson(name, true, addressList, emailList, phoneNumberList,
                birthday, signupDay, true, true,
                true, true, license, teamListGymnast, teamListCoach);

        IllegalObjectException e = assertThrows(IllegalObjectException.class,
                () -> club.editPerson(1, "", true, addressList, emailList, phoneNumberList, birthday, signupDay,
                        true, true, true, true, license, teamListGymnast, teamListCoach));

        assertEquals("Person edit failed; class Model.Exceptions.IllegalFormatException.", e.getMessage());

    }

    //Tests that a exception is thrown when null is put into one of the values
    @Test
    void editPerson14() {
        club.createPerson(name, true, addressList, emailList, phoneNumberList,
                birthday, signupDay, true, true,
                true, true, license, teamListGymnast, teamListCoach);

        IllegalObjectException e = assertThrows(IllegalObjectException.class,
                () -> club.editPerson(1, name, true, addressList, emailList, phoneNumberList, null, signupDay,
                        true, true, true, true, license, teamListGymnast, teamListCoach));

        assertEquals("Person edit failed; class java.lang.NullPointerException.", e.getMessage());

    }

    @Test
    void editPerson15() {
        club.createPerson(name, true, addressList, emailList, phoneNumberList,
                birthday, signupDay, true, true,
                true, true, license, teamListGymnast, teamListCoach);

        IllegalObjectException e = assertThrows(IllegalObjectException.class,
                () -> club.editPerson(1, name, true, addressList, emailList, phoneNumberList, birthday, null,
                        true, true, true, true, license, teamListGymnast, teamListCoach));

        assertEquals("Person edit failed; class java.lang.NullPointerException.", e.getMessage());
    }

    //Tests that a exception is thrown because the addressList is too big
    @Test
    void editPerson16() {
        club.createPerson(name, true, addressList, emailList, phoneNumberList,
                birthday, signupDay, true, true,
                true, true, license, teamListGymnast, teamListCoach);

        addressList.add(new Address("Envej 21", "9000 Aalborg"));
        addressList.add(new Address("Tovej 21", "9000 Aalborg"));

        IllegalObjectException e = assertThrows(IllegalObjectException.class,
                () -> club.editPerson(1, name, true, addressList, emailList, phoneNumberList, birthday, signupDay,
                        true, true, true, true, license, teamListGymnast, teamListCoach));

        assertEquals("Person edit failed; class Model.Exceptions.IllegalObjectException.", e.getMessage());

    }

    //Tests that a exception is thrown because the emailList is too big
    @Test
    void editPerson17() {
        club.createPerson(name, true, addressList, emailList, phoneNumberList, birthday, signupDay, true,
                true, true, true, license, teamListGymnast, teamListCoach);

        emailList.add("test_1@gmail.com");
        emailList.add("test_2@gmail.com");

        IllegalObjectException e = assertThrows(IllegalObjectException.class,
                () -> club.editPerson(1, name, true, addressList, emailList, phoneNumberList, birthday, signupDay,
                        true, true, true, true, license, teamListGymnast, teamListCoach));

        assertEquals("Person edit failed; class Model.Exceptions.IllegalObjectException.", e.getMessage());

    }

    //Tests that a exception is thrown because the phoneList is too big
    @Test
    void editPerson18() {
        club.createPerson(name, true, addressList, emailList, phoneNumberList,
                birthday, signupDay, true, true,
                true, true, license, teamListGymnast, teamListCoach);

        phoneNumberList.add("87654321");
        phoneNumberList.add("22446688");

        IllegalObjectException e = assertThrows(IllegalObjectException.class,
                () -> club.editPerson(1, name, true, addressList, emailList, phoneNumberList, birthday, signupDay,
                        true, true, true, true, license, teamListGymnast, teamListCoach));

        assertEquals("Person edit failed; class Model.Exceptions.IllegalObjectException.", e.getMessage());

    }


    // ---------- Tests related to creating a 'Team' object ----------
    @Test
    void createTeam01() {
        assertEquals(2, club.viewTeams().size());
        assertEquals("Red", club.viewTeams().get(0).getName());
        assertEquals("Yellow", club.viewTeams().get(1).getName());
    }

    @Test
    void createTeam02() {
        String eventName;
        Address eventAddress;
        int judgeAndCoach;


        eventName = "Dancup";
        eventAddress = new Address("Enellerandenvej 2", "9000 Aalborg");
        judgeAndCoach = 2;


        club.viewSeasons().get(0).createEvent(eventName, eventAddress, today.plusMonths(1).format(dateTimeFormat),
                today.plusMonths(1).toLocalDate().toString(), judgeAndCoach, judgeAndCoach);

        club.createTeam("Blue");
        assertEquals(3, club.viewTeams().size());
        assertEquals("Red", club.viewTeams().get(0).getName());
        assertEquals("Yellow", club.viewTeams().get(1).getName());
        assertEquals("Blue", club.viewTeams().get(2).getName());
        assertEquals(3, club.viewSeasons().get(0).viewEvent(1).viewTeamSignups().size());
    }

    //Tests if a team with the same name already exist
    @Test
    void createTeam03() {
        IllegalObjectException e = assertThrows(IllegalObjectException.class, () -> club.createTeam("Red"));
        assertEquals("A team with name Red already exists.", e.getMessage());
    }

    // ---------- Tests related to editing a specific 'Team' object ----------
    @Test
    void editTeam1() {
        assertEquals("Red", club.viewTeams().get(0).getName());
        club.editTeam(1, "Blue", new ArrayList<>(), new ArrayList<>());
        assertEquals("Blue", club.viewTeams().get(0).getName());
    }

    //Tests that a exception is thrown if there already exist a team with the new name.
    @Test
    void editTeam2() {
        IllegalObjectException e = assertThrows(IllegalObjectException.class,
                () -> club.editTeam(1, "Yellow", new ArrayList<>(), new ArrayList<>()));

        assertEquals("A team with name Yellow already exists.", e.getMessage());
    }

    //Tests that a exception is thrown if the team does not exist
    @Test
    void editTeam3() {
        ObjectNotFoundException e = assertThrows(ObjectNotFoundException.class,
                () -> club.editTeam(3, "Blue", new ArrayList<>(), new ArrayList<>()));

        assertEquals("No such team was found in the club.", e.getMessage());
    }

    // ---------- Tests related to deleting a specific 'Team' object ----------
    @Test
    void deleteTeam1() {
        assertEquals("Red", club.viewTeams().get(0).getName());

        club.deleteTeam(1);

        assertEquals("Yellow", club.viewTeams().get(0).getName());
    }

    @Test
    void deleteTeam2() {
        ObjectNotFoundException e = assertThrows(ObjectNotFoundException.class, () -> club.deleteTeam(3));

        assertEquals("No such team was found in the club.", e.getMessage());
    }


    // ---------- Tests related to ID that are not allowed ----------
    @Test
    void checkID01() {
        IllegalIDException e = assertThrows(IllegalIDException.class, () -> Club.checkID(0));

        assertEquals("The id is illegal.", e.getMessage());
    }


    @Test
    void checkID02() {
        IllegalIDException e = assertThrows(IllegalIDException.class, () -> Club.checkID(-1));

        assertEquals("The id is illegal.", e.getMessage());
    }

    //Tests that all data is deleted when clear is called
    @Test
    void clear() {
        club.createPerson(name, true, addressList, emailList, phoneNumberList, birthday, signupDay,
                true, true, true, true, license, teamListGymnast, teamListCoach);
        club.clear();

        assertEquals(true, club.viewSeasons().isEmpty());
        assertEquals(true, club.viewPersons().isEmpty());
        assertEquals(true, club.viewTeams().isEmpty());
    }

    // ---------- Tests related to correct format when getting dates as string ----------
    @Test
    void getDatesAsStrings01() {
        List<String> dates = club.getDatesAsStrings(0, 1, 1, LocalDate.of(2018, 8, 1));

        assertEquals("2018-08-01 00:00", dates.get(0));
        assertEquals("2019-07-31 23:59", dates.get(1));
    }

    @Test
    void getDatesAsStrings02() {
        List<String> dates = club.getDatesAsStrings(0, 1, 1, LocalDate.of(2018, 12, 31));

        assertEquals("2018-08-01 00:00", dates.get(0));
        assertEquals("2019-07-31 23:59", dates.get(1));
    }

    @Test
    void getDatesAsStrings03() {
        List<String> dates = club.getDatesAsStrings(0, 1, 1, LocalDate.of(2019, 1, 1));

        assertEquals("2018-08-01 00:00", dates.get(0));
        assertEquals("2019-07-31 23:59", dates.get(1));
    }

    @Test
    void getDatesAsStrings04() {
        List<String> dates = club.getDatesAsStrings(0, 1, 1, LocalDate.of(2019, 7, 30));

        assertEquals("2018-08-01 00:00", dates.get(0));
        assertEquals("2019-07-31 23:59", dates.get(1));
    }

    @Test
    void getDatesAsStrings05() {
        List<String> dates = club.getDatesAsStrings(0, 1, 1, LocalDate.of(2019, 7, 31));

        assertEquals("2019-08-01 00:00", dates.get(0));
        assertEquals("2020-07-31 23:59", dates.get(1));
    }
}
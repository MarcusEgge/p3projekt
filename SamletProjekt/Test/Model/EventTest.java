package Model;

import Model.Exceptions.*;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static org.junit.jupiter.api.Assertions.*;

class EventTest {
    private String eventName;
    private Address eventAddress;
    private LocalDateTime eventStartDate, today;
    private LocalDate eventEndDate;
    private Season season;
    private Event event1;
    private Team team;
    private Person person;
    private Gymnast gymnast;
    private TeamSignup teamSignup;
    private PersonSignup personSignup;
    private int judgeAndCoach;

    @BeforeEach
    void initObjects() {
        today = LocalDateTime.now();
        season = new Season(today, today);
        eventStartDate = today.plusDays(10);
        eventEndDate = today.toLocalDate().plusDays(10);
        eventName = "SpringDanmark";
        eventAddress = new Address("Kalderupvej 21", "9000 Aalborg");
        judgeAndCoach = 2;
        event1 = new Event(eventName, eventAddress, eventStartDate, eventEndDate, season, judgeAndCoach, judgeAndCoach);
        team = new Team("Rød");
        person = new Person("Anders", true, today.toLocalDate(), true);
        gymnast = new Gymnast(person, "121202", true);
        teamSignup = new TeamSignup(1, team, event1, true);
        personSignup = new PersonSignup(1, gymnast, event1, PersonSignupState.NOT_SIGNED_UP);
        teamSignup.addPersonSignup(personSignup);
    }

    @BeforeAll
    static void beforeEach() {
        Club.INSTANCE.getDatabaseManager().deleteAllDataFromDatabase();
    }

    @Test
    void constructor01() {
        Event eventTest = new Event(10, eventName, eventAddress, eventStartDate, eventEndDate, season, judgeAndCoach, judgeAndCoach, true);
        assertEquals(10, eventTest.getID());

    }

    @Test
    void constructor02() {
        IllegalIDException exception = assertThrows(IllegalIDException.class, () -> new Event(0, eventName, eventAddress, eventStartDate, eventEndDate, season, judgeAndCoach, judgeAndCoach, true));
        assertEquals("The id is illegal.", exception.getMessage());
    }

    @Test
    void editBasicData01() {
        IllegalFormatException e = assertThrows(IllegalFormatException.class, () -> event1.editBasicData("", eventAddress, eventStartDate, eventEndDate, judgeAndCoach, judgeAndCoach));
        assertEquals("The name of an Event object cannot be an empty string.", e.getMessage());
    }

    @Test
    void editBasicData02() {
        event1.editBasicData("Dancup", eventAddress, eventStartDate, eventEndDate, judgeAndCoach, judgeAndCoach);
        assertEquals("Dancup", event1.getName());

    }

    @Test
    void getCoachRequirement() {
        assertEquals(2, event1.getCoachRequirement());
    }

    @Test
    void getJudgeRequirement() {
        assertEquals(2, event1.getJudgeRequirement());
    }

    @Test
    void updateState01() {
        assertTrue(event1.isActive());
    }

    @Test
    void updateState02() {
        event1.setEndDate(eventEndDate.minusMonths(1));
        event1.updateState();
        assertFalse(event1.isActive());
    }

    @Test
    void editPersonSignups01() {
        Person personMartin = new Person("Martin", true, today.toLocalDate(), true);
        Gymnast gymnastTest = new Gymnast(personMartin, "23231212", true);

        PersonSignup personSignupTest = new PersonSignup(2, gymnastTest, event1, PersonSignupState.NOT_SIGNED_UP);

        event1.addPersonSignup(personSignup);
        event1.addPersonSignup(personSignupTest);

        assertEquals(PersonSignupState.NOT_SIGNED_UP, personSignup.getState());
        assertEquals(PersonSignupState.NOT_SIGNED_UP, personSignupTest.getState());

        event1.editPersonSignup(2, PersonSignupState.SIGNED_UP);

        assertEquals(PersonSignupState.NOT_SIGNED_UP, personSignup.getState());
        assertEquals(PersonSignupState.SIGNED_UP, personSignupTest.getState());

    }

    @Test
    void editPersonSignups02() {
        Person personMartin = new Person("Martin", true, today.toLocalDate(), true);
        Person personPeter = new Person("Peter", true, today.toLocalDate(), true);

        Gymnast gymnastMartin = new Gymnast(personMartin, "23231212", true);
        Gymnast gymnastPeter = new Gymnast(personPeter, "23231234", true);

        PersonSignup personSignupMartin = new PersonSignup(2, gymnastMartin, event1, PersonSignupState.NOT_SIGNED_UP);
        PersonSignup personSignupPeter = new PersonSignup(3, gymnastPeter, event1, PersonSignupState.NOT_SIGNED_UP);

        event1.addPersonSignup(personSignup);
        event1.addPersonSignup(personSignupMartin);
        event1.addPersonSignup(personSignupPeter);

        ObjectNotFoundException e = assertThrows(ObjectNotFoundException.class, () -> event1.editPersonSignup(4, PersonSignupState.SIGNED_UP));
        assertEquals("When editing the PersonSignup object, the object did not exist.", e.getMessage());

        assertEquals(PersonSignupState.NOT_SIGNED_UP, personSignup.getState());
        assertEquals(PersonSignupState.NOT_SIGNED_UP, personSignupMartin.getState());
        assertEquals(PersonSignupState.NOT_SIGNED_UP, personSignupPeter.getState());
    }

    @Test
    void editTeamSignups01() {
        Team blueTeam = new Team("blue");
        Team greenTeam = new Team("green");

        TeamSignup teamSignupBlue = new TeamSignup(2, blueTeam, event1, false);
        TeamSignup teamSignupGreen = new TeamSignup(3, greenTeam, event1, false);

        teamSignupGreen.addPersonSignup(personSignup);

        event1.addTeamSignup(teamSignup);
        event1.addTeamSignup(teamSignupBlue);
        event1.addTeamSignup(teamSignupGreen);

        assertTrue(teamSignup.isSignedUp());
        assertFalse(teamSignupBlue.isSignedUp());
        assertFalse(teamSignupGreen.isSignedUp());
        assertEquals(PersonSignupState.NOT_SIGNED_UP, personSignup.getState());

        event1.editTeamSignup(3, true);

        assertTrue(teamSignup.isSignedUp());
        assertFalse(teamSignupBlue.isSignedUp());
        assertTrue(teamSignupGreen.isSignedUp());
        assertEquals(PersonSignupState.SIGNED_UP, personSignup.getState());

    }

    @Test
    void editTeamSignups02() {
        Team blueTeam = new Team("blue");
        Team greenTeam = new Team("green");

        TeamSignup teamSignupBlue = new TeamSignup(2, blueTeam, event1, false);
        TeamSignup teamSignupGreen = new TeamSignup(3, greenTeam, event1, false);

        teamSignupGreen.addPersonSignup(personSignup);

        event1.addTeamSignup(teamSignup);
        event1.addTeamSignup(teamSignupBlue);
        event1.addTeamSignup(teamSignupGreen);

        ObjectNotFoundException e = assertThrows(ObjectNotFoundException.class, () -> event1.editTeamSignup(4, true));
        assertEquals("When editing the TeamSignup object, the object did not exist.", e.getMessage());

        assertTrue(teamSignup.isSignedUp());
        assertFalse(teamSignupBlue.isSignedUp());
        assertFalse(teamSignupGreen.isSignedUp());
    }

    @Test
    void getName() {
        assertEquals(eventName, event1.getName());
    }

    @Test
    void setName() {
        String newName = "Dancup";
        event1.setName(newName);
        assertEquals(newName, event1.getName());
    }

    @Test
    void getAddress() {
        assertEquals(eventAddress, event1.getAddress());

    }

    @Test
    void setAddress() {
        Address newAddress = new Address("Kalderupvej 42", "9000 Aalborg");
        event1.setAddress(newAddress);
        assertEquals(newAddress, event1.getAddress());

    }

    @Test
    void getCarList() {
        assertTrue(event1.getCarList().isEmpty());

    }

    @Test
    void viewTeamSignups() {
        assertTrue(event1.viewTeamSignups().isEmpty());

    }

    @Test
    void addTeamSignup01() {
        event1.addTeamSignup(teamSignup);
        assertEquals(1, event1.viewTeamSignups().size());
    }

    @Test
    void addTeamSignup02() {
        event1.addTeamSignup(teamSignup);
        IllegalObjectException e = assertThrows(IllegalObjectException.class, () -> event1.addTeamSignup(teamSignup));

        assertEquals("The TeamSignup object cannot be added to the list in the Event object.", e.getMessage());
    }

    @Test
    void addTeamSignup03() {
        TeamSignup teamSignupTest = new TeamSignup(team, new Event("Dancup", eventAddress, eventStartDate, eventEndDate, season, judgeAndCoach, judgeAndCoach), true);
        event1.addTeamSignup(teamSignup);
        IllegalObjectException e = assertThrows(IllegalObjectException.class, () -> event1.addTeamSignup(teamSignupTest));
        assertEquals("The TeamSignup object cannot be added to the list in the Event object.", e.getMessage());

    }


    @Test
    void deleteTeamSignup01() {
        event1.addTeamSignup(teamSignup);
        event1.deleteTeamSignup(teamSignup);
        assertTrue(event1.viewTeamSignups().isEmpty());
    }

    @Test
    void deleteTeamSignup02() {
        ObjectNotFoundException e = assertThrows(ObjectNotFoundException.class, () -> event1.deleteTeamSignup(teamSignup));
        assertEquals("The TeamSignup object was not found in the Event object.", e.getMessage());
    }

    @Test
    void getStartDate() {
        assertEquals(eventStartDate, event1.getStartDate());
    }

    @Test
    void setStartDate() {
        LocalDateTime newDate = today.plusMonths(1);
        event1.setStartDate(newDate);
        assertEquals(newDate, event1.getStartDate());
    }

    @Test
    void getEndDate() {
        assertEquals(eventEndDate, event1.getEndDate());

    }

    @Test
    void setEndDate() {
        LocalDate newDate = today.toLocalDate().plusMonths(1);
        event1.setEndDate(newDate);
        assertEquals(newDate, event1.getEndDate());
    }

    @Test
    void getSeason() {
        assertEquals(season, event1.getSeason());

    }

    @Test
    void isActive() {
        assertTrue(event1.isActive());
    }


    @Test
    void addCar01() {

        event1.addCar(person);
        assertEquals(1, event1.getCarList().size());
        assertTrue(event1.getCarList().get(0).hasPerson(person));
    }

    @Test
    void addCar02() {
        Car car = new Car(event1, person);

        event1.addCar(car);
        assertEquals(1, event1.getCarList().size());
        assertTrue(event1.getCarList().get(0).hasPerson(person));
    }

    @Test
    void addCar03() {
        event1.addCar(person);
        IllegalObjectException e = assertThrows(IllegalObjectException.class, () -> event1.addCar(person));
        assertEquals("This Person object is already assigned to a car in the Event object.", e.getMessage());
    }

    @Test
    void addCar04() {
        Car car = new Car(event1, person);

        event1.addCar(car);
        IllegalObjectException e = assertThrows(IllegalObjectException.class, () -> event1.addCar(car));

        assertEquals("The Car object cannot be added to the list in the Event object.", e.getMessage());
    }

    @Test
    void addCar05() {
        Car car = new Car(new Event("Dancup", eventAddress, eventStartDate, eventEndDate, season, judgeAndCoach, judgeAndCoach), person);
        IllegalObjectException e = assertThrows(IllegalObjectException.class, () -> event1.addCar(car));

        assertEquals("The Car object cannot be added to the list in the Event object.", e.getMessage());
    }

    @Test
    void addPersonSignup01() {
        event1.addPersonSignup(personSignup);
        assertEquals(personSignup, event1.getPersonSignupList().get(0));
    }

    @Test
    void addPersonSignup02() {
        event1.addPersonSignup(personSignup);
        IllegalObjectException e = assertThrows(IllegalObjectException.class, () -> event1.addPersonSignup(personSignup));

        assertEquals("The PersonSignup object cannot be added to the list in the Event object.", e.getMessage());

    }

    @Test
    void addPersonSignup03() {
        PersonSignup personSignupTest = new PersonSignup(gymnast, new Event("Dancup", eventAddress, eventStartDate, eventEndDate, season, judgeAndCoach, judgeAndCoach));
        event1.addPersonSignup(personSignup);
        IllegalObjectException e = assertThrows(IllegalObjectException.class, () -> event1.addPersonSignup(personSignupTest));

        assertEquals("The PersonSignup object cannot be added to the list in the Event object.", e.getMessage());
    }


    @Test
    void getPersonSignupList() {
        event1.addPersonSignup(personSignup);
        assertEquals(1, event1.getPersonSignupList().size());

    }

    @Test
    void deletePersonSignup01() {
        event1.addPersonSignup(personSignup);
        assertEquals(personSignup, event1.getPersonSignupList().get(0));
        event1.deletePersonSignup(personSignup);
        assertTrue(event1.getPersonSignupList().isEmpty());
    }

    @Test
    void deletePersonSignup02() {
        ObjectNotFoundException e = assertThrows(ObjectNotFoundException.class, () -> event1.deletePersonSignup(personSignup));
        assertEquals("The PersonSignup object was not found in the Event object.", e.getMessage());
    }


    @Test
    void getPersonsSignedUp01() {
        assertTrue(event1.getPersonsSignedUp().isEmpty());
        event1.addPersonSignup(personSignup);
        assertEquals(person, event1.getPersonsSignedUp().get(0));
    }

    @Test
    void getPersonsSignedUp02() {
        Coach coach = new Coach(person, true);
        Judge judge = new Judge(person, true);

        PersonSignup personSignupCoach = new PersonSignup(coach, event1);
        PersonSignup personSignupJudge = new PersonSignup(judge, event1);

        event1.addPersonSignup(personSignup);
        event1.addPersonSignup(personSignupCoach);
        event1.addPersonSignup(personSignupJudge);

        assertEquals(1, event1.getPersonsSignedUp().size());
        assertEquals(person, event1.getPersonsSignedUp().get(0));
    }

    @Test
    void viewGymnastPersonSignups() {
        Coach coach = new Coach(person, true);
        Judge judge = new Judge(person, true);

        PersonSignup personSignupCoach = new PersonSignup(coach, event1);
        PersonSignup personSignupJudge = new PersonSignup(judge, event1);

        event1.addPersonSignup(personSignup);
        event1.addPersonSignup(personSignupCoach);
        event1.addPersonSignup(personSignupJudge);

        assertEquals(1, event1.viewGymnastPersonSignups().size());
        assertEquals(personSignup, event1.viewGymnastPersonSignups().get(0));

    }

    @Test
    void viewCoachPersonSignups() {
        Coach coach = new Coach(person, true);
        Judge judge = new Judge(person, true);

        PersonSignup personSignupCoach = new PersonSignup(coach, event1);
        PersonSignup personSignupJudge = new PersonSignup(judge, event1);

        event1.addPersonSignup(personSignup);
        event1.addPersonSignup(personSignupCoach);
        event1.addPersonSignup(personSignupJudge);

        assertEquals(1, event1.viewCoachPersonSignups().size());
        assertEquals(personSignupCoach, event1.viewCoachPersonSignups().get(0));

    }

    @Test
    void viewJudgePersonSignups() {
        Coach coach = new Coach(person, true);
        Judge judge = new Judge(person, true);

        PersonSignup personSignupCoach = new PersonSignup(coach, event1);
        PersonSignup personSignupJudge = new PersonSignup(judge, event1);

        event1.addPersonSignup(personSignup);
        event1.addPersonSignup(personSignupCoach);
        event1.addPersonSignup(personSignupJudge);

        assertEquals(1, event1.viewJudgePersonSignups().size());
        assertEquals(personSignupJudge, event1.viewJudgePersonSignups().get(0));

    }


    @Test
    void delete01() {
        event1.addPersonSignup(personSignup);
        person.addPersonSignup(personSignup);

        event1.addTeamSignup(teamSignup);
        team.addTeamSignup(teamSignup);

        event1.addCar(person);

        assertEquals(1, person.getPersonSignupList().size());
        assertEquals(1, team.getTeamSignupList().size());
        assertEquals(1, person.getCarAssignmentList().size());
        assertEquals(1, event1.getPersonSignupList().size());
        assertEquals(1, event1.viewTeamSignups().size());
        assertEquals(1, event1.getCarList().size());

        event1.delete();

        assertEquals(1, event1.getPersonSignupList().size());
        assertEquals(1, event1.viewTeamSignups().size());
        assertEquals(1, event1.getCarList().size());

        assertTrue(person.getPersonSignupList().isEmpty());
        assertTrue(team.getTeamSignupList().isEmpty());
        assertTrue(person.getCarAssignmentList().isEmpty());

    }

    @Test
    void delete02() {
        Coach coach = new Coach(person, true);
        Judge judge = new Judge(person, true);
        PersonSignup personSignup2 = new PersonSignup(coach, event1);
        PersonSignup personSignup3 = new PersonSignup(judge, event1);

        event1.addPersonSignup(personSignup);
        event1.addPersonSignup(personSignup2);
        event1.addPersonSignup(personSignup3);

        person.addPersonSignup(personSignup);
        person.addPersonSignup(personSignup2);
        person.addPersonSignup(personSignup3);

        assertEquals(3, person.getPersonSignupList().size());
        assertEquals(3, event1.getPersonSignupList().size());

        event1.delete();

        assertEquals(3, event1.getPersonSignupList().size());
        assertTrue(person.getPersonSignupList().isEmpty());
    }

    @Test
    void delete03() {
        Team blueTeam = new Team("blue");
        Team greenTeam = new Team("green");
        TeamSignup blueTeamSignup = new TeamSignup(blueTeam, event1, true);
        TeamSignup greenTeamSignup = new TeamSignup(greenTeam, event1, true);

        event1.addTeamSignup(teamSignup);
        event1.addTeamSignup(blueTeamSignup);
        event1.addTeamSignup(greenTeamSignup);

        team.addTeamSignup(teamSignup);
        blueTeam.addTeamSignup(blueTeamSignup);
        greenTeam.addTeamSignup(greenTeamSignup);

        assertEquals(1, team.getTeamSignupList().size());
        assertEquals(1, blueTeam.getTeamSignupList().size());
        assertEquals(1, greenTeam.getTeamSignupList().size());
        assertEquals(3, event1.viewTeamSignups().size());

        event1.delete();

        assertEquals(3, event1.viewTeamSignups().size());
        assertTrue(team.getTeamSignupList().isEmpty());
        assertTrue(blueTeam.getTeamSignupList().isEmpty());
        assertTrue(greenTeam.getTeamSignupList().isEmpty());

    }

    @Test
    void delete04() {
        Person personMartin = new Person("Martin", true, today.toLocalDate(), true);
        Person personPeter = new Person("Peter", true, today.toLocalDate(), true);

        event1.addCar(person);
        event1.addCar(personMartin);
        event1.addCar(personPeter);

        assertEquals(1, person.getCarAssignmentList().size());
        assertEquals(1, personMartin.getCarAssignmentList().size());
        assertEquals(1, personPeter.getCarAssignmentList().size());
        assertEquals(3, event1.getCarList().size());

        event1.delete();

        assertEquals(3, event1.getCarList().size());
        assertTrue(person.getCarAssignmentList().isEmpty());
        assertTrue(personMartin.getCarAssignmentList().isEmpty());
        assertTrue(personPeter.getCarAssignmentList().isEmpty());


    }

    @Test
    void clearCarsForDrivers() {
        event1.addCar(person);
        event1.addCar(new Person("Peter", true, today.toLocalDate(), true));

        event1.clearAllCars();

        assertEquals(2, event1.getCarList().size());
        assertEquals(4, event1.getCarList().get(0).getAvailableSeatCount());
        assertEquals(4, event1.getCarList().get(1).getAvailableSeatCount());

    }

    @Test
    void daysTillDeadline() {
        event1.setStartDate(LocalDateTime.now().plusDays(22));
        assertEquals(1, event1.daysTillDeadline());
    }


    @Test
    void countRoles01() {
        event1.countRoles();
        assertEquals(0, event1.getNumberOfGymnastsSignedUp());
        assertEquals(0, event1.getNumberOfCoachesSignedUp());
        assertEquals(0, event1.getNumberOfJudgesSignedUp());
    }

    @Test
    void countRoles02() {
        Person personMartin = new Person("Martin", true, today.toLocalDate(), true);
        Gymnast gymnastMartin = new Gymnast(personMartin, "0123456789", true);
        Gymnast gymnast = new Gymnast(person, "9876543210", true);

        PersonSignup personSignupMartin = new PersonSignup(gymnastMartin, event1);
        personSignup = new PersonSignup(gymnast, event1);

        personSignupMartin.setState(PersonSignupState.SIGNED_UP);
        personSignup.setState(PersonSignupState.SIGNED_UP);

        event1.addPersonSignup(personSignupMartin);
        event1.addPersonSignup(personSignup);

        event1.countRoles();

        assertEquals(2, event1.getNumberOfGymnastsSignedUp());
    }

    @Test
    void countRoles03() {
        Person personMartin = new Person("Martin", true, today.toLocalDate(), true);
        Coach coachMartin = new Coach(personMartin, true);
        Coach coach = new Coach(person, true);

        PersonSignup personSignupMartin = new PersonSignup(coachMartin, event1);
        personSignup = new PersonSignup(coach, event1);

        personSignupMartin.setState(PersonSignupState.SIGNED_UP);
        personSignup.setState(PersonSignupState.SIGNED_UP);

        event1.addPersonSignup(personSignupMartin);
        event1.addPersonSignup(personSignup);

        event1.countRoles();

        assertEquals(2, event1.getNumberOfCoachesSignedUp());
    }

    @Test
    void countRoles04() {
        Person personMartin = new Person("Martin", true, today.toLocalDate(), true);
        Judge judgeMartin = new Judge(personMartin, true);
        Judge judge = new Judge(person, true);

        PersonSignup personSignupMartin = new PersonSignup(judgeMartin, event1);
        personSignup = new PersonSignup(judge, event1);

        personSignupMartin.setState(PersonSignupState.SIGNED_UP);
        personSignup.setState(PersonSignupState.SIGNED_UP);

        event1.addPersonSignup(personSignupMartin);
        event1.addPersonSignup(personSignup);

        event1.countRoles();

        assertEquals(2, event1.getNumberOfJudgesSignedUp());
    }

    @Test
    void generatePersonSignupsFromList01() {
        List<Team> teamList = new ArrayList<>();  // Used when generateTeamsignupsFromList() is called.
        List<Person> personList = new ArrayList<>(); //Used when generateTeamAssignments() and generatePersonSignupsFromList is called.
        List<String> teamNameList = new ArrayList<>(); //Used when updateTeamAssignments() is called.

        Person personPeter = new Person("Peter", true, today.toLocalDate(), true); // Person initiated
        Gymnast gymnastPeter = new Gymnast(personPeter, "123456", true); // Gymnast initiated
        personPeter.setRole(gymnastPeter, Person.GYMNAST_INDEX); // personPeter assigned to gymnastPeter.

        person.setRole(gymnast, Person.GYMNAST_INDEX); //person assigned to gymnast

        personList.add(person);
        personList.add(personPeter);

        Team teamTalent = new Team("Talent");
        Team teamElite = new Team("Elite");

        teamList.add(teamTalent);
        teamList.add(teamElite);

        teamTalent.generateTeamAssignments(personList); //TeamAssignments generated for personPeter and person
        teamElite.generateTeamAssignments(personList); //TeamAssignments generated for personPeter and person

        teamNameList.add(teamElite.getName());
        teamNameList.add(teamTalent.getName());

        gymnast.updateTeamAssignments(teamNameList); //gymnast joins Talent and Elite.
        gymnastPeter.updateTeamAssignments(teamNameList); //gymnastPeter joins Talent and Elite.

        event1.generateTeamSignupsFromList(teamList); //Events generate teamsignup from list of teams.
        event1.generatePersonSignupsFromList(personList); //

        assertEquals(2, gymnast.getTeamAssignmentList().size());
        assertEquals(2, event1.getPersonSignupList().size());
        assertEquals(2, event1.viewTeamSignups().size());
        assertEquals(1, teamElite.getTeamSignupList().size());
        assertEquals(1, teamTalent.getTeamSignupList().size());
        assertEquals(1, person.getPersonSignupList().size());
        assertEquals(1, personPeter.getPersonSignupList().size());
        assertEquals(2, event1.viewTeamSignups().get(0).getPersonSignupList().size());

    }

    @Test
    void generatePersonSignupsFromList02() {
        List<Person> personList = new ArrayList<>();

        Person personMartin = new Person("Martin", true, today.toLocalDate(), true);
        Coach coach = new Coach(personMartin, true);
        personMartin.setRole(coach, Person.COACH_INDEX);

        Person personPeter = new Person("Peter", true, today.toLocalDate(), true);
        Judge judge = new Judge(personPeter, true);
        personPeter.setRole(judge, Person.JUDGE_INDEX);


        personList.add(personMartin);
        personList.add(personPeter);

        event1.generatePersonSignupsFromList(personList);

        assertEquals(2, event1.getPersonSignupList().size());

    }

    @Test
    void generatePersonSignupsFromList03() {
        List<Person> personList = new ArrayList<>();
        List<String> teamNameList = new ArrayList<>();
        Team teamTalent = new Team("Talent");

        person.setRole(gymnast, Person.GYMNAST_INDEX);

        personList.add(person);
        teamNameList.add(teamTalent.getName());

        teamTalent.generateTeamAssignments(personList);

        gymnast.updateTeamAssignments(teamNameList);

        IllegalObjectException e = assertThrows(IllegalObjectException.class, () -> event1.generatePersonSignupsFromList(personList));
        assertEquals("Could not find TeamSignup for the PersonSignup of person " + person.getID() + ".", e.getMessage());

    }

    @Test
    void editPersonSignup01() {
        PersonSignup personSignupTest = new PersonSignup(1, gymnast, event1, PersonSignupState.NOT_SIGNED_UP);
        event1.addPersonSignup(personSignupTest);

        event1.editPersonSignup(1, PersonSignupState.SIGNED_UP);

        assertEquals(PersonSignupState.SIGNED_UP, event1.getPersonSignupList().get(0).getState());
    }

    @Test
    void editPersonSignup02() {
        PersonSignup personSignupTest = new PersonSignup(1, gymnast, event1, PersonSignupState.NOT_SIGNED_UP);
        event1.addPersonSignup(personSignupTest);

        ObjectNotFoundException e = assertThrows(ObjectNotFoundException.class, () -> event1.editPersonSignup(2, PersonSignupState.SIGNED_UP));
        assertEquals("When editing the PersonSignup object, the object did not exist.", e.getMessage());

    }

    @Test
    void equals1() {
        Event testEvent = new Event(eventName, eventAddress, eventStartDate, eventEndDate, season, judgeAndCoach, judgeAndCoach);
        assertTrue(testEvent.equals(event1));
    }

    @Test
    void equals2() {
        Event testEvent = new Event("SpringNorge", eventAddress, eventStartDate, eventEndDate, season, judgeAndCoach, judgeAndCoach);
        assertFalse(testEvent.equals(event1));
    }

    @Test
    void equals3() {
        Event testEvent = new Event(eventName, new Address("Kalderupvej 24", "9000 Aalborg"), eventStartDate, eventEndDate, season, judgeAndCoach, judgeAndCoach);
        assertFalse(testEvent.equals(event1));
    }

    @Test
    void equals4() {
        LocalDateTime startDate = eventStartDate.minusDays(2);
        Event testEvent = new Event(eventName, eventAddress, startDate, eventEndDate, season, judgeAndCoach, judgeAndCoach);
        assertFalse(testEvent.equals(event1));
    }

    @Test
    void equals5() {
        LocalDate endDate = eventEndDate.plusDays(2);
        Event testEvent = new Event("SpringDanmark", new Address("Kalderupvej 21", "9000 Aalborg,"), eventStartDate, endDate, season, judgeAndCoach, judgeAndCoach);
        assertFalse(testEvent.equals(event1));
    }

    @Test
    void equals6() {
        LocalDateTime startDate = today;
        LocalDate endDate = today.toLocalDate();
        Event testEvent = new Event(eventName, eventAddress, startDate, endDate, new Season(startDate.minusYears(30), today), judgeAndCoach, judgeAndCoach);
        assertFalse(testEvent.equals(event1));
    }


    @Test
    void hashcode() {
        assertEquals(Objects.hash(eventName, eventAddress, eventStartDate, eventEndDate, season), event1.hashCode());

    }

    @Test
    void compareTo1() {
        Event testEvent = new Event(eventName, eventAddress, eventStartDate, eventEndDate, season, judgeAndCoach, judgeAndCoach);
        assertTrue(testEvent.compareTo(event1) == 0);
    }

    @Test
    void compareTo2() {
        Event testEvent = new Event(eventName, eventAddress, eventStartDate, eventEndDate.minusDays(20), season, judgeAndCoach, judgeAndCoach);
        assertTrue(0 < testEvent.compareTo(event1));
    }

    //Skal de her to ikke vendes om så man får en højrer tal jo nyere datoen er?
    @Test
    void compareTo3() {
        LocalDateTime startDate = eventStartDate.minusDays(7);
        Event testEvent = new Event(eventName, eventAddress, startDate, eventEndDate, season, judgeAndCoach, judgeAndCoach);
        assertTrue(testEvent.compareTo(event1) < 0);
    }

    @Test
    void compareTo4() {
        LocalDateTime startDate = eventStartDate.plusDays(7);
        Event testEvent = new Event(eventName, eventAddress, startDate, eventEndDate, season, judgeAndCoach, judgeAndCoach);
        assertTrue(testEvent.compareTo(event1) > 0);
    }

    @Test
    void compareTo5() {
        Event testEvent = new Event("AndetNavn", eventAddress, eventStartDate, eventEndDate, season, judgeAndCoach, judgeAndCoach);
        assertTrue(testEvent.compareTo(event1) < 0);
    }

    @Test
    void compareTo6() {
        Event testEvent = new Event("ÅndetNavn", eventAddress, eventStartDate, eventEndDate, season, judgeAndCoach, judgeAndCoach);
        assertTrue(testEvent.compareTo(event1) > 0);
    }

    @Test
    void compareTo7() {
        Event testEvent = new Event(eventName, eventAddress, eventStartDate, eventEndDate, season, judgeAndCoach, judgeAndCoach);
        event1.setEndDate(eventEndDate.minusMonths(1));
        event1.updateState();
        assertTrue(0 > testEvent.compareTo(event1));

    }

    @Test
    void getID() {
        assertEquals(1, event1.getID());

    }
}
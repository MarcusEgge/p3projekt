package Model;

import Model.Exceptions.IllegalIDException;
import Model.Exceptions.IllegalObjectException;
import Model.Exceptions.ObjectNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Objects;

import static org.junit.jupiter.api.Assertions.*;

class PersonSignupTest {

    private Gymnast gymnast;
    private Judge judge;
    private Season season;
    private Event event1;
    private Person person1, person2;
    private Team team;
    private PersonSignup personSignup1, personSignup2;
    private TeamSignup teamSignup;
    private LocalDateTime today;
    private Address address;
    private LocalDateTime startDate;
    private LocalDate endDate;
    private int judgeAndCoach;

    //2 Person, 1 Team, 1 Gymnast, 1 Judge, 1 Season, 1 Address, 1 Event, 1 TeamSignup and 2 PersonSignup.
    @BeforeEach
    void initObjects() {
        today = LocalDateTime.now();
        person1 = new Person("Martin", true, LocalDate.now(), true);
        person2 = new Person("Sigurd", true, LocalDate.now(), true);
        team = new Team("Rød");
        gymnast = new Gymnast(person1, "121202", true);
        judge = new Judge(person2, true);
        season = new Season(today, today);
        address = new Address("Kalderupvej 21", "9000 Aalborg");
        startDate = LocalDateTime.now();
        endDate = LocalDate.now();
        judgeAndCoach = 6;
        event1 = new Event("SpringDanmark", address, startDate, endDate, season, judgeAndCoach, judgeAndCoach);
        teamSignup = new TeamSignup(team, event1, true);
        personSignup1 = new PersonSignup(gymnast, event1);
        personSignup2 = new PersonSignup(judge, event1);
        teamSignup.addPersonSignup(personSignup1);  //personSignup1 added to teamSignup.
        personSignup1.addTeamSignup(teamSignup);    //teamSignup added to personSignup1.
        event1.addPersonSignup(personSignup1);      //personSignup1 added to event1, but gymnast is not signed up to event.

    }

    //Exception test. Not allowed to initiate with a negative id.
    @Test
    void constructor01() {
        IllegalIDException e = assertThrows(IllegalIDException.class, () -> new PersonSignup(-1, gymnast, event1, PersonSignupState.SIGNED_UP));
        assertEquals("The PersonSignup id is illegal.", e.getMessage());
    }

    //Exception test. Not allowed to initiate with id 0.
    @Test
    void constructor02() {
        IllegalIDException e = assertThrows(IllegalIDException.class, () -> new PersonSignup(0, gymnast, event1, PersonSignupState.SIGNED_UP));
        assertEquals("The PersonSignup id is illegal.", e.getMessage());
    }

    //Exception test. Not allowed to initiate with null as Role.
    @Test
    void constructor03() {
        assertThrows(NullPointerException.class, () -> new PersonSignup(1, null, event1, PersonSignupState.SIGNED_UP));
    }

    //Exception test. Not allowed to initiate with null as Event.
    @Test
    void constructor04() {
        assertThrows(NullPointerException.class, () -> new PersonSignup(1, gymnast, null, PersonSignupState.SIGNED_UP));
    }

    //Exception test. Not allowed to initiate with null as PersonSignupState.
    @Test
    void constructor05() {
        assertThrows(NullPointerException.class, () -> new PersonSignup(1, gymnast, event1, null));
    }

    //Exception test. Gymnast can't be undecided.
    @Test
    void constructor06() {
        IllegalObjectException e = assertThrows(IllegalObjectException.class, () -> new PersonSignup(1, gymnast, event1, PersonSignupState.UNDECIDED));
        assertEquals("The state of a PersonSignup object with a Gymnast object for field 'role' cannot be undecided", e.getMessage());
    }

    @Test
    void getID() {
        assertEquals(0, personSignup1.getID());
    }

    @Test
    void setID01() {
        personSignup1.setID(10);
        assertEquals(10, personSignup1.getID());
    }

    //Exception test. Not allowed to change id, if id > 0.
    @Test
    void setID02() {
        PersonSignup personSignupTest = new PersonSignup(10, gymnast, event1, PersonSignupState.SIGNED_UP);
        IllegalIDException e = assertThrows(IllegalIDException.class, () -> personSignupTest.setID(2));
        assertEquals("The PersonSignup id is illegal.", e.getMessage());
        assertEquals(10, personSignupTest.getID());
    }


    @Test
    void getPerson01() {
        assertEquals(person1, personSignup1.getPerson());
    }

    @Test
    void getPerson02() {
        assertEquals(person2, personSignup2.getPerson());
    }

    @Test
    void getRole01() {
        assertEquals(gymnast, personSignup1.getRole());
    }

    @Test
    void getRole02() {
        assertEquals(judge, personSignup2.getRole());
    }

    @Test
    void getEvent01() {
        assertEquals(event1, personSignup1.getEvent());
    }

    @Test
    void getEvent02() {
        assertEquals(event1, personSignup2.getEvent());
    }

    @Test
    void isSignedUp01() {
        assertEquals(PersonSignupState.NOT_SIGNED_UP, personSignup1.getState());
    }

    @Test
    void isSignedUp02() {
        personSignup2.setState(PersonSignupState.SIGNED_UP);
        assertEquals(PersonSignupState.SIGNED_UP, personSignup2.getState());
    }

    //Exception test. Gymnast state can't be set to undecided.
    @Test
    void setSignedUp01() {
        IllegalObjectException e = assertThrows(IllegalObjectException.class, () -> personSignup1.setState(PersonSignupState.UNDECIDED));
        assertEquals("A PersonSignup with a Gymnast object for role cannot be undecided.", e.getMessage());
    }

    @Test
    void setSignedUp02() {
        personSignup2.setState(PersonSignupState.UNDECIDED);
        assertEquals(PersonSignupState.UNDECIDED, personSignup2.getState());
    }

    @Test
    void deleteFromEvent() {
        event1.addPersonSignup(personSignup2);
        assertEquals(2, event1.getPersonsSignedUp().size());

        personSignup2.deleteFromEvent();
        assertEquals(1, event1.getPersonsSignedUp().size());
    }


    @Test
    void deleteFromTeamSignup() {
        assertFalse(teamSignup.getPersonSignupList().isEmpty());
        personSignup1.deleteFromTeamSignups();

        assertTrue(teamSignup.getPersonSignupList().isEmpty());
    }

    @Test
    void deleteTeamSignup01() {
        personSignup1.deleteTeamSignup(teamSignup);
        assertTrue(personSignup1.getTeamSignupList().isEmpty());
    }

    //Exception test. Unable to delete a TeamSignup that doesn't exist.
    @Test
    void deleteTeamSignup02() {
        ObjectNotFoundException e = assertThrows(ObjectNotFoundException.class, () -> personSignup2.deleteTeamSignup(teamSignup));
        assertEquals("The TeamSignup object was not found in the PersonSignup object.", e.getMessage());

    }

    @Test
    void getTeamSignupList() {
        assertEquals(1, personSignup1.getTeamSignupList().size());
    }

    @Test
    void addTeamSignup01() {
        assertTrue(personSignup1.getTeamSignupList().contains(teamSignup));
    }

    //Exception test. Not allowed to add teamSignup if PersonSignup isn't for a Gymnast.
    @Test
    void addTeamSignup02() {
        IllegalObjectException e = assertThrows(IllegalObjectException.class, () -> personSignup2.addTeamSignup(teamSignup));
        assertEquals("The TeamSignup object cannot be added to the list in the PersonSignup object.", e.getMessage());
    }

    //Exception test. Not allowed to add teamSignup from a another event.
    @Test
    void addTeamSignup03() {
        TeamSignup teamSignupTest = new TeamSignup(team, new Event("Dancup", address, startDate, endDate, season, judgeAndCoach, judgeAndCoach), true);
        IllegalObjectException e = assertThrows(IllegalObjectException.class, () -> personSignup2.addTeamSignup(teamSignupTest));
        assertEquals("The TeamSignup object cannot be added to the list in the PersonSignup object.", e.getMessage());
    }

    @Test
    void deleteFromPerson() {
        person1.addPersonSignup(personSignup1);
        assertTrue(person1.getPersonSignupList().contains(personSignup1));

        personSignup1.deleteFromPerson();
        assertFalse(person1.getPersonSignupList().contains(personSignup1));
    }

    @Test
    void equals01() {
        Event eventTemp = new Event("Dancup", address, startDate, endDate, season, judgeAndCoach, judgeAndCoach);
        PersonSignup personSignupTemp = new PersonSignup(gymnast, eventTemp);

        assertNotEquals(personSignupTemp, personSignup1);
    }

    @Test
    void equals02() {
        PersonSignup personSignupTemp = new PersonSignup(gymnast, event1);

        assertEquals(personSignupTemp, personSignup1);

    }

    @Test
    void hashcode() {
        Person personTemp = new Person("Martin", true, LocalDate.now(), true);
        Gymnast gymnastTemp = new Gymnast(personTemp, "121202", true);

        assertEquals(Objects.hash(personTemp, gymnastTemp, event1), personSignup1.hashCode());
    }

}
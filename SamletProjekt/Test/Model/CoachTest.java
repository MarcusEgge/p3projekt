package Model;

import Model.Exceptions.IllegalIDException;
import Model.Exceptions.IllegalObjectException;
import Model.Exceptions.ObjectNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class CoachTest {

    private Coach coach;
    private Person person;
    private List<TeamAssignment> teamAssignmentsList;
    private Team redTeam, blueTeam;
    private TeamAssignment teamAssignment;

    //1 Person, 2 Teams, 1 Coach, 1 TeamAssignment and 1 list of TeamAssignment.
    @BeforeEach
    public void initObjects() {
        teamAssignmentsList = new ArrayList<>();
        person = new Person("Rudolf", true, LocalDate.now(), true);
        redTeam = new Team("Red");
        blueTeam = new Team("Blue");
        coach = new Coach(person, true);
        teamAssignment = new TeamAssignment(redTeam, coach, true);
        redTeam.addTeamAssignment(teamAssignment); //teamAssignment assigned to redTeam.
    }

    //Exception test. Not allowed to instantiate a Coach with id 0.
    @Test
    void constructor01() {
        IllegalIDException e = assertThrows(IllegalIDException.class, () -> new Coach(0, person, true));
        assertEquals("The id is illegal.", e.getMessage());
    }

    //Exception test. Not allowed to instantiate a Coach with negative integer as id.
    @Test
    void constructor02() {
        IllegalIDException e = assertThrows(IllegalIDException.class, () -> new Coach(-1, person, true));
        assertEquals("The id is illegal.", e.getMessage());
    }

    //Exception test. Not allowed to instantiate a Coach with null as person.
    @Test
    void constructor03() {
        assertThrows(NullPointerException.class, () -> new Coach(1, null, true));
    }

    @Test
    void getID() {
        Coach coachTest = new Coach(1, person, true);
        assertEquals(1, coachTest.getID());
    }

    @Test
    void getTeamAssignmentList() {
        assertEquals(teamAssignmentsList, coach.getTeamAssignmentList());
    }

    @Test
    void addTeamAssignment01() {
        coach.addTeamAssignment(teamAssignment);
        assertEquals(teamAssignment, coach.getTeamAssignmentList().get(0));
    }

    //Exception test. Not allowed to add the same TeamAssignment. A TeamAssignment is unique by Team and TeamMember.
    @Test
    void addTeamAssignment02() {
        coach.addTeamAssignment(teamAssignment);
        IllegalObjectException e = assertThrows(IllegalObjectException.class, () -> coach.addTeamAssignment(teamAssignment));
        assertEquals("The TeamAssignment object cannot be added to the list in the Coach object.", e.getMessage());
        assertEquals(1, coach.getTeamAssignmentList().size());
    }

    //Exception test. Not allowed to add a TeamAssignment with a different TeamMember.
    @Test
    void addTeamAssignment03() {
        Person personPeter = new Person("Peter", true, LocalDate.now(), false);

        Coach coachPeter = new Coach(personPeter, true);

        TeamAssignment teamAssignmentPeter = new TeamAssignment(blueTeam, coachPeter, true);

        IllegalObjectException e = assertThrows(IllegalObjectException.class, () -> coach.addTeamAssignment(teamAssignmentPeter));
        assertEquals("The TeamAssignment object cannot be added to the list in the Coach object.", e.getMessage());

    }


    @Test
    void deleteTeamAssignment01() {
        coach.addTeamAssignment(teamAssignment);
        assertEquals(1, coach.getTeamAssignmentList().size());

        coach.deleteTeamAssignment(teamAssignment);
        assertEquals(true, coach.getTeamAssignmentList().isEmpty());
    }


    //Exception test. Unable to delete a TeamAssignment that doesn't exist.
    @Test
    void deleteTeamAssignment02() {

        ObjectNotFoundException e = assertThrows(ObjectNotFoundException.class, () -> coach.deleteTeamAssignment(teamAssignment));
        assertEquals("The TeamAssignment object was not found in the Coach object.", e.getMessage());
    }

    @Test
    void isGymnast() {
        assertFalse(coach.isGymnast());
    }

    @Test
    void isCoach() {
        assertTrue(coach.isCoach());
    }

    @Test
    void isJudge() {
        assertFalse((coach.isJudge()));
    }

    @Test
    void getPerson() {
        assertEquals(person, coach.getPerson());
    }

    @Test
    void isActive() {
        assertTrue(coach.isActive());
    }

    @Test
    void setActive() {
        coach.addTeamAssignment(teamAssignment);

        assertEquals(1, redTeam.getTeamAssignmentList().size());
        assertEquals(1, coach.getTeamAssignmentList().size());
        assertTrue(coach.isActive());

        coach.setActive(false);

        assertEquals(0, redTeam.getTeamAssignmentList().size());
        assertEquals(0, coach.getTeamAssignmentList().size());
        assertFalse(coach.isActive());
    }

    //Stay signed to Blue team and resign from Red team
    @Test
    void updateTeamAssignment01s() {
        coach.addTeamAssignment(teamAssignment);
        coach.addTeamAssignment(new TeamAssignment(blueTeam, coach, true));

        List<String> list = new ArrayList<>();
        list.add("Blue");


        coach.updateTeamAssignments(list);

        assertEquals(2, coach.getTeamAssignmentList().size());
        assertFalse(coach.getTeamAssignmentList().get(0).isSignedUp());
        assertTrue(coach.getTeamAssignmentList().get(1).isSignedUp());

    }

    //Resign from Blue team and Red team
    @Test
    void updateTeamAssignments02() {
        coach.addTeamAssignment(teamAssignment);
        coach.addTeamAssignment(new TeamAssignment(blueTeam, coach, true));

        assertEquals(2, coach.getTeamAssignmentList().size());
        assertTrue(coach.getTeamAssignmentList().get(0).isSignedUp());
        assertTrue(coach.getTeamAssignmentList().get(1).isSignedUp());

        coach.updateTeamAssignments(new ArrayList<>());

        assertEquals(2, coach.getTeamAssignmentList().size());
        assertFalse(coach.getTeamAssignmentList().get(0).isSignedUp());
        assertFalse(coach.getTeamAssignmentList().get(1).isSignedUp());
    }

    //Coach teamAssignmentList is divided into a list with id > 0 and a list with id == 0.
    @Test
    void getTeamAssignmentsSavedAndNotSaved() {

        List<TeamAssignment> saved = new ArrayList<>();
        List<TeamAssignment> notSaved = new ArrayList<>();

        coach.addTeamAssignment(new TeamAssignment(1, new Team("Green1"), coach, true));
        coach.addTeamAssignment(new TeamAssignment(2, new Team("Green2"), coach, true));
        coach.addTeamAssignment(new TeamAssignment(new Team("Green3"), coach, true));
        coach.addTeamAssignment(new TeamAssignment(4, new Team("Green4"), coach, true));
        coach.addTeamAssignment(new TeamAssignment(new Team("Green5"), coach, true));
        coach.addTeamAssignment(new TeamAssignment(6, new Team("Green6"), coach, true));
        coach.addTeamAssignment(new TeamAssignment(7, new Team("Green7"), coach, true));

        coach.getTeamAssignmentsSavedAndNotSaved(saved, notSaved);

        assertEquals(5, saved.size());
        assertEquals(2, notSaved.size());


    }

    @Test
    void equals01() {
        Person personPeter = new Person("Peter", true, LocalDate.now(), false);

        Coach coachPeter = new Coach(personPeter, true);

        assertNotEquals(coachPeter, coach);
    }

    @Test
    void equals02() {
        Coach coachTest = new Coach(person, true);

        assertEquals(coachTest, coach);
    }

    @Test
    void hashCode01() {
        Person personPeter = new Person("Peter", true, LocalDate.now(), false);
        Coach coachPeter = new Coach(personPeter, true);

        assertNotEquals(coachPeter.hashCode(), coach.hashCode());
    }

    @Test
    void hashCode02() {
        Coach coachTest = new Coach(person, true);

        assertEquals(coachTest.hashCode(), coach.hashCode());
    }
}
package Model;

import Model.Exceptions.IllegalFormatException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class AddressTest {
    private Address address;
    String road, city;

    // 1 Adress.
    @BeforeEach
    void initObjects() {
        road = "Enellerandenvej 10";
        city = "Aalborg";
        address = new Address(road, city);
    }

    @Test
    void constructor01() {
        String roadAndCity = road + " | " + city;
        assertEquals(new Address(roadAndCity), address);

    }


    // Exception test. Wrong format not allowed. Need format ("road | city").
    @Test
    void constructor02() {
        String roadAndCity = road + " " + city;
        IllegalFormatException e = assertThrows(IllegalFormatException.class, () -> new Address(roadAndCity));
        assertEquals("The format of the address string is illegal.", e.getMessage());
    }

    @Test
    void getRoad() {
        assertEquals(road, address.getRoad());
    }

    @Test
    void getCity() {
        assertEquals(city, address.getCity());
    }

    @Test
    void toString01() {
        assertEquals("'" + road + " | " + city + "'", address.toString());
    }

    @Test
    void equals01() {
        assertNotEquals(new Address("Enellerandenvej 2", city), address);
    }

    @Test
    void equals02() {
        assertNotEquals(new Address(road, "Randers"), address);
    }

    @Test
    void equals04() {
        assertEquals(new Address(road, city), address);
    }

    @Test
    void hashCode01() {
        Address addressTest = new Address(road, city);

        assertEquals(addressTest.hashCode(), address.hashCode());

    }
}
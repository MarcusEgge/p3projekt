package Model;

import Model.Exceptions.IllegalFormatException;
import Model.Exceptions.IllegalIDException;
import Model.Exceptions.IllegalObjectException;
import Model.Exceptions.ObjectNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class TeamTest {

    private Judge judge;
    private Person person;
    private Team team;
    private Gymnast gymnast;
    private List<TeamAssignment> teamAssignmentsList;
    private Event event;
    private LocalDateTime today;
    private String name;

    //1 Person, 1 Judge, 1 Event, 1 Gymnast, 1 team and a list of TeamAssignment.
    @BeforeEach
    public void initObjects() {
        today = LocalDateTime.now();
        name = "Anders";
        person = new Person(name, true, today.toLocalDate(), true);
        judge = new Judge(person, true);
        event = new Event("Mikkels dans", new Address("Vej 12", "9000 Aalborg"), today, today.toLocalDate(), new Season(today, today), 6, 6);
        teamAssignmentsList = new ArrayList<>();
        gymnast = new Gymnast(person, "12324", true);
        team = new Team("Ræd");
    }

    @Test
    void getID() {
        Team teamTest = new Team(1, "Ræd");
        assertEquals(1, teamTest.getID());
    }

    //Exception test. Name not allowed to be empty, need to be initiated with a name.
    @Test
    void checkName01() {
        IllegalFormatException exception = assertThrows(IllegalFormatException.class, () -> new Team(""));
        assertEquals("Name of Team object cannot be empty string.", exception.getMessage());
    }

    //Exception test. Not allowed to set name to null.
    @Test
    void checkName02() {
        assertThrows(NullPointerException.class, () -> new Team(null));
    }

    //Exception test. Not allowed to initiate with id = 0.
    @Test
    void constructor01() {
        IllegalIDException exception = assertThrows(IllegalIDException.class, () -> new Team(0, "Red"));
        assertEquals("The id is illegal.", exception.getMessage());
    }

    //Exception test. Not allowed to initiate with a negative id.
    @Test
    void constructor02() {
        IllegalIDException exception = assertThrows(IllegalIDException.class, () -> new Team(-1, "Red"));
        assertEquals("The id is illegal.", exception.getMessage());
    }

    //Only adds gymnast to upcoming events which event isn't.
    @Test
    void addGymnastToFutureEvents1() {
        team.addTeamSignup(new TeamSignup(team, event, true));
        team.addGymnastToFutureEvents(gymnast);

        assertTrue(team.getTeamSignupList().get(0).getPersonSignupList().isEmpty());
    }

    @Test
    void addGymnastToFutureEvents2() {
        event.setStartDate(today.plusYears(10));
        team.addTeamSignup(new TeamSignup(team, event, true));

        assertEquals(0, event.getPersonSignupList().size());

        team.addGymnastToFutureEvents(gymnast);

        assertEquals(1, event.getPersonSignupList().size());
        assertEquals(1, team.getTeamSignupList().get(0).getPersonSignupList().size());
        assertEquals(gymnast, team.getTeamSignupList().get(0).getPersonSignupList().get(0).getRole());
    }

    //Test to confirm that addGymnastToFutureEvents() doesn't create a new PersonSignup to event
    //if it already have that Gymnast PersonSignup
    @Test
    void addGymnastToFutureEvents3() {
        event.setStartDate(today.plusYears(10));
        team.addTeamSignup(new TeamSignup(team, event, true));
        event.addPersonSignup(new PersonSignup(gymnast, event));

        assertEquals(1, event.getPersonSignupList().size());

        team.addGymnastToFutureEvents(gymnast);

        assertEquals(1, event.getPersonSignupList().size());
        assertEquals(1, team.getTeamSignupList().get(0).getPersonSignupList().size());
        assertEquals(gymnast, team.getTeamSignupList().get(0).getPersonSignupList().get(0).getRole());
    }

    //Delete removes a Persons PersonSignup from a Teams TeamSignup.
    @Test
    void deleteGymnastFromFutureEvents1() {
        event.setStartDate(today.plusYears(10));
        team.addTeamSignup(new TeamSignup(team, event, true));
        team.addGymnastToFutureEvents(gymnast);
        team.deleteGymnastFromFutureEvents(gymnast);

        assertTrue(team.getTeamSignupList().get(0).getPersonSignupList().isEmpty());
    }

    //Exception test. Unable to delete a PersonSignup if it doesn't exist in TeamSignup.
    @Test
    void deleteGymnastFromFutureEvents2() {
        event.setStartDate(today.plusYears(10));
        team.addTeamSignup(new TeamSignup(team, event, true));
        ObjectNotFoundException exception = assertThrows(ObjectNotFoundException.class, () -> team.deleteGymnastFromFutureEvents(gymnast));
        assertEquals("Gymnast has no PersonSignup object in the TeamSignup.", exception.getMessage());
    }

    //TeamAssignments generated is added to the team and the gymnast.
    @Test
    void generateTeamAssignments1() {
        List<Person> personList = new ArrayList<>();
        person.setRole(gymnast, Person.GYMNAST_INDEX);
        personList.add(person);

        team.generateTeamAssignments(personList);
        assertEquals(team.getTeamAssignmentList().get(0), gymnast.getTeamAssignmentList().get(0));
    }

    //Doesn't generate TeamAssignments if the Person is only a Judge.
    @Test
    void generateTeamAssignments2() {
        List<Person> personList = new ArrayList<>();
        person.setRole(judge, Person.JUDGE_INDEX);
        personList.add(person);

        team.generateTeamAssignments(personList);
        assertTrue((team.getTeamAssignmentList().isEmpty()));
    }

    //Sets TeamAssignments to true if they match the Person name in tthe lsit,
    @Test
    void updateTeamAssignment() {
        TeamAssignment teamAssignment = new TeamAssignment(team, gymnast, false);
        teamAssignment.setSignedUp(false);

        team.addTeamAssignment(teamAssignment);

        Coach coach = new Coach(person, true);
        TeamAssignment teamAssignment1 = new TeamAssignment(team, coach, false);
        teamAssignment1.setSignedUp(false);

        team.addTeamAssignment(teamAssignment1);

        List<String> names = new ArrayList<>();
        names.add(name);

        team.updateTeamAssignments(names, names);

        assertTrue(gymnast.isGymnast());
        assertTrue(teamAssignment.isSignedUp());
        assertTrue(teamAssignment1.isSignedUp());

    }

    @Test
    void setAndGetName() {
        assertEquals("Ræd", team.getName());

        team.setName("Blå");

        assertEquals("Blå", team.getName());
    }

    @Test
    void addTeamAssignment01() {
        TeamAssignment teamAssignment = new TeamAssignment(team, gymnast, true);
        team.addTeamAssignment(teamAssignment);

        assertEquals(teamAssignment, team.getTeamAssignmentList().get(0));

    }

    //Exception test. Not allowed to add a TeamAssignment that belong to a different Team.
    @Test
    void addTeamAssignment02() {
        TeamAssignment teamAssignment = new TeamAssignment(new Team("And"), gymnast, true);

        IllegalObjectException exception = assertThrows(IllegalObjectException.class, () -> team.addTeamAssignment(teamAssignment));
        assertEquals("The TeamAssignment object cannot be added to the list in the Team object.", exception.getMessage());
    }

    //Exception test. Not allowed to add the TeamAssignment if it already exist in team.
    @Test
    void addTeamAssignment03() {
        TeamAssignment teamAssignment = new TeamAssignment(team, gymnast, true);
        team.addTeamAssignment(teamAssignment);
        IllegalObjectException e = assertThrows(IllegalObjectException.class, () -> team.addTeamAssignment(teamAssignment));
        assertEquals("The TeamAssignment object cannot be added to the list in the Team object.", e.getMessage());
    }

    //Exception test. Not allowed to add null as a TeamAssignment.
    @Test
    void addTeamAssignment04() {
        assertThrows(NullPointerException.class, () -> team.addTeamAssignment(null));
    }

    @Test
    void deleteTeamAssignment01() {
        TeamAssignment teamAssignment = new TeamAssignment(team, gymnast, true);
        team.addTeamAssignment(teamAssignment);

        team.deleteTeamAssignment(teamAssignment);

        assertTrue(team.getTeamAssignmentList().isEmpty());
    }

    //Exception test. Unable to delete a TeamAssignment if it doesn't exist in Team.
    @Test
    void deleteTeamAssignment02() {
        TeamAssignment teamAssignment = new TeamAssignment(team, gymnast, true);

        ObjectNotFoundException exception = assertThrows(ObjectNotFoundException.class, () -> team.deleteTeamAssignment(teamAssignment));
        assertEquals("The TeamAssignment object was not found in the Team object.", exception.getMessage());
    }

    //Exception test. Not allowed to add a TeamSignup that belongs to a different Team.
    @Test
    void addTeamSignups01() {
        TeamSignup teamSignup = new TeamSignup(new Team("And"), event, true);
        IllegalObjectException exception = assertThrows(IllegalObjectException.class, () -> team.addTeamSignup(teamSignup));
        assertEquals("The TeamSignup object cannot be added to the list in the Team object.", exception.getMessage());
    }

    //Exception test. Not allowed to add TeamSignup if it already exist in Team.
    @Test
    void addTeamSignups02() {
        TeamSignup teamSignup = new TeamSignup(team, event, true);
        team.addTeamSignup(teamSignup);
        IllegalObjectException exception = assertThrows(IllegalObjectException.class, () -> team.addTeamSignup(teamSignup));
        assertEquals("The TeamSignup object cannot be added to the list in the Team object.", exception.getMessage());
    }

    @Test
    void addTeamSignups03() {
        TeamSignup teamSignup = new TeamSignup(team, event, true);
        team.addTeamSignup(teamSignup);
        assertEquals(teamSignup, team.getTeamSignupList().get(0));
    }

    //Exception test. Unable to delete TeamSignup that doesn't exist.
    @Test
    void deleteTeamSignup1() {
        ObjectNotFoundException exception = assertThrows(ObjectNotFoundException.class, () -> team.deleteTeamSignup(new TeamSignup(team, event, true)));
        assertEquals("The TeamSignup object was not found in the Team object.", exception.getMessage());
    }


    @Test
    void deleteTeamSignup2() {
        TeamSignup teamSignup = new TeamSignup(team, event, true);
        team.addTeamSignup(teamSignup);

        assertEquals(teamSignup, team.getTeamSignupList().get(0));

        team.deleteTeamSignup(teamSignup);
        assertTrue(team.getTeamSignupList().isEmpty());
    }

    @Test
    void getGymnastsInTeam() {
        List<Person> personList = new ArrayList<>();
        List<String> names = new ArrayList<>();

        Person person1 = new Person("Anker", true, LocalDate.now(), true);
        Person person2 = new Person("Ralph", true, LocalDate.now(), true);
        Person person3 = new Person("Jack", true, LocalDate.now(), true);

        Coach coach = new Coach(person2, true);
        Judge judge1 = new Judge(person1, true);
        Gymnast gymnast1 = new Gymnast(person3, "12312", true);

        person.setRole(gymnast, Person.GYMNAST_INDEX);
        person1.setRole(judge1, Person.JUDGE_INDEX);
        person2.setRole(coach, Person.COACH_INDEX);
        person3.setRole(gymnast1, Person.GYMNAST_INDEX);

        personList.add(person);
        personList.add(person1);
        personList.add(person2);
        personList.add(person3);

        names.add(person.getName());

        team.generateTeamAssignments(personList);
        team.updateTeamAssignments(names,new ArrayList<>());

        assertEquals(person, team.getGymnastsInTeam().get(0));
        assertEquals(1, team.getGymnastsInTeam().size());
    }

    @Test
    void getCoachesInTeam(){
        List<Person> personList = new ArrayList<>();
        List<String> names = new ArrayList<>();

        Person person1 = new Person("Anker", true, LocalDate.now(), true);
        Person person2 = new Person("Ralph", true, LocalDate.now(), true);

        Judge judge1 = new Judge(person1, true);
        Coach coach = new Coach(person2, true);

        person.setRole(gymnast, Person.GYMNAST_INDEX);
        person1.setRole(judge1, Person.JUDGE_INDEX);
        person2.setRole(coach, Person.COACH_INDEX);

        personList.add(person);
        personList.add(person1);
        personList.add(person2);

        names.add(person2.getName());

        team.generateTeamAssignments(personList);
        team.updateTeamAssignments(new ArrayList<>(), names);

        assertEquals(person2, team.getCoachesInTeam().get(0));
        assertEquals(1, team.getCoachesInTeam().size());
    }

    //Removes the Team's TeamSignups and TeamAssignments from Roles, Events and PersonSignups.
    @Test
    void delete01() {
        TeamSignup teamSignup = new TeamSignup(team, event, true);
        TeamAssignment teamAssignment = new TeamAssignment(team, gymnast, true);
        PersonSignup personSignup = new PersonSignup(gymnast, teamSignup.getEvent());

        gymnast.addTeamAssignment(teamAssignment);
        teamSignup.addPersonSignup(personSignup);
        personSignup.addTeamSignup(teamSignup);
        event.addTeamSignup(teamSignup);

        team.addTeamAssignment(teamAssignment);
        team.addTeamSignup(teamSignup);

        team.delete();

        assertFalse(event.viewTeamSignups().contains(teamSignup));
        assertFalse(gymnast.getTeamAssignmentList().contains(teamAssignment));
        assertFalse(personSignup.getTeamSignupList().contains(teamSignup));
    }

    //Exception test. Unable to delete TeamSignup if it doesn't exist in PersonSignup.
    @Test
    void delete02() {
        TeamSignup teamSignup = new TeamSignup(team, event, true);
        TeamAssignment teamAssignment = new TeamAssignment(team, gymnast, true);
        PersonSignup personSignup = new PersonSignup(gymnast, teamSignup.getEvent());

        gymnast.addTeamAssignment(teamAssignment);
        teamSignup.addPersonSignup(personSignup);
        event.addTeamSignup(teamSignup);

        team.addTeamAssignment(teamAssignment);
        team.addTeamSignup(teamSignup);

        ObjectNotFoundException exception = assertThrows(ObjectNotFoundException.class, () -> team.delete());
        assertEquals("The TeamSignup object was not found in the PersonSignup object.", exception.getMessage());
    }

    //Exception test. Unable to delete TeamSignup if it doesn't exist in Event.
    @Test
    void delete03() {
        TeamSignup teamSignup = new TeamSignup(team, event, true);
        TeamAssignment teamAssignment = new TeamAssignment(team, gymnast, true);
        PersonSignup personSignup = new PersonSignup(gymnast, teamSignup.getEvent());

        gymnast.addTeamAssignment(teamAssignment);
        teamSignup.addPersonSignup(personSignup);
        personSignup.addTeamSignup(teamSignup);

        team.addTeamAssignment(teamAssignment);
        team.addTeamSignup(teamSignup);

        ObjectNotFoundException exception = assertThrows(ObjectNotFoundException.class, () -> team.delete());
        assertEquals("The TeamSignup object was not found in the Event object.", exception.getMessage());
    }

    //Exception test. Unable to delete TeamAssignment if it doesn't exist in Gymnast.
    @Test
    void delete04() {
        TeamSignup teamSignup = new TeamSignup(team, event, true);
        TeamAssignment teamAssignment = new TeamAssignment(team, gymnast, true);
        PersonSignup personSignup = new PersonSignup(gymnast, teamSignup.getEvent());

        teamSignup.addPersonSignup(personSignup);
        personSignup.addTeamSignup(teamSignup);
        event.addTeamSignup(teamSignup);

        team.addTeamAssignment(teamAssignment);
        team.addTeamSignup(teamSignup);

        ObjectNotFoundException exception = assertThrows(ObjectNotFoundException.class, () -> team.delete());
        assertEquals("The TeamAssignment object was not found in the Gymnast object.", exception.getMessage());
    }

    @Test
    void toString01() {
        Team teamTest = new Team(2, "test");

        assertEquals("Team{" + "id=" + 2 + '}', teamTest.toString());
    }


    @Test
    void compareToTest1() {
        Team team1 = new Team("Ræd");
        assertEquals(0, team.compareTo(team1));
    }

    @Test
    void compareToTest2() {
        Team team1 = new Team("åæd");
        assertTrue(0 > team.compareTo(team1));
    }

    @Test
    void compareToTest3() {
        Team team1 = new Team("Aæd");
        assertTrue(0 < team.compareTo(team1));
    }

    @Test
    void equalsTest1() {
        Team team1 = new Team("Ræd");
        assertTrue(team.equals(team1));
    }

    @Test
    void equalsTest2() {
        Team team1 = new Team("Rød");
        assertFalse(team.equals(team1));
    }

    @Test
    void hashCodeTest() {
        Team team1 = new Team("Ræd");
        assertEquals(team1.hashCode(), team.hashCode());
    }
}
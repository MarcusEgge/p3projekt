package Model;

import Model.Exceptions.CarCapacityException;
import Model.Exceptions.IllegalObjectException;
import Model.Exceptions.ObjectNotFoundException;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static org.junit.jupiter.api.Assertions.*;

class CarTest {
    private Car car1, car2, car3;
    private CarAssignment carAssignment1, carAssignment2, carAssignment3;
    private Season season;
    private Event event;
    private Person driver, passenger1, passenger2, passenger3;

    // Information in the database is wiped.
    @BeforeAll
    static void beforeAll() {
        Club.INSTANCE.getDatabaseManager().deleteAllDataFromDatabase();
    }

    // 1 season, 1 event, 4 person, 3 car, 3 carAssignment.
    @BeforeEach
    void initObjects() {
        season = new Season(LocalDateTime.now(), LocalDateTime.now());
        event = new Event("Dancup", new Address("Kalderupvej 21", "9000 Aalborg"), LocalDateTime.now(), LocalDate.now(), season, 6, 6);

        driver = new Person("Lenny", true, LocalDate.now(), true);
        passenger1 = new Person("Martin", true, LocalDate.now(), true);
        passenger2 = new Person("Marcus", true, LocalDate.now(), true);
        passenger3 = new Person("Marinus", true, LocalDate.now(), true);

        car1 = new Car(event, driver);
        car2 = new Car(3, event);
        car3 = new Car(event);

        carAssignment1 = new CarAssignment(false, passenger1, car1);
        carAssignment2 = new CarAssignment(false, passenger2, car1);
        carAssignment3 = new CarAssignment(false, passenger3, car1);

        passenger1.addCarAssignment(carAssignment1); // carAssignment1 added to Person passenger1.
        passenger2.addCarAssignment(carAssignment2); // carAssignment2 added to Person passenger2.
        passenger3.addCarAssignment(carAssignment3); // carAssignment3 added to Person passenger3.
    }

    // Test for car1 constructor.
    @Test
    void constructor01() {
        List<CarAssignment> carAssignmentList = new ArrayList<>();
        carAssignmentList.add(new CarAssignment(true, driver, car1));

        assertEquals(1, car1.getID());
        assertEquals(driver, car1.getDriver());
        assertEquals(3, car1.getAvailableSeatCount());
        assertEquals(event, car1.getEvent());
        assertEquals(carAssignmentList, car1.getCarAssignmentList());
    }

    // Exception test. Not allowed to give first constructor null as event.
    @Test
    void constructor02() {
        assertThrows(NullPointerException.class, () -> new Car(null, driver));
    }

    // Exception test. Not allowed to give first constructor null as driver.
    @Test
    void constructor03() {
        assertThrows(NullPointerException.class, () -> new Car(event, null));
    }

    //Test for car2 constructor.
    @Test
    void constructor04() {
        List<CarAssignment> carAssignmentList = new ArrayList<>();

        assertEquals(3, car2.getID());
        assertEquals(null, car2.getDriver());
        assertEquals(4, car2.getAvailableSeatCount());
        assertEquals(event, car2.getEvent());
        assertEquals(carAssignmentList, car2.getCarAssignmentList());
    }

    //Exception test. Not allowed to give second constructor null as event.
    @Test
    void constructor05() {
        assertThrows(NullPointerException.class, () -> new Car(1, null));
    }

    //Test for car3 constructor.
    @Test
    void constructor06() {
        List<CarAssignment> carAssignmentList = new ArrayList<>();

        assertEquals(1, car3.getID());
        assertEquals(null, car3.getDriver());
        assertEquals(4, car3.getAvailableSeatCount());
        assertEquals(event, car3.getEvent());
        assertEquals(carAssignmentList, car3.getCarAssignmentList());
    }

    //Exception test. Not allowed to give third constructor null as event.
    @Test
    void constructor07() {
        assertThrows(NullPointerException.class, () -> new Car(null));
    }

    @Test
    void getID() {
        assertEquals(3, car2.getID());
    }

    @Test
    void getAvailableSeats() {
        assertEquals(3, car1.getAvailableSeatCount());
    }

    @Test
    void getEvent() {
        assertEquals(event, car2.getEvent());
    }

    // car1 gets a CarAssignment. addCarAssignment takes a CarAssignment object.
    @Test
    void addCarAssignment01() {
        assertEquals(3, car1.getAvailableSeatCount());
        assertEquals(1, car1.getCarAssignmentList().size());

        car1.addCarAssignment(new CarAssignment(false, passenger1, car1));

        assertEquals(2, car1.getAvailableSeatCount());
        assertEquals(2, car1.getCarAssignmentList().size());
    }

    //Exception test. Not allowed to assign more than 1 driver. addCarAssignment takes a CarAssignment object.
    @Test
    void addCarAssignment02() {

        IllegalObjectException exception = assertThrows(IllegalObjectException.class, () -> car1.addCarAssignment(new CarAssignment(true, passenger1, car1)));
        assertEquals(exception.getMessage(), "The CarAssignment object cannot be added to the list in the Car object.");

        assertEquals(3, car1.getAvailableSeatCount());
        assertEquals(1, car1.getCarAssignmentList().size());
    }

    //Exception test. Not allowed to assign the same CarAssignment. A CarAssignment is unique by Person and Car. addCarAssignment takes a CarAssignment object.
    @Test
    void addCarAssignment03() {

        IllegalObjectException exception = assertThrows(IllegalObjectException.class, () -> car1.addCarAssignment(new CarAssignment(false, driver, car1)));
        assertEquals(exception.getMessage(), "The CarAssignment object cannot be added to the list in the Car object.");

        assertEquals(3, car1.getAvailableSeatCount());
        assertEquals(1, car1.getCarAssignmentList().size());
    }

    //Exception test. Not allowed to give a Car object a CarAssignment that contains a different Car object. addCarAssignment takes a CarAssignment object.
    @Test
    void addCarAssignment04() {

        IllegalObjectException exception = assertThrows(IllegalObjectException.class, () -> car1.addCarAssignment(new CarAssignment(false, passenger1, car3)));
        assertEquals(exception.getMessage(), "The CarAssignment object cannot be added to the list in the Car object.");

        assertEquals(3, car1.getAvailableSeatCount());
        assertEquals(1, car1.getCarAssignmentList().size());
    }

    //Exception test. Not allowed to assign a CarAssignment if Car's AvailableSeatCount is 0. addCarAssignment takes a CarAssignment object.
    @Test
    void addCarAssignment05() {
        Person passenger4 = new Person("Dagmar", false, LocalDate.now(), true);

        car1.addCarAssignment(new CarAssignment(false, passenger1, car1));
        car1.addCarAssignment(new CarAssignment(false, passenger2, car1));
        car1.addCarAssignment(new CarAssignment(false, passenger3, car1));

        assertEquals(0, car1.getAvailableSeatCount());
        assertEquals(4, car1.getCarAssignmentList().size());

        CarCapacityException exception = assertThrows(CarCapacityException.class, () -> car1.addCarAssignment(new CarAssignment(false, passenger4, car1)));
        assertEquals(exception.getMessage(), "There are no more available seats in the Car object.");

        assertEquals(0, car1.getAvailableSeatCount());
        assertEquals(4, car1.getCarAssignmentList().size());
    }

    //Deletes a CarAssignment that just got added in car1.
    @Test
    void deleteCarAssignment01() {
        CarAssignment carAssignment = new CarAssignment(false, passenger1, car1);
        car1.addCarAssignment(carAssignment);
        assertEquals(2, car1.getAvailableSeatCount());

        car1.deleteCarAssignment(new CarAssignment(false, passenger1, car1));
        assertEquals(3, car1.getAvailableSeatCount());

    }

    //Exception test. Unable to delete a CarAssignment that doesn't exist in Car.
    @Test
    void deleteCarAssignment02() {
        CarAssignment carAssignment = new CarAssignment(false, passenger1, car1);
        car1.addCarAssignment(carAssignment);

        ObjectNotFoundException exception = assertThrows(ObjectNotFoundException.class, () -> car1.deleteCarAssignment(new CarAssignment(false, passenger2, car1)));
        assertEquals("The CarAssignment object was not found in the Car object.", exception.getMessage());
    }


    @Test
    void deleteAllCarAssignments01() {
        car1.addCarAssignment(carAssignment1);
        car1.addCarAssignment(carAssignment2);
        car1.addCarAssignment(carAssignment3);

        assertEquals(0, car1.getAvailableSeatCount());
        assertEquals(4, car1.getCarAssignmentList().size());

        car1.deleteAllCarAssignments();

        assertEquals(4, car1.getAvailableSeatCount());
        assertEquals(0, car1.getCarAssignmentList().size());
    }

    @Test
    void deleteAllCarAssignments02() {
        car1.addCarAssignment(carAssignment1);
        car1.addCarAssignment(carAssignment2);
        car1.addCarAssignment(carAssignment3);

        assertTrue(passenger1.getCarAssignmentList().contains(carAssignment1));
        assertTrue(passenger2.getCarAssignmentList().contains(carAssignment2));
        assertTrue(passenger3.getCarAssignmentList().contains(carAssignment3));

        car1.deleteAllCarAssignments();

        assertFalse(passenger1.getCarAssignmentList().contains(carAssignment1));
        assertFalse(passenger2.getCarAssignmentList().contains(carAssignment2));
        assertFalse(passenger3.getCarAssignmentList().contains(carAssignment3));
    }

    @Test
    void deleteAllPassengers01() {
        car1.addCarAssignment(carAssignment1);
        car1.addCarAssignment(carAssignment2);
        car1.addCarAssignment(carAssignment3);

        assertEquals(0, car1.getAvailableSeatCount());
        assertEquals(4, car1.getCarAssignmentList().size());

        car1.deleteAllPassengers();

        assertEquals(3, car1.getAvailableSeatCount());
        assertEquals(1, car1.getCarAssignmentList().size());
    }

    @Test
    void deleteAllPassengers02() {
        car1.addCarAssignment(carAssignment1);
        car1.addCarAssignment(carAssignment2);
        car1.addCarAssignment(carAssignment3);

        assertTrue(passenger1.getCarAssignmentList().contains(carAssignment1));
        assertTrue(passenger2.getCarAssignmentList().contains(carAssignment2));
        assertTrue(passenger3.getCarAssignmentList().contains(carAssignment3));

        car1.deleteAllPassengers();

        assertFalse(passenger1.getCarAssignmentList().contains(carAssignment1));
        assertFalse(passenger2.getCarAssignmentList().contains(carAssignment2));
        assertFalse(passenger3.getCarAssignmentList().contains(carAssignment3));
    }

    @Test
    void getDriver01() {
        assertEquals(driver, car1.getDriver());
    }

    //getDriver() returns null if car doesn't have a driver.
    @Test
    void getDriver02() {
        assertEquals(null, car2.getDriver());
    }

    //getDriver() returns null if car doesn't have a driver.
    @Test
    void getDriver03() {
        car2.addCarAssignment(new CarAssignment(false, passenger2, car2));
        assertEquals(null, car2.getDriver());
    }

    @Test
    void getDriver04() {
        car2.addCarAssignment(new CarAssignment(false, passenger1, car2));
        car2.addCarAssignment(new CarAssignment(false, passenger2, car2));
        car2.addCarAssignment(new CarAssignment(false, passenger3, car2));
        car2.addCarAssignment(new CarAssignment(true, driver, car2));
        assertEquals(driver, car2.getDriver());
    }

    @Test
    void hasPerson01() {
        assertTrue(car1.hasPerson(driver));
    }

    @Test
    void hasPerson02() {
        assertFalse(car1.hasPerson(passenger1));
    }

    @Test
    void hasPerson() {
        assertTrue(car1.hasPerson(driver));
    }

    @Test
    void equals01() {
        car3.addCarAssignment(new CarAssignment(true, driver, car3));
        assertEquals(car1, car3);
    }

    @Test
    void equals02() {
        car3.addCarAssignment(new CarAssignment(true, driver, car3));
        assertEquals(car1, car3);
    }

    @Test
    void equals03() {
        assertNotEquals(car1, car2);
    }

    @Test
    void equals04() {
        car2.addCarAssignment(new CarAssignment(true, passenger1, car2));
        assertNotEquals(car1, car2);
    }

    @Test
    void equals05() {
        Car carTemp = new Car(new Event("Event", new Address("A road", "A city"), LocalDateTime.now(), LocalDate.now().plusDays(1), season, 5, 5));
        assertNotEquals(car1, carTemp);
    }

    @Test
    void hashCode01() {
        assertEquals(Objects.hash(1, event, car1.getCarAssignmentList()), car1.hashCode());
    }

    @Test
    void hashCode02() {
        assertNotEquals(car2.hashCode(), car3.hashCode());
    }
}
package Model;

import Model.*;
import Model.Exceptions.IllegalIDException;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Objects;

import static org.junit.jupiter.api.Assertions.*;

class JudgeTest {
    private Season season;
    private Event event;
    private Person person;
    private Judge judge;
    private PersonSignup personSignup;

    //1 Season, 1 event, 1 Person, 1 Judge and 1 PersonSignup.
    @BeforeEach
    public void initObjects() {
        season = new Season(LocalDateTime.now(), LocalDateTime.now().plusMonths(1));
        event = new Event("Dancup", new Address("Kalderupvej 21", "9000 Aalborg"), LocalDateTime.now().plusDays(3), LocalDate.now().plusDays(3), season, 6, 6);
        person = new Person("Martin", true, LocalDate.now(), true);
        judge = new Judge(person, true);
        personSignup = new PersonSignup(judge, event);
        event.addPersonSignup(personSignup); //PersonSignup added to Event.
        person.addPersonSignup(personSignup);//PersonSignup added to Person.

    }

    @Test
    void getID(){
        assertEquals(1, judge.getID());
    }

    //Exception test. Not allowed to initiate with id 0.
    @Test
    void constructor01(){
        IllegalIDException e = assertThrows(IllegalIDException.class,()->new Judge(0, person, true));
        assertEquals("The id is illegal.",e.getMessage());
    }

    //Exception test. Not allowed to initiate with negative id.
    @Test
    void constructor02(){
        IllegalIDException e = assertThrows(IllegalIDException.class,()->new Judge(-1, person, true));
        assertEquals("The id is illegal.",e.getMessage());
    }

    //Exception test. Not allowed to initiate with null as person.
    @Test
    void constructor03(){
        assertThrows(NullPointerException.class,()->new Judge(1, null, true));
    }

    @Test
    void constructor04(){
        Judge judgeTemp = new Judge(5, person,true);
        assertEquals(5, judgeTemp.getID());
    }

    @Test
    void isGymnast() {
        assertFalse(judge.isGymnast());
    }

    @Test
    void isCoach() {
        assertFalse(judge.isCoach());
    }

    @Test
    void isJudge() {
        assertTrue(judge.isJudge());
    }

    @Test
    void getPerson() {
        assertEquals(person, judge.getPerson());
    }

    @Test
    void isActive() {
        assertTrue(judge.isActive());
    }

    @Test
    void setActive() {
        assertTrue(judge.isActive());

        judge.setActive(false);

        assertFalse(judge.isActive());
    }

    @Test
    void equals01() {
        Judge judgeTemp = new Judge(new Person("Martin", true, LocalDate.now(), true), true);
        assertEquals(judgeTemp, judge);
    }

    @Test
    void equals02() {
        Judge judgeTemp = new Judge(new Person("Peter", true, LocalDate.now(), true), true);
        assertNotEquals(judgeTemp, judge);

    }

    @Test
    void hashcode(){
        assertEquals(Objects.hash(person),judge.hashCode());
    }


}
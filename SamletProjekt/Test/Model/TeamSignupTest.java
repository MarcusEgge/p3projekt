package Model;

import Model.Exceptions.IllegalIDException;
import Model.Exceptions.IllegalObjectException;
import Model.Exceptions.ObjectNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.*;

class TeamSignupTest {
    private TeamSignup teamSignup;
    private Person person1, person2, person3;
    private Gymnast gymnast1, gymnast2, gymnast3;
    private PersonSignup personSignup1, personSignup2, personSignup3;
    private Team team;
    private Event event;
    private Season season;
    private LocalDateTime startDate;
    private LocalDate endDate;
    private Address address;
    private int judgeAndCoach;

    //1 Season, 1 Address, 1 Event, 1 Team, 1 TeamSignup, 3 Person, 3 Gymnast and 3 PersonSignup.
    @BeforeEach
    public void initObjects() {
        startDate = LocalDateTime.now();
        endDate = LocalDate.now();
        season = new Season(startDate, startDate);
        judgeAndCoach = 6;
        address = new Address("Gaden 2", "9000 Aalborg");
        event = new Event("Dino", address, startDate, endDate, season, judgeAndCoach, judgeAndCoach);
        team = new Team("Ræd");
        teamSignup = new TeamSignup(team, event, true);
        person1 = new Person("Peter", true, LocalDate.now(), false);
        person2 = new Person("Pedro", true, LocalDate.now(), false);
        person3 = new Person("Peterino", true, LocalDate.now(), false);
        gymnast1 = new Gymnast(person1, "1234444", true);
        gymnast2 = new Gymnast(person2, "1234567", true);
        gymnast3 = new Gymnast(person3, "9876543210", true);

        personSignup1 = new PersonSignup(gymnast1, event);
        personSignup2 = new PersonSignup(gymnast2, event);
        personSignup3 = new PersonSignup(gymnast3, event);
    }

    @Test
    void constructor01() {
        TeamSignup teamSignupTest = new TeamSignup(1, team, event, true);
        assertEquals(1, teamSignupTest.getID());
        assertEquals(team, teamSignupTest.getTeam());
        assertEquals(event, teamSignupTest.getEvent());
        assertEquals(true, teamSignupTest.isSignedUp());
    }

    //Exception test. Not allowed to initiate TeamSignup with id 0.
    @Test
    void constructor02() {
        IllegalIDException exception = assertThrows(IllegalIDException.class, () -> new TeamSignup(0, team, event, true));
        assertEquals("The TeamSignup id is illegal.", exception.getMessage());
    }

    //Exception test. Not allowed to initiate TeamSignup with a negative id.
    @Test
    void constructor03() {
        IllegalIDException exception = assertThrows(IllegalIDException.class, () -> new TeamSignup(-1, team, event, true));
        assertEquals("The TeamSignup id is illegal.", exception.getMessage());
    }

    //Exception test. Not allowed to initiate TeamSignup with null as Team.
    @Test
    void constructor04() {
        assertThrows(NullPointerException.class, () -> new TeamSignup(1, null, event, true));
    }

    //Exception test. Not allowed to initiate TeamSignup with null as Event.
    @Test
    void constructor05() {
        assertThrows(NullPointerException.class, () -> new TeamSignup(1, team, null, true));
    }

    @Test
    void getId() {
        teamSignup.setID(1);
        assertEquals(1, teamSignup.getID());
    }

    //Exception test. Unable to change id.
    @Test
    void setID() {
        TeamSignup teamSignupTest = new TeamSignup(1, team, event, true);
        IllegalIDException exception = assertThrows(IllegalIDException.class, () -> teamSignupTest.setID(1));
        assertEquals("The TeamSignup id is illegal.", exception.getMessage());
    }

    @Test
    void getPersonSignupList1() {
        assertTrue(teamSignup.getPersonSignupList().isEmpty());
    }

    @Test
    void getAndAddPersonSignup() {
        teamSignup.addPersonSignup(personSignup1);
        assertEquals(personSignup1, teamSignup.getPersonSignupList().get(0));
    }

    //Exception test. Not allowed to add PersonSignup for a different event.
    @Test
    void addPersonSignup() {
        String eventName = "SpringDanmark";
        Address eventAddress = new Address("Kalderupvej 21", "9000 Aalborg");
        int judgeAndCoach = 2;
        Event event1 = new Event(eventName, eventAddress, startDate.plusDays(10), endDate.plusDays(10), season, judgeAndCoach, judgeAndCoach);

        PersonSignup personSignup = new PersonSignup(gymnast1, event1);

        IllegalObjectException exception = assertThrows(IllegalObjectException.class, () -> teamSignup.addPersonSignup(personSignup));
        assertEquals("The PersonSignup object cannot be added to the list in the TeamSignup object.", exception.getMessage());
    }


    @Test
    void deletePersonSignup1() {
        teamSignup.addPersonSignup(personSignup1);
        assertEquals(personSignup1, teamSignup.getPersonSignupList().get(0));

        teamSignup.deletePersonSignup(personSignup1);
        assertTrue(teamSignup.getPersonSignupList().isEmpty());
    }

    //Exception test. Unable to delete PersonSignup that doesn't exist.
    @Test
    void deletePersonSignup2() {
        ObjectNotFoundException exception = assertThrows(ObjectNotFoundException.class, () -> teamSignup.deletePersonSignup(personSignup1));
        assertEquals("The PersonSignup object was not found in the TeamSignup object.", exception.getMessage());
    }

    @Test
    void getTeam() {
        assertEquals(team, teamSignup.getTeam());
    }

    @Test
    void isSignedUp() {
        assertTrue(teamSignup.isSignedUp());
    }

    @Test
    void setSignedUp1() {
        teamSignup.setSignedUp(false);
        assertFalse(teamSignup.isSignedUp());
    }

    //Setting sign up state for a TeamSignup also changes all it's PersonSignup to the same state
    @Test
    void setSignedUp2() {
        personSignup1.setState(PersonSignupState.SIGNED_UP);
        teamSignup.addPersonSignup(personSignup1);

        assertEquals(PersonSignupState.SIGNED_UP, personSignup1.getState());
        assertEquals(true, teamSignup.isSignedUp());

        teamSignup.setSignedUp(false);

        assertEquals(PersonSignupState.NOT_SIGNED_UP, personSignup1.getState());
        assertEquals(false, teamSignup.isSignedUp());
    }

    @Test
    void deleteFromEvent01() {
        event.addTeamSignup(teamSignup);
        teamSignup.deleteFromEvent();

        assertTrue(event.viewTeamSignups().isEmpty());
    }

    //Exception test. Unable to delete TeamSignup if it doesn't exist in Event.
    @Test
    void deleteFromEvent02() {
        ObjectNotFoundException e = assertThrows(ObjectNotFoundException.class, () -> teamSignup.deleteFromEvent());
        assertEquals("The TeamSignup object was not found in the Event object.", e.getMessage());
    }


    @Test
    void deleteFromTeam01() {
        team.addTeamSignup(teamSignup);
        teamSignup.deleteFromTeam();

        assertTrue(team.getTeamSignupList().isEmpty());
    }

    //Exception test. Unable to delete TeamSignup if it doesn't exist in Team.
    @Test
    void deleteFromTeam02() {
        ObjectNotFoundException e = assertThrows(ObjectNotFoundException.class, () -> teamSignup.deleteFromTeam());
        assertEquals("The TeamSignup object was not found in the Team object.", e.getMessage());

    }

    @Test
    void deleteFromPersonSignups01() {
        personSignup1.addTeamSignup(teamSignup);
        personSignup2.addTeamSignup(teamSignup);
        personSignup3.addTeamSignup(teamSignup);

        teamSignup.addPersonSignup(personSignup1);
        teamSignup.addPersonSignup(personSignup2);
        teamSignup.addPersonSignup(personSignup3);

        teamSignup.deleteFromPersonSignups();

        assertTrue(personSignup1.getTeamSignupList().isEmpty());
        assertTrue(personSignup2.getTeamSignupList().isEmpty());
        assertTrue(personSignup3.getTeamSignupList().isEmpty());
    }

    //Exception test. Unable to delete TeamSignup if it doesn't exist in PersonSignup.
    @Test
    void deleteFromPersonSignups02() {
        personSignup1.addTeamSignup(teamSignup);
        personSignup3.addTeamSignup(teamSignup);

        teamSignup.addPersonSignup(personSignup1);
        teamSignup.addPersonSignup(personSignup2);
        teamSignup.addPersonSignup(personSignup3);

        assertFalse(personSignup1.getTeamSignupList().isEmpty());
        assertTrue(personSignup2.getTeamSignupList().isEmpty());
        assertFalse(personSignup3.getTeamSignupList().isEmpty());

        ObjectNotFoundException e = assertThrows(ObjectNotFoundException.class, () -> teamSignup.deleteFromPersonSignups());
        assertEquals("The TeamSignup object was not found in the PersonSignup object.", e.getMessage());

        assertTrue(personSignup1.getTeamSignupList().isEmpty());
        assertTrue(personSignup2.getTeamSignupList().isEmpty());
        assertFalse(personSignup3.getTeamSignupList().isEmpty());
    }

    @Test
    void hashCodeTest() {
        TeamSignup teamSignup1 = new TeamSignup(team, event, true);
        assertEquals(teamSignup1.hashCode(), teamSignup.hashCode());
    }

    @Test
    void equals01() {
        TeamSignup teamSignupTest = new TeamSignup(team, event, false);

        assertEquals(teamSignupTest, teamSignup);
    }

    @Test
    void equals02() {
        TeamSignup teamSignupTest = new TeamSignup(new Team("Blue"), event, false);

        assertNotEquals(teamSignupTest, teamSignup);
    }

    @Test
    void equals03() {
        TeamSignup teamSignupTest = new TeamSignup(team, new Event("Dancup", address, startDate, endDate, season, judgeAndCoach, judgeAndCoach), false);

        assertNotEquals(teamSignupTest, teamSignup);
    }
}
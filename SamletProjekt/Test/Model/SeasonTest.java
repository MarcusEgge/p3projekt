package Model;

import Model.Exceptions.IllegalIDException;
import Model.Exceptions.IllegalObjectException;
import Model.Exceptions.ObjectNotFoundException;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Objects;

import static org.junit.jupiter.api.Assertions.*;

class SeasonTest {
    private Season season;
    private Event event;
    private LocalDateTime start, end, today, eventStartDate;
    private String eventName;
    private DateTimeFormatter dateFormat;
    private Address address;
    private int judgeAndCoach;
    private LocalDate eventEndDate;

    //1 Sesaon, 1 Address and 2 Event, the first event is created in Season.
    @BeforeEach
    void initObjects() {
        Club.INSTANCE.clear();
        Club.INSTANCE.getDatabaseManager().deleteAllDataFromDatabase();
        today = LocalDateTime.now();
        start = LocalDateTime.now().minusDays(10);
        end = LocalDateTime.now().plusDays(1);
        season = new Season(start, end);
        Club.INSTANCE.getDatabaseManager().saveSeasonData(season);
        eventName = "Dancup";
        address = new Address("Kalderupvej 21", "9000 Aalborg");
        eventStartDate = LocalDateTime.of(2018, 11, 21, 12, 12);
        dateFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        eventEndDate = LocalDate.now();
        judgeAndCoach = 6;
        season.createEvent(eventName, address, eventStartDate.format(dateFormat), eventEndDate.toString(), judgeAndCoach, judgeAndCoach);
        event = new Event(eventName, address, eventStartDate, eventEndDate, season, judgeAndCoach, judgeAndCoach);

    }

    //Clear the database from data after the test.
    @AfterAll
    static void afterAll() {
        Club.INSTANCE.clear();
        Club.INSTANCE.getDatabaseManager().deleteAllDataFromDatabase();
    }

    @Test
    void getID() {
        assertEquals(1, season.getID());
    }

    //Exception test. Not allowed to initiate with id 0.
    @Test
    void constructor01() {
        IllegalIDException e = assertThrows(IllegalIDException.class, () -> new Season(0, start, end, true));
        assertEquals("The id is illegal.", e.getMessage());
    }

    //Exception test. Not allowed to initiate with a negative id.
    @Test
    void constructor02() {
        Season seasonTest = new Season(2, start, end, true);
        IllegalIDException e = assertThrows(IllegalIDException.class, () -> new Season(-1, start, end, true));
        assertEquals("The id is illegal.", e.getMessage());
        assertEquals(2, seasonTest.getID());
    }

    @Test
    void updateState_isActive01() {
        season.updateState();
        assertEquals(true, season.isActive());
    }

    @Test
    void updateState_isActive02() {
        season = new Season(start, end.minusDays(2));
        season.updateState();
        assertEquals(false, season.isActive());
    }

    @Test
    void addEvent01() {
        season.addEvent(new Event("klubtræf", address, start, eventEndDate, season, judgeAndCoach, judgeAndCoach));
        assertEquals(2, season.viewEvents().size());
    }

    // Exception test. Not allowed to add a Event that belongs to a different Season.
    @Test
    void addEvent02() {
        Event eventTest = new Event("klubtræf", address, start, eventEndDate, new Season(start.plusYears(1), end.plusYears(1)), judgeAndCoach, judgeAndCoach);
        IllegalObjectException e = assertThrows(IllegalObjectException.class, () -> season.addEvent(eventTest));
        assertEquals("The Event object cannot be added to the list in the Event object.", e.getMessage());
        assertEquals(1, season.viewEvents().size());
    }

    //Exception test. Not allowed to add the same Event.
    @Test
    void addEvent03() {
        IllegalObjectException e = assertThrows(IllegalObjectException.class, () -> season.addEvent(event));
        assertEquals("The Event object cannot be added to the list in the Event object.", e.getMessage());
        assertEquals(1, season.viewEvents().size());
    }


    @Test
    void getStartDate() {
        assertEquals(start, season.getStartDate());
    }

    @Test
    void getEndDate() {
        assertEquals(end, season.getEndDate());
    }

    @Test
    void viewEvents() {
        assertEquals(1, season.viewEvents().size());
        assertEquals(eventName, season.viewEvents().get(0).getName());
    }

    @Test
    void createEvent01() {
        assertEquals(event, season.viewEvents().get(0));
    }

    @Test
    void createEvent02() {
        assertEquals(1, season.viewEvents().size());
    }

    //Exception test. Not allowed to create the Event if it already exist.
    @Test
    void createEvent03() {
        IllegalObjectException e = assertThrows(IllegalObjectException.class, () -> season.createEvent(eventName, address, eventStartDate.format(dateFormat), eventEndDate.toString(), judgeAndCoach, judgeAndCoach));

        assertEquals("Such an Event object already exists", e.getMessage());
        assertEquals(1, season.viewEvents().size());
    }

    @Test
    void editEvent01() {
        LocalDateTime localDateTime = LocalDateTime.of(2200, 9, 15, 12, 15);
        LocalDate localDate = LocalDate.of(2201, 12, 9);

        season.editEvent(1, "NewEvent", new Address("urtyp 11", "3891 Aalborg"), localDateTime.format(dateFormat), localDate.toString(), judgeAndCoach, judgeAndCoach);

        assertEquals("NewEvent", season.viewEvents().get(0).getName());
        assertEquals(new Address("urtyp 11", "3891 Aalborg"), season.viewEvents().get(0).getAddress());
        assertEquals(localDateTime, season.viewEvents().get(0).getStartDate());
        assertEquals(localDate, season.viewEvents().get(0).getEndDate());

    }

    //Exception test. Not allowed to edit event with id = 0.
    @Test
    void editEvent02() {
        LocalDateTime localDateTime = LocalDateTime.of(2200, 9, 15, 12, 15);
        LocalDate localDate = LocalDate.of(2201, 12, 9);

        IllegalIDException e = assertThrows(IllegalIDException.class, () -> season.editEvent(0, "NewEvent", new Address("urtyp 11", "3891 Aalborg"), localDateTime.format(dateFormat), localDate.toString(), judgeAndCoach, judgeAndCoach));
        assertEquals("Event id is illegal.", e.getMessage());
    }

    //Exception test. Unable to edit a event that doesn't exist.
    @Test
    void editEvent03() {
        LocalDateTime localDateTime = LocalDateTime.of(2200, 9, 15, 12, 15);
        LocalDate localDate = LocalDate.of(2201, 12, 9);

        ObjectNotFoundException e = assertThrows(ObjectNotFoundException.class, () -> season.editEvent(3, "NewEvent", new Address("urtyp 11", "3891 Aalborg"), localDateTime.format(dateFormat), localDate.toString(), judgeAndCoach, judgeAndCoach));
        assertEquals("There was no such Event object in the Season object.", e.getMessage());
    }

    @Test
    void deleteEvent01() {
        season.deleteEvent(1);
        assertEquals(0, season.viewEvents().size());
    }

    //Exception test. Not allowed to delete event with id = 0.
    @Test
    void deleteEvent02() {
        IllegalIDException e = assertThrows(IllegalIDException.class, () -> season.deleteEvent(0));
        assertEquals("Event id is illegal.", e.getMessage());
    }

    //Exception test. Not allowed to delete a event that doesn't exist.
    @Test
    void deleteEvent03() {
        ObjectNotFoundException e = assertThrows(ObjectNotFoundException.class, () -> season.deleteEvent(2));
        assertEquals("There was no such Event object in the Season object.", e.getMessage());
    }

    @Test
    void getName() {
        assertEquals(start.getYear() + "/" + end.getYear(), season.getName());
    }

    @Test
    void distributeDrivers() {
        Person person1 = new Person("Person 1", true, LocalDate.now(), false);
        Person person2 = new Person("Person 2", false, LocalDate.now(), true);
        Coach coach1 = new Coach(person1, true);
        Judge judge2 = new Judge(person2, true);

        person1.setRole(coach1, Person.COACH_INDEX);
        person2.setRole(judge2, Person.JUDGE_INDEX);

        season.createEvent("Event", new Address("Somewhere 1", "1010 Nowhere"), "2018-12-14 10:00", "2018-12-15", 1, 1);

        Event event = season.viewEvents().get(0);

        event.addPersonSignup(new PersonSignup(coach1, event));
        event.viewCoachPersonSignups().get(0).setState(PersonSignupState.SIGNED_UP);
        season.distributeDrivers();

        assertEquals(1, event.getCarList().size());
    }

    @Test
    void equals01() {
        assertEquals(new Season(today, today), season);
    }

    @Test
    void equals02() {
        assertNotEquals(new Season(today, today.plusYears(1)), season);
    }

    @Test
    void equals03() {
        assertEquals(season, season);
    }

    @Test
    void equals04() {
        Season season1 = null;
        assertFalse(season.equals(season1));
    }

    @Test
    void compareTo01() {
        Season season2 = new Season(start.minusDays(20), end.plusDays(20));

        assertTrue(1 <= season.compareTo(season2));
    }


    @Test
    void compareTo02() {
        Season season2 = new Season(today.plusDays(10), end);

        assertTrue(-1 >= season.compareTo(season2));
    }

    @Test
    void compareTo03() {
        Season season2 = new Season(start, end);
        assertEquals(0, season.compareTo(season2));
    }

    @Test
    void compareTo04() {
        Season season2 = new Season(start, end.minusDays(10));
        assertEquals(0, season.compareTo(season2));
    }

    @Test
    void hashCodeTest() {
        String name = season.getName();
        assertEquals(Objects.hash(name), season.hashCode());
    }
}
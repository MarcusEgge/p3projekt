package Model;

import Model.*;
import Model.Exceptions.IllegalIDException;
import Model.Exceptions.IllegalObjectException;
import Model.Exceptions.ObjectNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static org.junit.jupiter.api.Assertions.*;

class GymnastTest {
    private Season season;
    private Event event;
    private Team team;
    private Person person;
    private Gymnast gymnast;
    private List<TeamAssignment> teamAssignmentsList;
    private TeamAssignment teamAssignment;
    private String license;

    //1 Season, 1 Event, 1 Team, 1 Person, 1 license, 1 Gymnast, 1 TeamAssignment and a list of TeamAssignments.
    @BeforeEach
    void initObjects() {
        season = new Season(LocalDateTime.now(), LocalDateTime.now().plusMonths(1));
        event = new Event("Dancup", new Address("Kalderupvej 21", "9000 Aalborg"), LocalDateTime.now().plusDays(3), LocalDate.now().plusDays(3), season, 6, 6);
        team = new Team("red");
        person = new Person("Martin", true, LocalDate.now(), true);
        teamAssignmentsList = new ArrayList<>();
        license = "121202";
        gymnast = new Gymnast(person, license, true);
        teamAssignment = new TeamAssignment(team, gymnast, true);
        team.addTeamAssignment(teamAssignment); // gymnast teamAssignment is added to team.
    }

    //Exception test. Not allowed to initiate with id 0.
    @Test
    void constructor01() {
        IllegalIDException e = assertThrows(IllegalIDException.class, () -> new Gymnast(0, person, license, true));
        assertEquals("The id is illegal.", e.getMessage());
    }

    //Exception test. Not allowed to initiate with a negative id.
    @Test
    void constructor02() {
        IllegalIDException e = assertThrows(IllegalIDException.class, () -> new Gymnast(-1, person, license, true));
        assertEquals("The id is illegal.", e.getMessage());
    }

    //Exception test. Not allowed to initiate with null as person
    @Test
    void constructor03() {
        assertThrows(NullPointerException.class, () -> new Gymnast(1, null, license, true));
    }

    @Test
    void getID() {
        assertEquals(1, gymnast.getID());
    }

    @Test
    void setID01() {
        Gymnast gymnastTest = new Gymnast(1, person, license, true);
        assertEquals(1, gymnastTest.getID());
    }

    @Test
    void getLicense() {
        assertEquals(license, gymnast.getLicense());
    }

    @Test
    void setLicense() {
        String licenseTemp = "121323";
        gymnast.setLicense(licenseTemp);
        assertEquals(licenseTemp, gymnast.getLicense());
    }

    @Test
    void getTeamAssignmentList() {
        assertEquals(teamAssignmentsList, gymnast.getTeamAssignmentList());
    }

    @Test
    void addTeamAssignment01() {
        gymnast.addTeamAssignment(teamAssignment);
        assertTrue(gymnast.getTeamAssignmentList().contains(teamAssignment));
    }

    //Exception test. Not allowed to add the same TeamAssignment. A TeamAssignment is unique by Team and TeamMember.
    @Test
    void addTeamAssignment02() {
        gymnast.addTeamAssignment(teamAssignment);
        IllegalObjectException e = assertThrows(IllegalObjectException.class, () -> gymnast.addTeamAssignment(teamAssignment));
        assertEquals("The TeamAssignment object cannot be added to the list in the Gymnast object.", e.getMessage());
    }

    //Exception test. Not allowed to add TeamAssignment if TeamAssignments TeamMember doesn't match Gymnast.
    @Test
    void addTeamAssignment03() {
        Person personPeter = new Person("Peter", true, LocalDate.now(), false);

        Gymnast gymnastPeter = new Gymnast(personPeter, "123567", true);

        TeamAssignment teamAssignmentPeter = new TeamAssignment(team, gymnastPeter, true);
        IllegalObjectException e = assertThrows(IllegalObjectException.class, () -> gymnast.addTeamAssignment(teamAssignmentPeter));
        assertEquals("The TeamAssignment object cannot be added to the list in the Gymnast object.", e.getMessage());
    }


    @Test
    void deleteTeamAssignment01() {
        gymnast.addTeamAssignment(teamAssignment);
        assertEquals(1, gymnast.getTeamAssignmentList().size());

        gymnast.deleteTeamAssignment(teamAssignment);
        assertTrue(gymnast.getTeamAssignmentList().isEmpty());
    }

    //Exception test. Unable to delete a TeamAssignment that doesn't exist.
    @Test
    void deleteTeamAssignment02() {
        ObjectNotFoundException e = assertThrows(ObjectNotFoundException.class, () -> gymnast.deleteTeamAssignment(teamAssignment));
        assertEquals("The TeamAssignment object was not found in the Gymnast object.", e.getMessage());
        assertTrue(gymnast.getTeamAssignmentList().isEmpty());
    }

    @Test
    void isGymnast() {
        assertTrue(gymnast.isGymnast());
    }

    @Test
    void isCoach() {
        assertFalse(gymnast.isCoach());
    }

    @Test
    void isJudge() {
        assertFalse((gymnast.isJudge()));
    }

    @Test
    void getPerson() {
        assertEquals(person, gymnast.getPerson());
    }

    @Test
    void isActive() {
        assertTrue(gymnast.isActive());
    }

    @Test
    void setActive() {
        gymnast.addTeamAssignment(teamAssignment);

        assertEquals(1, gymnast.getTeamAssignmentList().size());
        assertEquals(1, team.getTeamAssignmentList().size());
        assertTrue(gymnast.isActive());

        gymnast.setActive(false);

        assertEquals(0, gymnast.getTeamAssignmentList().size());
        assertEquals(0, team.getTeamAssignmentList().size());
        assertFalse(gymnast.isActive());
    }

    //Blue and Green team is signup to event. TeamAssignments gets added to gymnast which is not signed up.
    //Gymnast get signed to both teams and generate PersonSignup to the event.
    //The PersonSignup contains both TeamSignup and both TeamSignup contains the PersonSignup.
    @Test
    void updateTeamAssignments01() {
        String blue = "Blue";
        String green = "Green";
        Team blueTeam = new Team(blue);
        Team greenTeam = new Team(green);

        blueTeam.addTeamSignup(new TeamSignup(blueTeam, event, true));
        greenTeam.addTeamSignup(new TeamSignup(greenTeam, event, true));

        gymnast.addTeamAssignment(new TeamAssignment(blueTeam, gymnast, false));
        gymnast.addTeamAssignment(new TeamAssignment(greenTeam, gymnast, false));

        List<String> teamNameList = new ArrayList<>();
        teamNameList.add(blue);
        teamNameList.add(green);

        gymnast.updateTeamAssignments(teamNameList);

        assertEquals(1, event.getPersonSignupList().size());
        assertEquals(1, blueTeam.getTeamSignupList().size());
        assertEquals(1, blueTeam.getTeamSignupList().get(0).getPersonSignupList().size());
        assertEquals(1, greenTeam.getTeamSignupList().size());
        assertEquals(1, greenTeam.getTeamSignupList().get(0).getPersonSignupList().size());
        assertEquals(2, event.getPersonSignupList().get(0).getTeamSignupList().size());
    }

    //A continuation from test updateTeamAssignments01()
    //Gymnast resign from both teams. PersonSignup and TeamSignup no longer contain each other.
    @Test
    void updateTeamAssignments02() {
        String blue = "Blue";
        String green = "Green";
        Team blueTeam = new Team(blue);
        Team greenTeam = new Team(green);

        blueTeam.addTeamSignup(new TeamSignup(blueTeam, event, true));
        greenTeam.addTeamSignup(new TeamSignup(greenTeam, event, true));

        gymnast.addTeamAssignment(new TeamAssignment(blueTeam, gymnast, false));
        gymnast.addTeamAssignment(new TeamAssignment(greenTeam, gymnast, false));

        List<String> teamNameList = new ArrayList<>();
        teamNameList.add(blue);
        teamNameList.add(green);

        gymnast.updateTeamAssignments(teamNameList);
        gymnast.updateTeamAssignments(new ArrayList<>());

        assertEquals(1, event.getPersonSignupList().size());
        assertEquals(1, blueTeam.getTeamSignupList().size());
        assertEquals(0, blueTeam.getTeamSignupList().get(0).getPersonSignupList().size());
        assertEquals(1, greenTeam.getTeamSignupList().size());
        assertEquals(0, greenTeam.getTeamSignupList().get(0).getPersonSignupList().size());
        assertTrue(event.getPersonSignupList().get(0).getTeamSignupList().isEmpty());
    }

    //Gymnast teamAssignmentList is divided into a list with id > 0 and a list with id == 0.
    @Test
    void getTeamAssignmentsSavedAndNotSaved() {

        List<TeamAssignment> saved = new ArrayList<>();
        List<TeamAssignment> notSaved = new ArrayList<>();

        gymnast.addTeamAssignment(new TeamAssignment(1, new Team("Blue1"), gymnast, true));
        gymnast.addTeamAssignment(new TeamAssignment(2, new Team("Blue2"), gymnast, true));
        gymnast.addTeamAssignment(new TeamAssignment(new Team("Blue3"), gymnast, true));
        gymnast.addTeamAssignment(new TeamAssignment(4, new Team("Blue4"), gymnast, true));
        gymnast.addTeamAssignment(new TeamAssignment(new Team("Blue5"), gymnast, true));
        gymnast.addTeamAssignment(new TeamAssignment(6, new Team("Blue6"), gymnast, true));
        gymnast.addTeamAssignment(new TeamAssignment(7, new Team("Blue7"), gymnast, true));

        gymnast.getTeamAssignmentsSavedAndNotSaved(saved, notSaved);

        assertEquals(5, saved.size());
        assertEquals(2, notSaved.size());

    }

    @Test
    void equals01() {
        Gymnast gymnastTemp = new Gymnast(person, license, true);
        assertEquals(gymnastTemp, gymnast);
    }

    @Test
    void equals02() {
        Gymnast gymnastTemp = new Gymnast(new Person("Peter", true, LocalDate.now(), true), "323212", true);
        assertNotEquals(gymnastTemp, gymnast);

    }

    @Test
    void hashcode() {
        assertEquals(Objects.hash(person), gymnast.hashCode());
    }
}
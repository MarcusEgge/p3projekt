package Model;

import java.time.LocalDateTime;
import java.util.*;

public class Team implements Comparable<Team> {
    private int id;
    private String name;
    private List<TeamSignup> teamSignupList;
    private List<TeamAssignment> teamAssignmentList;

    public Team(int id, String name) {
        this.id = id;
        this.name = name;
        this.teamSignupList = new ArrayList<>();
        this.teamAssignmentList = new ArrayList<>();
    }

    public Team( String name) {
        this.id = 0;
        this.name = name;
        this.teamSignupList = new ArrayList<>();
        this.teamAssignmentList = new ArrayList<>();
    }

    public int getID() {
        return id;
    }

    void deleteGymnastFromFutureEvents(TeamMember gymnast){
        LocalDateTime now = LocalDateTime.now();
        Event event;

        for (TeamSignup t: teamSignupList) {
            event = t.getEvent();
            if (event.getStartDate().isAfter(now)) {
                deleteGymnast(gymnast, t, event);
            }
        }

    }

    private void deleteGymnast(TeamMember gymnast, TeamSignup teamSignup, Event event) {
        PersonSignup personSignup, temp = new PersonSignup(gymnast, event);
        List<PersonSignup> personSignupList = teamSignup.getPersonSignupList();
        int index = personSignupList.indexOf(temp);

        if (index != -1) {
            personSignup = personSignupList.get(index);

            personSignup.deleteTeamSignup(teamSignup);
            teamSignup.deletePersonSignup(personSignup);
        }
    }

    void addGymnastToFutureEvents(TeamMember gymnast) {
        LocalDateTime now = LocalDateTime.now();
        Event event;

        for (TeamSignup t: teamSignupList) {
            event = t.getEvent();
            if (event.getStartDate().isAfter(now)) {
                addToEvent(event, gymnast, t);
            }
        }
    }

    private void addToEvent(Event event, TeamMember gymnast, TeamSignup teamSignup) {
        PersonSignup personSignup = new PersonSignup(gymnast, event);
        List<PersonSignup> personSignupsTemp = event.getPersonSignupList();
        int index = personSignupsTemp.indexOf(personSignup);

        if (index != -1) {
            personSignup = personSignupsTemp.get(index);
        } else {
            event.addPersonSignup(personSignup);
        }
        personSignup.addTeamSignup(teamSignup);
        teamSignup.addPersonSignup(personSignup);
    }

    public void generateTeamAssignments(List<Person> personList) {
        for (Person p : personList) {
            if (p.isGymnast()) {
                makeTeamAssignment(p, (TeamMember) p.getRole(Person.GYMNAST_INDEX));
            }
            if (p.isCoach()) {
                makeTeamAssignment(p, (TeamMember) p.getRole(Person.COACH_INDEX));
            }
        }
    }

    private void makeTeamAssignment(Person person, TeamMember teamMember) {
        TeamAssignment teamAssignment = new TeamAssignment(this, teamMember, false);

        teamMember.addTeamAssignment(teamAssignment);
        teamAssignmentList.add(teamAssignment);
    }

    void updateTeamAssignments(List<String> gymnastNames, List<String> coachNames){
        TeamMember teamMember;
        boolean isSignedUp;
        for (TeamAssignment t: teamAssignmentList) {
            teamMember = t.getTeamMember();
            if (teamMember.isGymnast()) {
                isSignedUp = gymnastNames.contains(teamMember.getPerson().getName());
                t.setSignedUp(isSignedUp);
            }
            if (teamMember.isCoach()) {
                isSignedUp = coachNames.contains(teamMember.getPerson().getName());
                t.setSignedUp(isSignedUp);
            }
        }
    }

    public void setName(String name) {
        if (name == null) {
            // TODO: Throw exception.
        }
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public List<Role> getAllGymnastsInTeam() {
        List<Role> allGymnasts = new ArrayList<>();

        for (TeamAssignment t : teamAssignmentList) {
            if (t.getTeamMember().isGymnast()) {
                allGymnasts.add(t.getTeamMember());
            }
        }
        return allGymnasts;
    }

    public List<Role> getAllCoachesInTeam() {
        List<Role> allCoaches = new ArrayList<>();

        for (TeamAssignment t : teamAssignmentList) {
            if (t.getTeamMember().isCoach()) {
                allCoaches.add(t.getTeamMember());
            }
        }
        return allCoaches;
    }

    public void addTeamAssignment(TeamAssignment teamAssignment) {
        teamAssignmentList.add(teamAssignment);
    }

    public void addTeamSignup(TeamSignup teamSignup) {
        teamSignupList.add(teamSignup);
    }

    public void deleteTeamAssignment(TeamAssignment teamAssignment) {
        teamAssignmentList.remove(teamAssignment);
    }

    void deleteTeamSignup(TeamSignup teamSignup) {
        teamSignupList.remove(teamSignup);
    }

    public List<TeamAssignment> getTeamAssignmentList() {
        return teamAssignmentList;
    }

    public List<Person> getGymnastsInTeam() {
        List<Person> gymnastList = new ArrayList<>();

        for (TeamAssignment t : teamAssignmentList) {
            if (!t.isSignedUp()) {
                break;
            } else if (t.getTeamMember().isGymnast()) {
                gymnastList.add(t.getTeamMember().getPerson());
            }
        }
        return gymnastList;
    }

    public List<Person> getCoachesInTeam() {
        List<Person> coachList = new ArrayList<>();

        for (TeamAssignment t : teamAssignmentList) {
            if (!t.isSignedUp()) {
                break;
            } else if (t.getTeamMember().isCoach()) {
                coachList.add(t.getTeamMember().getPerson());
            }
        }
        return coachList;
    }

    public List<TeamSignup> getTeamSignupList(){
      return teamSignupList;
    }

    void delete() {
        for (TeamAssignment ta : teamAssignmentList) {
            ta.deleteFromRole();
        }

        for (TeamSignup ts : teamSignupList) {
            ts.deleteFromEvent();
            ts.deleteFromPersonSignups();
        }
    }

    @Override
    public int compareTo(Team that) {
        return this.name.compareTo(that.name);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Team team = (Team) o;
        return Objects.equals(name, team.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }
}

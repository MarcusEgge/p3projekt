package Model;

import java.util.*;

public class Coach implements TeamMember{
    private int id;
    private Person person;
    private List<TeamAssignment> teamAssignmentList;
    private boolean isActive;

    public Coach(int id, Person person, boolean isActive) {
        this.id = id;
        this.person = person;
        this.teamAssignmentList = new ArrayList<>();
        this.isActive = isActive;
    }

    public Coach(Person person, boolean isActive) {
        this.id = 0;
        this.person = person;
        this.teamAssignmentList = new ArrayList<>();
        this.isActive = isActive;
    }

    @Override
    public List<TeamAssignment> getTeamAssignmentList(){
        return teamAssignmentList;
    }

    @Override
    public void addTeamAssignment(TeamAssignment teamAssignment) {
        if (teamAssignment == null) {
            //TODO: Throw exception.
        }
        teamAssignmentList.add(teamAssignment);
    }

    @Override
    public void deleteTeamAssignment(TeamAssignment teamAssignment) {
        teamAssignmentList.remove(teamAssignment);
    }

    private void deleteAllTeamAssignments() {
        for (int i = teamAssignmentList.size() - 1; i >= 0; i--){
            teamAssignmentList.remove(i).deleteFromTeam();
        }
    }

    @Override
    public boolean isOnTeam(Team team) {
        for (TeamAssignment t: teamAssignmentList) {
            if (t.getTeam().equals(team) && t.isSignedUp()) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean hasNoTeam() {
        for (TeamAssignment t : teamAssignmentList) {
            if (t.isSignedUp()) {
                return false;
            }
        }
        return true;
    }

    @Override
    public boolean isGymnast() {
        return false;
    }

    @Override
    public boolean isCoach() {
        return true;
    }

    @Override
    public boolean isJudge() {
        return false;
    }

    @Override
    public Person getPerson() {
        return person;
    }

    @Override
    public boolean isActive() {
        return isActive;
    }

    @Override
    public void setActive(boolean active) {
        if (isActive && !active) {
            deleteAllTeamAssignments();
        }
        isActive = active;
    }

    @Override
    public void updateTeamAssignments(List<String> teamNameList) {
        for (TeamAssignment t: teamAssignmentList) {
            t.setSignedUp(teamNameList.contains(t.getTeam().getName()));
        }
    }

    @Override
    public int getID() {
        return id;
    }

    @Override
    public void setID(int id) {
        if (this.id != 0 || id == 0) {
            // TODO: Throw exception.
        }
        this.id = id;
    }
}

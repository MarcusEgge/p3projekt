package Model;

import java.util.Objects;

public class CarAssignment {
    private int id;
    private boolean isDriver;
    private Person person;
    private Car car;

    public CarAssignment(boolean isDriver, Person person, Car car) {
        this.id = 0;
        this.isDriver = isDriver;
        this.person = person;
        this.car = car;
    }

    public CarAssignment(int id, boolean isDriver, Person person, Car car) {
        this.id = id;
        this.isDriver = isDriver;
        this.person = person;
        this.car = car;
    }

    public void setID(int id) {
        if (this.id != 0 || id == 0) {
            // TODO: Throw exception.
        }
        this.id = id;
    }

    public Car getCar() {
        return car;
    }

    public void deleteFromCar(){
        car.deleteCarAssignment(this);
    }

    public Person getPerson() {
        return person;
    }

    void deleteFromPerson() {
        person.deleteCarAssignment(this);
    }

    public boolean isDriver() {
        return isDriver;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CarAssignment that = (CarAssignment) o;
        return  Objects.equals(person, that.person) &&
                Objects.equals(car, that.car);
    }

    @Override
    public int hashCode() {
        return Objects.hash( person, car);
    }

    public int getID() {
        return id;
    }
}

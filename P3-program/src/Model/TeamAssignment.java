package Model;

public class TeamAssignment implements Comparable<TeamAssignment> {
    private int id;
    private Team team;
    private TeamMember teamMember;
    private boolean isSignedUp;

    public TeamAssignment(int id, Team team, TeamMember teamMember, boolean isSignedUp) {
        this.id = id;
        this.team = team;
        this.isSignedUp = isSignedUp;

        checkRole(teamMember);
        this.teamMember = teamMember;
    }

    public TeamAssignment(Team team, TeamMember teamMember, boolean isSignedUp) {
        this.id = 0;
        this.team = team;
        this.isSignedUp = isSignedUp;

        checkRole(teamMember);
        this.teamMember = teamMember;
    }

    public TeamAssignment(Team team, TeamMember teamMember) {
        this.id = 0;
        this.team = team;
        this.isSignedUp = false;

        checkRole(teamMember);
        this.teamMember = teamMember;
    }

    private void checkRole(Role role) {
        if (role.isJudge()) {
            // TODO: Throw exception.
        }
    }

    public Team getTeam() {
        return team;
    }

    public boolean isSignedUp() {
        return isSignedUp;
    }

    public void setSignedUp(boolean signedUp) {
        if (teamMember.isGymnast()) {
            if (isSignedUp && !signedUp) {
                team.deleteGymnastFromFutureEvents(teamMember);
            } else if (!isSignedUp && signedUp){
                team.addGymnastToFutureEvents(teamMember);
            }
        }
        isSignedUp = signedUp;
    }

    public TeamMember getTeamMember() {
        return teamMember;
    }

    public void deleteFromTeam() {
        team.deleteTeamAssignment(this);
    }

    public void deleteFromRole() {
        teamMember.deleteTeamAssignment(this);
    }

    @Override
    public int compareTo(TeamAssignment that) {
        if (this.isSignedUp == that.isSignedUp){
            if (!this.getTeamMember().getPerson().equals(that.getTeamMember().getPerson())) {
                return this.getTeamMember().getPerson().compareTo(that.getTeamMember().getPerson());
            } else {
                return this.getTeam().compareTo(that.getTeam());
            }
        } else if (this.isSignedUp) {
            return -1;
        } else {
            return 1;
        }
    }

    public int getID() {
        return id;
    }
}

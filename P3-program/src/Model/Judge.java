package Model;

import java.util.Objects;

public class Judge implements Role {
    private int id;
    private Person person;
    private boolean isActive;

    public Judge(int id, Person person, boolean isActive) {
        this.id = id;
        this.person = person;
        this.isActive = isActive;
    }

    public Judge(Person person, boolean isActive) {
        this.id = 0;
        this.person = person;
        this.isActive = isActive;
    }

    @Override
    public boolean isGymnast() {
        return false;
    }

    @Override
    public boolean isCoach() {
        return false;
    }

    @Override
    public boolean isJudge() {
        return true;
    }

    @Override
    public Person getPerson() {
        return person;
    }

    @Override
    public boolean isActive() {
        return isActive;
    }

    @Override
    public void setActive(boolean active) {
        if (isActive && !active) {
            person.deletePersonSignupsToFutureEvents(this);
        }
        isActive = active;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Judge judge = (Judge) o;
        return isActive == judge.isActive &&
                Objects.equals(person, judge.person);
    }

    @Override
    public int hashCode() {
        return Objects.hash(person, isActive);
    }

    @Override
    public int getID() {
        return id;
    }

    @Override
    public void setID(int id) {
        if (this.id != 0 || id == 0) {
            // TODO: Throw exception.
        }
        this.id = id;
    }
}

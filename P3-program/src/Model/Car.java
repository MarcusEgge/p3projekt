package Model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Car{
    public static final int SEATS = 4;
    private int id;
    private int availableSeats;
    private Event event;
    private List<CarAssignment> carAssignmentList;

    public Car(int id, Event event) {
        this.id = id;
        this.availableSeats = SEATS;
        this.event = event;
        this.carAssignmentList = new ArrayList<>();
    }

    public Car(Event event, Person driver) {
        this.id = 0;
        this.availableSeats = SEATS - 1;
        this.event = event;
        this.carAssignmentList = new ArrayList<>();

        CarAssignment carAssignment = new CarAssignment(true, driver, this);
        carAssignmentList.add(carAssignment);
        driver.addCarAssignment(carAssignment);
    }

    public Car(Event event) {
        this.id = 0;
        this.availableSeats = SEATS;
        this.event = event;
        this.carAssignmentList = new ArrayList<>();
    }

    public List<CarAssignment> getCarAssignmentList() {
        return Collections.unmodifiableList(carAssignmentList);
    }

    boolean hasPerson(Person person){
        for (CarAssignment c: carAssignmentList) {
            if (c.getPerson().equals(person)) {
                return true;
            }
        }
        return false;
    }

    void addCarAssignment(CarAssignment carAssignment) {
        if (carAssignment == null || this != carAssignment.getCar()) {
            // TODO: Throw exception.
        }

        carAssignmentList.add(carAssignment);
        availableSeats--;
    }

    public Event getEvent() {
        return event;
    }

    Person getDriver() {
        for (CarAssignment c: carAssignmentList) {
            if (c.isDriver()) {
                return c.getPerson();
            }
        }
        return null;
    }

    void deleteDriver() {
        CarAssignment carAssignment;

        for (int i = carAssignmentList.size() - 1; i >= 0; i--) {
            if (carAssignmentList.get(i).isDriver()) {
                carAssignment = carAssignmentList.remove(i);
                carAssignment.deleteFromPerson();
                availableSeats++;
                break;
            }
        }
    }

    void deleteAllPassengers() {
        CarAssignment carAssignment;

        for (int i = carAssignmentList.size() - 1; i >= 0; i--) {
            if (!carAssignmentList.get(i).isDriver()) {
                carAssignment = carAssignmentList.remove(i);
                carAssignment.deleteFromPerson();
                availableSeats++;
            }
        }
    }

    public int getAvailableSeats() {
        return availableSeats;
    }

    public void addPerson(boolean isDriver, Person person){
        CarAssignment carAssignment = new CarAssignment(isDriver, person, this);

        checkCarAssignment(carAssignment);

        carAssignmentList.add(carAssignment);
        availableSeats--;
    }

    private void checkCarAssignment(CarAssignment carAssignment){
        if ((carAssignment.isDriver() && hasDriver()) || availableSeats == 0){
            // TODO: Throw exception.
        }
        if (carAssignmentList.contains(carAssignment)){
            //TODO: Throw exception.
        }
    }

    private boolean hasDriver(){
        for (CarAssignment c: carAssignmentList){
            if (c.isDriver()){
                return true;
            }
        }
        return false;
    }

    public void deleteCarAssignment(CarAssignment carAssignment){
        if (carAssignmentList.remove(carAssignment)){
            availableSeats++;
        }
    }

    public int getID() {
        return id;
    }
}
package Model;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import static java.time.temporal.ChronoUnit.DAYS;

public class Event implements Comparable<Event>{
    private int id;
    private String name, address;
    private LocalDateTime startDate;
    private LocalDate endDate;
    private Season season;
    private List<PersonSignup> personSignupList;
    private List<TeamSignup> teamSignupList;
    private List<Car> carList;
    private int coachRequirement, judgeRequirement;
    private boolean isActive;

    public Event(int id, String name, String address, LocalDateTime startDate, LocalDate endDate, Season season,
                 int coachRequirement, int judgeRequirement, boolean isActive) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.startDate = startDate;
        this.endDate = endDate;
        this.season = season;
        this.personSignupList = new ArrayList<>();
        this.teamSignupList = new ArrayList<>();
        this.carList = new ArrayList<>();
        this.isActive = isActive;
        this.coachRequirement = coachRequirement;
        this.judgeRequirement = judgeRequirement;
    }

    public Event(String name, String address, LocalDateTime startDate, LocalDate endDate, Season season,
                 int coachRequirement, int judgeRequirement) {
        this.id = 0;
        this.name = name;
        this.address = address;
        this.startDate = startDate;
        this.endDate = endDate;
        this.season = season;
        this.personSignupList = new ArrayList<>();
        this.teamSignupList = new ArrayList<>();
        this.carList = new ArrayList<>();
        this.coachRequirement = coachRequirement;
        this.judgeRequirement = judgeRequirement;

        updateState();
    }

    public int getID() {
        return id;
    }

    String getName() {
        return name;
    }

    void setName(String name) {
        this.name = name;
    }

    String getAddress() {
        return address;
    }

    void setAddress(String address) {
        this.address = address;
    }

    LocalDateTime getStartDate() {
        return startDate;
    }

    void setStartDate(LocalDateTime startDate) {
        this.startDate = startDate;
    }

    LocalDate getEndDate() {
        return endDate;
    }

    void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public Season getSeason() {
        return season;
    }

    public boolean isActive() {
        return isActive;
    }

    public List<PersonSignup> getPersonSignupList() {
        return personSignupList;
    }

    public List<Person> getPersonsSignedUp() {
        int length = personSignupList.size();
        Person person;
        List<Person> personList = new ArrayList<>();

        for (int i = 0; i < length; i++) {
            person = personSignupList.get(i).getPerson();
            if (!hasDuplicatePerson(i + 1, length, person)) {
                personList.add(person);
            }
        }

        return personList;
    }

    private boolean hasDuplicatePerson(int start, int length, Person person) {
        for (int i = start; i < length; i++) {
            if (personSignupList.get(i).getPerson().equals(person)) {
                return true;
            }
        }
        return false;
    }

    void addPersonSignup(PersonSignup personSignup) {
        if (personSignup == null || this != personSignup.getEvent()) {
            // TODO: Throw exception.
        }
        personSignupList.add(personSignup);
    }

    void addTeamSignup(TeamSignup teamSignup) {
        if (teamSignup == null || teamSignupList.contains(teamSignup) || this != teamSignup.getEvent()) {
            // TODO: Throw exception.
        }
        teamSignupList.add(teamSignup);
    }

    void generatePersonSignUpsFromList(List<Person> personList) {
        for (Person p : personList) {
            generatePersonSignupsFromPerson(p, true, true, true);
        }
    }

    void generatePersonSignupsFromPerson(Person person, boolean makeGymnastSignup, boolean makeCoachSignup, boolean makeJudgeSignup) {
        if (makeGymnastSignup && person.isGymnast()) {
            makePersonSignupForGymnast(person);
        }
        if (makeCoachSignup && person.isCoach()) {
            makePersonSignupForCoachOrJudge(person, Person.COACH_INDEX);
        }
        if (makeJudgeSignup && person.isJudge()) {
            makePersonSignupForCoachOrJudge(person, Person.JUDGE_INDEX);
        }
    }

    private void makePersonSignupForGymnast(Person person) {
        Gymnast gymnast = (Gymnast) person.getRole(Person.GYMNAST_INDEX);
        List<TeamAssignment> teamAssignments = gymnast.getTeamAssignmentList();
        PersonSignup personSignup = new PersonSignup(gymnast, this);
        Team team;
        TeamSignup teamSignup;

        for (TeamAssignment t : teamAssignments) {
            if (t.isSignedUp()) {
                team = t.getTeam();
                teamSignup = new TeamSignup(team, this, false);

                personSignup.addTeamSignup(teamSignup);
                teamSignupList.add(teamSignup);
                team.addTeamSignup(teamSignup);
            }
        }
        person.addPersonSignup(personSignup);
        this.personSignupList.add(personSignup);
    }

    private void makePersonSignupForCoachOrJudge(Person person, int roleIndex) {
        PersonSignup personSignup = new PersonSignup(person.getRole(roleIndex), this);

        person.addPersonSignup(personSignup);
        this.personSignupList.add(personSignup);
    }

    void updatePersonSignups(List<PersonSignup> newPersonSignups) {
        int index;

        for (PersonSignup p : personSignupList) {
            index = newPersonSignups.indexOf(p);

            if (index == -1) {
                //TODO: Throw exception.
            }
            p.setSignedUp(newPersonSignups.get(index).isSignedUp());

        }
    }

    void deletePersonSignup(PersonSignup personSignup) {
        personSignupList.remove(personSignup);
    }

    public List<TeamSignup> getTeamSignupList() {
        return Collections.unmodifiableList(teamSignupList);
    }

    void updateTeamSignups(List<TeamSignup> newTeamSignups) {
        int index;

        for (TeamSignup t : teamSignupList) {
            index = newTeamSignups.indexOf(t);

            if (index == -1) {
                // TODO: Throw exception.
            }
            t.setSignedUp(newTeamSignups.get(index).isSignedUp());
        }
    }

    void deleteTeamSignup(TeamSignup teamSignup) {
        teamSignupList.remove(teamSignup);
    }

    public List<Car> getCarList() {
        return Collections.unmodifiableList(carList);
    }

    void addCar(Car car) {
        if (car == null || !(this == car.getEvent())) {
            // TODO: Throw exception.
        }
        carList.add(car);
    }

    void addCar() {
        carList.add(new Car(this));
    }

    void addCar(Person driver) {
        if (personAlreadyHasCarAssignment(driver)) {
            // TODO: Throw exception.
        }
        carList.add(new Car(this, driver));
    }

    private boolean personAlreadyHasCarAssignment(Person person) {
        for (Car c : carList) {
            if (c.hasPerson(person)) {
                return true;
            }
        }
        return false;
    }

    public void distributePassengers() { // TODO: Unfinished method.
        clearCarsForPassengers();
    }//TODO: method not finished

    private void clearCarsForPassengers() {
        for (Car c : carList) {
            c.deleteAllPassengers();
        }
    }

    void clearCarsForDrivers() {
        for (Car c : carList) {
            c.deleteDriver();
        }
    }

    void updateState() {
        LocalDate now = LocalDate.now();

        isActive = endDate.isAfter(now);
    }

    void delete() {
        List<CarAssignment> carAssignmentList = getAllCarAssignments();

        for (PersonSignup p : personSignupList) {
            p.deleteFromPerson();
        }
        for (TeamSignup t : teamSignupList) {
            t.deleteFromTeam();
        }
        for (CarAssignment c : carAssignmentList) {
            c.deleteFromPerson();
        }
    }

    private List<CarAssignment> getAllCarAssignments() {
        List<CarAssignment> allCarAssignments = new ArrayList<>();

        for (Car c : carList) {
            allCarAssignments.addAll(c.getCarAssignmentList());
        }

        return allCarAssignments;
    }

    public long daysTillDeadline() {
        LocalDateTime deadline = startDate.minusDays(3 * 7);
        LocalDateTime now = LocalDateTime.now();

        return DAYS.between(now, deadline);
    }

    public void correctBasicData() {
        // TODO: Empty method.
    }

    public boolean requirementStatus(String role) {
        if ("Coach".equals(role)) {
            return numberOfCoachesSignedUp() >= coachRequirement;
        } else if ("Judge".equals(role)) {
            return numberOfJudgesSignedUp() >= judgeRequirement;
        } else {
            return false; // TODO: Throw exception.
        }
    }

    private int numberOfCoachesSignedUp() {
        int numberOfCoaches = 0;

        for (PersonSignup p : personSignupList) {
            if (p.isSignedUp() == PersonSignupState.SIGNED_UP && p.isCoach()) {
                numberOfCoaches++;
            }
        }

        return numberOfCoaches;
    }

    private int numberOfJudgesSignedUp() {
        int numberOfJudges = 0;

        for (PersonSignup p : personSignupList) {
            if (p.isSignedUp() == PersonSignupState.SIGNED_UP && p.isJudge()) {
                numberOfJudges++;
            }
        }

        return numberOfJudges;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Event event = (Event) o;
        return Objects.equals(name, event.name) &&
                Objects.equals(address, event.address) &&
                Objects.equals(startDate, event.startDate) &&
                Objects.equals(endDate, event.endDate) &&
                Objects.equals(season, event.season);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, address, startDate, endDate, season);
    }

    @Override
    public int compareTo(Event that) {
        if (this.isActive == that.isActive) {
            if (this.startDate.equals(that.startDate)) {
                return this.name.compareTo(that.name);
            } else {
                return this.startDate.compareTo(that.startDate);
            }
        } else if (this.isActive) {
            return -1;
        } else {
            return 1;
        }
    }
}

package Model;

import java.sql.*;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

class DatabaseManager {
    private Club club;
    private Connection connection;
    private Statement statement;
    private Calendar calendar;

    public DatabaseManager(Club club) {
        this.club = club;
        this.connection = null;
        this.statement = null;
        this.calendar = Calendar.getInstance();
        connectToDatabase();
    }

    private void connectToDatabase() {
        try {
            // Connecting to the database.
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/club?serverTimezone=UTC", "root", "password");

            // The Statement object is used to send SQL statements to the database.
            statement = connection.createStatement();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    void loadAllData(List<Season> seasonList, MemberList memberList, List<Team> teamList) {
        try {
            List<Event> eventList = new ArrayList<>();
            List<TeamSignup> teamSignupList = new ArrayList<>();
            List<Role> gymnastList = new ArrayList<>(), coachList = new ArrayList<>(), judgeList = new ArrayList<>();

            loadPersons(gymnastList, coachList, judgeList, memberList);
            loadSeasons(seasonList, eventList, memberList.getAllMembers());
            loadTeams(teamList);

            loadTeamSignups(eventList, teamList, teamSignupList);
            loadPersonSignups(gymnastList, coachList, judgeList, eventList, teamSignupList);
            loadTeamAssignments(teamList, gymnastList, coachList);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void loadPersons(List<Role> gymnastList, List<Role> coachList, List<Role> judgeList, MemberList memberList) throws SQLException {
        Person person;

        ResultSet resultSet = statement.executeQuery("SELECT * FROM person "
                + "LEFT JOIN gymnast "
                + "ON person.gymnast_id = gymnast.id_gymnast "
                + "LEFT JOIN coach "
                + "ON person.coach_id = coach.id_coach "
                + "LEFT JOIN judge "
                + "ON person.judge_id = judge.id_judge");

        while (resultSet.next()) {
            person = loadPerson(resultSet);
            loadRoles(resultSet, person, gymnastList, coachList, judgeList);

            memberList.addMember(person);
        }
    }

    private Person loadPerson(ResultSet resultSet) throws SQLException {
        List<String> addressList = new ArrayList<>(), emailList = new ArrayList<>(), phoneNumberList = new ArrayList<>();
        Person person = new Person();

        fillLists(resultSet, addressList, emailList, phoneNumberList);

        person.setID(resultSet.getInt("id_person"))
                .setName(resultSet.getString("name"))
                .setGender(resultSet.getBoolean("is_male"))
                .setAddressList(addressList)
                .setEmailList(emailList)
                .setPhoneNumberList(phoneNumberList)
                .setBirthday(resultSet.getDate("birthday").toLocalDate())
                .setClubRegistrationDate(resultSet.getDate("reg_date").toLocalDate())
                .setClubSignupDate(resultSet.getDate("signup_date").toLocalDate())
                .setPhotoConsent(resultSet.getBoolean("photo_consent"));
        return person;
    }

    private void fillLists(ResultSet resultSet, List<String> addressList, List<String> emailList, List<String> phoneNumberList) throws SQLException {
        addressList.add(resultSet.getString("address1"));
        addressList.add(resultSet.getString("address2"));

        emailList.add(resultSet.getString("email1"));
        emailList.add(resultSet.getString("email2"));

        phoneNumberList.add(resultSet.getString("phone_number1"));
        phoneNumberList.add(resultSet.getString("phone_number2"));
    }

    private void loadRoles(ResultSet resultSet, Person person, List<Role> gymnastList, List<Role> coachList, List<Role> judgeList) throws SQLException {
        person.setRole(loadRole(resultSet, "gymnast", person, gymnastList), Person.GYMNAST_INDEX);
        person.setRole(loadRole(resultSet, "coach", person, coachList), Person.COACH_INDEX);
        person.setRole(loadRole(resultSet, "judge", person, judgeList), Person.JUDGE_INDEX);

    }

    private Role loadRole(ResultSet resultSet, String roleType, Person person, List<Role> roleList) throws SQLException {
        int id = resultSet.getInt("person." + roleType + "_id");
        Role role = null;

        if (id != 0) {
            if ("gymnast".equals(roleType)) {
                role = new Gymnast(id, person, resultSet.getString("license"), resultSet.getBoolean("gymnast.is_active"));
            } else if ("coach".equals(roleType)) {
                role = new Coach(id, person, resultSet.getBoolean("coach.is_active"));
            } else {
                role = new Judge(id, person, resultSet.getBoolean("judge.is_active"));
            }
            roleList.add(role);
        }

        return role;
    }

    private void loadSeasons(List<Season> seasonList, List<Event> eventList, List<Person> personList) throws SQLException {
        Season season;
        ResultSet resultSet = statement.executeQuery("SELECT * FROM season");

        while (resultSet.next()) {
            season = loadSeason(resultSet);
            loadEvents(season, personList, eventList);
            seasonList.add(season);
        }
    }

    private Season loadSeason(ResultSet resultSet) throws SQLException {
        return new Season(resultSet.getInt("id_season")
                , club
                , resultSet.getTimestamp("start_date", calendar).toLocalDateTime()
                , resultSet.getTimestamp("end_date", calendar).toLocalDateTime()
                , resultSet.getBoolean("is_active"));
    }

    private void loadEvents(Season season, List<Person> personList, List<Event> eventList) throws SQLException {
        Event event;
        ResultSet resultSet = statement.executeQuery("SELECT * FROM event WHERE season_id = " + season.getId());

        while (resultSet.next()) {
            event = loadEvent(resultSet, season);
            loadCars(event, personList);
            eventList.add(event);
            season.addEvent(event);
        }
    }

    private Event loadEvent(ResultSet resultSet, Season season) throws SQLException {
        return new Event(resultSet.getInt("id_event")
                , resultSet.getString("name")
                , resultSet.getString("address")
                , resultSet.getTimestamp("start_date", calendar).toLocalDateTime()
                , resultSet.getDate("end_date").toLocalDate()
                , season
                , resultSet.getInt("coach_need")
                , resultSet.getInt("judge_need")
                , resultSet.getBoolean("is_active"));
    }

    private void loadCars(Event event, List<Person> personList) throws SQLException {
        Car car;
        ResultSet resultSet = statement.executeQuery("SELECT * FROM car WHERE event_id = " + event.getID() + " "
                + "LEFT JOIN car_assignment AS driver ON car.driver_id = driver.id_carassignment "
                + "LEFT JOIN car_assignment AS pass1 ON car.passenger1_id = pass1.id_carassignment "
                + "LEFT JOIN car_assignment AS pass2 ON car.passenger2_id = pass2.id_carassignment "
                + "LEFT JOIN car_assignment AS pass3 ON car.passenger3_id = pass3.id_carassignment ");

        while (resultSet.next()) {
            car = loadCar(resultSet, event);
            loadCarAssignments(resultSet, car, personList);
        }
    }

    private Car loadCar(ResultSet resultSet, Event event) throws SQLException {
        return new Car(resultSet.getInt("car.id_car"), event);
    }

    private void loadCarAssignments(ResultSet resultSet, Car car, List<Person> personList) throws SQLException {
        int driverID = resultSet.getInt("car.driver_id");
        int pass1ID = resultSet.getInt("car.passenger1_id");
        int pass2ID = resultSet.getInt("car.passenger2_id");
        int pass3ID = resultSet.getInt("car.passenger3_id");

        if (driverID != 0) {
            loadCarAssignment(resultSet, "driver", driverID, car, personList);
        }
        if (pass1ID != 0) {
            loadCarAssignment(resultSet, "pass1", pass1ID, car, personList);
        }
        if (pass2ID != 0) {
            loadCarAssignment(resultSet, "pass2", pass2ID, car, personList);
        }
        if (pass3ID != 0) {
            loadCarAssignment(resultSet, "pass3", pass3ID, car, personList);
        }
    }

    private void loadCarAssignment(ResultSet resultSet, String tableAlias, int id, Car car, List<Person> personList) throws SQLException {
        Person person = getPersonFromList(resultSet.getInt(tableAlias + ".person_id"), personList);
        CarAssignment carAssignment = new CarAssignment(id, resultSet.getBoolean(tableAlias + ".is_driver"), person, car);

        car.addCarAssignment(carAssignment);
        person.addCarAssignment(carAssignment);
    }

    private Person getPersonFromList(int personID, List<Person> personList) {
        for (Person p: personList) {
            if (p.getID() == personID) {
                return p;
            }
        }
        return null;
    }

    private void loadTeams(List<Team> teamList) throws SQLException {
        ResultSet resultSet = statement.executeQuery("select * from team");

        while (resultSet.next()) {
            teamList.add(new Team(resultSet.getInt("id_team"), resultSet.getString("name")));
        }
    }

    private void loadTeamSignups(List<Event> eventList, List<Team> teamList, List<TeamSignup> teamSignupList ) throws SQLException {
        TeamSignup teamSignup;
        ResultSet resultSet = statement.executeQuery("SELECT * FROM team_signup");

        while (resultSet.next()) {
            teamSignup = loadTeamSignup(resultSet, eventList, teamList);

            teamSignup.getEvent().addTeamSignup(teamSignup);
            teamSignup.getTeam().addTeamSignup(teamSignup);
            teamSignupList.add(teamSignup);
        }
    }

    private TeamSignup loadTeamSignup(ResultSet resultSet, List<Event> eventList, List<Team> teamList) throws SQLException {
        Event event = getEventFromList(resultSet.getInt("event_id"), eventList);
        Team team = getTeamFromList(resultSet.getInt("team_id"), teamList);

        if (event == null || team == null) {
            // TODO: Throw exception.
        }

        return new TeamSignup(resultSet.getInt("id_teamsignup"), team, event, resultSet.getBoolean("is_signed_up"));
    }

    private void loadPersonSignups(List<Role> gymnastList, List<Role> coachList, List<Role> judgeList, List<Event> eventList, List<TeamSignup> teamSignupList) throws SQLException {
        PersonSignup personSignup;
        ResultSet resultSet = statement.executeQuery("SELECT * FROM person_signup");

        while (resultSet.next()) {
            personSignup = loadPersonSigup(resultSet, eventList, gymnastList, coachList, judgeList);

            if (personSignup.getRole().isGymnast()) {
                addRelatedTeamSignups(personSignup, teamSignupList);
            }
            personSignup.getEvent().addPersonSignup(personSignup);
            personSignup.getPerson().addPersonSignup(personSignup);
        }
    }

    private PersonSignup loadPersonSigup(ResultSet resultSet, List<Event> eventList, List<Role> gymnastList, List<Role> coachList, List<Role> judgeList) throws SQLException {
        Event event = getEventFromList(resultSet.getInt("event_id"), eventList);
        Role role = getRole(resultSet.getInt("gymnast_id"), resultSet.getInt("coach_id")
                , resultSet.getInt("judge_id"), gymnastList, coachList, judgeList);
        PersonSignupState isSignedUp = PersonSignupState.values()[resultSet.getInt("is_signed_up")];

        if (role == null || event == null) {
            // TODO: Throw exception.
        }
        return new PersonSignup(resultSet.getInt("id_personsignup"), role, event, isSignedUp);
    }

    private Event getEventFromList(int eventID, List<Event> eventList) {
        for (Event e : eventList) {
            if (e.getID() == eventID) {
                return e;
            }
        }

        return null;
    }

    private void addRelatedTeamSignups(PersonSignup personSignup, List<TeamSignup> teamSignupList) throws SQLException {
        TeamSignup teamSignup;
        ResultSet resultSet = statement.executeQuery("SELECT teamsignup_id FROM signup_indexes "
                + "WHERE personsignup_id = " + personSignup.getID());

        while (resultSet.next()) {
            teamSignup = getTeamSignupFromList(resultSet.getInt("teamsignup_id"), teamSignupList);
            if (teamSignup == null || teamSignup.getEvent().getID() != personSignup.getEvent().getID()) {
                // TODO: Throw exception.
            }
            personSignup.addTeamSignup(teamSignup);
            teamSignup.addPersonSignup(personSignup);
        }
    }

    private TeamSignup getTeamSignupFromList(int teamSignupID, List<TeamSignup> teamSignupList) {
        for (TeamSignup t : teamSignupList) {
            if (t.getID() == teamSignupID) {
                return t;
            }
        }

        return null;
    }

    private void loadTeamAssignments(List<Team> teamList, List<Role> gymnastList, List<Role> coachList) throws SQLException {
        TeamAssignment teamAssignment;
        ResultSet resultSet = statement.executeQuery("SELECT * FROM team_assignment");

        while (resultSet.next()) {
            teamAssignment = loadTeamAssignment(resultSet, teamList, gymnastList, coachList);

            teamAssignment.getTeamMember().addTeamAssignment(teamAssignment);
            teamAssignment.getTeam().addTeamAssignment(teamAssignment);
        }
    }

    private TeamAssignment loadTeamAssignment(ResultSet resultSet, List<Team> teamList, List<Role> gymnastList
            , List<Role> coachList) throws SQLException {
        int gymnastID = resultSet.getInt("gymnast_id"), coachID = resultSet.getInt("coach_id");
        Team team = getTeamFromList(resultSet.getInt("team_id"), teamList);
        Role teamMember = getRole(gymnastID, coachID, 0, gymnastList, coachList, null);

        return new TeamAssignment(resultSet.getInt("id_teamassignment"), team, (TeamMember) teamMember
                , resultSet.getBoolean("is_signed_up"));
    }

    private Team getTeamFromList(int teamID, List<Team> teamList) {
        for (Team t : teamList) {
            if (t.getID() == teamID) {
                return t;
            }
        }
        return null;
    }

    private Role getRole(int gymnastID, int coachID, int judgeID, List<Role> gymnastList, List<Role> coachList, List<Role> judgeList) {
        if (gymnastID != 0 && coachID == 0 && judgeID == 0) {
            return getRoleFromList(gymnastID, gymnastList);
        } else if (coachID != 0 && gymnastID == 0 && judgeID == 0) {
            return getRoleFromList(coachID, coachList);
        } else if (judgeID != 0 && gymnastID == 0 && coachID == 0) {
            return getRoleFromList(judgeID, judgeList);
        } else {
            return null; // TODO: Throw exception.
        }
    }

    private Role getRoleFromList(int roleID, List<Role> roleList) {
        for (Role r : roleList) {
            if (r.getID() == roleID) {
                return r;
            }
        }
        return null;
    }

    void savePersonData(Person person) {
        if (person == null) {
            // TODO: Throw exception.
        }
        try {
            Role gymnast = person.getRole(Person.GYMNAST_INDEX);
            Role coach = person.getRole(Person.COACH_INDEX);
            Role judge = person.getRole(Person.JUDGE_INDEX);

            person.setID(getNextID("person", "id_person"));
            savePerson(person);
            saveRoles(gymnast, coach, judge);
            savePersonSignups(person.getPersonSignupList());
            saveCarAssignments(person.getCarAssignmentList());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void savePerson(Person person) throws SQLException {
        statement.executeUpdate("INSERT INTO person VALUES(" + Integer.toString(person.getID()) + ", "
                + person.getName() + ", "
                + person.getBirthday().format(Club.DATE_FORMATTER) + ", "
                + Integer.toString(person.isMale() ? 1 : 0) + ", "
                + person.getRegistrationDate().format(Club.DATE_FORMATTER) + ", "
                + person.getSignupDate().format(Club.DATE_FORMATTER) + ", "
                + person.getAddressList().get(0) + ", "
                + person.getAddressList().get(1) + ", "
                + person.getPhoneNumberList().get(0) + ", "
                + person.getPhoneNumberList().get(1) + ", "
                + person.getEmailList().get(0) + ", "
                + person.getEmailList().get(1) + ", "
                + Integer.toString(person.getPhotoConsent() ? 1 : 0) + ", "
                + "null, "
                + "null, "
                + "null)");
    }

    private void saveRoles(Role gymnast, Role coach, Role judge) throws SQLException {
        if (gymnast != null) {
            gymnast.setID(getNextID("gymnast", "id_gymnast"));
            saveGymnast((Gymnast) gymnast);
        }
        if (coach != null) {
            coach.setID(getNextID("coach", "id_coach"));
            saveCoachOrJudge(coach, "coach");
        }
        if (judge != null) {
            judge.setID(getNextID("judge", "id_judge"));
            saveCoachOrJudge(judge, "judge");
        }
    }

    private void saveGymnast(Gymnast gymnast) throws SQLException {
        statement.executeUpdate("INSERT INTO gymnast VALUES(" + gymnast.getID() + ", "
                + gymnast.getLicense() + ", "
                + (gymnast.isActive() ? 1 : 0) +")");

        updateIndexesInPerson(gymnast, "gymnast");
    }

    private void saveCoachOrJudge(Role role, String tableName) throws SQLException {
        statement.executeUpdate("INSERT INTO "+ tableName + " VALUES(" + role.getID() + ", "
                + (role.isActive() ? 1 : 0) +")");
        updateIndexesInPerson(role, tableName);
    }

    private void updateIndexesInPerson(Role role, String tableName) throws SQLException {
        statement.executeUpdate("UPDATE person SET "+ tableName +"_id = " + role.getID()
                + " WHERE id_person = " + role.getPerson().getID());
    }

    private void savePersonSignups(List<PersonSignup> personSignupList) throws SQLException {
        for (PersonSignup p: personSignupList){
            p.setID(getNextID("person_signup", "id_personsignup"));
            savePersonSignup(p);
        }
    }

    private void savePersonSignup(PersonSignup personSignup) throws SQLException {
        Role role = personSignup.getRole();
        String gymnastID = role.isGymnast() ?  Integer.toString(role.getID()) : "null";
        String coachID = role.isCoach() ?  Integer.toString(role.getID()) : "null";
        String judgeID = role.isJudge() ?  Integer.toString(role.getID()) : "null";;

        statement.executeUpdate("INSERT INTO person_signup VALUES(" + personSignup.getID() + ", "
                + personSignup.isSignedUp().ordinal() + ", "
                + personSignup.getPerson().getID() + ", "
                + gymnastID + ", "
                + coachID + ", "
                + judgeID + ", "
                + personSignup.getEvent().getID() + ")");
    }

    private void saveCarAssignments(List<CarAssignment> carAssignmentList) throws SQLException {
        for (CarAssignment c: carAssignmentList) {
            c.setID(getNextID("car_assignment", "id_carassignment"));
            saveCarAssignment(c);
        }
    }

    private void saveCarAssignment(CarAssignment carAssignment) {
    }

    int getNextID(String tableName, String columnName) throws SQLException {
        ResultSet resultSet = statement.executeQuery("SELECT max(" + columnName + ") from " + tableName);
        if (resultSet.next()) {
            return resultSet.getInt(1);
        }
        return 0; //TODO: Throw exception.
    }
}

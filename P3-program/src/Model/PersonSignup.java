package Model;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class PersonSignup {
    private int id;
    private Person person;
    private Role role;
    private Event event;
    private List<TeamSignup> teamSignupList;
    private PersonSignupState isSignedUp;

    public PersonSignup(int id, Role role, Event event, PersonSignupState isSignedUp) {
        this.id = id;
        this.person = role.getPerson();
        this.role = role;
        this.event = event;
        this.teamSignupList = new ArrayList<>();
        this.isSignedUp = isSignedUp;
    }

    public PersonSignup(Role role, Event event) {
        this.id = 0;
        this.person = role.getPerson();
        this.role = role;
        this.event = event;
        this.teamSignupList = new ArrayList<>();
        this.isSignedUp = role.isGymnast() ? PersonSignupState.NOT_SIGNED_UP : PersonSignupState.UNDECIDED;
    }

    public int getID() {
        return id;
    }

    public void setID(int id) {
        if (this.id != 0 || id == 0) {
            // TODO: Throw exception.
        }
        this.id = id;
    }

    public Person getPerson() {
        return person;
    }

    public Role getRole() {
        return role;
    }

    public Event getEvent() {
        return event;
    }

    public List<TeamSignup> getTeamSignupList() {
        return teamSignupList;
    }

    void addTeamSignup(TeamSignup teamSignup){
        if (teamSignup == null || this.event != teamSignup.getEvent() || teamSignupList.contains(teamSignup)) {
            // TODO: Throw exception.
        }
        teamSignupList.add(teamSignup);
    }

    public PersonSignupState isSignedUp() {
        return isSignedUp;
    }

    public void setSignedUp(PersonSignupState signedUp) {
        isSignedUp = signedUp;
    }

    public void deleteFromEvent(){
        event.deletePersonSignup(this);
    }

    public void deleteFromTeamSignup(){
        for (TeamSignup t: teamSignupList) {
            t.deletePersonSignup(this);
        }
    }

    public void deleteFromPerson(){
        person.deletePersonSignup(this);
    }

    void deleteTeamSignup(TeamSignup teamSignup) {
        teamSignupList.remove(teamSignup);
    }

    public boolean isGymnast(){
        return role.isGymnast();
    }

    public boolean isCoach(){
        return role.isCoach();
    }

    public boolean isJudge(){
        return role.isJudge();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PersonSignup that = (PersonSignup) o;
        return Objects.equals(person, that.person) &&
                Objects.equals(role, that.role) &&
                Objects.equals(event, that.event);
    }

    @Override
    public int hashCode() {
        return Objects.hash(person, role, event);
    }
}

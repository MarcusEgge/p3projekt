package Model;

import java.util.*;

class MemberList {
    private int numberOfMembers, gymInd, gymJudInd, gymCoaJudInd, gymCoaInd, coaInd, coaJudInd, judInd;
    private List<Integer> indexList;
    private List<Person> personList; // TODO: One list instead of three. Check if gymnast license already exists.
    private int isOnlyJudge;

    MemberList() {
        this.numberOfMembers = 0;
        this.gymInd = 0;
        this.gymJudInd = 1;
        this.gymCoaJudInd = 2;
        this.gymCoaInd = 3;
        this.coaInd = 4;
        this.coaJudInd = 5;
        this.judInd = 6;
        this.isOnlyJudge = -2;
        this.indexList = new ArrayList<>();
        this.personList = new ArrayList<>();

        fillIndexList();
    }

    private void fillIndexList() {
        for (int i = 0; i < judInd + 1; i++) {
            indexList.add(0);
        }
    }

    void addMembers(List<Person> personList) {
        for (Person p: personList) {
            addMember(p);
        }
    }

    List<Person> getAllMembers() {
        return Collections.unmodifiableList(personList);
    }

    void addMember(Person newMember) {
        checkMember(newMember);
        addPerson(newMember);
    }

    private void checkMember(Person member) {
        if (member == null || personList.contains(member)) {
            // TODO: Throw exception.
        }
    }

    private void addPerson(Person person) {
        int iIndexList = getIndex(person);
        int iPersonList;

        if (iIndexList == isOnlyJudge) {
            iPersonList = numberOfMembers;
        } else {
            iPersonList = indexList.get(iIndexList);
            updateIndexes(iIndexList, 1);
        }

        personList.add(iPersonList, person);
        numberOfMembers++;
    }

    private int getIndex(Person member) {
        boolean isGymnast = member.isGymnast(), isCoach = member.isCoach(), isJudge = member.isJudge();

        if (isGymnast && isCoach && isJudge) {
            return gymCoaInd;
        } else if (isGymnast && isCoach) {
            return coaInd;
        } else if (isGymnast && isJudge) {
            return gymCoaJudInd;
        } else if (isCoach && isJudge) {
            return judInd;
        } else if (isGymnast) {
            return gymJudInd;
        } else if (isCoach) {
            return coaJudInd;
        } else if (isJudge) {
            return isOnlyJudge;
        } else {
            return -1; // TODO: Throw exception.
        }
    }

    private void updateIndexes(int indexFrom, int toBeAdded) {
        for (int i = indexFrom; i < judInd + 1; i++) {
            indexList.set(i, indexList.get(i) + toBeAdded);
        }
    }

    Person getMember(String name) {
        for (Person p : personList) {
            if (p.getName().equals(name)) {
                return p;
            }
        }
        return null;
    }

    Person getMember(int index) {
        return personList.get(index);
    }

    int indexOf(String name) {
        if (name != null) {
            for (int i = 0; i < numberOfMembers; i++) {
                if (personList.get(i).getName().equals(name)) {
                    return i;
                }
            }
        }
        return -1;
    }

    Person deleteMember(Person member) {
        int indexMember = personList.indexOf(member);

        if (indexMember != -1) {
            return deletePerson(indexMember);
        } else {
            return null; // TODO: Throw exception instead.
        }
    }

    private Person deletePerson(int indexMember) {
        int indexUpdate;

        if (indexMember < indexList.get(judInd)) {
            if (indexMember >= indexList.get(coaJudInd)) {
                indexUpdate = judInd;
            } else if (indexMember >= indexList.get(coaInd)) {
                indexUpdate = coaJudInd;
            } else if (indexMember >= gymCoaInd) {
                indexUpdate = coaInd;
            } else if (indexMember >= gymCoaJudInd) {
                indexUpdate = gymCoaInd;
            } else if (indexMember >= gymJudInd) {
                indexUpdate = gymCoaJudInd;
            } else {
                indexUpdate = gymJudInd;
            }
            updateIndexes(indexUpdate, -1);
        }
        numberOfMembers--;
        return personList.remove(indexMember);
    }

    void updateMember(int indexActual) {
        Person member = personList.get(indexActual);
        int iIndexList = getIndex(member);
        int iExpectedFrom, iExpectedTo;

        if (iIndexList == isOnlyJudge) {
            iExpectedFrom = indexList.get(judInd);
            iExpectedTo = numberOfMembers - 1;
        } else {
            iExpectedFrom = indexList.get(iIndexList - 1);
            iExpectedTo = indexList.get(iIndexList) - 1;
        }

        if (indexActual < iExpectedFrom || indexActual > iExpectedTo) {
            deletePerson(indexActual);
            addPerson(member);
        }
    }

    List<Person> getGymnastList() {
        List<Person> gymnastList = getSubList(indexList.get(gymInd), indexList.get(coaInd));
        Collections.sort(gymnastList);
        return gymnastList;
    }

    List<Person> getCoachList() {
        List<Person> coachList = getSubList(indexList.get(gymCoaJudInd), indexList.get(judInd));
        Collections.sort(coachList);
        return coachList;
    }

    List<Person> getJudgeList() {
        List<Person> judgeList = getSubList(indexList.get(coaJudInd), numberOfMembers);
        judgeList.addAll(getSubList(indexList.get(gymJudInd), indexList.get(gymCoaInd)));
        Collections.sort(judgeList);
        return judgeList;
    }

    private List<Person> getSubList(int indexFrom, int indexTo) {
        List<Person> subList = new ArrayList<>();

        for (int i = indexFrom; i < indexTo; i++){
            subList.add(personList.get(i));
        }

        return subList;
    }
}

package Model;

import java.util.List;

public interface TeamMember extends Role {
    List<TeamAssignment> getTeamAssignmentList();

    void addTeamAssignment(TeamAssignment teamAssignment);

    void deleteTeamAssignment(TeamAssignment teamAssignment);

    boolean isOnTeam(Team team);

    boolean hasNoTeam();

    void updateTeamAssignments(List<String> teamNameList);
}

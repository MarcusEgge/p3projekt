package Model;

import java.util.*;

public class Gymnast implements TeamMember {
    private int id;
    private Person person;
    private String license;
    private List<TeamAssignment> teamAssignmentList;
    private boolean isActive;

    public Gymnast(int id, Person person, String license, boolean isActive) {
        this.id = id;
        this.person = person;
        this.license = license;
        this.teamAssignmentList = new ArrayList<>();
        this.isActive = isActive;
    }

    public Gymnast(Person person, String license, boolean isActive) {
        this.id = 0;
        this.person = person;
        this.license = license;
        this.teamAssignmentList = new ArrayList<>();
        this.isActive = isActive;
    }

    public String getLicense() {
        return license;
    }

    public void setLicense(String license) {
        if (license == null) {
            // TODO: Throw exception.
        }
        this.license = license;
    }

    @Override
    public List<TeamAssignment> getTeamAssignmentList() {
        return teamAssignmentList;
    }

    @Override
    public void addTeamAssignment(TeamAssignment teamAssignment) {
        if (teamAssignment == null) {
            //TODO: Throw exception.
        }
        teamAssignmentList.add(teamAssignment);
    }

    @Override
    public void deleteTeamAssignment(TeamAssignment teamAssignment) {
        teamAssignmentList.remove(teamAssignment);
    }

    @Override
    public boolean isOnTeam(Team team) {
        for (TeamAssignment t : teamAssignmentList) {
            if (t.getTeam().equals(team) && t.isSignedUp()) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean isGymnast() {
        return true;
    }

    @Override
    public boolean isCoach() {
        return false;
    }

    @Override
    public boolean isJudge() {
        return false;
    }

    @Override
    public Person getPerson() {
        return person;
    }

    @Override
    public boolean isActive() {
        return isActive;
    }

    @Override
    public void setActive(boolean active) {
        if (isActive && !active) {
            deleteAllTeamAssignments();
        }
        isActive = active;
    }

    private void deleteAllTeamAssignments() {
        for (int i = teamAssignmentList.size() - 1; i >= 0; i--) {
            teamAssignmentList.remove(i).deleteFromTeam();
        }
    }

    @Override
    public void updateTeamAssignments(List<String> teamNameList) {
        for (TeamAssignment t : teamAssignmentList) {
            t.setSignedUp(teamNameList.contains(t.getTeam().getName()));
        }
    }

    @Override
    public boolean hasNoTeam() {
        for (TeamAssignment t : teamAssignmentList) {
            if (t.isSignedUp()) {
                return false;
            }
        }
        return true;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Gymnast gymnast = (Gymnast) o;
        return isActive == gymnast.isActive &&
                Objects.equals(person, gymnast.person) &&
                Objects.equals(license, gymnast.license) &&
                Objects.equals(teamAssignmentList, gymnast.teamAssignmentList);
    }

    @Override
    public int hashCode() {
        return Objects.hash(person, license, teamAssignmentList, isActive);
    }

    @Override
    public int getID() {
        return id;
    }

    @Override
    public void setID(int id) {
        if (this.id != 0 || id == 0) {
            // TODO: Throw exception.
        }
        this.id = id;
    }
}
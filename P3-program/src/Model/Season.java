package Model;

import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;

public class Season implements Comparable<Season> {
    private int id;
    private Club club;
    private String name;
    private LocalDateTime startDate, endDate;
    private List<Event> eventList;
    private boolean isActive;

    public Season(int id, Club club, LocalDateTime startDate, LocalDateTime endDate, boolean isActive) {
        this.id = id;
        this.club = club;
        this.name = Integer.toString(startDate.getYear()) + "/" + Integer.toString(endDate.getYear());
        this.startDate = startDate;
        this.endDate = endDate;
        this.eventList = new ArrayList<>();
        this.isActive = isActive;
    }

    public Season(Club club, LocalDateTime startDate, LocalDateTime endDate) {
        this.id = 0;
        this.club = club;
        this.name = Integer.toString(startDate.getYear()) + "/" + Integer.toString(endDate.getYear());
        this.startDate = startDate;
        this.endDate = endDate;
        this.eventList = new ArrayList<>();
        updateState();
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public LocalDateTime getStartDate() {
        return startDate;
    }

    public LocalDateTime getEndDate() {
        return endDate;
    }

    //TODO: Unfinished method. Less access. Test.
    List<Event> viewEvents(){
        updateEvents();
        Collections.sort(eventList);
        return Collections.unmodifiableList(eventList);
    }

    private void updateEvents(){
        for (Event e: eventList) {
            e.updateState();
        }
    }

    public boolean isActive() {
        return isActive;
    }

    void updateState(){
        LocalDateTime now = LocalDateTime.now();

        isActive = startDate.isBefore(now) && endDate.isAfter(now);
    }

    void addEvent(Event event) {
        if (event == null || !(this == event.getSeason())) {
            // TODO: Throw exception.
        }
        eventList.add(event);
    }

    void createEvent(String name, String address, String startDateTimeString, String endDateString,
                     int coachRequirement, int judgeRequirement){
        LocalDateTime startDate = LocalDateTime.parse(startDateTimeString, Club.DATE_TIME_FORMATTER);
        LocalDate endDate = LocalDate.parse(endDateString, Club.DATE_FORMATTER);

        Event event = new Event(name, address, startDate, endDate, this, coachRequirement, judgeRequirement);

        if (eventList.contains(event)) {
            // TODO: Throw exception.
        }

        event.generatePersonSignUpsFromList(club.getAllMembers());

        eventList.add(event);
    }

    void editEvent(Event eventOld, Event eventNew){
        eventOld = getEvent(eventOld);

        eventOld.setName(eventNew.getName());
        eventOld.setAddress(eventNew.getAddress());
        eventOld.setStartDate(eventNew.getStartDate());
        eventOld.setEndDate(eventNew.getEndDate());
    }

    private Event getEvent(Event event) {
        int index = eventList.indexOf(event);

        if (index != -1){
            return eventList.get(index);
        } else {
            throw new RuntimeException(); // TODO: Make own exception.
        }
    }

    void deleteEvent(Event event){
        event = getEvent(event);

        eventList.remove(event);
        event.delete();
    }

    public void distributeDrivers(){
        for (Event e: eventList){
            e.clearCarsForDrivers();
            distributeForEvent(e);
        }
    }

    private void distributeForEvent(Event event){      // TODO: Unfinished method.
        List<Person> personList = event.getPersonsSignedUp();
        int numberOfCars = personList.size() / Car.SEATS;

        for (int i = 0; i < numberOfCars; i++){
            event.addCar(chooseDriver(personList));
        }
    }

    private Person chooseDriver(List<Person> personList){
        int bound = personList.size();
        Random random = new Random();

        return personList.remove(random.nextInt(bound)); // TODO: Unfinished method.
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null || getClass() != that.getClass()) {
            return false;
        }
        Season season = (Season) that;
        return Objects.equals(name, season.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    @Override
    public int compareTo(Season that) {
        if (this.isActive == that.isActive){
            return -this.startDate.compareTo(that.startDate);
        }
        else if (this.isActive){
            return -1;
        }
        else {
            return 1;
        }
    }

    public int getID() {
        return id;
    }
}

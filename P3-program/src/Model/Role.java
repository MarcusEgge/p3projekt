package Model;

public interface Role {
    Person getPerson();
    void setActive(boolean active);
    boolean isActive();
    boolean isGymnast();
    boolean isCoach();
    boolean isJudge();
    int getID();
    void setID(int id);
}

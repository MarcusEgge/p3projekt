package Model;

import java.util.ArrayList;
import java.util.List;

public class TeamSignup {
    private int id;
    private Team team;
    private Event event;
    private List<PersonSignup> personSignupList;
    private boolean isSignedUp;

    public TeamSignup(int id, Team team, Event event, boolean isSignedUp) {
        this.id = id;
        this.team = team;
        this.event = event;
        this.personSignupList = new ArrayList<>();
        this.isSignedUp = isSignedUp;
    }

    public TeamSignup(Team team, Event event, boolean isSignedUp) {
        this.id = 0;
        this.team = team;
        this.event = event;
        this.personSignupList = new ArrayList<>();
        this.isSignedUp = isSignedUp;
    }

    public int getID() {
        return id;
    }

    public Team getTeam() {
        return team;
    }

    public Event getEvent() {
        return event;
    }

    public boolean isSignedUp() {
        return isSignedUp;
    }

    void setSignedUp(boolean signedUp) {
        if (isSignedUp != signedUp) {
            isSignedUp = signedUp;

            for (PersonSignup p: personSignupList) {
                p.setSignedUp(isSignedUp ? PersonSignupState.SIGNED_UP : PersonSignupState.NOT_SIGNED_UP);
            }
        }
    }

    public List<PersonSignup> getPersonSignupList() {
        return personSignupList;
    }

    void addPersonSignup(PersonSignup personSignup) {
        if (personSignup == null || this.event != personSignup.getEvent()) {
            // TODO: Throw exception.
        }
        personSignupList.add(personSignup);
    }

    public void deletePersonSignup(PersonSignup personSignup) {
        personSignupList.remove(personSignup);
    }

    void deleteFromEvent() {
        event.deleteTeamSignup(this);
    }

    void deleteFromTeam() {
        team.deleteTeamSignup(this);
    }

    void deleteFromPersonSignups() {
        for (PersonSignup p : personSignupList) {
            p.deleteTeamSignup(this);
        }
    }
}

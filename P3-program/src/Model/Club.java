package Model;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class Club {
    private MemberList memberList;
    private List<Season> seasonList;
    private List<Team> teamList;
    private Map<Person, List<Integer>> drivingStatistics;
    private DatabaseManager databaseManager;
    static final public DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm"); // TODO: May have to be changed.
    static final public DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd");

    public Club() {
        databaseManager = new DatabaseManager(this);
        memberList = new MemberList();
        seasonList = new ArrayList<>();
        teamList = new ArrayList<>();
        drivingStatistics = new HashMap<>();
    }

    public void getDataFromDatabase(){
        databaseManager.loadAllData(seasonList, memberList, teamList);
    }

    public List<Season> viewSeasons() {
        return Collections.unmodifiableList(seasonList);
    }

    public List<Event> viewEventsInSeason(String seasonName) {
        Season season = viewSeason(seasonName);

        if (season == null) {
            // TODO: Throw exception.
        }
        return season.viewEvents();
    }

    private Season viewSeason(String seasonName) {
        for (Season s : seasonList) {
            if (s.getName().equals(seasonName)) {
                return s;
            }
        }
        return null;
    }

    List<Person> getAllMembers() {
        return Collections.unmodifiableList(memberList.getAllMembers());
    }

    public List<Person> viewPersons(String personType) {
        if ("Gymnast".equals(personType)) {
            return viewGymnasts();
        } else if ("Coach".equals(personType)) {
            return viewCoaches();
        } else if ("Judge".equals(personType)) {
            return viewJudges();
        } else {
            return null; // TODO: Throw exception.
        }
    }

    List<Person> viewGymnasts() {
        return memberList.getGymnastList();
    }

    private List<Person> viewCoaches() {
        return memberList.getCoachList();
    }

    private List<Person> viewJudges() {
        return memberList.getJudgeList();
    }

    public List<Season> filterSeasons() {
        Collections.sort(seasonList);
        return seasonList;
    }

    public Team viewTeam(String teamName) {
        for (Team t : teamList) {
            if (t.getName().equals(teamName)) {
                return t;
            }
        }
        return null; // TODO: Throw exception.
    }

    public List<Person> filterPersons(String personType, String teamName) {
        Team team = viewTeam(teamName);

        if ("Gymnast".equals(personType)) {
            return team.getGymnastsInTeam();
        } else if ("Coach".equals(personType)) {
            return team.getCoachesInTeam();
        } else {
            // TODO: Throw exception.
            return null;
        }
    }

    public Person viewPerson(String name) {
        Person person = memberList.getMember(name);

        if (person == null) {
            // TODO: Throw exception.
        }

        return person;
    }

    public List<Team> getTeamList() {
        return Collections.unmodifiableList(teamList);
    }

    public void createSeason() {
        int startDateInd = 0, endDateInd = 1;
        LocalDateTime startDate, endDate;
        List<LocalDateTime> dateList;

        updateSeasons();
        Collections.sort(seasonList);

        dateList = getDates(startDateInd, endDateInd);
        startDate = dateList.get(startDateInd);
        endDate = dateList.get(endDateInd);


        seasonList.add(new Season(this, startDate, endDate));
        Collections.sort(seasonList);
    }

    private void updateSeasons() {
        for (Season s : seasonList) {
            s.updateState();
        }
    }

    private List<LocalDateTime> getDates(int startDateInd, int endDateInd) {
        int yearJump = 1;
        List<LocalDateTime> dateList;

        if (!seasonList.isEmpty()) {
            dateList = getDatesBasedOnFormerSeason(startDateInd, endDateInd, yearJump);
        } else {
            dateList = getDatesBasedOnCurrentYear(startDateInd, endDateInd, yearJump);
        }

        return dateList;
    }

    private List<LocalDateTime> getDatesBasedOnFormerSeason(int startDateInd, int endDateInd, int yearJump) {
        Season formerSeason;
        List<LocalDateTime> dateList = new ArrayList<>();

        if (seasonList.size() > 1 && seasonList.get(0).getStartDate().compareTo(seasonList.get(1).getStartDate()) < 0) {
            formerSeason = seasonList.get(1);
        } else {
            formerSeason = seasonList.get(0);
        }
        dateList.add(startDateInd, formerSeason.getStartDate().plusYears(yearJump));
        dateList.add(endDateInd, formerSeason.getEndDate().plusYears(yearJump));

        return dateList;
    }

    private List<LocalDateTime> getDatesBasedOnCurrentYear(int startDateInd, int endDateInd, int yearJump) {
        List<LocalDateTime> dateList = new ArrayList<>();
        List<String> stringDateList;

        stringDateList = getDatesAsStrings(startDateInd, endDateInd, yearJump);

        dateList.add(startDateInd, LocalDateTime.parse(stringDateList.get(startDateInd), DATE_TIME_FORMATTER));
        dateList.add(endDateInd, LocalDateTime.parse(stringDateList.get(endDateInd), DATE_TIME_FORMATTER));

        return dateList;
    }

    private List<String> getDatesAsStrings(int startDateInd, int endDateInd, int yearJump) {
        List<String> stringDateList = new ArrayList<>();
        String startDayAndMonth = "-08-01", endDayAndMonth = "-07-31", startTime = " 00:00", endTime = " 23:59";
        LocalDate now = LocalDate.now();

        stringDateList.add(startDateInd, Integer.toString(now.getYear()) + startDayAndMonth + startTime);
        stringDateList.add(endDateInd, Integer.toString(now.getYear() + yearJump) + endDayAndMonth + endTime);

        return stringDateList;
    }

    public void createPerson(String name, boolean isMale, List<String> addrList, List<String> emailList,
                             List<String> phoneList, String bDay, String regDate, String signUpDate, boolean photoConsent,
                             boolean isGymnast, boolean isCoach, boolean isJudge, String license,
                             List<String> teamListGymnast, List<String> teamListCoach) {
        Person person = new Person();

        if (!isGymnast && !isCoach && !isJudge) {
            // TODO: Throw exception.
        }

        buildPerson(person, name, isMale, addrList, emailList, phoneList, bDay, regDate, signUpDate, photoConsent);
        registerRoles(person, isGymnast, isCoach, isJudge, license, teamListGymnast, teamListCoach);
        generatePersonSignupsForFutureEvents(person, true, true, true);

        memberList.addMember(person);
        //databaseManager.savePersonData(person);
    }

    void buildPerson(Person person, String name, boolean isMale, List<String> addrList, List<String> emailList,
                             List<String> phoneList, String bDay, String regDate, String signUpDate, boolean photoConsent) {
        person.setName(name)
                .setGender(isMale)
                .setAddressList(addrList)
                .setEmailList(emailList)
                .setPhoneNumberList(phoneList)
                .setBirthday(LocalDate.parse(bDay, DATE_TIME_FORMATTER))
                .setClubRegistrationDate(LocalDate.parse(regDate, DATE_TIME_FORMATTER))
                .setClubSignupDate(LocalDate.parse(signUpDate, DATE_TIME_FORMATTER))
                .setPhotoConsent(photoConsent);
    }

    private void registerRoles(Person person, boolean isGymnast, boolean isCoach, boolean isJudge, String license,
                               List<String> teamNameListGymnast, List<String> teamNameListCoach) {
        if (isGymnast) {
            registerGymnast(person, license, teamNameListGymnast);
        }
        if (isCoach) {
            registerCoach(person, teamNameListCoach);
        }
        if (isJudge) {
            registerJudge(person);
        }
    }

    private void registerGymnast(Person person, String license, List<String> teamNameList) {
        TeamMember gymnast = new Gymnast(person, license, true);

        generateTeamAssignments(gymnast, teamNameList);
        person.setRole(gymnast, Person.GYMNAST_INDEX);
    }

    private void registerCoach(Person person, List<String> teamNameList) {
        Coach coach = new Coach(person, true);

        generateTeamAssignments(coach, teamNameList);
        person.setRole(coach, Person.COACH_INDEX);
    }

    private void generateTeamAssignments(TeamMember teamMember, List<String> teamNameList) {
        TeamAssignment teamAssignment;
        boolean isOnTeam;

        for (Team t : teamList) {
            isOnTeam = teamNameList.contains(t.getName());

            teamAssignment = new TeamAssignment(t, teamMember, isOnTeam);
            t.addTeamAssignment(teamAssignment);
            teamMember.addTeamAssignment(teamAssignment);
        }
    }

    private void registerJudge(Person person) {
        person.setRole(new Judge(person, true), Person.JUDGE_INDEX);
    }

    public void editPerson(String nameOld, String name, boolean isMale, List<String> addrList, List<String> emailList,
                           List<String> phoneList, String bDay, String regDate, String signUpDate, boolean photoConsent,
                           boolean isGymnast, boolean isCoach, boolean isJudge, String license,
                           List<String> teamListGymnast, List<String> teamListCoach) {
        Person person;
        int index = memberList.indexOf(nameOld);
        boolean makeGymnastSignups, makeCoachSignups, makeJudgeSignups;

        if (index == -1  || (!isGymnast && !isCoach && !isJudge)) {
            // TODO: Throw exception.
        }
        person = memberList.getMember(index);

        buildPerson(person, name, isMale, addrList, emailList, phoneList, bDay, regDate, signUpDate, photoConsent);

        makeGymnastSignups = editGymnast(person, isGymnast, license, teamListGymnast);
        makeCoachSignups = editCoach(person, isCoach, teamListCoach);
        makeJudgeSignups = editJudge(person, isJudge);

        memberList.updateMember(index);

        generatePersonSignupsForFutureEvents(person, makeGymnastSignups, makeCoachSignups, makeJudgeSignups);
    }

    private boolean editGymnast(Person person, boolean isGymnast, String license, List<String> teamNameList) {
        TeamMember gymnast = (TeamMember) person.getRole(Person.GYMNAST_INDEX);

        if (gymnast != null) {
            return doTeamMemberEdit(person, isGymnast, teamNameList, gymnast);
        } else if (isGymnast) {
            registerGymnast(person, license, teamNameList);
            return true;
        }

        return false;
    }

    private boolean editCoach(Person person, boolean isCoach, List<String> teamNameList) {
        TeamMember coach = (TeamMember) person.getRole(Person.COACH_INDEX);

        if (coach != null) {
            return doTeamMemberEdit(person, isCoach, teamNameList, coach);
        } else if (isCoach) {
            registerCoach(person, teamNameList);
            return true;
        }

        return false;
    }

    private boolean doTeamMemberEdit(Person person, boolean isTeamMember, List<String> teamNameList, TeamMember teamMember) {
        if (teamMember.isActive() && !isTeamMember) {
            teamMember.setActive(false);
            person.deletePersonSignupsToFutureEvents(teamMember);
        } else if (!teamMember.isActive() && isTeamMember) {
            teamMember.setActive(true);
            generateTeamAssignments(teamMember, teamNameList);
            return true;
        } else if (teamMember.isActive() && isTeamMember) {
            teamMember.updateTeamAssignments(teamNameList);
        }

        return false;
    }

    private boolean editJudge(Person person, boolean isJudge) {
        Role judge = person.getRole(Person.JUDGE_INDEX);

        if (judge != null) {
            if (judge.isActive() && !isJudge) {
                judge.setActive(false);
                person.deletePersonSignupsToFutureEvents(judge);
            } else if (!judge.isActive() && isJudge) {
                judge.setActive(true);
                return true;
            }
        } else if (isJudge) {
            registerJudge(person);
            return true;
        }
        return false;
    }

    private void generatePersonSignupsForFutureEvents(Person person, boolean makeGymnastSignup, boolean makeCoachSignup,
                                                      boolean makeJudgeSignup) {
        LocalDateTime now = LocalDateTime.now();

        for (Season s : seasonList) {
            if (s.getEndDate().isAfter(now)) {
                loopEventsInSeason(person, s.viewEvents(), now, makeGymnastSignup, makeCoachSignup, makeJudgeSignup);
            }
        }
    }

    private void loopEventsInSeason(Person person, List<Event> eventList, LocalDateTime now, boolean makeGymnastSignup,
                                    boolean makeCoachSignup, boolean makeJudgeSignup) {
        for (Event e : eventList) {
            if (e.getStartDate().isAfter(now)) {
                e.generatePersonSignupsFromPerson(person, makeGymnastSignup, makeCoachSignup, makeJudgeSignup);
            }
        }
    }

    public void deletePerson(Person person) {
        person = memberList.deleteMember(person);

        if (person == null) {
            // TODO: Throw exception.
        }
        person.delete();
    }

    public void createTeam(String teamName) {
        Team newTeam = new Team(teamName);

        if (teamList.contains(newTeam)) {
            // TODO: Throw exception.
        }

        generateTeamSignupsForFutureEvents(newTeam);
        newTeam.generateTeamAssignments(memberList.getAllMembers());

        teamList.add(newTeam);
    }

    private void generateTeamSignupsForFutureEvents(Team team) {
        LocalDateTime now = LocalDateTime.now();

        for (Season s: seasonList){
            if (s.getEndDate().isAfter(now)){
                loopEventsInSeason(s.viewEvents(), team, now);
            }
        }
    }

    private void loopEventsInSeason(List<Event> eventList, Team team, LocalDateTime now) {
        for (Event e: eventList){
            if (e.getStartDate().isAfter(now)) {
                e.addTeamSignup(new TeamSignup(team, e, false));
            }
        }
    }

    public void editTeam(String oldName, String newName, List<String> gymnastNames, List<String> coachNames) {
        Team team = viewTeam(oldName);

        if (!oldName.equals(newName) && teamNameTaken(newName)) {
            // TODO: Throw exception.
        }

        team.setName(newName);
        team.updateTeamAssignments(gymnastNames, coachNames);
    }

    private boolean teamNameTaken(String teamName) {
        for (Team t : teamList) {
            if (t.getName().equals(teamName)) {
                return true;
            }
        }
        return false;
    }

    public void deleteTeam(Team team) {
        team = teamList.remove(teamList.indexOf(team));
        team.delete();
    }
}

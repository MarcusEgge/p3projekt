package Model;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;

public class Person implements Comparable<Person> {
    public final static int GYMNAST_INDEX = 0, COACH_INDEX = 1, JUDGE_INDEX = 2;
    private int id;
    private String name;
    private boolean isMale;
    private LocalDate birthday, registrationDate, signupDate;
    private List<String> addressList, emailList, phoneNumberList;
    private List<Role> roleList;
    private List<PersonSignup> personSignupList;
    private List<CarAssignment> carAssignmentList;
    private boolean photoConsent;

    public Person() {
        this.id = 0;
        addressList = new ArrayList<>(2);
        emailList = new ArrayList<>(2);
        phoneNumberList = new ArrayList<>(2);
        roleList = new ArrayList<>(3);
        fillRoleList();
        personSignupList = new ArrayList<>();
        carAssignmentList = new ArrayList<>();
        photoConsent = false;
    }

    public LocalDate getRegistrationDate() {
        return registrationDate;
    }

    public LocalDate getSignupDate() {
        return signupDate;
    }

    public boolean getPhotoConsent() {
        return photoConsent;
    }

    private void fillRoleList() {
        for (int i = 0; i < 3; i++) {
            roleList.add(null);
        }
    }

    public int getID() {
        return id;
    }

    Person setID(int id) {
        if (this.id != 0  || id <= 0) {
            // TODO: Throw exception.
        }
        this.id = id;
        return this;
    }

    public Person setName(String name) {
        if (name == null || name.isEmpty()) {
            // TODO: Throw exception.
        }
        this.name = name;
        return this;
    }

    public String getName() {
        return name;
    }

    public Person setGender(boolean isMale) {
        this.isMale = isMale;
        return this;
    }

    public Person setAddressList(List<String> addressList) {
        checkForInvalidStrings(addressList, "Addresses");
        this.addressList = addressList;
        return this;
    }

    public Person setEmailList(List<String> emailList) {
        checkForInvalidStrings(emailList, "Emails");
        this.emailList = emailList;
        return this;
    }

    public Person setPhoneNumberList(List<String> phoneNumberList) {
        checkForInvalidStrings(phoneNumberList, "Phone numbers");
        this.phoneNumberList = phoneNumberList;
        return this;
    }

    private void checkForInvalidStrings(List<String> stringList, String listType) {
        if (stringList == null) {
            // TODO: Throw exception.
        } else if (stringList.size() != 2) {
            // TODO: Throw exception.
        } else if (stringList.get(0) == null && stringList.get(1) == null) {
            // TODO: Throw exception.
        } else if (stringList.get(0).isEmpty() && stringList.get(1).isEmpty()) {
            // TODO: Throw exception.
        }
    }


    public Person setBirthday(LocalDate birthday) {
        checkForNull(birthday);
        this.birthday = birthday;
        return this;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public Person setClubRegistrationDate(LocalDate registrationDate) {
        checkForNull(registrationDate);
        this.registrationDate = registrationDate;
        return this;
    }

    public Person setClubSignupDate(LocalDate signupDate) {
        checkForNull(signupDate);
        this.signupDate = signupDate;
        return this;
    }

    private void checkForNull(Object object) {
        if (object == null) {
            // TODO: Throw exception.
        }
    }

    public void setRole(Role role, int index) {
        if (role == null || indexAndRoleDoNotMatch(role, index)) {
            // TODO: Throw exception.
        }
        roleList.set(index, role);
    }

    private boolean indexAndRoleDoNotMatch(Role role, int index) {
        return (role.isGymnast() && index != GYMNAST_INDEX) || (role.isCoach() && index != COACH_INDEX)
                || (role.isJudge() && index != JUDGE_INDEX);
    }

    public void addPersonSignup(PersonSignup personSignup) {
        if (personSignup == null) {
            // TODO: Throw exception.
        }
        personSignupList.add(personSignup);
    }

    public Person setPhotoConsent(boolean photoConsent) {
        this.photoConsent = photoConsent;

        return this;
    }

    public boolean isGymnast() {
        return roleList.get(GYMNAST_INDEX) != null && roleList.get(GYMNAST_INDEX).isActive();
    }

    public boolean isCoach() {
        return roleList.get(COACH_INDEX) != null && roleList.get(COACH_INDEX).isActive();
    }

    public boolean isJudge() {
        return roleList.get(JUDGE_INDEX) != null && roleList.get(JUDGE_INDEX).isActive();
    }


    public void delete() {
        List<TeamAssignment> teamAssignmentList = getAllTeamAssignments();

        for (PersonSignup p : personSignupList) {
            p.deleteFromEvent();
            p.deleteFromTeamSignup();
        }

        for (TeamAssignment t : teamAssignmentList) {
            t.deleteFromTeam();
        }

        for (CarAssignment c : carAssignmentList) {
            c.deleteFromCar();
        }
    }

    void deleteCarAssignment(CarAssignment carAssignment) {
        carAssignmentList.remove(carAssignment);
    }

    public List<PersonSignup> getPersonSignupList() {
        return Collections.unmodifiableList(personSignupList);
    }

    void deletePersonSignup(PersonSignup personSignup) {
        personSignupList.remove(personSignup);
    }

    void deletePersonSignupsToFutureEvents(Role role) {
        LocalDateTime now = LocalDateTime.now();
        PersonSignup personSignup;

        for (int i = personSignupList.size() - 1; i >= 0; i--) {
            personSignup = personSignupList.get(i);
            if (personSignup.getEvent().getStartDate().isAfter(now) && role == personSignup.getRole()) {
                personSignup.deleteFromEvent();
                personSignup.deleteFromTeamSignup();
                personSignup.deleteFromPerson();
            }
        }
    }

    public List<CarAssignment> getCarAssignmentList() {
        return Collections.unmodifiableList(carAssignmentList);
    }

    public void addCarAssignment(CarAssignment carAssignment) {
        if (carAssignment == null || this != carAssignment.getPerson()) {
            // TODO: Throw exception.
        }
        this.carAssignmentList.add(carAssignment);
    }

    private List<TeamAssignment> getAllTeamAssignments() {
        List<TeamAssignment> allTeamAssignments = new ArrayList<>();
        Gymnast gymnast = (Gymnast) roleList.get(GYMNAST_INDEX);
        Coach coach = (Coach) roleList.get(COACH_INDEX);

        if (gymnast != null) {
            allTeamAssignments.addAll(gymnast.getTeamAssignmentList());
        }
        if (coach != null) {
            allTeamAssignments.addAll(coach.getTeamAssignmentList());
        }

        return allTeamAssignments;
    }

    public Role getRole(int index) {
        if (indexIsInvalid(index)) {
            // TODO: Throw exception.
        }
        return roleList.get(index);
    }

    private boolean indexIsInvalid(int index) {
        return index != GYMNAST_INDEX && index != COACH_INDEX && index != JUDGE_INDEX;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null || this.getClass() != that.getClass()) {
            return false;
        }
        Person person = (Person) that;
        return Objects.equals(this.name, person.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.name);
    }

    @Override
    public int compareTo(Person that) {
        return this.name.compareTo(that.name);
    }

    public boolean isMale() {
        return isMale;
    }

    public List<String> getAddressList() {
        return addressList;
    }

    public List<String> getEmailList() {
        return emailList;
    }

    public List<String> getPhoneNumberList() {
        return phoneNumberList;
    }
}
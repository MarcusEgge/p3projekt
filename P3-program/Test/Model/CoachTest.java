package Model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class CoachTest {

    private Coach coach;
    private Person person;
    private List<TeamAssignment> teamAssignmentsList;
    private Club club;
    private Team redTeam,blueTeam;
    private TeamAssignment teamAssignment;

    @BeforeEach
    public void initObjects(){
        teamAssignmentsList= new ArrayList<>();
        person = new Person();
        club= new Club();
        redTeam = new Team("Red");
        blueTeam = new Team("Blue");
        coach = new Coach(person,true);
        teamAssignment = new TeamAssignment(redTeam,coach, true);
        redTeam.addTeamAssignment(teamAssignment);
    }

    @Test
    void getTeamAssignmentList() {
        assertEquals(teamAssignmentsList,coach.getTeamAssignmentList());
    }

    @Test
    void addTeamAssignment() {
        coach.addTeamAssignment(teamAssignment);
        assertEquals(teamAssignment,coach.getTeamAssignmentList().get(0));
    }

    @Test
    void deleteTeamAssignment() {
        coach.addTeamAssignment(teamAssignment);
        assertEquals(teamAssignment,coach.getTeamAssignmentList().get(0));
        coach.deleteTeamAssignment(teamAssignment);
        assertEquals(teamAssignmentsList,coach.getTeamAssignmentList());
    }

    @Test
    void isOnTeam1() {
        coach.addTeamAssignment(teamAssignment);
        assertFalse(coach.isOnTeam(blueTeam));
    }

    @Test
    void isOnTeam2() {
        coach.addTeamAssignment(teamAssignment);
        assertTrue(coach.isOnTeam(redTeam));
    }



    @Test
    void isGymnast() {
        assertFalse(coach.isGymnast());
    }

    @Test
    void isCoach() {
        assertTrue(coach.isCoach());
    }

    @Test
    void isJudge() {
        assertFalse((coach.isJudge()));
    }

    @Test
    void getPerson() {
        assertEquals(person,coach.getPerson());
    }

    @Test
    void isActive(){
        assertTrue(coach.isActive());
    }

    @Test
    void hasNoTeam01(){
        assertTrue(coach.hasNoTeam());
    }

    @Test
    void hasNoTeam02(){
        coach.addTeamAssignment(teamAssignment);
        assertFalse(coach.hasNoTeam());
    }

    @Test
    void setActive(){
        coach.addTeamAssignment(teamAssignment);
        assertEquals(1, redTeam.getTeamAssignmentList().size());
        assertEquals(1,coach.getTeamAssignmentList().size());
        coach.setActive(false);

        assertEquals(0, redTeam.getTeamAssignmentList().size());
        assertTrue(coach.hasNoTeam());
        assertFalse(coach.isActive());

    }

    @Test
    void updateTeamAssignment01s(){
        coach.addTeamAssignment(teamAssignment);
        coach.addTeamAssignment(new TeamAssignment(blueTeam,coach,true));

        List<String> list = new ArrayList<>();
        list.add("Blue");


        coach.updateTeamAssignments(list);

        assertEquals(2,coach.getTeamAssignmentList().size());
        assertFalse(coach.getTeamAssignmentList().get(0).isSignedUp());
        assertTrue(coach.getTeamAssignmentList().get(1).isSignedUp());

    }

    @Test
    void updateTeamAssignments02(){
        coach.addTeamAssignment(teamAssignment);
        coach.addTeamAssignment(new TeamAssignment(blueTeam,coach,true));

        coach.updateTeamAssignments(new ArrayList<>());

        assertEquals(2,coach.getTeamAssignmentList().size());
        assertTrue(coach.hasNoTeam());

    }
}
package Model;

import Model.*;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Objects;

import static org.junit.jupiter.api.Assertions.*;

class JudgeTest {

    private Club club;
    private Season season;
    private Event event;
    private Person person;
    private Judge judge;
    private PersonSignup personSignup;

    @BeforeEach
    public void initObjects() {
        club = new Club();
        //club.createPerson("Martin", true, new ArrayList<>(),new ArrayList<>(),new ArrayList<>(),"1996-12-12", LocalDate.now().toString(),LocalDate.now().toString(),false,false,false,true,"", new ArrayList<>(), new ArrayList<>());
        //club.createSeason();
        season = new Season(club, LocalDateTime.now(), LocalDateTime.now().plusMonths(1));
        event = new Event("Dancup", "9000 Aalborg, Kalderupvej 21", LocalDateTime.now().plusDays(3), LocalDate.now().plusDays(3), season, 6, 6);
        person = new Person().setName("Martin");
        judge = new Judge(person, true);
        personSignup = new PersonSignup(judge, event);
        event.addPersonSignup(personSignup);
        person.addPersonSignup(personSignup);

    }


    @Test
    void isGymnast() {
        assertFalse(judge.isGymnast());
    }

    @Test
    void isCoach() {
        assertFalse(judge.isCoach());
    }

    @Test
    void isJudge() {
        assertTrue(judge.isJudge());
    }

    @Test
    void getPerson() {
        assertEquals(person, judge.getPerson());
    }

    @Test
    void isActive() {
        assertTrue(judge.isActive());
    }

    @Test
    void setActive() {

        assertTrue(event.getPersonSignupList().contains(personSignup));
        assertTrue(person.getPersonSignupList().contains(personSignup));

        judge.setActive(false);

        assertFalse(judge.isActive());
        assertFalse(event.getPersonSignupList().contains(personSignup));
        assertFalse(person.getPersonSignupList().contains(personSignup));

    }

    @Test
    void equals01() {
        Judge judgeTemp = new Judge(new Person().setName("Martin"), true);
        assertEquals(judgeTemp, judge);
    }

    @Test
    void equals02() {
        Judge judgeTemp = new Judge(new Person().setName("Peter"), true);
        assertNotEquals(judgeTemp, judge);

    }

    @Test
    void hashcode(){
        assertEquals(Objects.hash(person,true),judge.hashCode());
    }


}
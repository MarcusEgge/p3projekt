package Model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static org.junit.jupiter.api.Assertions.*;

class EventTest {

    private Club club;
    private String eventName, eventAddress;
    private LocalDateTime eventStartDate;
    private LocalDate eventEndDate;
    private Season season;
    private Event event1;
    private Team team;
    private Person person;
    private List<TeamAssignment> teamAssignmentsList;
    private Gymnast gymnast;
    private TeamSignup teamSignup;
    private PersonSignup personSignup;

    @BeforeEach
    void initObjects() {
        club = new Club();
        season = new Season(club, LocalDateTime.now(), LocalDateTime.now());
        eventStartDate = LocalDateTime.now().plusDays(10);
        eventEndDate = LocalDate.now().plusDays(10);
        eventName = "SpringDanmark";
        eventAddress = "9000 Aalborg, Kalderupvej 21";
        event1 = new Event(eventName, eventAddress, eventStartDate, eventEndDate, season, 2, 2);
        team = new Team("Rød");
        person = new Person().setName("Anders");
        teamAssignmentsList = new ArrayList<>();
        gymnast = new Gymnast(person, "121202", true);
        teamSignup = new TeamSignup(team, event1, true);
        personSignup = new PersonSignup(gymnast, teamSignup.getEvent());
        teamSignup.getPersonSignupList().add(personSignup);
    }

    @Test
    void updateState01() {
        assertTrue(event1.isActive());
    }

    @Test
    void updateState02() {
        event1.setEndDate(eventEndDate.minusMonths(1));
        event1.updateState();
        assertFalse(event1.isActive());
    }

    @Test
    void updatePersonSignups() {

    }

    @Test
    void updateTeamSignups() {

    }

    @Test
    void getName() {
        assertEquals(eventName, event1.getName());
    }

    @Test
    void setName() {
        String newName = "Dancup";
        event1.setName(newName);
        assertEquals(newName, event1.getName());
    }

    @Test
    void getAddress() {
        assertEquals(eventAddress, event1.getAddress());

    }

    @Test
    void setAddress() {
        String newAddress = "9000 Aalborg, Kalderupvej 42";
        event1.setAddress(newAddress);
        assertEquals(newAddress, event1.getAddress());

    }

    @Test
    void getCarList() {
        assertTrue(event1.getCarList().isEmpty());

    }

    @Test
    void getTeamSignupList() {
        assertTrue(event1.getTeamSignupList().isEmpty());

    }

    @Test
    void addTeamSignup() {
        event1.addTeamSignup(teamSignup);
        assertEquals(1, event1.getTeamSignupList().size());
    }

    @Test
    void deleteTeamSignup() {
        event1.addTeamSignup(teamSignup);
        event1.deleteTeamSignup(teamSignup);
        assertTrue(event1.getTeamSignupList().isEmpty());

    }

    @Test
    void getStartDate() {
        assertEquals(eventStartDate, event1.getStartDate());

    }

    @Test
    void setStartDate() {
        LocalDateTime newDate = LocalDateTime.now().plusMonths(1);
        event1.setStartDate(newDate);
        assertEquals(newDate, event1.getStartDate());
    }

    @Test
    void getEndDate() {
        assertEquals(eventEndDate, event1.getEndDate());

    }

    @Test
    void setEndDate() {
        LocalDate newDate = LocalDate.now().plusMonths(1);
        event1.setEndDate(newDate);
        assertEquals(newDate, event1.getEndDate());
    }

    @Test
    void getSeason() {
        assertEquals(season, event1.getSeason());

    }

    @Test
    void isActive() {
        assertTrue(event1.isActive());
    }

    @Test
    void addCar01() {

        event1.addCar();
        assertEquals(1, event1.getCarList().size());

    }

    @Test
    void addCar02() {

        event1.addCar(person);
        assertEquals(1, event1.getCarList().size());
        assertTrue(event1.getCarList().get(0).hasPerson(person));
    }

    @Test
    void addCar03() {
        Car car = new Car(event1, person);

        event1.addCar(car);
        assertEquals(1, event1.getCarList().size());
        assertTrue(event1.getCarList().get(0).hasPerson(person));
    }

    @Test
    void addPersonSignup() {
        event1.addPersonSignup(personSignup);
        assertEquals(personSignup, event1.getPersonSignupList().get(0));
    }

    @Test
    void getPersonSignupList() {
        event1.addPersonSignup(personSignup);
        assertEquals(1, event1.getPersonSignupList().size());

    }

    @Test
    void deletePersonSignup() {
        event1.addPersonSignup(personSignup);
        assertEquals(personSignup, event1.getPersonSignupList().get(0));
        event1.deletePersonSignup(personSignup);
        assertTrue(event1.getPersonSignupList().isEmpty());
    }

    @Test
    void getPersonsSignedUp01() {
        assertTrue(event1.getPersonsSignedUp().isEmpty());
        event1.addPersonSignup(personSignup);
        assertEquals(person, event1.getPersonsSignedUp().get(0));
    }

    @Test
    void getPersonsSignedUp02() {
        Coach coach = new Coach(person, true);
        Judge judge = new Judge(person, true);

        PersonSignup personSignupCoach = new PersonSignup(coach, event1);
        PersonSignup personSignupJudge = new PersonSignup(judge, event1);

        event1.addPersonSignup(personSignup);
        event1.addPersonSignup(personSignupCoach);
        event1.addPersonSignup(personSignupJudge);

        assertEquals(1, event1.getPersonsSignedUp().size());
        assertEquals(person,event1.getPersonsSignedUp().get(0));
    }


    @Test
    void delete01() {
        event1.addPersonSignup(personSignup);
        person.addPersonSignup(personSignup);

        event1.addTeamSignup(teamSignup);
        team.addTeamSignup(teamSignup);

        event1.addCar(person);

        assertEquals(1, person.getPersonSignupList().size());
        assertEquals(1, team.getTeamSignupList().size());
        assertEquals(1, person.getCarAssignmentList().size());
        assertEquals(1, event1.getPersonSignupList().size());
        assertEquals(1, event1.getTeamSignupList().size());
        assertEquals(1, event1.getCarList().size());

        event1.delete();

        assertEquals(1, event1.getPersonSignupList().size());
        assertEquals(1, event1.getTeamSignupList().size());
        assertEquals(1, event1.getCarList().size());

        assertTrue(person.getPersonSignupList().isEmpty());
        assertTrue(team.getTeamSignupList().isEmpty());
        assertTrue(person.getCarAssignmentList().isEmpty());

    }

    @Test
    void delete02() {
        Coach coach = new Coach(person, true);
        Judge judge = new Judge(person, true);
        PersonSignup personSignup2 = new PersonSignup(coach, event1);
        PersonSignup personSignup3 = new PersonSignup(judge, event1);

        event1.addPersonSignup(personSignup);
        event1.addPersonSignup(personSignup2);
        event1.addPersonSignup(personSignup3);

        person.addPersonSignup(personSignup);
        person.addPersonSignup(personSignup2);
        person.addPersonSignup(personSignup3);

        assertEquals(3, person.getPersonSignupList().size());
        assertEquals(3, event1.getPersonSignupList().size());

        event1.delete();

        assertEquals(3, event1.getPersonSignupList().size());
        assertTrue(person.getPersonSignupList().isEmpty());
    }

    @Test
    void delete03() {
        Team blueTeam = new Team("blue");
        Team greenTeam = new Team("green");
        TeamSignup blueTeamSignup = new TeamSignup(blueTeam, event1, true);
        TeamSignup greenTeamSignup = new TeamSignup(greenTeam, event1, true);

        event1.addTeamSignup(teamSignup);
        event1.addTeamSignup(blueTeamSignup);
        event1.addTeamSignup(greenTeamSignup);

        team.addTeamSignup(teamSignup);
        blueTeam.addTeamSignup(blueTeamSignup);
        greenTeam.addTeamSignup(greenTeamSignup);

        assertEquals(1, team.getTeamSignupList().size());
        assertEquals(1, blueTeam.getTeamSignupList().size());
        assertEquals(1, greenTeam.getTeamSignupList().size());
        assertEquals(3, event1.getTeamSignupList().size());

        event1.delete();

        assertEquals(3, event1.getTeamSignupList().size());
        assertTrue(team.getTeamSignupList().isEmpty());
        assertTrue(blueTeam.getTeamSignupList().isEmpty());
        assertTrue(greenTeam.getTeamSignupList().isEmpty());

    }

    @Test
    void delete04() {
        Person personMartin = new Person().setName("Martin");
        Person personPeter = new Person().setName("Peter");

        event1.addCar(person);
        event1.addCar(personMartin);
        event1.addCar(personPeter);

        assertEquals(1, person.getCarAssignmentList().size());
        assertEquals(1, personMartin.getCarAssignmentList().size());
        assertEquals(1, personPeter.getCarAssignmentList().size());
        assertEquals(3, event1.getCarList().size());

        event1.delete();

        assertEquals(3, event1.getCarList().size());
        assertTrue(person.getCarAssignmentList().isEmpty());
        assertTrue(personMartin.getCarAssignmentList().isEmpty());
        assertTrue(personPeter.getCarAssignmentList().isEmpty());


    }

    //TODO: NOT DONE METHOD Mangler at blive lavet færdig og skal testes for at de placeres tilfædligt?

    @Test
    void distributePassengers() {

    }

    @Test
    void clearCarsForDrivers() {
        event1.addCar(person);
        event1.addCar(new Person().setName("Peter"));

        event1.clearCarsForDrivers();

        assertEquals(2, event1.getCarList().size());
        assertEquals(4, event1.getCarList().get(0).getAvailableSeats());
        assertEquals(4, event1.getCarList().get(1).getAvailableSeats());

    }

    //TODO: Skal den stadig bruges
    @Test
    void daysTillDeadline() {
        event1.setStartDate(LocalDateTime.now().plusDays(22));

        assertEquals(1,event1.daysTillDeadline());
    }

    //TODO: NOT DONE METHOD Har lavet om i den orginale funktion så den tager en liste med paremeter.
    @Test
    void correctBasicData() {
        List<Object> list = new ArrayList();
        list.add("Name");
        list.add("Adresse");
        list.add(LocalDateTime.now());
        list.add(LocalDate.now());
        list.add(season);
        event1.correctBasicData();
    }


    @Test
    void requirementStatus01() {
        assertFalse(event1.requirementStatus("Coach"));
    }


    @Test
    void requirementStatus02() {
        assertFalse(event1.requirementStatus("Judge"));

    }

    @Test
    void requirementStatus03() {
        Person personMartin = new Person().setName("Martin");
        Coach coachMartin = new Coach(personMartin, true);
        Coach coach = new Coach(person,true);

        PersonSignup personSignupMartin = new PersonSignup(coachMartin, event1);
        personSignup = new PersonSignup(coach,event1);

        personSignupMartin.setSignedUp(PersonSignupState.SIGNED_UP);
        personSignup.setSignedUp(PersonSignupState.SIGNED_UP);

        event1.addPersonSignup(personSignupMartin);
        event1.addPersonSignup(personSignup);

        assertTrue(event1.requirementStatus("Coach"));
    }

    @Test
    void requirementStatus04() {
        Person personMartin = new Person().setName("Martin");
        Coach coachMartin = new Coach(personMartin, true);
        Coach coach = new Coach(person,true);

        PersonSignup personSignupMartin = new PersonSignup(coachMartin, event1);
        personSignup = new PersonSignup(coach,event1);

        event1.addPersonSignup(personSignupMartin);
        event1.addPersonSignup(personSignup);

        assertFalse(event1.requirementStatus("Coach"));
    }

    @Test
    void requirementStatus05() {
        Person personMartin = new Person().setName("Martin");
        Judge judgeMartin = new Judge(personMartin, true);
        Judge judge = new Judge(person,true);

        PersonSignup personSignupMartin = new PersonSignup(judgeMartin, event1);
        personSignup = new PersonSignup(judge,event1);

        personSignupMartin.setSignedUp(PersonSignupState.SIGNED_UP);
        personSignup.setSignedUp(PersonSignupState.SIGNED_UP);

        event1.addPersonSignup(personSignupMartin);
        event1.addPersonSignup(personSignup);

        assertTrue(event1.requirementStatus("Judge"));
    }

    @Test
    void requirementStatus06() {
        Person personMartin = new Person().setName("Martin");
        Judge judgeMartin = new Judge(personMartin, true);
        Judge judge = new Judge(person,true);

        PersonSignup personSignupMartin = new PersonSignup(judgeMartin, event1);
        personSignup = new PersonSignup(judge,event1);

        event1.addPersonSignup(personSignupMartin);
        event1.addPersonSignup(personSignup);

        assertFalse(event1.requirementStatus("Judge"));
    }

    @Test
    void generatePersonSignupsFromList01() {
        List<Person> personList = new ArrayList<>();
        List<String> teamNameList = new ArrayList<>();
        person.setRole(gymnast,Person.GYMNAST_INDEX);
        personList.add(person);

        Team teamTalent = new Team("Talent");
        Team teamElite = new Team("Elite");

        teamTalent.generateTeamAssignments(personList);
        teamElite.generateTeamAssignments(personList);

        teamNameList.add(teamElite.getName());
        teamNameList.add(teamTalent.getName());

        gymnast.updateTeamAssignments(teamNameList);

        event1.generatePersonSignUpsFromList(personList);

        assertEquals(2,gymnast.getTeamAssignmentList().size());
        assertEquals(1,event1.getPersonSignupList().size());
        assertEquals(2,event1.getTeamSignupList().size());


        //giv den en liste som kun består af gymnast som den skal genererer.
    }

    @Test
    void generatePersonSignupsFromList02() {
        List<Person> personList = new ArrayList<>();

        Person personMartin = new Person().setName("Martin");
        Coach coach = new Coach(personMartin,true);
        personMartin.setRole(coach,Person.COACH_INDEX);

        Person personPeter = new Person().setName("Peter");
        Judge judge = new Judge(personPeter, true);
        personPeter.setRole(judge,Person.JUDGE_INDEX);


        personList.add(personMartin);
        personList.add(personPeter);

        event1.generatePersonSignUpsFromList(personList);

        assertEquals(2, event1.getPersonSignupList().size());


    }

    @Test
    void generatePersonSignupsFromPerson() {

        //behøves ikke, da den bruges af generatePersonsignupsfromlist

    }
    //Hvad med gymnasts?Hvad er iden med denne funktion hvis en del af hvad den skal allerede ligger i generate team signups?

    @Test
    void equals1() {
        Event testEvent = new Event("SpringDanmark", "9000 Aalborg, Kalderupvej 21", eventStartDate, eventEndDate, season, 6, 6);
        assertTrue(testEvent.equals(event1));
    }

    @Test
    void equals2() {
        Event testEvent = new Event("SpringNorge", eventAddress, eventStartDate, eventEndDate, season, 6, 6);
        assertFalse(testEvent.equals(event1));
    }

    @Test
    void equals3() {
        Event testEvent = new Event(eventName, "9000 Aalborg, Kalderupvej 24", eventStartDate, eventEndDate, season, 6, 6);
        assertFalse(testEvent.equals(event1));
    }

    @Test
    void equals4() {
        LocalDateTime startDate = eventStartDate.minusDays(2);
        Event testEvent = new Event(eventName, eventAddress, startDate, eventEndDate, season, 6, 6);
        assertFalse(testEvent.equals(event1));
    }

    @Test
    void equals5() {
        LocalDate endDate = eventEndDate.plusDays(2);
        Event testEvent = new Event("SpringDanmark", "9000 Aalborg, Kalderupvej 21", eventStartDate, endDate, season, 6, 6);
        assertFalse(testEvent.equals(event1));
    }

    @Test
    void equals6() {
        LocalDateTime startDate = LocalDateTime.now();
        LocalDate endDate = LocalDate.now();
        Event testEvent = new Event("SpringDanmark", "9000 Aalborg, Kalderupvej 21", startDate, endDate, new Season(club, LocalDateTime.now().minusYears(20), LocalDateTime.now()), 6, 6);
        assertFalse(testEvent.equals(event1));
    }


    @Test
    void hashcode() {
        assertEquals(Objects.hash(eventName, eventAddress, eventStartDate, eventEndDate, season), event1.hashCode());

    }

    @Test
    void compareTo1() {
        Event testEvent = new Event(eventName, eventAddress, eventStartDate, eventEndDate, season, 6, 6);
        assertTrue(testEvent.compareTo(event1) == 0);
    }

    @Test
    void compareTo2() {
        Event testEvent = new Event(eventName, eventAddress, eventStartDate, eventEndDate.minusDays(20), season, 6, 6);
        assertTrue(0 < testEvent.compareTo(event1));
    }

    //Skal de her to ikke vendes om så man får en højrer tal jo nyere datoen er?
    @Test
    void compareTo3() {
        LocalDateTime startDate = eventStartDate.minusDays(7);
        Event testEvent = new Event(eventName, eventAddress, startDate, eventEndDate, season, 6, 6);
        assertTrue(testEvent.compareTo(event1) < 0);
    }

    @Test
    void compareTo4() {
        LocalDateTime startDate = eventStartDate.plusDays(7);
        Event testEvent = new Event("SpringDanmark", "9000 Aalborg, Kalderupvej 21", startDate, eventEndDate, season, 6, 6);
        assertTrue(testEvent.compareTo(event1) > 0);
    }

    @Test
    void compareTo5() {
        Event testEvent = new Event("AndetNavn", eventAddress, eventStartDate, eventEndDate, season, 6, 6);
        assertTrue(testEvent.compareTo(event1) < 0);
    }

    @Test
    void compareTo6() {
        Event testEvent = new Event("ÅndetNavn", eventAddress, eventStartDate, eventEndDate, season, 6, 6);
        assertTrue(testEvent.compareTo(event1) > 0);
    }

    @Test
    void getID() {
        assertEquals(0, event1.getID());

    }
}
package Model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.*;

class CarAssignmentTest {
    private Car car;
    private Club club;
    private Season season;
    private Event event;
    private Person driver;
    private CarAssignment carAssignment, carAssignment2;

    @BeforeEach
    void initObjects() {
        club = new Club();
        season = new Season(club, LocalDateTime.now(), LocalDateTime.now());
        event = new Event("Dancup", "9000 Aalborg, Kalderupvej 42", LocalDateTime.now(), LocalDate.now(), season, 6, 6);
        driver = new Person();
        car = new Car(event);
        carAssignment = new CarAssignment(true, driver, car);
        carAssignment2 = new CarAssignment(false, driver, car);

    }

    @Test
    void getCar() {
        assertEquals(car, carAssignment.getCar());
    }

    @Test
    void delete() {
        carAssignment.deleteFromCar();
        assertEquals(4,car.getAvailableSeats());
    }

    @Test
    void isDriver() {
        assertEquals(true, carAssignment.isDriver());
    }

    @Test
    void equals() {
        assertEquals(carAssignment,carAssignment2);
    }
}
package Model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class ClubTest {

    private Person person;
    private Club club;
    private Team team;
    private Gymnast gymnast;
    private List<TeamAssignment> teamAssignmentsList;
    @BeforeEach
    public void initObjects(){
        person = new Person();
        club= new Club();
        teamAssignmentsList= new ArrayList<>();
        gymnast = new Gymnast(person,"12324", true);
        team = new Team("Ræd");
        club.createTeam("red");
    }

    @Test
    void getTeamList() {
        assertEquals(1,club.getTeamList().size());


        assertTrue(club.getTeamList().contains(new Team("red")));
    }

    //Der bliver ikke tjekket om hvis en person er gymnast, judge eller coach.
    //Der mangler en måde i person at tjekke ens køn
    //Der manger en måde i person at få Lister
    //Tester også getAllMembers
    @Test
    void createPerson() {
        List<String> emailList = new ArrayList<>();
        emailList.add("email@gmail.com");

        List<String> adresseList = new ArrayList<>();
        adresseList.add("9000 Aalbog, nederengade 21");

        List<String> phoneList = new ArrayList<>();
        phoneList.add("2692929");

        List<String> teamListGymnast = new ArrayList<>();
        teamListGymnast.add("red");

        List<String>  teamListCoach = new ArrayList<>();


        club.createPerson("Anders",true,emailList,adresseList,phoneList,"1998-11-11 10:10","1198-10-11 10:10",
                "2017-10-11 10:10",true,true,false,false,"2121",teamListGymnast,teamListCoach);
        assertEquals("Anders",club.getAllMembers().get(0).getName());
        //assertTrue(club.getMemberList().getAllMembers().get(0).isMale);
        //assertEquals(emailList,club.getMemberList().getAllMembers().get(0).getEmailList);
        //assertEquals(adresseList,club.getMemberList().getAllMembers().get(0).getAdresseList);
        //assertEquals(phoneList,club.getMemberList().getAllMembers().get(0).getPhoneList);
        assertEquals("1998-11-11",club.getAllMembers().get(0).getBirthday().toString());
        //assertEquals("1198/10/11 10:10:10",club.getMemberList().getAllMembers().get(0).getRegistrationDate);
        //assertEquals("2017/10/11 10:10:10",club.getMemberList().getAllMembers().get(0).getSignUpDate);
        //assertTrue(club.getMemberList().getAllMembers().get(0).getPhotoConsent);
        assertTrue(club.getAllMembers().get(0).isGymnast());
        assertFalse(club.getAllMembers().get(0).isCoach());
        assertFalse(club.getAllMembers().get(0).isJudge());
        //assertEquals("2121",club.getMemberList().getAllMembers().get(0).getLicense);
        Gymnast gymnast = (Gymnast) club.getAllMembers().get(0).getRole(Person.GYMNAST_INDEX);
        assertEquals(club.getTeamList().get(0),gymnast.getTeamAssignmentList().get(0).getTeam());
        assertTrue(club.getAllMembers().get(0).getRole(Person.COACH_INDEX)==null);
    }

    //Person get all team assignments does not check if the lists are empty before adding them
    //The deletePerson does not check if the person even exsist in the memberList. Just assumes it does.
    @Test
    void deletePerson() {
        List<String> emailList = new ArrayList<>();
        emailList.add("email@gmail.com");

        List<String> adresseList = new ArrayList<>();
        adresseList.add("9000 Aalbog, nederengade 21");

        List<String> phoneList = new ArrayList<>();
        phoneList.add("2692929");

        List<String> teamListGymnast = new ArrayList<>();
        teamListGymnast.add("red");

        List<String>  teamListCoach = new ArrayList<>();


        club.createPerson("Anders",true,emailList,adresseList,phoneList,"1998-11-11 10:10","1198-10-11 10:10",
                "2017-10-11 10:10",true,true,false,
                false,"2121",teamListGymnast,teamListCoach);
        assertFalse(club.getAllMembers().isEmpty());
        club.deletePerson(club.getAllMembers().get(0));
        assertTrue(club.getAllMembers().isEmpty());
    }

    //Der mangler noget i club til at se seasons
    @Test
    void createSeason() {
        //assertTrue(club.getSeasons.isEmpty())
        club.createSeason();
        //assertFalse(club.getSeasons.isEmpty());
    }

    //Mangler i team en måde at få creation date
    @Test
    void createTeam() {
        assertEquals(1, club.getTeamList().size());
        assertEquals("red",club.getTeamList().get(0).getName());
        //assertEquals("2012-10-10",club.getTeamList().get(0).getCreationDate.toString());
    }

    @Test
    void editTeam1() {
        club.editTeam("red","Blue", new ArrayList<>(), new ArrayList<>());
        assertEquals("Blue",club.getTeamList().get(0).getName());
    }
    //De to næste skal fange exceptions der ikke er lavet endnu
    @Test
    void editTeam2() {

        /*try{
            club.editTeam("red","Blue");
        }catch (Exception e){}*/
    }
    @Test
    void editTeam3() {
        club.createTeam("Blue");

        /*try {
            club.editTeam("red","Blue");
        }catch (Exception e){}*/
    }

    @Test
    void deleteTeam1() {
        club.createTeam("Blue");
        club.deleteTeam(club.viewTeam("red"));

        assertEquals("Blue",club.getTeamList().get(0).getName());
    }

    //Manlger at blive lavet en exception hvis holdet ikke exsister
    @Test
    void deleteTeam2() {
        List<String> names = new ArrayList<>();
        Team team1 = new Team("Blå");

        /*
        try{
            club.deleteTeam(team1);
        }catch (Exception e){}
          */
        assertEquals("red",club.getTeamList().get(0).getName());
    }

    //Igen create person laver folk til roler selvom de ikke er
    @Test
    void viewGymnasts() {
        List<String> emailList = new ArrayList<>();
        emailList.add("email@gmail.com");

        List<String> adresseList = new ArrayList<>();
        adresseList.add("9000 Aalbog, nederengade 21");

        List<String> phoneList = new ArrayList<>();
        phoneList.add("2692929");

        List<String> teamListGymnast = new ArrayList<>();
        teamListGymnast.add("red");

        List<String>  teamListCoach = new ArrayList<>();


        club.createPerson("Anders",true,emailList,adresseList,phoneList,"1998-11-11 10:10","1198-10-11 10:10",
                "2017-10-11 10:10",true,true,false,
                false,"2121",teamListGymnast,teamListCoach);
        club.createPerson("Kenny",true,emailList,adresseList,phoneList,"1998-11-11 10:10","1198-10-11 10:10",
                "2017-10-11 10:10",true,false,true,
                false,"2121",teamListGymnast,teamListCoach);
        club.createPerson("Kyle",true,emailList,adresseList,phoneList,"1998-11-11 10:10","1198-10-11 10:10",
                "2017-10-11 10:10",true,false,false,
                true,"2121",teamListGymnast,teamListCoach);
        club.createPerson("Kannie",true,emailList,adresseList,phoneList,"1998-11-11 10:10","1198-10-11 10:10",
                "2017-10-11 10:10",true,true,true,
                true,"2121",teamListGymnast,teamListCoach);
        //assertEquals(2,club.viewGymnasts().size());
        assertEquals("Anders",club.viewGymnasts().get(0).getName());
    }

    @Test
    void viewCoaches() {
        List<String> emailList = new ArrayList<>();
        emailList.add("email@gmail.com");

        List<String> adresseList = new ArrayList<>();
        adresseList.add("9000 Aalbog, nederengade 21");

        List<String> phoneList = new ArrayList<>();
        phoneList.add("2692929");

        List<String> teamListGymnast = new ArrayList<>();
        teamListGymnast.add("red");

        List<String>  teamListCoach = new ArrayList<>();


        club.createPerson("Anders",true,emailList,adresseList,phoneList,"1998-11-11 10:10","1198-10-11 10:10",
                "2017-10-11 10:10",true,true,false,
                false,"2121",teamListGymnast,teamListCoach);
        club.createPerson("Kenny",true,emailList,adresseList,phoneList,"1998-11-11 10:10","1198-10-11 10:10",
                "2017-10-11 10:10",true,false,true,
                false,"2121",teamListGymnast,teamListCoach);
        club.createPerson("Kyle",true,emailList,adresseList,phoneList,"1998-11-11 10:10","1198-10-11 10:10",
                "2017-10-11 10:10",true,false,false,
                true,"2121",teamListGymnast,teamListCoach);
        club.createPerson("Kannie",true,emailList,adresseList,phoneList,"1998-11-11 10:10","1198-10-11 10:10",
                "2017-10-11 10:10",true,true,true,
                true,"2121",teamListGymnast,teamListCoach);
        //assertEquals(2,club.viewCoaches().size());
//        assertEquals("Kenny",club.viewCoaches().get(0).getName());
    }

    @Test
    void viewJudges() {
        List<String> emailList = new ArrayList<>();
        emailList.add("email@gmail.com");
        emailList.add(null);

        List<String> adresseList = new ArrayList<>();
        adresseList.add("9000 Aalbog, nederengade 21");
        adresseList.add(null);

        List<String> phoneList = new ArrayList<>();
        phoneList.add("2692929");
        phoneList.add(null);

        List<String> teamListGymnast = new ArrayList<>();
        teamListGymnast.add("red");

        List<String>  teamListCoach = new ArrayList<>();

        club.createPerson("Anders",true,emailList,adresseList,phoneList,"1998-11-11 10:10","1198-10-11 10:10",
                "2017-10-11 10:10",true,true,false,
                false,"2121",teamListGymnast,teamListCoach);
        club.createPerson("Kenny",true,emailList,adresseList,phoneList,"1998-11-11 10:10","1198-10-11 10:10",
                "2017-10-11 10:10",true,false,true,
                false,"2121",teamListGymnast,teamListCoach);
        club.createPerson("Kyle",true,emailList,adresseList,phoneList,"1998-11-11 10:10","1198-10-11 10:10",
                "2017-10-11 10:10",true,false,false,
                true,"2121",teamListGymnast,teamListCoach);
        club.createPerson("Kannie",true,emailList,adresseList,phoneList,"1998-11-11 10:10","1198-10-11 10:10",
                "2017-10-11 10:10",true,true,true,
                true,"2121",teamListGymnast,teamListCoach);
        //assertEquals(2,club.viewJudges().size());
 //       assertEquals("Kyle",club.viewJudges().get(0).getName());
    }

    @Test
    void viewPerson() {
        List<String> emailList = new ArrayList<>();
        emailList.add("email@gmail.com");

        List<String> adresseList = new ArrayList<>();
        adresseList.add("9000 Aalbog, nederengade 21");

        List<String> phoneList = new ArrayList<>();
        phoneList.add("2692929");

        List<String> teamListGymnast = new ArrayList<>();
        teamListGymnast.add("red");

        List<String>  teamListCoach = new ArrayList<>();


        club.createPerson("Anders",true,emailList,adresseList,phoneList,"1998-11-11 10:10","1198-10-11 10:10",
                "2017-10-11 10:10",true,true,false,
                false,"2121",teamListGymnast,teamListCoach);
        assertEquals("Anders",club.viewPerson("Anders").getName());
    }
//Skal view persons ikke bare retunere en liste over alle personerner? Ellers gør den bare det samme som de overstående
    @Test
    void viewPersons() {
        List<String> emailList = new ArrayList<>();
        emailList.add("email@gmail.com");

        List<String> adresseList = new ArrayList<>();
        adresseList.add("9000 Aalbog, nederengade 21");

        List<String> phoneList = new ArrayList<>();
        phoneList.add("2692929");

        List<String> teamListGymnast = new ArrayList<>();
        teamListGymnast.add("red");

        List<String>  teamListCoach = new ArrayList<>();


        club.createPerson("Anders",true,emailList,adresseList,phoneList,"1998-11-11 10:10","1198-10-11 10:10",
                "2017-10-11 10:10",true,true,false,
                false,"2121",teamListGymnast,teamListCoach);
        //assertEquals("Anders",club.viewPersons().get(0).getName());
    }

    //skal have et bedre navn f.eks filterPersonsByTeam
    @Test
    void filterPersons1() {
        Coach coach = new Coach(person, true);

        club.viewTeam("red").addTeamAssignment(new TeamAssignment(club.viewTeam("red"), gymnast, true));
        club.viewTeam("red").addTeamAssignment(new TeamAssignment(club.viewTeam("red"),coach,true));
        assertEquals(1,club.filterPersons("Gymnast","red").size());
        assertEquals(gymnast.getPerson(), club.filterPersons("Gymnast", "red").get(0));
    }

    @Test
    void filterPersons2() {

        Coach coach = new Coach(person, true);

        club.viewTeam("red").addTeamAssignment(new TeamAssignment(club.viewTeam("red"),gymnast,true));
        club.viewTeam("red").addTeamAssignment(new TeamAssignment(club.viewTeam("red"),coach,true));
        assertEquals(1,club.filterPersons("Coach","red").size());
        assertEquals(gymnast.getPerson(), club.filterPersons("Coach", "red").get(0));
    }
    //mangler en exception at catche
    @Test
    void filterPersons3() {

        Coach coach = new Coach(person, true);

        club.viewTeam("red").addTeamAssignment(new TeamAssignment(club.viewTeam("red"),gymnast,true));
        club.viewTeam("red").addTeamAssignment(new TeamAssignment(club.viewTeam("red"),coach,true));
        /*try{
            club.filterPersons("Wrong","red");
        }catch (Exception e){}
        try{
            club.filterPersons("Gymnast","Wrong");
        }catch (Exception e){}*/
    }

    //Dårlig test, men ved ikke hvordan jeg ellers skal gøre det, eftersom jeg ved intet om den nye season
    @Test
    void viewSeason() {
        club.createSeason();
        assertFalse(club.filterSeasons().isEmpty());
    }

    //Den retuner bare en season liste, den fra og til vælger ikke seasoner
    @Test
    void filterSeasons() {
        club.createSeason();
        club.createSeason();
        assertEquals(2,club.filterSeasons().size());
    }

    @Test
    void viewTeam() {
        assertEquals("red",club.viewTeam("red").getName());
    }
}
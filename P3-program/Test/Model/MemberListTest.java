package Model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class MemberListTest {

    private Gymnast gymnast;
    private Person person;
    private List<TeamAssignment> teamAssignmentsList;
    private Club club;
    private MemberList memberList;
    @BeforeEach
    public void initObjects(){
        teamAssignmentsList= new ArrayList<>();
        person = new Person();
        person.setName("Anders");
        club= new Club();
        gymnast = new Gymnast(person,"121202",true);
        memberList = new MemberList();
    }

    @Test
    void getAllMembers1() {
        List<Person> list = new ArrayList();
        assertEquals(list,memberList.getAllMembers());
    }

    @Test
    void getAllMembers2() {
        List<Person> list = new ArrayList();
        person.setRole(gymnast,Person.GYMNAST_INDEX);
        list.add(person);
        memberList.addMember(person);
        assertEquals(list,memberList.getAllMembers());
    }

    @Test
    void addAndGetMember() {
        person.setRole(gymnast,Person.GYMNAST_INDEX);
        memberList.addMember(person);
        assertEquals(person,memberList.getMember(person.getName()));
        assertEquals(person,memberList.getMember(0));
    }

    @Test
    void getMember() {
        assertNull(memberList.getMember(person.getName()));
    }

    @Test
    void addMember1(){
        ArrayList<Integer> arrayList = new ArrayList<>();

        Coach coach = new Coach(person,true);
        Judge judge = new Judge(person, true);
        gymnast.setActive(true);
        coach.setActive(true);
        judge.setActive(true);
        person.setRole(coach, Person.COACH_INDEX);
        person.setRole(judge,Person.JUDGE_INDEX);
        memberList.addMember(person);
        memberList.addMember(person);
        memberList.addMember(person);
        Person person1 = new Person();
        person1.setRole(gymnast,Person.GYMNAST_INDEX);
        person1.setName("Danna");
        memberList.addMember(person1);
        Person person2 = new Person();
        person2.setRole(coach,Person.COACH_INDEX);
        person2.setRole(gymnast,Person.GYMNAST_INDEX);
        memberList.addMember(person2);
        memberList.addMember(person2);
        memberList.addMember(person2);
        person2.setName("opp");

        assertEquals(person1,memberList.getAllMembers().get(0));
        assertEquals(7,memberList.getAllMembers().size());
        assertEquals(4,memberList.getGymnastList().size());
    }
    @Test
    void addMember2(){
        Coach coach = new Coach(person,true);
        person.setRole(coach, Person.COACH_INDEX);
        memberList.addMember(person);
        assertEquals(person,memberList.getMember(person.getName()));
    }

    @Test
    void addMember3(){
        Judge judge = new Judge(person, true);
        person.setRole(judge,Person.JUDGE_INDEX);
        memberList.addMember(person);
        assertEquals(person,memberList.getMember(person.getName()));
    }

    //Den samme person kan addes flere gange og hvad hvis man skifter role mens man er i listen?
    // en af linjerne i switchen kan ikke testes eftersom den crasher hele programmet.
    @Test
    void addMember4(){
        Person person1 = new Person();
        Coach coach = new Coach(person1,true);
        Judge judge = new Judge(person1, true);
        person1.setRole(coach, Person.COACH_INDEX);
        person1.setRole(judge,Person.JUDGE_INDEX);
        person1.setName("Donald");
        memberList.addMember(person1);
        person1.setRole(gymnast,Person.GYMNAST_INDEX);
        memberList.addMember(person1);
        person.setRole(judge,Person.JUDGE_INDEX);
        person.setRole(gymnast, Person.GYMNAST_INDEX);
        memberList.addMember(person);
        assertEquals(person1,memberList.getMember(person1.getName()));
    }

    @Test
    void indexOf(){
        person.setName("Anders");
        person.setRole(gymnast,Person.GYMNAST_INDEX);
        memberList.addMember(person);
        assertEquals(-1,memberList.indexOf(null));
        assertEquals(0,memberList.indexOf(person.getName()));
    }

    @Test
    void getGymnastList() {

        person.setRole(gymnast,Person.GYMNAST_INDEX);
        memberList.addMember(person);

        assertEquals(person,memberList.getGymnastList().get(0));
    }

    @Test
    void getCoachList() {

        Coach coach = new Coach(person,true);
        person.setRole(coach,Person.COACH_INDEX);
        memberList.addMember(person);

        assertEquals(person,memberList.getCoachList().get(0));
    }

    @Test
    void getJudgeList() {
        Judge judge = new Judge(person, true);
        judge.setActive(true);
        person.setRole(judge,Person.JUDGE_INDEX);
        memberList.addMember(person);

        assertEquals(person,memberList.getJudgeList().get(0));
    }

    @Test
    void deleteMember1() {
        person.setRole(gymnast,Person.GYMNAST_INDEX);
        memberList.addMember(person);
        assertEquals(person,memberList.getAllMembers().get(0));
        memberList.deleteMember(person);
        assertFalse(memberList.getAllMembers().contains(person));
    }

    @Test
    void deleteMember2() {
        person.setRole(gymnast,Person.GYMNAST_INDEX);
        memberList.addMember(person);
        assertEquals(person,memberList.getAllMembers().get(0));
        memberList.deleteMember(person);
        assertFalse(memberList.getAllMembers().contains(person));
    }

    @Test
    void deleteMember3() {
        Judge judge = new Judge(person, true);
        judge.setActive(true);
        Person person1 = new Person();
        person1.setName("Anders");
        person1.setRole(judge,Person.JUDGE_INDEX);
        memberList.addMember(person1);
        assertEquals(person1,memberList.getAllMembers().get(0));
        memberList.deleteMember(person1);
        assertFalse(memberList.getAllMembers().contains(person));
    }


    @Test
    void deleteMember4() {
        assertNull(memberList.deleteMember(person));
    }

    @Test
    void deleteMember5() {
        Person person1 = new Person();
        Coach coach = new Coach(person1, true);
        Judge judge = new Judge(person1, true);
        judge.setActive(true);
        person1.setName("Anders");
        person1.setRole(coach,Person.COACH_INDEX);
        memberList.addMember(person1);
        memberList.deleteMember(person1);
        person1.setRole(judge,Person.JUDGE_INDEX);
        memberList.addMember(person1);
        assertEquals(person1,memberList.getAllMembers().get(0));
        memberList.deleteMember(person1);
        assertFalse(memberList.getAllMembers().contains(person));
        person1.setRole(gymnast,Person.GYMNAST_INDEX);
        memberList.addMember(person1);
        memberList.deleteMember(person1);
    }

    //Can ikke finde ud af at komme ind i de tre sidste
    @Test
    void deleteMember6() {
        Person person1 = new Person();
        Coach coach = new Coach(person1, true);
        Judge judge = new Judge(person1, true);
        judge.setActive(true);
        gymnast.setActive(true);
        person1.setName("Anders");
        person1.setRole(gymnast,Person.GYMNAST_INDEX);
        person1.setRole(judge,Person.JUDGE_INDEX);
        memberList.addMember(person1);
        assertEquals(person1,memberList.getAllMembers().get(0));
        memberList.deleteMember(person1);
        assertFalse(memberList.getAllMembers().contains(person));
        person.setRole(gymnast,Person.GYMNAST_INDEX);
        person.setRole(coach,Person.COACH_INDEX);
        memberList.addMember(person);
        memberList.deleteMember(person);
    }

    //GetIndex skal kigge på om rollerne er aktive
    @Test
    void updateMember(){
        Person person1 = new Person();
        Coach coach = new Coach(person1,true);
        Judge judge = new Judge(person1, true);
        person.setRole(gymnast,Person.GYMNAST_INDEX);
        person.setRole(judge,Person.JUDGE_INDEX);
        memberList.addMember(person);
        person1.setRole(coach,Person.COACH_INDEX);
        memberList.addMember(person1);
        assertEquals(person,memberList.getAllMembers().get(0));
        gymnast.setActive(false);
        memberList.updateMember(0);

        assertEquals(person1, memberList.getAllMembers().get(0));
    }




}
package Model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.sql.SQLOutput;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static org.junit.jupiter.api.Assertions.*;

class SeasonTest {
    private Club club;
    private Season season;
    private Event event;
    private LocalDateTime start, end, today;

    @BeforeEach
    void initObjects(){
        club = new Club();
        start = LocalDateTime.now().minusDays(10);
        end = LocalDateTime.now().plusDays(1);
        today = LocalDateTime.now();
        season = new Season(club, start, end);
        season.createEvent("Dancup","9000 Aalborg, Kalderupvej 21","2018-11-21 12:12","2018-11-21", 6, 6);
        event = new Event("Dancup","9000 Aalborg, Kalderupvej 21", LocalDateTime.of(2018,11,21,12,12),LocalDate.of(2018,11,21), season, 6, 6);

    }


    @Test
    void updateState_isActive01() {
        season.updateState();
        assertEquals(true, season.isActive());
    }

    @Test
    void updateState_isActive02() {
        season = new Season(club, start,end.minusDays(2));
        season.updateState();
        assertEquals(false, season.isActive());
    }

    @Test
    void getStartDate() {
        assertEquals(start, season.getStartDate());
    }

    @Test
    void getEndDate() {
        assertEquals(end,season.getEndDate());
    }

    @Test
    void viewEvents(){
        assertEquals(1,season.viewEvents().size());
        assertEquals("Dancup",season.viewEvents().get(0).getName());
    }

    @Test
    void createEvent01() {
        assertEquals(event,season.viewEvents().get(0));
    }

    @Test
    void createEvent02() {
        assertEquals(1,season.viewEvents().size());
    }

    @Test
    void editEvent(){
        LocalDateTime localDateTime =LocalDateTime.of(2200,9,15,12,15);
        LocalDate localDate = LocalDate.of(2201,12,9);
        Event newEvent = new Event("NewEvent","3891 Aalborg, urtyp 11", localDateTime,localDate,season, 6, 6);
        season.editEvent(season.viewEvents().get(0),newEvent);


        assertEquals("NewEvent",season.viewEvents().get(0).getName());
        assertEquals("3891 Aalborg, urtyp 11",season.viewEvents().get(0).getAddress());
        assertEquals(localDateTime,season.viewEvents().get(0).getStartDate());
        assertEquals(localDate,season.viewEvents().get(0).getEndDate());

    }

    @Test
    void deleteEvent() {
        season.deleteEvent(event);
        assertEquals(0, season.viewEvents().size());

    }

    @Test
    void getName() {
        assertEquals(start.getYear()+"/"+end.getYear(),season.getName());
    }

    @Test
    void distributeDrivers() {
        season.distributeDrivers();
        //TODO: Method not done
    }

    @Test
    void equals01() {
        assertTrue(new Season(club,today,today).equals(season));
    }

    @Test
    void equals02() {
        assertFalse(new Season(club,today,today.plusYears(1)).equals(season));
    }
    @Test
    void equals03() {
        assertTrue(season.equals(season));
    }
    @Test
    void equals04() {
        Season season1 = null;
        assertFalse(season.equals(season1));
    }

    @Test
    void compareTo01() {
        Season season2 = new Season(club, start.minusDays(20), end.plusDays(20));
        assertTrue( -1>= season.compareTo(season2));
    }


    @Test
    void compareTo02() {
        Season season2 = new Season(club, today.minusDays(1),end);
        assertTrue(1<= season.compareTo(season2));
    }

    @Test
    void compareTo03(){
        Season season2 = new Season(club, start, end);
        assertTrue(0 == season.compareTo(season2));
    }

    @Test
    void compareTo04(){
        Season season2 = new Season(club, start, end.minusDays(10));
        assertTrue(-1 == season.compareTo(season2));
    }

    @Test
    void compareTo05(){
        Season season2 = new Season(club, start, end.minusDays(10));
        assertTrue(1 == season2.compareTo(season));
    }

    @Test
    void hashCodeTest(){
        String name= season.getName();
        assertEquals(Objects.hash(name),season.hashCode());
    }
}
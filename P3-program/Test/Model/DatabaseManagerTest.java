package Model;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class DatabaseManagerTest {

    @Test
    void loadAllData() {
        Club club = new Club();
        DatabaseManager databaseManager = new DatabaseManager(club);
        MemberList memberList = new MemberList();
        databaseManager.loadAllData(club.viewSeasons(),memberList,club.getTeamList());
        assertTrue(memberList.getAllMembers().isEmpty());
        assertTrue(club.viewSeasons().isEmpty());
        assertTrue(club.getTeamList().isEmpty());
    }
}
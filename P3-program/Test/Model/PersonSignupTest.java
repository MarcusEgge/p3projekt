package Model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static org.junit.jupiter.api.Assertions.*;

class PersonSignupTest {

    private Gymnast gymnast;
    private Judge judge;
    private Season season;
    private Event event1;
    private Person person1,person2;
    private Club club;
    private Team team;
    private PersonSignup personSignup1, personSignup2;
    private TeamSignup teamSignup;
    private LocalDateTime today;

    @BeforeEach
    void initObjects() {
        today = LocalDateTime.now();
        person1 = new Person().setName("Martin");
        person2 = new Person().setName("Sigurd");
        club = new Club();
        team = new Team("Rød");
        gymnast = new Gymnast(person1, "121202", true);
        judge = new Judge(person2, true);
        season = new Season(club, today, today);
        event1 = new Event("SpringDanmark", "9000 Aalborg, Kalderupvej 21", LocalDateTime.now(), LocalDate.now(), season, 6, 6);
        teamSignup = new TeamSignup(team, event1, true);
        personSignup1 = new PersonSignup(gymnast, teamSignup.getEvent());
        personSignup2 = new PersonSignup(judge, event1);
        teamSignup.addPersonSignup(personSignup1);
        personSignup1.addTeamSignup(teamSignup);
        event1.addPersonSignup(personSignup1);

    }


    @Test
    void getPerson01() {
        assertEquals(person1, personSignup1.getPerson());
    }

    @Test
    void getPerson02() {
        assertEquals(person2, personSignup2.getPerson());
    }

    @Test
    void getRole01() {
        assertEquals(gymnast, personSignup1.getRole());
    }

    @Test
    void getRole02() {
        assertEquals(judge, personSignup2.getRole());
    }

    @Test
    void getEvent01() {
        assertEquals(event1, personSignup1.getEvent());
    }

    @Test
    void getEvent02() {
        assertEquals(event1, personSignup2.getEvent());
    }


    @Test
    void isGymnast01() {
        assertTrue(personSignup1.isGymnast());
    }

    @Test
    void isGymnast02() {
        assertFalse(personSignup2.isGymnast());
    }

    @Test
    void isCoach01() {
        assertFalse(personSignup1.isCoach());
    }

    @Test
    void isCoach02() {
        assertFalse(personSignup2.isCoach());
    }

    @Test
    void isJudge01() {
        assertFalse(personSignup1.isJudge());
    }

    @Test
    void isJudge02() {
        assertTrue(personSignup2.isJudge());
    }

    @Test
    void isSignedUp01(){
        assertEquals(PersonSignupState.NOT_SIGNED_UP,personSignup1.isSignedUp());
    }

    @Test
    void isSignedUp02(){
        personSignup2.setSignedUp(PersonSignupState.SIGNED_UP);
        assertEquals(PersonSignupState.SIGNED_UP,personSignup2.isSignedUp());
    }

    @Test
    void deleteFromEvent(){
        event1.addPersonSignup(personSignup2);
        assertEquals(2,event1.getPersonsSignedUp().size());
        personSignup2.deleteFromEvent();
        assertEquals(1,event1.getPersonsSignedUp().size());

    }


    @Test
    void deleteFromTeamSignup(){
        assertFalse(teamSignup.getPersonSignupList().isEmpty());
        personSignup1.deleteFromTeamSignup();
        assertTrue(teamSignup.getPersonSignupList().isEmpty());
    }

    @Test
    void getTeamSignupList(){
        assertEquals(1,personSignup1.getTeamSignupList().size());
    }

    @Test
    void addTeamSignuo(){
        personSignup2.addTeamSignup(teamSignup);
        assertTrue(personSignup2.getTeamSignupList().contains(teamSignup));
    }

    @Test
    void deleteFromPerson(){
        person1.addPersonSignup(personSignup1);
        assertTrue(person1.getPersonSignupList().contains(personSignup1));
        personSignup1.deleteFromPerson();
        assertFalse(person1.getPersonSignupList().contains(personSignup1));

    }

    @Test
    void equals01(){
        Event eventTemp = new Event("Dancup", "9000 Aalborg, Kalderupvej 21",LocalDateTime.now(),LocalDate.now(),season, 6,6);
        PersonSignup personSignupTemp = new PersonSignup(gymnast, eventTemp);

        assertNotEquals(personSignupTemp,personSignup1);
    }

    @Test
    void equals02(){
        PersonSignup personSignupTemp = new PersonSignup(gymnast, event1);

        assertEquals(personSignupTemp, personSignup1);

    }

    @Test
    void hashcode(){
        Person personTemp = new Person().setName("Martin");
        Gymnast gymnastTemp = new Gymnast(personTemp,"121202", true);

        assertEquals(Objects.hash(personTemp,gymnastTemp,event1), personSignup1.hashCode());
    }

}
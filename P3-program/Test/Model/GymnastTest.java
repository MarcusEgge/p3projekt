package Model;

import Model.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static org.junit.jupiter.api.Assertions.*;

class GymnastTest {

    private Club club;
    private Season season;
    private Event event;
    private Team team;
    private Person person;
    private Gymnast gymnast;
    private List<TeamAssignment> teamAssignmentsList;
    private TeamAssignment teamAssignment;
    private String license;

    @BeforeEach
    void initObjects() {
        club = new Club();
        season = new Season(club, LocalDateTime.now(), LocalDateTime.now().plusMonths(1));
        event = new Event("Dancup", "9000 Aalborg, Kalderupvej 21", LocalDateTime.now().plusDays(3), LocalDate.now().plusDays(3), season, 6, 6);
        team = new Team("red");
        person = new Person().setName("Martin");
        teamAssignmentsList = new ArrayList<>();
        license = "121202";
        gymnast = new Gymnast(person, license, true);
        teamAssignment = new TeamAssignment(team, gymnast, true);
        team.addTeamAssignment(teamAssignment);
    }

    @Test
    void getLicense() {
        assertEquals(license, gymnast.getLicense());
    }

    @Test
    void setLicense() {
        String licenseTemp = "121323";
        gymnast.setLicense(licenseTemp);
        assertEquals(licenseTemp, gymnast.getLicense());
    }

    //TODO: Exception test for setLicense()

    @Test
    void getTeamAssignmentList() {
        assertEquals(teamAssignmentsList, gymnast.getTeamAssignmentList());
    }

    @Test
    void addTeamAssignment() {
        gymnast.addTeamAssignment(teamAssignment);
        assertTrue(gymnast.getTeamAssignmentList().contains(teamAssignment));
    }

    //TODO: Exception test for addTeamAssignment()

    @Test
    void deleteTeamAssignment() {
        gymnast.addTeamAssignment(teamAssignment);
        assertEquals(1, gymnast.getTeamAssignmentList().size());

        gymnast.deleteTeamAssignment(teamAssignment);
        assertTrue(gymnast.getTeamAssignmentList().isEmpty());
    }

    @Test
    void isOnTeam1() {
        String blue = "Blue";
        String green = "Green";
        Team blueTeam = new Team(blue);
        Team greenTeam = new Team(green);
        gymnast.addTeamAssignment(new TeamAssignment(blueTeam, gymnast, false));
        gymnast.addTeamAssignment(new TeamAssignment(greenTeam, gymnast, false));

        assertFalse(gymnast.isOnTeam(team));
    }

    @Test
    void isOnTeam2() {
        gymnast.addTeamAssignment(teamAssignment);
        assertTrue(gymnast.isOnTeam(team));
    }

    @Test
    void isGymnast() {
        assertTrue(gymnast.isGymnast());
    }

    @Test
    void isCoach() {
        assertFalse(gymnast.isCoach());
    }

    @Test
    void isJudge() {
        assertFalse((gymnast.isJudge()));
    }

    @Test
    void getPerson() {
        assertEquals(person, gymnast.getPerson());
    }

    @Test
    void isActive() {
        assertTrue(gymnast.isActive());
    }

    @Test
    void setActive() {

        gymnast.addTeamAssignment(teamAssignment);
        assertEquals(1, gymnast.getTeamAssignmentList().size());
        assertEquals(1, team.getTeamAssignmentList().size());
        gymnast.setActive(false);
        assertEquals(0, team.getTeamAssignmentList().size());
        assertFalse(gymnast.isActive());
        assertTrue(gymnast.hasNoTeam());
    }

    @Test
    void updateTeamAssignments01() {
        //Lav nogle flere teamassignments og sætte dem ind i gymnast, hvor man den så kan genere auto fra teamassignment.
        //Lav dem isSignedup false og ændre dem til true
        String blue = "Blue";
        String green = "Green";
        Team blueTeam = new Team(blue);
        Team greenTeam = new Team(green);

        blueTeam.addTeamSignup(new TeamSignup(blueTeam,event,true));
        greenTeam.addTeamSignup(new TeamSignup(greenTeam,event,true));

        gymnast.addTeamAssignment(new TeamAssignment(blueTeam, gymnast, false));
        gymnast.addTeamAssignment(new TeamAssignment(greenTeam, gymnast, false));

        List<String> teamNameList = new ArrayList<>();
        teamNameList.add(blue);
        teamNameList.add(green);

        gymnast.updateTeamAssignments(teamNameList);

        assertEquals(1,event.getPersonSignupList().size());
        assertEquals(1,blueTeam.getTeamSignupList().size());
        assertEquals(1,greenTeam.getTeamSignupList().size());
        //assertEquals(1,gymnast.getPerson().getPersonSignupList().size());
        assertEquals(2,event.getPersonSignupList().get(0).getTeamSignupList().size());



    }

    @Test
    void updateTeamAssignments02() {
        //lav dem false og derefter ændre dem til true og så false igen, som fjerner forbindelsen mellem person signup og team sign up, men er stadig tilmeldt til event.
        String blue = "Blue";
        String green = "Green";
        Team blueTeam = new Team(blue);
        Team greenTeam = new Team(green);

        blueTeam.addTeamSignup(new TeamSignup(blueTeam,event,true));
        greenTeam.addTeamSignup(new TeamSignup(greenTeam,event,true));

        gymnast.addTeamAssignment(new TeamAssignment(blueTeam, gymnast, false));
        gymnast.addTeamAssignment(new TeamAssignment(greenTeam, gymnast, false));

        List<String> teamNameList = new ArrayList<>();
        teamNameList.add(blue);
        teamNameList.add(green);

        gymnast.updateTeamAssignments(teamNameList);
        gymnast.updateTeamAssignments(new ArrayList<>());

        assertEquals(1,event.getPersonSignupList().size());
        assertTrue(blueTeam.getTeamAssignmentList().isEmpty());
        assertTrue(greenTeam.getTeamAssignmentList().isEmpty());
        assertTrue(event.getPersonSignupList().get(0).getTeamSignupList().isEmpty());
    }

    @Test
    void hasNoTeam01() {
        String blue = "Blue";
        String green = "Green";
        Team blueTeam = new Team(blue);
        Team greenTeam = new Team(green);
        gymnast.addTeamAssignment(new TeamAssignment(blueTeam, gymnast, false));
        gymnast.addTeamAssignment(new TeamAssignment(greenTeam, gymnast, false));

        assertTrue(gymnast.hasNoTeam());
    }

    @Test
    void hasNoTeam02() {
        gymnast.addTeamAssignment(teamAssignment);
        assertFalse(gymnast.hasNoTeam());
    }

    @Test
    void equals01() {
        Gymnast gymnastTemp = new Gymnast(person, license, true);
        assertEquals(gymnastTemp, gymnast);
    }

    @Test
    void equals02() {
        Gymnast gymnastTemp = new Gymnast(new Person().setName("Peter"), "323212", true);
        assertNotEquals(gymnastTemp, gymnast);

    }

    @Test
    void hashcode() {
        assertEquals(Objects.hash(person, license, teamAssignmentsList, true), gymnast.hashCode());
    }
}
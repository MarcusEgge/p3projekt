package Model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.*;

class CarTest {
    private Car car, car2;
    private Club club;
    private Season season;
    private Event event;
    private Person driver, passenger1, passenger2;

    @BeforeEach
    void initObjects() {
        club = new Club();
        season = new Season(club, LocalDateTime.now(), LocalDateTime.now());
        event = new Event("Dancup", "9000 Aalborg, Kalderupvej 21", LocalDateTime.now(), LocalDate.now(), season, 6, 6);
        driver = new Person();
        passenger1 = new Person().setName("Martin");
        passenger2 = new Person().setName("Marcus");
        car = new Car(event, driver);
        car2 = new Car(event);
    }

    @Test
    void getAvailableSeats() {
        assertEquals(3, car.getAvailableSeats());
    }

    @Test
    void getEvent() {
        assertEquals(event, car2.getEvent());
    }

    @Test
    void addPerson01() {
        car.addPerson(false, passenger1);
        assertEquals(2, car.getAvailableSeats());
    }

    @Test
    void addPerson02() {
        car2.addPerson(true, passenger1);
        assertEquals(3, car2.getAvailableSeats());
    }
    @Test
    void addPerson03() {
        car2.addPerson(true, passenger1);
        car2.addPerson(false,passenger1);
        assertEquals(2, car2.getAvailableSeats());
    }

    @Test
    void deleteCarAssignment() {
        car.addPerson(false, passenger1);
        car.deleteCarAssignment(new CarAssignment(false, passenger2, car));
        car.deleteCarAssignment(new CarAssignment(false, passenger1, car));
        assertEquals(3, car.getAvailableSeats());

    }
}
package Model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class TeamSignupTest {

    private Club club;
    private TeamSignup teamSignup;
    private Team team;
    private Event event;
    private Season season;
    @BeforeEach
    public void initObjects(){
        club= new Club();
        season=new Season(club,LocalDateTime.now(),LocalDateTime.now());
        event = new Event("Dino","9000 aalborg, Gaden 2",LocalDateTime.now(),LocalDate.now(),season, 6, 6);
        team = new Team("Ræd");
        teamSignup = new TeamSignup(team,event, true);
    }

    @Test
    void getPersonSignupList1() {
        assertTrue(teamSignup.getPersonSignupList().isEmpty());
    }

    @Test
    void getAndAddPersonSignup() {
        Person person = new Person();
        Gymnast gymnast = new Gymnast(person,"1231",true);
        PersonSignup personSignup = new PersonSignup(gymnast,teamSignup.getEvent());
        teamSignup.addPersonSignup(personSignup);
        assertEquals(personSignup,teamSignup.getPersonSignupList().get(0));
    }


    @Test
    void deletePersonSignup() {
        Person person = new Person();
        Gymnast gymnast = new Gymnast(person,"1231", true);
        PersonSignup personSignup = new PersonSignup(gymnast, teamSignup.getEvent());
        teamSignup.addPersonSignup(personSignup);
        teamSignup.deletePersonSignup(personSignup);
        assertTrue(teamSignup.getPersonSignupList().isEmpty());
    }
//Mangler noget til at se teamsignups i event
    /*
    @Test
    void deleteFromEvent() {
        List<Team> teamList = new ArrayList<>();
        teamList.add(team);
        List<TeamAssignment> teamAssignmentList = new ArrayList<>();
        Person person = new Person();
        Gymnast gymnast = new Gymnast(person,"1231",teamAssignmentList);
        team.addTeamAssignment(new TeamAssignment(team,gymnast,true));
        event.generateTeamSignups(teamList);
        assertTrue(event.getTeamSignupList.get(0));
        event.getTeamSignupList.deleteFromEvent();
        assertTrue(event.getTeamsignupList.isEmpty());

    }
*/
    //Skal den ikke sætte teamSignup til hold løs?
    //Mangler noget til at få teamSignup i personSignup
    /*
    @Test
    void deleteFromPersonSignups() {
        Person person = new Person();
        List<TeamAssignment> teamAssignmentList = new ArrayList<>();
        Gymnast gymnast = new Gymnast(person,"1231",teamAssignmentList);
        PersonSignup personSignup = new PersonSignup(gymnast,teamSignup);
        teamSignup.addPersonSignup(personSignup);
        teamSignup.deleteFromPersonSignups();
        assertNull(teamSignup.getPersonSignupList().get(0).getTeamSignup);


    }*/

    @Test
    void getTeam() {
        assertEquals(team,teamSignup.getTeam());
    }

    @Test
    void isSignedUp() {
        assertTrue(teamSignup.isSignedUp());
    }


    @Test
    void setSignedUp() {
        teamSignup.setSignedUp(false);
        assertFalse(teamSignup.isSignedUp());
    }
}
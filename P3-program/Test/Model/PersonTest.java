package Model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class PersonTest {

    private Gymnast gymnast;
    private Season season;
    private Event event1;
    private Person person;
    private List<TeamAssignment> teamAssignmentsList;
    private Club club;
    private Team team;
    private PersonSignup personSignup;
    private TeamSignup teamSignup;
    private LocalDateTime today;

    @BeforeEach
    void initObjects(){
        today = LocalDateTime.now();
        teamAssignmentsList= new ArrayList<>();
        person = new Person();
        person.setName("Anders");
        club= new Club();
        team = new Team("Rød");
        gymnast = new Gymnast(person,"121202", true);
        season = new Season(club,today,today);
        event1 = new Event("SpringDanmark","9000 Aalborg, Kalderupvej 21", LocalDateTime.now(),LocalDate.now(),season, 6, 6);
        teamSignup = new TeamSignup(team, event1,true);
        personSignup =new PersonSignup(gymnast, event1);
    }

    @Test
    void setAndGetName() {
        assertEquals("Anders",person.getName());
        person.setName("Dunno");
        assertEquals("Dunno",person.getName());
    }

    //Mangler vel en getter til gender
    @Test
    void setGender() {
        person.setGender(true);
    }

    //Mangler også en getter til addressen
    @Test
    void setAddressList() {
        List<String> stringList = new ArrayList<>();
        stringList.add("9000 Aalborg");
        stringList.add("Nedergaden 2");
        person.setAddressList(stringList);
    }

    //Mangler også en getter til adressen
    @Test
    void setEmailList() {
        List<String> stringList = new ArrayList<>();
        stringList.add("200neder@gmail.com");
        stringList.add("Nedergaden@yahoo.dk");
        person.setEmailList(stringList);
    }

    @Test
    void setPhoneNumberList() {
        List<String> stringList = new ArrayList<>();
        stringList.add("29682898");
        stringList.add("65897845");
        person.setPhoneNumberList(stringList);
    }

    @Test
    void setAndGetBirthday() {
        assertEquals(null,person.getBirthday());
        LocalDate date = LocalDate.now();
        person.setBirthday(date);
        assertEquals(date, person.getBirthday());
    }


    //Mangler en getter
    @Test
    void setClubRegistrationDate() {
        LocalDate date = LocalDate.now();
        person.setClubRegistrationDate(date);
    }

    //Mangler en getter
    @Test
    void setClubSignupDate() {
        LocalDate date = LocalDate.now();
        person.setClubSignupDate(date);
    }

    //Set role ser ikke ud til at gøre hvad den skal.
    @Test
    void setAndGetRole() {
        assertEquals(null,person.getRole(Person.GYMNAST_INDEX));
        person.setRole(gymnast,Person.GYMNAST_INDEX);
        assertEquals(gymnast,person.getRole(Person.GYMNAST_INDEX));
    }

    //Catcher exception som ikke er lavet
    /*
    @Test
    void setRole1(){
        person.setRole(gymnast,9);
    }*/

    //Skal catche flere exceptions
    /*
    @Test
    void setRole2(){
        person.setRole(gymnast,0);
        person.setRole(null,0);
        person.setRole(null,9);
        assertEquals(gymnast,person.getRole(Person.GYMNAST_INDEX));
    }*/

    //Mangler en getter
    @Test
    void addPersonSignup() {
        person.addPersonSignup(personSignup);
    }

    //Mangler en getter
    @Test
    void setPhotoConsent() {
        person.setPhotoConsent(true);
    }

    @Test
    void isGymnast() {
        assertFalse(person.isGymnast());
        person.setRole(gymnast,Person.GYMNAST_INDEX);
        assertTrue(person.isGymnast());
    }

    @Test
    void isCoach() {
        assertFalse(person.isCoach());
        Coach coach = new Coach(person, true);
        person.setRole(coach,Person.COACH_INDEX);
        assertTrue(person.isCoach());
    }

    @Test
    void isJudge() {
        assertFalse(person.isJudge());
        Judge judge = new Judge(person, true);
        person.setRole(judge,Person.JUDGE_INDEX);
        assertTrue(person.isJudge());
    }

    //Mangler en getter til person signup og en måde at få adgang til de to andre lister.
    @Test
    void delete() {
        person.setRole(gymnast,Person.GYMNAST_INDEX);
        TeamAssignment teamAssignment = new TeamAssignment(team,gymnast,true);
        event1.addPersonSignup(personSignup);
        team.addTeamAssignment(teamAssignment);
        person.addPersonSignup(personSignup);
        event1.addCar(person);
        gymnast.addTeamAssignment(teamAssignment);

        person.delete();

        assertFalse(event1.getPersonsSignedUp().contains(person));
        assertFalse(team.getTeamAssignmentList().contains(teamAssignment));
        assertTrue(team.getTeamSignupList().isEmpty());
        assertEquals(4,event1.getCarList().get(0).getAvailableSeats());
    }

    //Mangler en getter til person signup
    @Test
    void removePersonSignup() {
        person.addPersonSignup(personSignup);

    }


    @Test
    void equals1() {
        Person person1 = new Person();
        person1.setName("Anders");
        assertTrue(person1.equals(person));
    }
    @Test
    void equals2() {
        Person person1 = new Person();
        person1.setName("donners");
        assertFalse(person1.equals(person));
    }
    @Test
    void equals3() {
        Person person1 = new Person();
        person1.setName("AAAnders");
        assertFalse(person1.equals(person));
    }

    @Test
    void compareTo1() {
        Person person1 = new Person();
        person1.setName("Anders");
        assertTrue(0==person.compareTo(person1));
    }
    @Test
    void compareTo2() {
        Person person1 = new Person();
        person1.setName("donnders");
        assertTrue(0>person.compareTo(person1));
    }
    @Test
    void compareTo3() {
        Person person1 = new Person();
        person1.setName("AAAnders");
        assertTrue(0<person.compareTo(person1));
    }
}
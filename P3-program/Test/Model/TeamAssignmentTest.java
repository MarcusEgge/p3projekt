package Model;

import Model.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class TeamAssignmentTest {
    private Gymnast gymnast;
    private Person person;
    private List<TeamAssignment> teamAssignmentsList;
    private Club club;
    private Team team;
    private TeamAssignment teamAssignment;

    @BeforeEach
    public void initObjects(){
        teamAssignmentsList= new ArrayList<>();
        person = new Person();
        club= new Club();
        gymnast = new Gymnast(person,"121202", true);
        team = new Team("Red");
        teamAssignment= new TeamAssignment(team,gymnast,true);
    }

    @Test
    void getTeam() {
        assertEquals(team,teamAssignment.getTeam());
    }

    @Test
    void isSignedUp() {
        assertTrue(teamAssignment.isSignedUp());
    }

    @Test
    void setSignedUp1() {
        teamAssignment.setSignedUp(false);
        assertFalse(teamAssignment.isSignedUp());
    }
    @Test
    void setSignedUp2() {
        teamAssignment.setSignedUp(false);
        teamAssignment.setSignedUp(true);
        assertTrue(teamAssignment.isSignedUp());
    }

    @Test
    void getTeamMember() {
        assertEquals(gymnast,teamAssignment.getTeamMember());
    }

    //Skal den ikke også delete sig selv hos rolerne?
    @Test
    void deleteFromTeam() {
        team.addTeamAssignment(teamAssignment);
        teamAssignment.deleteFromTeam();
        assertFalse(teamAssignment.getTeam().getTeamAssignmentList().contains(teamAssignment));
    }
    //Skal laves færdig
    @Test
    void deleteFromRole() {
        gymnast.addTeamAssignment(teamAssignment);
        teamAssignment.deleteFromRole();
        assertFalse(teamAssignment.getTeamMember().getTeamAssignmentList().contains(teamAssignment));
    }

    @Test
    void compareTo1() {
        Team team2 = new Team("Red");
        TeamAssignment teamAssignment2 = new TeamAssignment(team2,gymnast,true);
        assertEquals(0,teamAssignment.compareTo(teamAssignment2));
    }

    @Test
    void compareTo2() {
        Team team2 = new Team("Blz");
        TeamAssignment teamAssignment2 = new TeamAssignment(team2,gymnast,true);
        assertTrue(0<teamAssignment.compareTo(teamAssignment2));
    }

    @Test
    void compareTo3() {
        Team team2 = new Team("znd");
        TeamAssignment teamAssignment2 = new TeamAssignment(team2,gymnast,true);
        assertTrue(0>teamAssignment.compareTo(teamAssignment2));
    }

    @Test
    void compareTo4() {
        Person person1 = new Person();
        person1.setName("Arkma");
        person.setName("ånding");
        Gymnast gymnast1 = new Gymnast(person1,"121202", true);
        TeamAssignment teamAssignment2 = new TeamAssignment(team,gymnast1,true);
        assertTrue(0<teamAssignment.compareTo(teamAssignment2));
    }
    @Test
    void compareTo5() {
        Person person1 = new Person();
        person1.setName("Ånding");
        person.setName("anders");

        Gymnast gymnast1 = new Gymnast(person1,"121202", true);
        TeamAssignment teamAssignment2 = new TeamAssignment(team,gymnast1,true);
        assertTrue(0>teamAssignment.compareTo(teamAssignment2));
    }

    @Test
    void compareTo6() {
        Person person1 = new Person();
        person1.setName("Ånding");
        person.setName("anders");

        Gymnast gymnast1 = new Gymnast(person1,"121202", true);
        TeamAssignment teamAssignment2 = new TeamAssignment(team,gymnast1,false);
        assertTrue(0>teamAssignment.compareTo(teamAssignment2));
    }

    @Test
    void compareTo7() {
        Person person1 = new Person();
        person1.setName("Ånding");
        person.setName("anders");

        Gymnast gymnast1 = new Gymnast(person1,"121202", true);
        TeamAssignment teamAssignment2 = new TeamAssignment(team,gymnast1,false);
        assertTrue(0<teamAssignment2.compareTo(teamAssignment));
    }




}
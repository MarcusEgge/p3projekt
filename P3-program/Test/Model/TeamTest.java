package Model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class TeamTest {

    private Judge judge;
    private Person person;
    private Club club;
    private Team team;
    private Gymnast gymnast;
    private List<TeamAssignment> teamAssignmentsList;
    private Event event;
    @BeforeEach
    public void initObjects(){

        person = new Person();
        club= new Club();
        judge= new Judge(person, true);
        event = new Event("Mikkels dans","9000 allborg, envej 12", LocalDateTime.now(),LocalDate.now(),new Season(club,LocalDateTime.now(),LocalDateTime.now()), 6, 6);
        teamAssignmentsList= new ArrayList<>();
        gymnast = new Gymnast(person,"12324", true);
        team = new Team("Ræd");
    }

    @Test
    void addGymnastToFutureEvents1(){
        team.addTeamSignup(new TeamSignup(team,event,true));
        team.addGymnastToFutureEvents(gymnast);
        assertTrue(team.getTeamSignupList().get(0).getPersonSignupList().isEmpty());
    }
    @Test
    void addGymnastToFutureEvents2(){
        event.setStartDate(LocalDateTime.of(2030,12,5,10,12));
        team.addTeamSignup(new TeamSignup(team,event,true));
        team.addGymnastToFutureEvents(gymnast);
        assertEquals(1,team.getTeamSignupList().get(0).getPersonSignupList().size());
        assertEquals(gymnast,team.getTeamSignupList().get(0).getPersonSignupList().get(0).getRole());
    }

    @Test
    void deleteGymnastFromFutureEvents1(){
        event.setStartDate(LocalDateTime.of(2030,12,5,10,12));
        team.addTeamSignup(new TeamSignup(team,event,true));
        team.addGymnastToFutureEvents(gymnast);
        team.deleteGymnastFromFutureEvents(gymnast);
        assertTrue(team.getTeamSignupList().get(0).getPersonSignupList().isEmpty());
    }



    //TODO: Testen skal kigges på igen for hvad vi regner med at teste for. generateTeamAssignments() laver TeamAssignments der er false men vi regner med at den er true.
    @Test
    void generateTeamAssignments1() {
        List<Person> personList = new ArrayList<>();
        person.setRole(gymnast,Person.GYMNAST_INDEX);
        personList.add(person);
        List<String> names= new ArrayList<>();
        names.add(person.getName());
        team.generateTeamAssignments(personList);
        TeamAssignment teamAssignment = new TeamAssignment(team, (TeamMember) person.getRole(Person.GYMNAST_INDEX), true);
        assertEquals(false,teamAssignment.equals(team.getTeamAssignmentList().get(0)));
    }

    @Test
    void generateTeamAssignments2() {
        List<Person> personList = new ArrayList<>();
        person.setRole(judge,Person.JUDGE_INDEX);
        personList.add(person);
        List<String> names = new ArrayList<>();
        team.generateTeamAssignments(personList);
        assertTrue((team.getTeamAssignmentList().isEmpty()));
    }

    @Test
    void setAndGetName() {
        assertEquals("Ræd",team.getName());
        team.setName("Blå");
        assertEquals("Blå",team.getName());
    }

    @Test
    void getAllGymnastsAndCoachesInTeam() {
        List<Person> personList = new ArrayList<>();
        Person person1 = new Person();
        Person person2 = new Person();
        Person person3 = new Person();
        Coach coach= new Coach(person2, true);
        Judge judge1= new Judge(person1, true);
        Gymnast gymnast1 = new Gymnast(person3,"12312", true);
        person.setRole(gymnast,Person.GYMNAST_INDEX);
        person1.setRole(judge1,Person.JUDGE_INDEX);
        person2.setRole(coach,Person.COACH_INDEX);
        person3.setRole(gymnast1,Person.GYMNAST_INDEX);

        personList.add(person);
        personList.add(person1);
        personList.add(person2);
        personList.add(person3);

        List<String> names1 = new ArrayList<>();
        List<String> names2 = new ArrayList<>();
        names1.add(person.getName());
        names1.add(person3.getName());
        names2.add(person2.getName());
        team.generateTeamAssignments(personList);
        List<TeamMember> teamMembers = new ArrayList<>();
        teamMembers.add(gymnast);
        teamMembers.add(gymnast1);

        assertEquals(teamMembers,team.getAllGymnastsInTeam());
        assertEquals(coach,team.getAllCoachesInTeam().get(0));
    }


    @Test
    void addAndDeleteTeamAssignment() {
        TeamAssignment teamAssignment = new TeamAssignment(team,gymnast,true);
        team.addTeamAssignment(teamAssignment);
        assertEquals(teamAssignment,team.getTeamAssignmentList().get(0));
        team.deleteTeamAssignment(teamAssignment);
        assertTrue(team.getTeamAssignmentList().isEmpty());
    }

    @Test
    void addTeamAssignment() {
        TeamAssignment teamAssignment = new TeamAssignment(team,gymnast,true);
        event.setStartDate(LocalDateTime.of(2030,12,5,10,12));
        team.addTeamSignup(new TeamSignup(team,event,true));
        team.addTeamAssignment(teamAssignment);
        assertEquals(teamAssignment,team.getTeamAssignmentList().get(0));
    }

    @Test
    void getGymnastsAndCoachesInTeam() {
        List<Person> personList = new ArrayList<>();
        Person person1 = new Person();
        Person person2 = new Person();
        Person person3 = new Person();
        Coach coach= new Coach(person2, true);
        Judge judge1= new Judge(person1, true);
        Gymnast gymnast1 = new Gymnast(person3,"12312", true);
        person.setRole(gymnast,Person.GYMNAST_INDEX);
        person1.setRole(judge1,Person.JUDGE_INDEX);
        person2.setRole(coach,Person.COACH_INDEX);
        person3.setRole(gymnast1,Person.GYMNAST_INDEX);

        personList.add(person);
        personList.add(person1);
        personList.add(person2);
        personList.add(person3);

        List<String> names1 = new ArrayList<>();
        List<String> names2 = new ArrayList<>();
        names1.add(person.getName());
        names1.add(person3.getName());
        names2.add(person2.getName());
        team.generateTeamAssignments(personList);
        team.getTeamAssignmentList().get(0).setSignedUp(true);
        assertEquals(person,team.getGymnastsInTeam().get(0));

        team.getTeamAssignmentList().get(1).setSignedUp(true);
        assertEquals(person2,team.getCoachesInTeam().get(0));
    }

    @Test
    void delete(){
        TeamSignup teamSignup = new TeamSignup(team,event,true);
        TeamAssignment teamAssignment =new TeamAssignment(team,gymnast,true);
        PersonSignup personSignup = new PersonSignup(gymnast,teamSignup.getEvent());
        teamSignup.addPersonSignup(personSignup);
        team.addTeamAssignment(teamAssignment);
        team.addTeamSignup(teamSignup);
        team.delete();

        assertFalse(event.getTeamSignupList().contains(teamSignup));
        assertFalse(gymnast.getTeamAssignmentList().contains(teamAssignment));
        assertFalse(personSignup.getTeamSignupList().contains(teamSignup));
    }



    @Test
    void compareToTest1() {
        Team team1= new Team("Ræd");
        assertEquals(0,team.compareTo(team1));
    }

    @Test
    void compareToTest2() {
        Team team1= new Team("åæd");
        assertTrue(0>team.compareTo(team1));
    }

    @Test
    void compareToTest3() {
        Team team1= new Team("Aæd");
        assertTrue(0<team.compareTo(team1));
    }

    @Test
    void equalsTest1() {
        Team team1 = new Team("Ræd");
        assertTrue(team.equals(team1));
    }
    @Test
    void equalsTest2() {
        Team team1 = new Team("Rød");
        assertFalse(team.equals(team1));
    }

    @Test
    void hashCodeTest() {
        Team team1= new Team("Ræd");
        assertEquals(team1.hashCode(),team.hashCode());
    }
}
-- MySQL dump 10.13  Distrib 8.0.13, for Win64 (x86_64)
--
-- Host: localhost    Database: club
-- ------------------------------------------------------
-- Server version	8.0.13

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `person_signup`
--

DROP TABLE IF EXISTS `person_signup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `person_signup` (
  `id_personsignup` int(11) NOT NULL AUTO_INCREMENT,
  `is_signed_up` int(11) NOT NULL,
  `person_id` int(11) NOT NULL,
  `gymnast_id` int(11) DEFAULT NULL,
  `coach_id` int(11) DEFAULT NULL,
  `judge_id` int(11) DEFAULT NULL,
  `event_id` int(11) NOT NULL,
  PRIMARY KEY (`id_personsignup`),
  KEY `personsignup_event_key_idx` (`event_id`),
  KEY `personsignup_person_key_idx` (`person_id`),
  KEY `personsignup_gymnast_key_idx` (`gymnast_id`),
  CONSTRAINT `personsignup_coach_key` FOREIGN KEY (`id_personsignup`) REFERENCES `coach` (`id_coach`),
  CONSTRAINT `personsignup_event_key` FOREIGN KEY (`event_id`) REFERENCES `event` (`id_event`),
  CONSTRAINT `personsignup_gymnast_key` FOREIGN KEY (`gymnast_id`) REFERENCES `gymnast` (`id_gymnast`),
  CONSTRAINT `personsignup_judge_key` FOREIGN KEY (`id_personsignup`) REFERENCES `judge` (`id_judge`),
  CONSTRAINT `personsignup_person_key` FOREIGN KEY (`person_id`) REFERENCES `person` (`id_person`),
  CONSTRAINT `personsignup_signupindexes_key` FOREIGN KEY (`id_personsignup`) REFERENCES `singup_indexes` (`person_signup_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-12-03 14:26:36

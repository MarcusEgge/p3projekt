$(document).ready(function(){
    loadNavBar(function () {
        $(".events").css("max-height", (window.innerHeight - ($(".navbarColor").height() + 1)));
    });

    $(document).on('click', ".sub-navbar-link", function () {
        if($("#ScheduleSubNav").hasClass("sub-navbar-link-selected")){
            loadEvents();
        }
        $(this)
            .siblings()
            .removeClass("sub-navbar-link-selected");
        $(this).addClass("sub-navbar-link-selected");
        loadTable();
    });
});


function loadNavBar(fun) {
    $(".navbarColor").load("Template-HTML-pages/nav-bar.html", function () {
        fun();
    });
}

$(document).ready(function () {
    loadEvents();
    loadMainWindow();

    //Sæt højde af events del ved resize
    $(window).resize(function () {
        $(".events").css("max-height", (window.innerHeight - ($(".navbarColor").height() + 1)));
        adjustscrollheight();
    });

    $(document).on('click', '.btnEdit', function (event) {
        $(this).parents(".event-box").addClass("event-box-selected");
        $(this).parents(".event-box").siblings().removeClass("event-box-selected");
        event.stopPropagation();
        loadEditEvent(function () {
            $.ajax({
                type: "GET",
                url: "/../eventServlet",
                success: function (data) {
                    var eventID = parseInt($(".event-box-selected")[0].getAttribute("data-id"));
                    var obj;
                    for (i = 0; i < data.length; i++) {
                        if (data[i].eventID === eventID) {
                            obj = data[i];
                            break;
                        }
                    }
                    $("#editStartDate").val(obj.startDate);
                    $("#editStartTime").val(obj.startTime);
                    $("#editEndDate").val(obj.endDate);
                    $("#editEventName").val(obj.name);
                    $("#editEventAdress").val(obj.adress);
                    $("#editEventTown").val(obj.city);
                    $("#editEventCoaches").val(obj.coaches);
                    $("#editEventJudges").val(obj.judges);
                }
            });
        });
    });

    $(document).on('click', '.event-box', function () {
        $(this)
            .siblings()
            .removeClass("event-box-selected");
        $(this).addClass("event-box-selected");
        loadMainWindow();
    });

    $(document).on('click', 'thead th', function () {
        colorRows();
    });

    $(document).delegate('.checkboxPerson', 'click', function () {
        var checkboxChecked = $(this).is(":checked");
        if (checkboxChecked === true) {
            $.ajax({
                type: "POST",
                url: "/../PersonEventSignup",
                data: {
                    personIndex: $(this).closest('tr').attr('person-id'),
                    eventIndex: $(".event-box-selected")[0].getAttribute("data-id")
                },
                success: function () {
                    //do shit
                }
            })
        } else if (checkboxChecked === false) {
            $.ajax({
                type: "DELETE",
                url: "/../PersonEventSignup/" + $(this).closest('tr').attr('person-id'),
                data: {
                    data: $(".event-box-selected")[0].getAttribute("data-id")
                },
                success: function () {
                    //do shit
                }
            })
        }
    });

    $(document).on('mouseenter', 'tbody tr', function () {
        $(this).css("background-color", "darkgrey")
    });

    $(document).on('mouseleave', 'tbody tr', function () {
        colorRows();
    });


    $(document).on('keyup', '#myInput', function () {
        var value = $(this).val().toLowerCase();
        $(".myTable tr").filter(function () {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1);
            colorRows();
        });
    });

    $(document).on('focus', '#myInput', function () {
        $(this).parents(".sub-navbar-offset").css("box-shadow", "0 0 0 0.2rem rgba(0,123,255,0.25)")
    });

    $(document).on('blur', '#myInput', function () {
        $(this).parents(".sub-navbar-offset").css("box-shadow", "none")
    });

    $(document).on('click', "#btnCreateEvent", function () {
        loadCreateEvent()
    });

    $(document).on('click', "#btnCancelCreateEvent", function () {
        loadMainWindow();
    });

    $(document).on('click', '.fa-copy', function () {
        copyToClipboard($(this).parent());
        $(this)
            .parents("tr")
            .addClass("gymnast-copied");
    });

    $(document).on('click', '.dropdown-item', function () {
        $(this).parent().siblings("button").html($(this).html());
    });

// When the user clicks on the button, open the modal
    $(document).on('click', '#modalBtn',function () {
        $("#myModal").css("display", "block");
    });

// When the user clicks on <span> (x), close the modal
    $(document).on('click', '#cancelDeleteBtn', function () {
        $("#myModal").css("display", "none");
    });

    $(document).on('click', '#confirmDeleteBtn', function () {
        $.ajax({
            type: "DELETE",
            url: "/../eventServlet/" + $(".event-box-selected")[0].getAttribute("data-id"),
            success: function () {
                loadEvents();
                loadMainWindow();
                selectTopEvent();
            }
        })
    });

    $(document).on("click", "#btnSaveEventChanges", function () {
        var startDate = $("#editStartDate").val();
        var endDate = $("#editEndDate").val();
        var startTime = $("#editStartTime").val();
        var name = $("#editEventName").val();
        var adress = $("#editEventAdress").val();
        var city = $("#editEventTown").val();
        var coaches = $("#editEventCoaches").val();
        var judges = $("#editEventJudges").val();

        var obj = {
            'startDate': startDate,
            'endDate': endDate,
            'startTime': startTime,
            'name': name,
            'adress': adress,
            'city': city,
            'coaches': coaches,
            'judges': judges
        };

        if ((startDate === "") ||
            (endDate === "") ||
            (startTime === "") ||
            (name === "") ||
            (adress === "") ||
            (city === "") ||
            (coaches === "") ||
            (judges === "")) {
            alert("Fejl: Udfyld alle felter.")
        } else {
            $.ajax({
                type: "PUT",
                url: "/../eventServlet/" + $(".event-box-selected")[0].getAttribute("data-id"),
                data: {
                    text: JSON.stringify(obj)
                },
                success: function () {
                    console.log(obj);
                    eventSavedPopUp();

                    loadEvents();
                    loadMainWindow();
                },
                error:function (xhr, ajaxOptions, thrownError) {
                    alert(xhr.status);
                    alert(thrownError);
                }
            });
        }
    });

    $(document).on("click", "#btnSaveNewEvent", function () {
        var startDate = $("#createStartDate").val();
        var endDate = $("#createEndDate").val();
        var startTime = $("#createStartTime").val();
        var name = $("#createEventName").val();
        var adress = $("#createEventAdress").val();
        var city = $("#createEventTown").val();
        var coaches = $("#createEventCoaches").val();
        var judges = $("#createEventJudges").val();

        var obj = {
            'startDate': startDate,
            'endDate': endDate,
            'startTime': startTime,
            'name': name,
            'adress': adress,
            'city': city,
            'coaches': coaches,
            'judges': judges
        };

        if ((startDate === "") ||
            (endDate === "") ||
            (startTime === "") ||
            (name === "") ||
            (adress === "") ||
            (city === "") ||
            (coaches === "") ||
            (judges === "")) {
            alert("Fejl: Udfyld alle felter.")
        } else {
            $.ajax({
                type: "POST",
                url: "/../eventServlet",
                data: {
                    text: JSON.stringify(obj)
                },
                success: function (data) {
                    addEvent(data);
                    eventSavedPopUp();
                    loadMainWindow();
                }
            });
        }
    });

    $(document).on('click', '.editCarBtn',function () {
        $("#eventSpawn").load("Template-HTML-pages/driveOverview-edit-template.html");

    })
});

//should be seperated into helping funktions copypaste code
function loadTable() {
    if ($("#GymnastsSubNav").hasClass("sub-navbar-link-selected")) {
        $('#mainArea').load("Template-HTML-pages/Main-Table-Template.html", function () {
            loadGymnastTable(function () {
                makeTable();
            });
        })

    } else if ($("#CoachesSubNav").hasClass("sub-navbar-link-selected")) {
        $('#mainArea').load("Template-HTML-pages/Main-Table-Template.html", function () {
            loadCoachTable(function () {
                makeTable();
            });
        })
    } else if ($("#JudgesSubNav").hasClass("sub-navbar-link-selected")) {
        $('#mainArea').load("Template-HTML-pages/Main-Table-Template.html", function () {
            loadJudgeTable(function () {
                makeTable();
            });
        })
    } else if ($("#TeamsSubNav").hasClass("sub-navbar-link-selected")) {

    } else if ($("#ScheduleSubNav").hasClass("sub-navbar-link-selected")) {
        loadDriveSchedule();
    }
}

function selectTopEvent() {
    $(".event-box:first").addClass("event-box-selected");
}

function eventSavedPopUp() {
    $("#save-message1").css("display", "block");
    setTimeout(function () {
        $("#save-message1").css("opacity", 0);
    }, 2000);
    setTimeout(function () {
        $("#save-message1").css("display", "none");
        $("#save-message1").css("opacity", 1);
    }, 4000);
}

function loadEvents() {
    $.ajax({
        type: "GET",
        url: "/../eventServlet",
        success: function (data) {
            $("#eventSpawn").empty();

            for (i = 0; i < data.length; i++) {
                addEvent(data[i]);
            }
            selectTopEvent();
        }
    })
}

function loadEditEvent(func) {
    $("#mainArea").load("Template-HTML-pages/Edit-Event-Template.html", function () {
        func();
    });
}

function loadCreateEvent() {
    $("#mainArea").load("Template-HTML-pages/Create-Event-Template.html");
}

function loadMainWindow() {
    $("#mainArea").load("Template-HTML-pages/Main-Table-Template.html", function () {
        loadGymnastTable(function () {
            makeTable();
        });
    });
}


function loadGymnastTable(func) {
    if ($.fn.dataTable.isDataTable('#dtVerticalScrollExample')) {
        $("#dtVerticalScrollExample").DataTable().destroy();
    }
    $.ajax({
        type: "GET",
        url: "/../personServlet",
        success: function (data) {
            $('tbody').empty();
            for (i = 0; i < data.length; i++) {
                if (data[i].isGymnast)
                    addGymnast(data[i])
            }
            func();
        }
    })
}

function loadCoachTable(func) {
    if ($.fn.dataTable.isDataTable('#dtVerticalScrollExample')) {
        $("#dtVerticalScrollExample").DataTable().destroy();
    }
    $.ajax({
        type: "GET",
        url: "/../personServlet",
        success: function (data) {
            $('tbody').empty();
            for (i = 0; i < data.length; i++) {
                if (data[i].isCoach)
                    addVolunteer(data[i])
            }
            func();
        }
    })
}

function loadJudgeTable(func) {
    if ($.fn.dataTable.isDataTable('#dtVerticalScrollExample')) {
        $("#dtVerticalScrollExample").DataTable().destroy();
    }
    $.ajax({
        type: "GET",
        url: "/../personServlet",
        success: function (data) {
            $('tbody').empty();
            for (i = 0; i < data.length; i++) {
                if (data[i].isJudge)
                    addVolunteer(data[i])
            }
            func();
        }
    })
}

function addEvent(event) {
    $("#eventSpawn").append(Mustache.render(eveTemp(event), event))
}

function addGymnast(person) {
    var test = parseInt($(".event-box-selected")[0].getAttribute("data-id"));
    if (person.events.length > 0) {
        if (person.events.includes(test)) {
            $('tbody').append(Mustache.render(gymnastInEventTemplate, person));
        } else {
            $('tbody').append(Mustache.render(gymnastNotInEventTemplate, person));
        }
    }
    else {
        $('tbody').append(Mustache.render(gymnastNotInEventTemplate, person));
    }
}

function addVolunteer(person) {
    $('tbody').append(Mustache.render(volunteerTemplate, person));
}

function eveTemp(data) {
    var startDate = new Date(data.startDate);
    var endDate = new Date(data.endDate);
    var startDay = startDate.getDate();
    var startMonth = startDate.getMonth() + 1;
    var endDay = endDate.getDate();
    var endMonth = endDate.getMonth() + 1;

    var eventTemplate = "" +
        "<div data-id='{{eventID}}' class='event-box'>" +
        "<div class='row'>" +
        "<div class='col-md-5 event-box-color'>" +
        "<div class='event-date'>Start: <span>" + startDay + "/" + startMonth + "</span></div>" +
        "<div class='event-name'>Kl. <span>{{startTime}}</span></div>" +
        "<div class='event-date'>Slut: <span>" + endDay + "/" + endMonth + "</span></div>" +
        "<div class='event-name'><span>{{name}}</span></div>" +
        "</div>" +
        "<div class='col-md-6'>" +
        "<div class='event-information'><span>{{adress}}</span></div>" +
        "<div class='event-information'><span>{{city}}</span></div>" +
        "<div class='event-information'>Springere <span>(TO DO)</span></div>" +
        "<div class='event-information'>Trænere <span class='font-weight-bold'>{{coaches}}</span> Dommere <span class='font-weight-bold'>{{judges}}</span></div>" +
        "</div>" +
        "<div class='col-md-1 override-padAndMargin'><i class='far fa-edit  btnEdit'></i></div>" +
        "</div>" +
        "</div>";

    return eventTemplate
}

var gymnastInEventTemplate = "" +
    "<tr person-id='{{id}}'>" +
    "<td scope='row'>" +
    "<label class='container'>" +
    "<input type='checkbox' class='checkboxPerson' checked>" +
    "<span class= 'checkmark'></span>" +
    "</label>" +
    "</td>" +
    "<td class='name'>" + "{{firstName}}" + " " + "{{lastName}}" + "</td>" +
    "<td>{{license}} <i class=\"far fa-copy\"></i></td>" +
    "<td>{{phoneNumber}}</td>" +
    "<td>{{email}}</td>" +
    "</tr>";

var gymnastNotInEventTemplate = "" +
    "<tr person-id='{{id}}'>" +
    "<td scope='row'>" +
    "<label class='container'>" +
    "<input type='checkbox' class='checkboxPerson' >" +
    "<span class= 'checkmark'></span>" +
    "</label>" +
    "</td>" +
    "<td class='name'>" + "{{firstName}}" + " " + "{{lastName}}" + "</td>" +
    "<td>{{license}} <i class=\"far fa-copy\"></i></td>" +
    "<td>{{phoneNumber}}</td>" +
    "<td>{{email}}</td>" +
    "</tr>";

var volunteerTemplate = "" +
    "<tr>" +
    "<td>" +
    "<div class=\"btn-group\" role=\"group\" aria-label=\"Button group with nested dropdown\">" +
    "<button id=\"btnGroupDrop1\" type=\"button\" class=\"btn btn-secondary dropdown-toggle attendenceBtn\"" +
    "data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\"> Ikke svaret" +
    "</button>" +
    "<div class=\"dropdown-menu\" aria-labelledby=\"btnGroupDrop1\" x-placement=\"bottom-start\">" +
    "<a class=\"dropdown-item\" href=\"#\">Ikke svaret</a>" +
    "<a class=\"dropdown-item\" href=\"#\">Deltager</a>" +
    "<a class=\"dropdown-item\" href=\"#\">Deltager ikke</a>" +
    "</div>" +
    "</div>" +
    "</td>" +
    "<td>" + "{{firstName}}" + " " + "{{lastName}}" + "</td>" +
    "<td>{{license}} <i class=\"far fa-copy\"></i></td>" +
    "<td>{{phoneNumber}}</td>" +
    "<td>{{email}}</td>" +
    "</tr>";


var mainRow = ""+
    "<div class='row' id='mainRow'>";

var driverCardNewCarTemplate = ""+
    "<div class='card border-secondary driverCard col-md-2'>" +
    "<div class='card-header'>Tilføj bil" +
    "</div>" +
    "<div class='card-body text-centrum'>" +
    "<i class='verticalCentrum fas fa-5x fa-plus-circle'></i>"+
    "</div>" +
    "</div>";

var driverCardTemplate= ""+
    "<div class='card border-secondary driverCard col-md-2'>" +
    "<div class='card-header'>Bil 1" +
    "<i class='far fa-edit floatRight editCarBtn'></i>" +
    "</div>" +
    "<div class='card-body'>" +
    "<h4 class='card-title'>" +
    "<i class='fas fa-car-alt'></i>"+
    "{{name}}"+
    "</h4>" +
    "<table class='table driverTable'>"+
    "<tbody>"+
    "<tr>" +
    "<td>Marcus Olsen</td>" +
    "</tr>"+
    "<tr>" +
    "<td>Marcus Olsen</td>" +
    "</tr>" +
    "<tr>"+
    "<td>Marcus Olsen</td>"+
    "</tr>"+
    "<tr>" +
    "<td>Marcus Olsen</td>"+
    "</tr>" +
    "</tbody>"+
    "</table>" +
    "</div>" +
    "</div>";

function adjustscrollheight() {
    $(".dataTables_scrollBody").css("min-height", (window.innerHeight - ($(".navbarColor").height() + 1 //These numbers are margins, padding and some stuff arbitrary divs from bootstrap
        + $(".sub-navbar-offset").height() + 5 + 1 + 1
        + $(".dataTables_scrollHead").height()
        + 2)));
}

function copyToClipboard(element) {
    var $temp = $("<input>");
    $("body").append($temp);
    $temp.val($(element).text()).select();
    document.execCommand("copy");
    $temp.remove();
}
function makeTable() {
    if (!$.fn.dataTable.isDataTable('#dtVerticalScrollExample')) {
        $('#dtVerticalScrollExample').DataTable({
            "scrollY": "400px",
            "scrollCollapse": true,
            "paging": false,
            "info": false,
            "filter": false,
            "autoWidth": false,
        });
        $('.dataTables_length').addClass("bs-select");
    }
    colorRows();
    adjustscrollheight();
}

function colorRows() {
    $("tbody tr:visible:odd").css("background-color", "rgb(242, 242, 242)");
    $("tbody tr:visible:even").css("background-color", "#fff");
}

function loadDriveSchedule() {

    clearMainArea();
    $('#mainArea').append(mainRow);

    for (var i = 0; i < 4; i++) {
        $('#mainRow').append(driverCardTemplate);
    }
    $('#mainRow').append(driverCardNewCarTemplate);

}

function clearMainArea() {
    $('#mainArea').empty();
}

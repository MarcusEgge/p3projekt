//giver et Array med random fra min til max og længden på array er count
function buildRandomDataArray(min, max, count) {
  var numberArray = [];
  for (var i = 0; i < count; i++) {
    numberArray.push(Math.floor((Math.random() * (max - min)) + min))
  }
  return numberArray;
};

//retunere gennemsnittet af et array i et array med samme længde
function avgCalculater(dataset) {
  var totalAmount = 0;
  var avgNumber = [];

  for (var i = 0; i < dataset.length; i++) {
    totalAmount += dataset[i];
  }
  totalAmount /= dataset.length;

  for (var i = 0; i < dataset.length; i++) {
    avgNumber.push(totalAmount);
  }

  return avgNumber;
}

var ctx = $('#myChart');
var distanceArray = buildRandomDataArray(25, 1500, 18);
var gennemsnitArray = avgCalculater(distanceArray);
var myChart = new Chart(ctx, {
  type: 'bar',
  data: {
    labels: ["Marcus", "Peter", "Tuan", "Martin", "Thea", "Frederik", "Marcus", "Peter", "Tuan", "Martin", "Thea", "Frederik", "Marcus", "Peter", "Tuan", "Martin", "Thea", "Frederik"],
    datasets: [{
        type: 'line',
        label: 'Gennemsnit ' + Math.floor(gennemsnitArray[0]),
        data: gennemsnitArray,
        fill: false,
        borderColor: 'rgba(0,0,0,1)',
        pointRadius: 0
      },
      {
        type: 'bar',
        label: 'Kilometer k�rt',
        data: distanceArray,
        backgroundColor: 'rgba(0, 164, 224, 1)',
        borderColor: 'rgba(0,0,0,1)',
        borderWidth: 1
      }
    ]
  },
  options: {
    scales: {
      yAxes: [{
        ticks: {
          beginAtZero: true
        }
      }]
    }
  }
});
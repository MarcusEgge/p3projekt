$(document).ready(function () {
    loadMainWindow();

    $(document).on('click', "tr", function () {
        $(this)
            .siblings()
            .removeClass("profile-selected");
        $(this).addClass("profile-selected");
        displayProfile();
    });

    $(document).on('click', ".btnCancelCreateProfile", function () {
        displayProfile();
    });

    $(document).on('click', '.btnDeleteProfile', function () {
        $("#myModal").css("display", "block");
    });

    $(document).on('click', '#cancelDeleteBtn', function () {
        $("#myModal").css("display", "none");
    });

    $(document).on('click', '#confirmDeleteBtn', function () {
        $.ajax({
            type: "POST",
            url: "/../personServlet/",
            data: {
                Action: "Delete",
                ID: $(".profile-selected")[0].getAttribute("person-id")
            },
            success: function () {
                $("#myModal").css("display", "none");
                loadMainWindow();
            }
        })
    });

    $(document).on('click', ".createProfileBtn", function () {
        $("#profileInfo").load("Template-HTML-pages/Create-Profile-Template.html")
    });

    $(document).on('click', ".btnConfirmCreateProfile", function () {
        var firstname = $("#createProfileFirstName").val();
        var lastname = $("#createProfileLastName").val();
        var birthsday = $("#createProfileBirthday").val();
        var phoneNumber = $("#createProfilePhoneNumber").val();
        var license = $("#createProfileLicense").val();
        var email = $("#createProfileEmail").val();
        var address = $("#createProfileAddress").val();
        var commune = $("#createProfileCommune").val();
        var postalCode = $("#createProfilePostalCode").val();
        var gender = radioButtonValidator($('[name="radioCreateGender"]'));
        var isGymnast = radioButtonValidator($('[name="radioCreateGymnast"]'));
        var isCoach = radioButtonValidator($('[name="radioCreateCoach"]'));
        var isJudge = radioButtonValidator($('[name="radioCreateJudge"]'));


        if ((firstname === "") ||
            (lastname === "") ||
            (birthsday === "") ||
            (phoneNumber === "") ||
            (license === "") ||
            (email === "") ||
            (address === "") ||
            (commune === "") ||
            (postalCode === "") ||
            (gender === "Error") ||
            (isGymnast === "Error") ||
            (isCoach === "Error") ||
            (isJudge === "Error")) {
            alert("Fejl: Udfyld alle felter.")
        } else {
            var obj = {
                'firstName': firstname,
                'lastName': lastname,
                'birthsday': birthsday,
                'phoneNumber': phoneNumber,
                'license': license,
                'email': email,
                'address': address,
                'commune': commune,
                'postalCode': postalCode,
                'gender': gender,
                'isGymnast': isGymnast,
                'isCoach': isCoach,
                'isJudge': isJudge
            };
            $.ajax({
                type: "POST",
                url: "/../personServlet",
                data: {
                    Action: "Create",
                    Person: JSON.stringify(obj)
                },
                success: function () {
                    loadGymnastTable();
                }
            });
        }
    });

    $(document).on('click', ".editProfileBtn", function () {
        editProfile(function () {
            $.ajax({
                type: "GET",
                url: "/../personServlet",
                success: function (data) {
                    var personID = parseInt($(".profile-selected")[0].getAttribute("person-id"));
                    var obj;
                    for (i = 0; i < data.length; i++) {
                        if (data[i].id === personID) {
                            obj = data[i];
                            break;
                        }
                    }
                    $("#editProfileFirstName").val(obj.firstName);
                    $("#editProfileLastName").val(obj.lastName);
                    $("#editProfilePhoneNumber").val(obj.phoneNumber);
                    $("#editProfileEmail").val(obj.email);
                    $("#editProfileLicense").val(obj.license);
                    $("#editProfileAddress").val(obj.address);
                    $("#editProfileCommune").val(obj.commune);
                    $("#editProfilePostalCode").val(obj.postalCode);
                    $("#editProfileBirthday").val(obj.birthsday);
                    profilesGender(obj);
                    isProfileGymnast(obj);
                    isProfileCoach(obj);
                    isProfileJudge(obj);
                }
            })
        })
    });

    $(document).on('click', ".btnConfirmProfileChanges", function () {
        var firstname = $("#editProfileFirstName").val();
        var lastname = $("#editProfileLastName").val();
        var birthsday = $("#editProfileBirthday").val();
        var phoneNumber = $("#editProfilePhoneNumber").val();
        var license = $("#editProfileLicense").val();
        var email = $("#editProfileEmail").val();
        var address = $("#editProfileAddress").val();
        var commune = $("#editProfileCommune").val();
        var postalCode = $("#editProfilePostalCode").val();
        var gender = radioButtonValidator($('[name="radioEditGender"]'));
        var isGymnast = radioButtonValidator($('[name="radioEditGymnast"]'));
        var isCoach = radioButtonValidator($('[name="radioEditCoach"]'));
        var isJudge = radioButtonValidator($('[name="radioEditJudge"]'));


        if ((firstname === "") ||
            (lastname === "") ||
            (birthsday === "") ||
            (phoneNumber === "") ||
            (license === "") ||
            (email === "") ||
            (address === "") ||
            (commune === "") ||
            (postalCode === "") ||
            (gender === "Error") ||
            (isGymnast === "Error") ||
            (isCoach === "Error") ||
            (isJudge === "Error")) {
            alert("Fejl: Udfyld alle felter.")
        } else {
            var obj = {
                'firstName': firstname,
                'lastName': lastname,
                'birthsday': birthsday,
                'phoneNumber': phoneNumber,
                'license': license,
                'email': email,
                'address': address,
                'commune': commune,
                'postalCode': postalCode,
                'gender': gender,
                'isGymnast': isGymnast,
                'isCoach': isCoach,
                'isJudge': isJudge
            };

            $.ajax({
                type: "POST",
                url: "/../personServlet",
                data: {
                    Action: "Update",
                    PersonID: $(".profile-selected")[0].getAttribute("person-id"),
                    Person: JSON.stringify(obj)
                },
                success: function () {
                    loadGymnastTable();
                    eventSavedPopUp()
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(xhr.status);
                    alert(thrownError);
                }
            });
        }
    });
});

function profilesGender(profile) {
    if (profile.gender === "Mand")
        $('[name="radioEditGender"]')[0].checked = true;
    else if (profile.gender === "Kvinde")
        $('[name="radioEditGender"]')[1].checked = true;
}

function isProfileGymnast(profile) {
    if (profile.isGymnast) {
        $('[name="radioEditGymnast"]')[0].checked = true;
    }
    else if (!profile.isGymnast) {
        $('[name="radioEditGymnast"]')[1].checked = true;
    }
}

function isProfileCoach(profile) {
    if (profile.isCoach)
        $('[name="radioEditCoach"]')[0].checked = true;
    else if (!profile.isCoach)
        $('[name="radioEditCoach"]')[1].checked = true;
}

function isProfileJudge(profile) {
    if (profile.isJudge)
        $('[name="radioEditJudge"]')[0].checked = true;
    else if (!profile.isJudge)
        $('[name="radioEditJudge"]')[1].checked = true;
}

function loadMainWindow() {
    $("#mainArea").load("Template-HTML-pages/Main-MemberTable-Template.html", function () {
        loadGymnastTable(function () {
            makeTable();
        });
    });
}

function loadGymnastTable() {
    $.ajax({
        type: "GET",
        url: "/../personServlet",
        success: function (data) {
            $('tbody').empty();
            for (i = 0; i < data.length; i++) {
                if (data[i].isGymnast)
                    appendGymnast(data[i])
            }
            selectTopProfile();
            displayProfile();
        }
    })
}

function loadCoachTable() {
    $.ajax({
        type: "GET",
        url: "/../personServlet",
        success: function (data) {
            $('tbody').empty();
            for (i = 0; i < data.length; i++) {
                if (data[i].isCoach)
                    appendGymnast(data[i])
            }
            selectTopProfile();
            displayProfile();
        }
    })
}

function loadJudgeTable() {
    $.ajax({
        type: "GET",
        url: "/../personServlet",
        success: function (data) {
            $('tbody').empty();
            for (i = 0; i < data.length; i++) {
                if (data[i].isJudge)
                    appendGymnast(data[i])
            }
            selectTopProfile();
            displayProfile();
        }
    })
}

function radioButtonValidator(radioGroupName) {
    for (i = 0; i < radioGroupName.length; i++) {
        if (radioGroupName[i].checked) {
            return radioGroupName[i].value;
        }
    }
    return "Error"
}

function loadCoaches() {
}

function loadJudges() {
}

function appendGymnast(person) {
    $('tbody').append(Mustache.render(gymnastTemplate, person))
}

function editProfile(func) {
    $("#profileInfo").load("Template-HTML-pages/Edit-Profile-Template.html", function () {
        func();
    });
}

function displayProfile() {
    $.ajax({
        type: "GET",
        url: "/../personServlet",
        success: function (data) {
            if (data.length > 0) {
                var personID = parseInt($(".profile-selected")[0].getAttribute("person-id"));
                var obj;
                for (i = 0; i < data.length; i++) {
                    if (data[i].id === personID) {
                        obj = data[i];
                        break;
                    }
                }
                $("#profileInfo").empty();
                loadProfile(function () {
                    $("#firstname")[0].innerText = obj.firstName;
                    $("#lastname")[0].innerText = obj.lastName;
                    $("#gender")[0].innerText = obj.gender;
                    $("#birthday")[0].innerText = obj.birthsday;
                    $("#adress")[0].innerText = obj.address;
                    $("#majorCity")[0].innerText = obj.commune;
                    $("#zipcode")[0].innerText = obj.postalCode;
                    $("#phoneNumber")[0].innerText = obj.phoneNumber;
                    $("#email")[0].innerText = obj.email;
                    $("#licenseNumber")[0].innerText = obj.license;
                    $("#memberSince")[0].innerText = "MEDLEM SIDEN";
                    $("#isGymnast")[0].innerText = obj.isGymnast;
                    $("#isCoach")[0].innerText = obj.isCoach;
                    $("#isJudge")[0].innerText = obj.isJudge;
                });
            } else {
                $("#profileInfo").empty();
            }
        }
    })
}

function loadProfile(func) {
    $("#profileInfo").load("Template-HTML-pages/Display-Profile-Template.html", function () {
        func();
    });
};

function makeTable() {
    if (!$.fn.dataTable.isDataTable('#dtVerticalScrollExample')) {
        $('#dtVerticalScrollExample').DataTable({
            "scrollY": "400px",
            "scrollCollapse": true,
            "paging": false,
            "info": false,
            "filter": false,
            "autoWidth": false,
        });
        $('.dataTables_length').addClass("bs-select");
    }
    colorRows();
    adjustscrollheight();
}

function eventSavedPopUp() {
    $("#save-message1").css("display", "block");
    setTimeout(function () {
        $("#save-message1").css("opacity", 0);
    }, 2000);
    setTimeout(function () {
        $("#save-message1").css("display", "none");
        $("#save-message1").css("opacity", 1);
    }, 4000);
}

function selectTopProfile() {
    $("tbody").find("tr:first").addClass("profile-selected");
}

var gymnastTemplate = "" +
    "<tr person-id='{{id}}'>" +
    "<td class='name'>" + "{{firstName}}" + " " + "{{lastName}}" + "</td>" +
    "<td>{{phoneNumber}}</td>" +
    "<td>{{email}}</td>" +
    "<td>HOLD (TO DO)</td>" +
    "</tr>";

function loadTable() {
    if ($("#GymnastsSubNav").hasClass("sub-navbar-link-selected")) {
        loadGymnastTable(function () {
            makeTable();
        });
    } else if ($("#CoachesSubNav").hasClass("sub-navbar-link-selected")) {
        loadCoachTable(function () {
            makeTable();
        })
    } else if ($("#JudgesSubNav").hasClass("sub-navbar-link-selected")) {
        loadJudgeTable(function () {
            makeTable();
        })
    }

}

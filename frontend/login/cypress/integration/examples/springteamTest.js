/// <reference types="Cypress" />


describe("Create/edit/delete Tests", () => {
    beforeEach(function () {
        login()
    })

    it("event", () => {
        fillOutEventForms();
        validateNewEvent();
        editEvent();
        deleteEvent();
    })

    it("Member", () => {
        cy.contains("Medlemmer").click();
        cy.wait(1000)
        validateMemberLenght(2);
        filloutProfileForms();
        validateMemberLenght(3);
        editProfile();
        deleteProfile();
        validateMemberLenght(2);
    })

});

function deleteProfile() {
    cy.contains("NewName").click();
    cy.get('.editProfileBtn').click();
    cy.get('.btnDeleteProfile').click();
    cy.get('#confirmDeleteBtn').click();
}

function editProfile() {
    cy.contains("Cypress").click();
    cy.get('.editProfileBtn').click();
    cy.get("#editProfileFirstName").clear().type("NewName");
    cy.get('.btnConfirmProfileChanges').click();
}

function validateMemberLenght(length) {
    cy.get('tr').should("have.length", length);
}

function filloutProfileForms() {
    cy.get('.createProfileBtn').click();
    cy.get('#createProfileFirstName').type("Cypress");
    cy.get('#createProfileLastName').type("Test");
    cy.get('#radioMale').click();
    cy.get("#createProfileBirthday").type("2018-10-20");
    cy.get("#createProfileAddress").type("CypressVej");
    cy.get("#createProfileCommune").type("CypressTown");
    cy.get("#createProfilePostalCode").type("1234");
    cy.get("#createProfilePhoneNumber").type("12345678");
    cy.get("#createProfileEmail").type("Cypress@IO");
    cy.get("#createProfileLicense").type("CY1234");
    cy.get('#gymnastYes').click();
    cy.get('#coachYes').click();
    cy.get('#judgeYes').click();

    cy.get('.btnConfirmCreateProfile').click()
}

function deleteEvent() {
    cy.get('.fa-edit').last().click();
    cy.get('#modalBtn').click();
    cy.get('#confirmDeleteBtn').click();
    cy.get('.event-box').should('have.length', 2);
}

function editEvent() {
    cy.get('.fa-edit').last().click();
    cy.get("#editEventName").clear();
    cy.get("#editEventName").type("EditedName");
    cy.get("#btnSaveEventChanges").click();
    cy.contains('EditedName').should('be.visible')
}

function validateNewEvent() {
    cy.get('.event-box').should('have.length', 3);
    cy.contains('CypressName').should('be.visible');
}

function login() {
    cy.visit("http://localhost:8081/loginPage.html");
    cy.get("#inputPassword").type("password");
    cy.get("#loginBtn").click();
    cy.url().should('include', 'admin/Event-springere.html');
}

function fillOutEventForms() {
    cy.get('.event-box').should('have.length', 2)

    cy.get('#btnCreateEvent').click()
    cy.get("#createStartDate").type("2018-10-20");
    cy.get("#createEndDate").type("2018-10-20");
    cy.get("#createStartTime").type("10:00");
    cy.get("#createEventName").type("CypressName");
    cy.get("#createEventAdress").type("CypressAdress");
    cy.get("#createEventTown").type("CypressTown");
    cy.get("#createEventCoaches").type("2");
    cy.get("#createEventJudges").type("1");
    cy.get("#btnSaveNewEvent").click();
}
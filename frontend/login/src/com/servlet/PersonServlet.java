package com.servlet;

import com.data.Database;
import com.data.Person;
import com.google.gson.Gson;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Comparator;

@WebServlet(name = "personServlet", urlPatterns = "/personServlet/*")
public class PersonServlet extends HttpServlet {
    Database database = Database.getInstance();
    Gson gson = new Gson();

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");

        String json = gson.toJson(database.getPeople());

        response.getWriter().write(json);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        String action = request.getParameter("Action").trim();

        if (action.equals("Create")){
            String json = request.getParameter("Person");
            Person person = gson.fromJson(json, Person.class);
            createID(person);
            person.initializeList();
            database.addPerson(person);
        } else if (action.equals("Update")){
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");

            String json = request.getParameter("Person").trim();
            int personID = Integer.parseInt(request.getParameter("PersonID"));
            Person person = gson.fromJson(json, Person.class);

            database.updatePerson(personID, person.getFirstName(), person.getLastName(), person.getBirthsday(), person.getPhoneNumber(), person.getLicense(), person.getEmail(), person.getAddress(), person.getCommune(), person.getPostalCode(), person.getGender(), person.isGymnast(), person.isCoach(), person.isJudge());
            database.getPeople().sort(Comparator.comparingInt(Person::getId));
            json = gson.toJson(person);
            response.getWriter().write(json);
        } else if (action.equals("Delete")) {
            int personID = Integer.parseInt(request.getParameter("ID"));
            database.deletePerson(personID);

            response.getWriter().write("hej");
        } else {
            System.out.println("fejl..");
        }
    }

    public void createID(Person person){
        boolean idTaken;
        for (int i = 0; i < 1000; i++){
            idTaken = false;
            for (int k = 0; k < database.getPeople().size(); k++){
                if (database.getPeople().get(k).getId() == i){
                    idTaken = true;
                    break;
                }
            }
            if (!idTaken){
                person.setId(i);
                break;
            }
        }
    }
}


package com.servlet;

import com.google.gson.Gson;
import com.data.Database;
import com.data.Event;
import com.data.Person;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.Scanner;

@WebServlet(name = "eventServlet", urlPatterns = "/eventServlet/*")
public class EventServlet extends HttpServlet {
    Database database = Database.getInstance();
    Gson gson = new Gson();

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");

        String json = gson.toJson(database.getEvents());

        response.getWriter().write(json);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");

        String json = request.getParameter("text").trim();

        Event event = gson.fromJson(json, Event.class);
        createID(event);
        event.initializePeopleList();

        database.addEvent(event);

        json = gson.toJson(event);
        response.getWriter().write(json);
    }

    protected void doPut(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        BufferedReader br = new BufferedReader(new InputStreamReader(request.getInputStream()));

        String data = ConvertData(br.readLine()).get("text");
        int id = retrieveUserid(request);

        System.out.println(data);
        Event event = gson.fromJson(data, Event.class);

        database.updateEvent(id, event.getStartDate(), event.getEndDate(), event.getStartTime(), event.getName(), event.getAdress(), event.getCity(), event.getCoaches(), event.getJudges());
    }

    protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        int id = retrieveUserid(request);

        database.deleteEvent(id);

        response.getWriter().write("success");
    }

    public void createID(Event event){
        boolean idTaken;
        for (int i = 0; i < 1000; i++){
            idTaken = false;
            for (int k = 0; k < database.getEvents().size(); k++){
                if (database.getEvents().get(k).getEventID() == i){
                    idTaken = true;
                    break;
                }
            }
            if (!idTaken){
                event.setEventID(i);
                break;
            }
        }
    }

    private static int retrieveUserid(HttpServletRequest req) {
        String pathInfo = req.getPathInfo();
        if (pathInfo.startsWith("/")) {
            pathInfo = pathInfo.substring(1);
        }
        return Integer.parseInt(pathInfo);
    }

    private static String inputStreamToString(InputStream inputStream) {
        Scanner scanner = new Scanner(inputStream, "UTF-8");
        return scanner.hasNext() ? scanner.useDelimiter("A").next() : "";
    }

    HashMap<String, String> ConvertData(String Data) {
        HashMap<String, String> map = new HashMap<String, String>();
        String[] Datas = Data.split("=");
        String key = null;
        for (int i = 0; i < Datas.length; i++) {
            if (i % 2 == 0) {
                try {
                    key = URLDecoder.decode(Datas[i], "UTF-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }
            else {
                try {
                    map.put(key, URLDecoder.decode((Datas[i]), "UTF-8"));
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }
        }
        return map;
    }
}
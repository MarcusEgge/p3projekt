package com.servlet;

import com.google.gson.Gson;
import com.data.Database;
import com.data.Person;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.Scanner;

@WebServlet(name = "PersonEventSignup", urlPatterns = "/PersonEventSignup/*")
public class PersonEventSignup extends HttpServlet {
    Database database = Database.getInstance();
    Gson gson = new Gson();

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        int personIndex = Integer.parseInt(request.getParameter("personIndex"));
        int eventIndex = Integer.parseInt(request.getParameter("eventIndex"));

        database.getEvents().get(eventIndex).addPerson(database.findPersonById(personIndex));
        database.addEventToPerson(eventIndex, personIndex);
    }

    protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        int personIndex = retrieveUserid(request);
        BufferedReader br = new BufferedReader(new InputStreamReader(request.getInputStream()));
        String data = ConvertData(br.readLine()).get("data");

        int eventIndex = Integer.parseInt(data);

        database.removePersonFromEvent(eventIndex, personIndex);
        database.removeEventFromPerson(eventIndex, personIndex);

    }

    private static int retrieveUserid(HttpServletRequest req) {
        String pathInfo = req.getPathInfo();
        if (pathInfo.startsWith("/")) {
            pathInfo = pathInfo.substring(1);
        }
        return Integer.parseInt(pathInfo);
    }

    private static String inputStreamToString(InputStream inputStream) {
        Scanner scanner = new Scanner(inputStream, "UTF-8");
        return scanner.hasNext() ? scanner.useDelimiter("A").next() : "";
    }

    HashMap<String, String> ConvertData(String Data) {
        HashMap<String, String> map = new HashMap<String, String>();
        String[] Datas = Data.split("=");
        String key = null;
        for (int i = 0; i < Datas.length; i++) {
            if (i % 2 == 0) key = URLDecoder.decode(Datas[i]);
            else map.put(key, URLDecoder.decode(Datas[i]));
        }
        return map;
    }
}
package com.data;

import java.util.ArrayList;
import java.util.List;

public class Database {
    List<Person> people;
    List<Event> events;

    private static Database single_instance = null;

    private Database() {
        people = new ArrayList<>();
        events = new ArrayList<>();
        Person person1 = new Person("Bob", "Jensen","0000-01-02",12, 30232112, "ASDSA231", "test@test.dk", "Sejvej 2","København Kommune", 9000, "Mand", true, false, false, 0);
        person1.initializeList();
        Event test = new Event("0001-01-01", "0001-01-02", "10:00", "Pølse fest", "Pøllevej 1", "Randers", 5, 2);
        Event test1 = new Event("0001-01-01", "0001-01-02", "10:00", "Pølse fest", "Pøllevej 1", "Randers", 5, 2);
        test.setEventID(0);
        test1.setEventID(1);
        test.initializePeopleList();
        test1.initializePeopleList();

        addPerson(person1);
        addEvent(test);
        addEvent(test1);
    }

    public void addPerson(Person person) { people.add(person);}
    public void addEvent(Event event){
        events.add(event);
    }

    public static Database getInstance() {
        if (single_instance == null)
            single_instance = new Database();

        return single_instance;
    }

    public List<Person> getPeople() {
        return people;
    }

    public List<Event> getEvents() {
        return events;
    }

    public void removePersonFromEvent(int eventIndex, int personIndex) {
        for (int i = 0; i < events.get(eventIndex).getPeopleAttending().size(); i++){
            if (events.get(eventIndex).getPeopleAttending().get(i).getId() == personIndex){
                events.get(eventIndex).getPeopleAttending().remove(i);
                break;
            }
        }
    }

    public void addEventToPerson(int eventIndex, int personIndex){
        people.get(personIndex).getEvents().add(eventIndex);
    }

    public void removeEventFromPerson(int eventIndex, int personIndex){
        for (int i = 0; i < events.size(); i++){
            if (people.get(personIndex).getEvents().get(i) == eventIndex) {
                people.get(personIndex).getEvents().remove(i);
                break;
            }
        }
    }

    public void deletePerson(int id) {
        for (int i = 0; i < people.size(); i++) {
            if (people.get(i).getId() == id){
                people.remove(i);
                break;
            }
        }
    }

    public void deleteEvent(int id) {
        for (int i = 0; i < events.size(); i++) {
            if (events.get(i).getEventID() == id){
                events.remove(i);
                break;
            }
        }
    }

    public void updatePerson(int id, String firstName, String lastName, String birthday, int phoneNumber, String license, String email, String address, String commune, int postalCode, String gender, boolean gymnast, boolean coach, boolean judge ) {
        for (int i = 0; i < people.size(); i++) {
            if (people.get(i).getId() == id) {
                people.get(i).setFirstName(firstName);
                people.get(i).setLastName(lastName);
                people.get(i).setBirthsday(birthday);
                people.get(i).setPhoneNumber(phoneNumber);
                people.get(i).setLicense(license);
                people.get(i).setEmail(email);
                people.get(i).setAddress(address);
                people.get(i).setCommune(commune);
                people.get(i).setPostalCode(postalCode);
                people.get(i).setGender(gender);
                people.get(i).setGymnast(gymnast);
                people.get(i).setCoach(coach);
                people.get(i).setJudge(judge);
            }
        }
    }

    public void updateEvent(int eventID, String startDate, String endDate, String time, String name, String adress, String city, int coach, int judge) {
        for (int i = 0; i < events.size(); i++){
            if (events.get(i).getEventID() == eventID){
                events.get(i).setStartDate(startDate);
                events.get(i).setEndDate(endDate);
                events.get(i).setStartTime(time);
                events.get(i).setName(name);
                events.get(i).setAdress(adress);
                events.get(i).setCity(city);
                events.get(i).setCoaches(coach);
                events.get(i).setJudges(judge);
            }
        }
    }

    public Person findPersonById(int id) {
        for (int i = 0; i < people.size(); i++) {
            if (people.get(i).getId() == id) {
                return people.get(i);
            }
        }
        return null;
    }
}

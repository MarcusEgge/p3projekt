package com.data;

import java.util.ArrayList;
import java.util.List;

public class Person {
    private String firstName;
    private String lastName;
    private String birthsday;
    private int age;
    private int phoneNumber;
    private String license;
    private String email;
    private String address;
    private String commune;
    private int postalCode;
    private String gender;
    private boolean isGymnast;
    private boolean isCoach;
    private boolean isJudge;
    private List<Integer> events;
    private int id;

    public Person(String firstName, String lastName, String birthsday, int age, int phoneNumber, String license, String email, String address, String commune, int postalCode, String gender, boolean isGymnast, boolean isCoach, boolean isJudge, int id) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthsday = birthsday;
        this.age = age;
        this.phoneNumber = phoneNumber;
        this.license = license;
        this.email = email;
        this.address = address;
        this.commune = commune;
        this.postalCode = postalCode;
        this.gender = gender;
        this.isGymnast = isGymnast;
        this.isCoach = isCoach;
        this.isJudge = isJudge;
        this.id = id;
    }

    //Getters
    public String getFirstName() {
        return firstName;
    }
    public String getLastName() {
        return lastName;
    }
    public String getBirthsday() {
        return birthsday;
    }
    public int getAge() {
        return age;
    }
    public int getPhoneNumber() {
        return phoneNumber;
    }
    public String getLicense() {
        return license;
    }
    public String getEmail() {
        return email;
    }
    public String getAddress() {
        return address;
    }
    public String getCommune() {
        return commune;
    }
    public int getPostalCode() {
        return postalCode;
    }
    public String getGender() {
        return gender;
    }
    public boolean isGymnast() {
        return isGymnast;
    }
    public boolean isCoach() {
        return isCoach;
    }
    public boolean isJudge() {
        return isJudge;
    }
    public List<Integer> getEvents() {
        return events;
    }
    public int getId() {
        return id;
    }

    //Setters
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    public void setBirthsday(String birthsday) {
        this.birthsday = birthsday;
    }
    public void setAge(int age) {
        this.age = age;
    }
    public void setPhoneNumber(int phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
    public void setLicense(String license) {
        this.license = license;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    public void setAddress(String address) {
        this.address = address;
    }
    public void setCommune(String commune) {
        this.commune = commune;
    }
    public void setPostalCode(int postalCode) {
        this.postalCode = postalCode;
    }
    public void setGender(String gender) {
        this.gender = gender;
    }
    public void setGymnast(boolean gymnast) {
        isGymnast = gymnast;
    }
    public void setCoach(boolean coach) {
        isCoach = coach;
    }
    public void setJudge(boolean judge) {
        isJudge = judge;
    }
    public void setId(int id) {
        this.id = id;
    }

    //Other methods
    public void initializeList() {
        this.events = new ArrayList<>();
    }
}



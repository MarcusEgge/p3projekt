package servlets;

import com.google.gson.Gson;
import data.Database;
import data.Event;
import data.Person;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "eventServlet", urlPatterns = "/eventServlet")
public class EventServlet extends HttpServlet {
    Database database = Database.getInstance();
    Gson gson = new Gson();

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");

        String json = gson.toJson(database.getEvents());

        System.out.println(json);

        response.getWriter().write(json);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");

        String json = request.getParameter("text").trim();

        Event event = gson.fromJson(json, Event.class);
        createID(event);

        database.getEvents().add(event);

        json = gson.toJson(event);
        System.out.println(json);
        response.getWriter().write(json);
    }

    protected void doPut(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
    }

    protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
    }

    public void createID(Event event){
        boolean idTaken;
        for (int i = 0; i < 1000; i++){
            idTaken = false;
            for (int k = 0; k < database.getEvents().size(); k++){
                if (database.getEvents().get(k).getEventID() == i){
                    idTaken = true;
                    break;
                }
            }
            if (!idTaken){
                event.setEventID(i);
                break;
            }
        }
    }
}

package servlets;

import com.google.gson.Gson;
import data.Database;
import data.Person;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URLDecoder;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Scanner;

@WebServlet(name = "personServlet", urlPatterns = "/personServlet/*")
public class PersonServlet extends HttpServlet {
    Database database = Database.getInstance();
    Gson gson = new Gson();

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");

        String json = gson.toJson(database.getPeople());

        response.getWriter().write(json);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");

        String json = request.getParameter("text").trim();

        Person person = gson.fromJson(json, Person.class);

        createID(person);

        database.getPeople().add(person);
        database.getEvents().get(0).addPerson(person);

        database.getPeople().sort(Comparator.comparingInt(Person::getId));
        json = gson.toJson(person);
        response.getWriter().write(json);
    }

    protected void doPut(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");

        BufferedReader br = new BufferedReader(new InputStreamReader(request.getInputStream()));

        String data = ConvertData(br.readLine()).get("text");
        int id = retrieveUserid(request);
        Person person = gson.fromJson(data, Person.class);

        database.updatePerson(id, person.getName(), person.getAge());

        person.setId(id);
        response.getWriter().write(gson.toJson(person));
    }

    protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        int id = retrieveUserid(request);

        database.deletePerson(id);

        response.getWriter().write("success");
    }

    private static int retrieveUserid(HttpServletRequest req) {
        String pathInfo = req.getPathInfo();
        if (pathInfo.startsWith("/")) {
            pathInfo = pathInfo.substring(1);
        }
        return Integer.parseInt(pathInfo);
    }

    private static String inputStreamToString(InputStream inputStream) {
        Scanner scanner = new Scanner(inputStream, "UTF-8");
        return scanner.hasNext() ? scanner.useDelimiter("A").next() : "";
    }
    HashMap<String, String> ConvertData(String Data) {
        HashMap<String, String> map = new HashMap<String, String>();
        String[] Datas = Data.split("=");
        String key = null;
        for (int i = 0; i < Datas.length; i++) {
            if (i % 2 == 0) key = URLDecoder.decode(Datas[i]);
            else map.put(key, URLDecoder.decode(Datas[i]));
        }
        return map;
    }

    public void createID(Person person){
        boolean idTaken;
        for (int i = 0; i < 1000; i++){
            idTaken = false;
            for (int k = 0; k < database.getPeople().size(); k++){
                if (database.getPeople().get(k).getId() == i){
                    idTaken = true;
                    break;
                }
            }
            if (!idTaken){
                person.setId(i);
                break;
            }
        }
    }
}

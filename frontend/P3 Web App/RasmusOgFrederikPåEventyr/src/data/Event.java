package data;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class Event {
    private String startDate;
    private String endDate;
    private String startTime;
    private String name;
    private String adress;
    private String city;
    private int coaches;
    private int judges;

    private int eventID;

    private List<Person> peopleAttending;

    public Event(String startDate, String endDate, String startTime, String name, String adress, String city, int coaches, int judges) {
        this.startDate = startDate;
        this.endDate = endDate;
        this.startTime = startTime;
        this.name = name;
        this.adress = adress;
        this.city = city;
        this.coaches = coaches;
        this.judges = judges;

        peopleAttending = new ArrayList<>();
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAdress() {
        return adress;
    }

    public void setAdress(String adress) {
        this.adress = adress;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public int getCoaches() {
        return coaches;
    }

    public void setCoaches(int coaches) {
        this.coaches = coaches;
    }

    public int getJudges() {
        return judges;
    }

    public void setJudges(int judges) {
        this.judges = judges;
    }

    public List<Person> getPeopleAttending() {
        return peopleAttending;
    }

    public void addPerson(Person person) {
        this.peopleAttending.add(person);
    }

    public int getEventID() {
        return eventID;
    }

    public void setEventID(int eventID) {
        this.eventID = eventID;
    }
}

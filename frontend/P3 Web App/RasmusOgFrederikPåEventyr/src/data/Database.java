package data;

import java.util.ArrayList;
import java.util.List;

public class Database {
    List<Person> people;
    List<Event> events;

    private static Database single_instance = null;

    private Database()
    {
        people = new ArrayList<>();
        events = new ArrayList<>();
        Event test = new Event("0001-01-01", "0001-01-02", "10:00", "Pølse fest", "Pøllevej 1", "Randers", 5, 2);
        Event test1 = new Event("0001-01-01", "0001-01-02", "10:00", "Pølse fest", "Pøllevej 1", "Randers", 5, 2);
        test.setEventID(0);
        test1.setEventID(1);
        Person rasmus = new Person("Rasmus", 23, 23423478, "ASD1234", "test@test.com", 0, true, false, false);
        Person frederik = new Person("Frederik", 24, 12345678, "GFD4321", "bob@bobsen.com",1, true, false, false);
        Person peter = new Person("Peter", 24, 12345678, "GFD4321", "bob@bobsen.com",2, false, true, false);
        Person marcus = new Person("Marcus", 24, 12345678, "GFD4321", "bob@bobsen.com",3, false, false, true);
        Person frederik1 = new Person("Peter", 24, 12345678, "GFD4321", "bob@bobsen.com",1, true, false, false);

        people.add(rasmus);
        people.add(frederik);
        people.add(peter);
        people.add(marcus);

        events.add(test);
        events.add(test1);
        events.add(test1);
        events.add(test1);
        events.add(test1);
        events.add(test1);
        events.add(test1);
        events.add(test1);
        events.add(test1);
        events.add(test1);
        events.add(test1);
        events.add(test1);
        events.add(test1);
        events.add(test1);
        events.add(test1);


        events.get(1).addPerson(rasmus);
        events.get(0).addPerson(frederik);
        events.get(0).addPerson(frederik);
        events.get(0).addPerson(frederik);
        events.get(0).addPerson(frederik);
        events.get(0).addPerson(frederik);
        events.get(0).addPerson(frederik1);
        events.get(0).addPerson(frederik1);
        events.get(0).addPerson(frederik1);
        events.get(0).addPerson(frederik1);
        events.get(0).addPerson(frederik);
        events.get(0).addPerson(frederik);
        events.get(0).addPerson(frederik);
        events.get(0).addPerson(frederik);
        events.get(0).addPerson(frederik);
        events.get(0).addPerson(frederik);
        events.get(0).addPerson(frederik);
        events.get(0).addPerson(frederik);
        events.get(1).addPerson(rasmus);
        events.get(0).addPerson(frederik);
        events.get(0).addPerson(frederik);
        events.get(0).addPerson(frederik);
        events.get(0).addPerson(frederik);
        events.get(0).addPerson(frederik);
        events.get(0).addPerson(frederik1);
        events.get(0).addPerson(frederik1);
        events.get(0).addPerson(frederik1);
        events.get(0).addPerson(frederik1);
        events.get(0).addPerson(frederik);
        events.get(0).addPerson(frederik);
        events.get(0).addPerson(frederik);
        events.get(0).addPerson(frederik);
        events.get(0).addPerson(frederik);
        events.get(0).addPerson(frederik);
        events.get(0).addPerson(frederik);
        events.get(0).addPerson(frederik);
        events.get(1).addPerson(rasmus);
        events.get(0).addPerson(frederik);
        events.get(0).addPerson(frederik);
        events.get(0).addPerson(frederik);
        events.get(0).addPerson(frederik);
        events.get(0).addPerson(frederik);
        events.get(0).addPerson(frederik1);
        events.get(0).addPerson(frederik1);
        events.get(0).addPerson(frederik1);
        events.get(0).addPerson(frederik1);
        events.get(0).addPerson(frederik);
        events.get(0).addPerson(frederik);
        events.get(0).addPerson(frederik);
        events.get(0).addPerson(frederik);
        events.get(0).addPerson(frederik);
        events.get(0).addPerson(frederik);
        events.get(0).addPerson(frederik);
        events.get(0).addPerson(frederik);
        events.get(1).addPerson(rasmus);
        events.get(0).addPerson(frederik);
        events.get(0).addPerson(frederik);
        events.get(0).addPerson(frederik);
        events.get(0).addPerson(frederik);
        events.get(0).addPerson(frederik);
        events.get(0).addPerson(frederik1);
        events.get(0).addPerson(frederik1);
        events.get(0).addPerson(frederik1);
        events.get(0).addPerson(frederik1);
        events.get(0).addPerson(frederik);
        events.get(0).addPerson(frederik);
        events.get(0).addPerson(frederik);
        events.get(0).addPerson(frederik);
        events.get(0).addPerson(frederik);
        events.get(0).addPerson(frederik);
        events.get(0).addPerson(frederik);
        events.get(0).addPerson(frederik);

        events.get(0).addPerson(peter);
        events.get(0).addPerson(peter);
        events.get(0).addPerson(peter);
        events.get(0).addPerson(peter);
        events.get(0).addPerson(peter);
        events.get(0).addPerson(peter);
        events.get(0).addPerson(peter);
        events.get(0).addPerson(peter);
        events.get(0).addPerson(peter);
        events.get(0).addPerson(peter);
        events.get(0).addPerson(peter);
        events.get(0).addPerson(peter);
        events.get(0).addPerson(peter);

        events.get(0).addPerson(marcus);
    }

    public static Database getInstance()
    {
        if (single_instance == null)
            single_instance = new Database();

        return single_instance;
    }

    public List<Person> getPeople() {
        return people;
    }

    public List<Event> getEvents() {
        return events;
    }

    public void deletePerson(int id){
        for (int i = 0; i < people.size(); i++){
            if (people.get(i).getId() == id)
                people.remove(i);
        }
    }

    public void updatePerson(int id, String name, int age){
        for (int i = 0; i < people.size(); i++){
            if (people.get(i).getId() == id){
                people.get(i).setName(name);
                people.get(i).setAge(age);
            }
        }
    }

}

package data;

import java.util.List;

public class Person {
    private String name;
    private int age;
    private int phoneNumber;
    private String license;
    private String email;
    private int id;
    private boolean isGymnast;
    private boolean isCoach;
    private boolean isJudge;

    private List<String> events;


    public Person(String name, int age, int phoneNumber, String license, String email, int id, boolean isGymnast, boolean isCoach, boolean isJudge) {
        this.name = name;
        this.age = age;
        this.phoneNumber = phoneNumber;
        this.license = license;
        this.email = email;
        this.id = id;
        this.isGymnast = isGymnast;
        this.isCoach = isCoach;
        this.isJudge = isJudge;
    }

    //Getters
    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public int getPhoneNumber() {
        return phoneNumber;
    }

    public String getLicense() {
        return license;
    }

    public String getEmail() {
        return email;
    }

    public int getId() {
        return id;
    }

    public boolean isGymnast() {
        return isGymnast;
    }

    public boolean isCoach() {
        return isCoach;
    }

    public boolean isJudge() {
        return isJudge;
    }

    //Setters
    public void setName(String name) {
        this.name = name;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setPhoneNumber(int phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public void setLicense(String license) {
        this.license = license;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setGymnast(boolean gymnast) {
        isGymnast = gymnast;
    }

    public void setCoach(boolean coach) {
        isCoach = coach;
    }

    public void setJudge(boolean judge) {
        isJudge = judge;
    }
}

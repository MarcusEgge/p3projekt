$(document).ready(function () {
    $("#btnGetReq").click(function () {
        refreshList();
    });
    $("#btnPostReq").click(function () {
        var name = $('#inputPostReq').val();
        var age = $('#inputAgePostReq').val();
        var obj = {'name': name, 'age': age};
        $.ajax({
            type: "POST",
            url: "personServlet",
            data: {
                text: JSON.stringify(obj)
            },
            success: function (data) {
                //addPerson(data)
                refreshList();
            }
        })
    });
    $("#btnPutReq").click(function () {
        var name = $("#inputNamePutReq").val();
        var age = $("#inputAgePutReq").val();
        var obj = {'name': name, 'age': age};
        $.ajax({
            type: "PUT",
            url: "personServlet/" + $("#inputIndexPutReq").val(),
            contentType: "application/json",
            data: {
                text: JSON.stringify(obj)
            },
            success: function (data) {
                refreshList();
            }
        })
    });

    $("#divGetResp").delegate('.update', 'click', function () {
        showUpdate($(this))
        /*
                var name = $("#inputNamePutReq").val();
                var age = $("#inputAgePutReq").val();
                var obj = {'name': name, 'age': age};

                $.ajax({
                    type: "PUT",
                    url: "personServlet/" + $(this).attr('data-id'),
                    contentType: "application/json",
                    data: {
                        text: JSON.stringify(obj)
                    },
                    success: function () {
                        $div.remove();
                    }
                })*/
    });

    $("#divGetResp").delegate('.cancel', 'click', function () {
        hideUpdate($(this));
    });

    $("#divGetResp").delegate('.remove', 'click', function () {
        var $div = $(this).closest('div');

        $.ajax({
            type: "DELETE",
            url: "eventServlet/" + $(this).attr('data-id'),
            contentType: "application/json",
            success: function () {
                $div.remove();
            }
        })
    })
});

function refreshList() {
    $.ajax({
        type: "GET",
        url: "eventServlet",
        success: function (data) {
            $("#divGetResp").empty();

            for (j = 0; j < data.length; j++){
                for (i = 0; i < data[j].peopleAttending.length; i++) {
                    addPerson(data[j].peopleAttending[i])
                }
            }
        }
    })
}

function showUpdate(element) {
    var removeButton = element.siblings('.remove');
    var updateButton = element;
    var textField = element.siblings('p');
    textField.hide();
    removeButton.hide();
    updateButton.hide();
    element.closest('div').append(updateTemplate)
}

function hideUpdate(element) {
    var removeButton = element.siblings('.remove');
    var saveButton = element.siblings('.save');
    var cancelButton = element;
    var updateButton = element.siblings('.update');
    var textField = element.siblings('p');

    saveButton.hide();
    cancelButton.hide();
    textField.show();
    removeButton.show();
    updateButton.show();
}

var updateTemplate = "" +
    "<button class='save'>Gem</button>" +
    "<button class='cancel'>Fortryd</button>";

function addPerson(person) {
    $("#divGetResp").append(Mustache.render(personTemplate, person))
}

var personTemplate = "" +
    "<div> " +
    "<p>name: {{name}}, age: {{age}}, number: {{phoneNumber}}, email: {{email}}, license: {{license}}, gymnast: {{isGymnast}}, coach: {{isCoach}}, judge: {{isJudge}}</p>" +
    "<button data-id='{{id}}' class='remove'>Delete</button>" +
    "<button class='update'>Update</button>" +
    "</div>";
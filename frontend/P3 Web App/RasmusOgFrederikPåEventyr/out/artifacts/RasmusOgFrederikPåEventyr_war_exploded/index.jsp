<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <title>JSON and Ajax</title>
    <link rel="stylesheet" href="myCSS.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
</head>
<body>
<!-- GET HTTP Request-->
<div>
    <button id="btnGetReq">HTTP GET request (list size)</button>
    <div id="divGetResp"></div>
</div>

<!-- POST HTTP Request-->
<div>
    <input type="text" id="inputPostReq" placeholder="POST Navn"/>
    <input type="number" id="inputAgePostReq" placeholder="POST Alder"/>
    <button id="btnPostReq">HTTP POST request (add string to list)</button>
</div>

<!-- PUT HTTP Request-->
<div>
    <input type="text" id="inputNamePutReq" placeholder="PUT name"/>
    <input type="number" id="inputAgePutReq" placeholder="PUT age"/>
    <input type="number" id="inputIndexPutReq" placeholder="index"/>
    <button id="btnPutReq">HTTP PUT request (change database at given index</button>
</div>
<script src="myJS.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/mustache.js/3.0.1/mustache.js"></script>
</body>
</html>

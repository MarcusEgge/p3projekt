$(document).ready(function () {
    loadNavBar();
    loadEvents();
    loadMainWindow();


    //TODO Make it run after tabel is loaded instead of 500 ms
    setTimeout(function () {
        $('#dtVerticalScrollExample').DataTable({
            "scrollY": "400px",
            "scrollCollapse": true,
            "paging": false,
            "info": false,
            "filter": false,
        })

        $('.dataTables_length').addClass("bs-select");
        $("tbody tr:visible:odd").css("background-color", "rgb(242, 242, 242)");
        adjustscrollheight();
    }, 500)

    //Sæt højde af events del ved load
    $(".events").ready(function () {
        $(".events").css("height", (window.innerHeight - ($(".navbarColor").height() + 1)));
    })

    //Sæt højde af events del ved resize
    $(window).resize(function () {
        $(".events").css("max-height", (window.innerHeight - ($(".navbarColor").height() + 1)));
        adjustscrollheight();
    })

    $(document).on('click', '.event-box', function () {
        $(this)
            .siblings()
            .removeClass("event-box-selected");
        $(this).addClass("event-box-selected");
    });

    $(document).on('click', ".sub-navbar-link", function () {
        $(this)
            .siblings()
            .removeClass("sub-navbar-link-selected");
        $(this).addClass("sub-navbar-link-selected");

        loadTable();
    });


    $(document).on('mouseenter', 'tbody tr', function () {
        $(this).css("background-color", "darkgrey")
    });

    $(document).on('mouseleave', 'tbody tr', function () {
        $("tbody tr:visible:odd").css("background-color", "rgb(242, 242, 242)");
        $("tbody tr:visible:even").css("background-color", "#fff");
    });

    $(document).on('keyup', '#myInput', function () {
        var value = $(this).val().toLowerCase();
        $(".myTable tr").filter(function () {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
            $("tbody tr:visible:odd").css("background-color", "rgb(242, 242, 242)");
            $("tbody tr:visible:even").css("background-color", "#fff");
        });
    });

    $(document).on('focus', '#myInput', function () {
        $(this).parents(".sub-navbar-offset").css("box-shadow", "0 0 0 0.2rem rgba(0,123,255,0.25)")
    })

    $(document).on('blur', '#myInput', function () {
        $(this).parents(".sub-navbar-offset").css("box-shadow", "none")
    })


    $(document).on('click', "#btnCreateEvent", function () {
        loadCreateEvent()
    });

    $(document).on('click', "#btnCancelCreateEvent", function () {
        loadMainWindow();
    });

    $(document).on('click', '.fa-copy', function () {
        $(this)
            .parents("tr")
            .addClass("gymnast-copied");
    });

    $(document).on('click', '.dropdown-item', function () {
        $(this).parent().siblings("button").html($(this).html());
    })


// When the user clicks on the button, open the modal
    $("#modalBtn").click(function () {
        $("#myModal").css("display", "block");
    });

//TODO lav slet knap og annuler til rigtigt luk modal
// When the user clicks on <span> (x), close the modal
    $(".closeModal").click(function () {
        $("#myModal").css("display", "none");
    });

    $(document).on("click", "#btnSaveChange", function () {
        if (($("#startDate").val() == "") ||
            ($("#endDate").val() == "") ||
            ($("#starttime").val() == "") ||
            ($("#name").val() == "") ||
            ($("#adress").val() == "") ||
            ($("#town").val() == "") ||
            ($("#coaches").val() == "") ||
            ($("#judges").val() == "")) {
            alert("Fejl: Udfyld alle felter.")
        } else {
            var startDate = $("#startDate").val();
            var endDate = $("#endDate").val();
            var startTime = $("#starttime").val();
            var name = $("#name").val();
            var adress = $("#adress").val();
            var city = $("#town").val();
            var coaches = $("#coaches").val();
            var judges = $("#judges").val();

            var obj = {
                'startDate': startDate,
                'endDate': endDate,
                'startTime': startTime,
                'name': name,
                'adress': adress,
                'city': city,
                'coaches': coaches,
                'judges': judges
            };

            $.ajax({
                type: "POST",
                url: "/../eventServlet",
                data: {
                    text: JSON.stringify(obj)
                },
                success: function (data) {
                    addEvent(data);
                    eventSavedPopUp()
                }
            });
        }
    });
});

function loadNavBar() {
    $(".navbarColor").load("Template-HTML-pages/nav-bar.html");
}

function selectTopEvent() {
    $(".event-box:first").addClass("event-box-selected");
}

function eventSavedPopUp() {
    $("#save-message1").css("display", "block");
    setTimeout(function () {
        $("#save-message1").css("opacity", 0);
    }, 2000);
    setTimeout(function () {
        $("#save-message1").css("display", "none");
        $("#save-message1").css("opacity", 1);
    }, 4000);
}

function loadEvents() {
    $.ajax({
        type: "GET",
        url: "/../eventServlet",
        success: function (data) {
            $("#eventSpawn").empty();

            for (i = 0; i < data.length; i++) {
                addEvent(data[i]);
            }
            selectTopEvent();
        }
    })
}

function loadCreateEvent() {
    $("#mainArea").load("Template-HTML-pages/Create-Event-Template.html");
}

function loadMainWindow() {
    $("#mainArea").load("Template-HTML-pages/Main-Table-Template.html");
    loadGymnastTable();
}


function loadTable() {
    if ($("#GymnastsSubNav").hasClass("sub-navbar-link-selected")) {
        loadGymnastTable();
    } else if ($("#CoachesSubNav").hasClass("sub-navbar-link-selected")) {
        loadCoachTable()
    } else if ($("#JudgesSubNav").hasClass("sub-navbar-link-selected")) {
        loadJudgeTable()
    } else if ($("#TeamsSubNav").hasClass("sub-navbar-link-selected")) {

    } else if ($("#ScheduleSubNav").hasClass("sub-navbar-link-selected")) {

    }
}

function loadGymnastTable() {
    $.ajax({
        type: "GET",
        url: "/../eventServlet",
        success: function (data) {
            var eventIndex = document.getElementsByClassName("event-box-selected")[0].getAttribute("data-id");
            $('tbody').empty();
            for (i = 0; i < data[eventIndex].peopleAttending.length; i++) {
                if (data[eventIndex].peopleAttending[i].isGymnast == true)
                    addGymnast(data[eventIndex].peopleAttending[i])
            }
        }
    })
    console.log("gymnast");
}

function loadCoachTable() {
    $.ajax({
        type: "GET",
        url: "/../eventServlet",
        success: function (data) {
            var eventIndex = document.getElementsByClassName("event-box-selected")[0].getAttribute("data-id");
            $('tbody').empty();
            for (i = 0; i < data[eventIndex].peopleAttending.length; i++) {
                if (data[eventIndex].peopleAttending[i].isCoach == true)
                    addVolunteer(data[eventIndex].peopleAttending[i])
            }
        }
    })
}

function loadJudgeTable() {
    $.ajax({
        type: "GET",
        url: "/../eventServlet",
        success: function (data) {
            var eventIndex = document.getElementsByClassName("event-box-selected")[0].getAttribute("data-id");
            $('tbody').empty();
            for (i = 0; i < data[eventIndex].peopleAttending.length; i++) {
                if (data[eventIndex].peopleAttending[i].isJudge == true)
                    addVolunteer(data[eventIndex].peopleAttending[i])
            }
        }
    })
}

function addEvent(event) {
    $("#eventSpawn").append(Mustache.render(eventTemplate, event))
}

function addGymnast(person) {
    $('tbody').append(Mustache.render(gymnastTemplate, person));
}

function addVolunteer(person) {
    $('tbody').append(Mustache.render(volunteerTemplate, person));
}

var eventTemplate = "" +
    "<div data-id='{{eventID}}' class='event-box'>" +
    "<div class='row'>" +
    "<div class='col-md-5 event-box-color'>" +
    "<div class='event-date'>{{startDate}}</div>" +
    "<div class='event-name'>{{name}}</div>" +
    "<div class='event-location'>{{city}}</div>" +
    "</div>" +
    "<div class='col-md-6'>" +
    "<div class='event-information'>{{adress}}</div>" +
    "<div class='event-information'>{{startTime}}</div>" +
    "<div class='event-information'>Springere (TO DO)</div>" +
    "<div class='event-information'>Trænere {{coaches}} - dommere {{judges}}</div>" +
    "</div>" +
    "<div class='col-md-1 override-padAndMargin'><i class='far fa-edit'></i></div>" +
    "</div>" +
    "</div>";

var gymnastTemplate = "" +
    "<tr>" +
    "<td><i class=\"far fa-lg fa-check-square\"></i></td>" +
    "<td>{{name}}</td>" +
    "<td>{{license}} <i class=\"far fa-copy\"></i></td>" +
    "<td>{{phoneNumber}}</td>" +
    "<td>{{email}}</td>" +
    "</tr>";

var volunteerTemplate = "" +
    "<tr>" +
    "<td>" +
    "<div class=\"btn-group\" role=\"group\" aria-label=\"Button group with nested dropdown\">" +
    "<button id=\"btnGroupDrop1\" type=\"button\" class=\"btn btn-secondary dropdown-toggle attendenceBtn\"" +
    "data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\"> Deltager" +
    "</button>" +
    "<div class=\"dropdown-menu\" aria-labelledby=\"btnGroupDrop1\" x-placement=\"bottom-start\">" +
    "<a class=\"dropdown-item\" href=\"#\">Deltager</a>" +
    "<a class=\"dropdown-item\" href=\"#\">Deltager ikke</a>" +
    "<a class=\"dropdown-item\" href=\"#\">Ikke svaret</a>" +
    "</div>" +
    "</div>" +
    "</td>" +
    "<td>{{name}}</td>" +
    "<td>{{license}} <i class=\"far fa-copy\"></i></td>" +
    "<td>{{phoneNumber}}</td>" +
    "<td>{{email}}</td>" +
    "</tr>";

function adjustscrollheight() {
    $(".dataTables_scrollBody").css("max-height", (window.innerHeight - ($(".navbarColor").height() + 1 //These numbers are margins, padding and some stuff arbitrary divs from bootstrap
        + $(".sub-navbar-offset").height() + 5 + 1 + 1
        + $(".dataTables_scrollHead").height()
        + 2)));
}
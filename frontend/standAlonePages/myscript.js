$(document).ready(function () {

  //Load navbar, skal være sat op med server
  $(".navbarColor").load("nav-bar.html");

  //Sæt højde af events del ved load
  $(".events").css("height", (window.innerHeight - ($(".navbarColor").height() + 1)));

  //Sæt højde af events del ved resize
  $(window).resize(function () {
    $(".events").css("height", (window.innerHeight - ($(".navbarColor").height() + 1)));
  })

  loadEvents();

  //Send nyt event til server og load det
  $(".savebtn").click(function () {
    $.ajax({
      type: "POST",
      url: "/HelloWorldServlet",
      data: {
        startDate: $('#startDate').val(),
        endDate: $("#endDate").val(),
        starttime: $("#starttime").val(),
        name: $("#name").val(),
        adress: $("#adress").val(),
        town: $("#town").val(),
        coaches: $("#coaches").val(),
        jugedes: $("#jugedes").val()
      },
      success: function (data) {
        loadEvents();
      }

    })
  });


  //Marker det valgte event
  $(document).on('click', '.event-box', function () {
    $(this)
      .siblings()
      .removeClass("event-box-selected");
    $(this).addClass("event-box-selected");
  });

  //Marker sub-navbar
  $(".sub-navbar-link").click(function () {
    $(this)
      .siblings()
      .removeClass("sub-navbar-link-selected");
    $(this).addClass("sub-navbar-link-selected");
  });

  //Efter kopiering lav grå
  $(".fa-copy").click(function () {
    $(this)
      .parents("tr")
      .addClass("gymnast-copied");
  });

  // Ændre dropdown i dommer / træner tilmelding
  $(".dropdown-item").click(function () {
    $(this).parent().siblings("button").html($(this).html());
  });

  // When the user clicks on the button, open the modal
  $("#modalBtn").click(function () {
    $("#myModal").css("display", "block");
  });

  //TODO lav slet knap og annuler til rigtigt luk modal
  // When the user clicks on <span> (x), close the modal
  $(".closeModal").click(function () {
    $("#myModal").css("display", "none");
  });

  // saveChangeBtn er til gem knappen som laver den lille grønne bar, saveWarning er selve elemented som er baren
  $("#saveChangeBtn").click(function () {
    $("#save-message1").css("display", "block");
    setTimeout(function () {
      $("#save-message1").css("opacity", 0);
    }, 2000);
    setTimeout(function () {
      $("#save-message1").css("display", "none");
      $("#save-message1").css("opacity", 1);
    }, 4000);
  });



  // marcus laver tabels 
  $('#dtVerticalScrollExample').DataTable({
    "scrollY": "200px",
    "scrollCollapse": true,
  });
  $('.dataTables_length').addClass('bs-select');

});

// load alle event fra serveren
function loadEvents() {
  $.ajax({
    type: "GET",
    url: "/HelloWorldServlet",
    success: function (data) {
      $(".events").html("");
      for (var i = 0; i < data.length; i++) {
        var j = 0;
        //      console.log(data);
        $(".events").append("<div class='event-box'></div>")
          .children("div").last().load("event-box.html", function () {
            //           console.log(data);
            $(this).find(".address").last().text(data[j].adresse);
            $(this).find(".time").last().text(data[j].startdato.time.hour + " " + data[j].startdato.time.minute);
            $(this).find(".gymnasts").last().text("20");
            $(this).find(".volunteers").last().text("Trænere: " + data[j].traenere + " - " + "Dommere: " + data[j].dommere);
            $(this).find(".event-date").last().text(data[j].startdato.date.year + " " + data[j].startdato.date.month + " " + data[j].startdato.date.day);
            $(this).find(".event-name").last().text(data[j].navn);
            $(this).find(".event-location").last().text(data[j].by);
            j++;
          });
      }
    }
  });
};


// marcus prøver chart js 

//giver et Array med random fra min til max og længden på array er count
function buildRandomDataArray(min, max, count) {
  var numberArray = [];
  for (var i = 0; i < count; i++) {
    numberArray.push(Math.floor((Math.random() * (max - min)) + min))
  }
  return numberArray;
};

//retunere gennemsnittet af et array i et array med samme længde
function avgCalculater(dataset) {
  var totalAmount = 0;
  var avgNumber = [];

  for (var i = 0; i < dataset.length; i++) {
    totalAmount += dataset[i];
  }
  totalAmount /= dataset.length;

  for (var i = 0; i < dataset.length; i++) {
    avgNumber.push(totalAmount);
  }

  return avgNumber;
}





//laver grafen, step 1 få fat i et < canvas > laver lidt dummy data og indsætter det med charts.js
var ctx = $('#myChart');
var distanceArray = buildRandomDataArray(25, 1500, 18);
var gennemsnitArray = avgCalculater(distanceArray);
var myChart = new Chart(ctx, {
  type: 'bar',
  data: {
    labels: ["Marcus", "Peter", "Tuan", "Martin", "Thea", "Frederik", "Marcus", "Peter", "Tuan", "Martin", "Thea", "Frederik", "Marcus", "Peter", "Tuan", "Martin", "Thea", "Frederik"],
    datasets: [{
        type: 'line',
        label: 'Gennemsnit ' + Math.floor(gennemsnitArray[0]),
        data: gennemsnitArray,
        fill: false,
        borderColor: 'rgba(0,0,0,1)',
        pointRadius: 0
      },
      {
        type: 'bar',
        label: 'Kilometer kørt',
        data: distanceArray,
        backgroundColor: 'rgba(0, 164, 224, 1)',
        borderColor: 'rgba(0,0,0,1)',
        borderWidth: 1
      }
    ]
  },
  options: {
    scales: {
      yAxes: [{
        ticks: {
          beginAtZero: true
        }
      }]
    }
  }
});

//marcus færdig med chart js
import java.util.ArrayList;
import java.util.List;

public class Database {
    List<String> database;

    public Database() {
        database = new ArrayList<>();

        database.add("first");
        database.add("second");
        database.add("third");
    }

    public List<String> getDatabase() {
        return database;
    }

    public void addData(String data) {
        database.add(data);
    }
}

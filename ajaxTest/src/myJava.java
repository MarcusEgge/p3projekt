import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/servlet")
public class myJava extends HttpServlet {

    Database base = new Database();

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
       int temp = base.getDatabase().size();
       response.getWriter().write("" + temp);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String temp = request.getParameter("temp").trim();
        base.addData(temp);
        response.getWriter().write(base.getDatabase().get(base.getDatabase().size() - 1));
    }

    protected void doPut(HttpServletResponse request, HttpServletResponse response) throws ServletException, IOException {

    }
}
